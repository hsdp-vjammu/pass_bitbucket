CREATE TABLE `tbl_binarydata` (
  `DownloadGUID` varchar(255) DEFAULT NULL,
  `BinaryLength` double DEFAULT NULL,
  `BinaryData` blob,
  `LogonUserName` varchar(255) DEFAULT NULL,
  `DataSource` varchar(255) DEFAULT NULL,
  `GMTOffSet` varchar(255) DEFAULT NULL,
  `TimeZone` datetime DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `TimeStamp` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_clinicaltrialinfo` (
  `ClinicalTrialID` int(11) NOT NULL,
  `ClinicalTrialDesc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ClinicalTrialID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_download` (
  `DownloadGUID` varchar(255) NOT NULL,
  `LogonUserName` varchar(255) DEFAULT NULL,
  `GMTStartDate` datetime DEFAULT NULL,
  `Epoch` int(255) DEFAULT NULL,
  `DeviceType` varchar(255) DEFAULT NULL,
  `DeviceSerialNum` int(11) DEFAULT NULL,
  `FirmwareVersion` varchar(255) DEFAULT NULL,
  `GMToffset` varchar(255) DEFAULT NULL,
  `TimeZone` datetime DEFAULT NULL,
  `UseDaylightTime` datetime DEFAULT NULL,
  `DeviceSource` varchar(255) DEFAULT NULL,
  `SleepDiary` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DownloadGUID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_roles` (
  `ID` int(11) NOT NULL,
  `Role` varchar(255) DEFAULT NULL,
  `RoleDescription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `DOB` datetime DEFAULT NULL,
  `Gender` varchar(255) DEFAULT NULL,
  `EmailId` varchar(255) DEFAULT NULL,
  `LogonUserName` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Active` varchar(255) DEFAULT NULL,
  `RoleID` int(11) NOT NULL,
  `BusinessName` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL,
  `State` varchar(255) DEFAULT NULL,
  `Zip` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Phone1` varchar(255) DEFAULT NULL,
  `Phone2` varchar(255) DEFAULT NULL,
  `DomainName` varchar(255) DEFAULT NULL,
  `ComputerName` varchar(255) DEFAULT NULL,
  `Comments` varchar(255) DEFAULT NULL,
  `GMTOffSet` int(255) DEFAULT NULL,
  `ClinialTrialID` int(11) DEFAULT NULL,
  `TimeStamp` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  CONSTRAINT `RoleID` FOREIGN KEY (`RoleID`) REFERENCES `tbl_roles` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

