(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('SiteselectioDialogController', SiteselectioDialogController);

    SiteselectioDialogController.$inject = ['$scope','$uibModalInstance','$rootScope','_', '$window'];

    function SiteselectioDialogController ($scope,$uibModalInstance, $rootScope, _, $window) {
        var vm = this;
        vm.selectedSites=[];
        vm.displayNewlySelectedSites = [];
        vm.sites=$rootScope.sites;
        vm.getAvailableSites = getAvailableSites;
        vm.addedSites = $rootScope.preSelectedSites;
        vm.selectedSiteRefs = $rootScope.selectedSiteRefs;
        vm.selectedStudyInformation = $rootScope.studySelected;
        getAvailableSites();

        function getAvailableSites() {
            vm.availableSites = [];
            angular.forEach(vm.sites, function (obj) {
                vm.availableSites.push(obj.externalReference);
            });
            //remove previously added/selected sites from available sites list
            angular.forEach(vm.selectedSiteRefs, function (addedSite) {
                vm.availableSites = _.without(_.map(vm.availableSites), addedSite);
            });
        }

        vm.saveSelection = function () {
            //access selected values, works with IE and all others
            var selectElement = document.getElementById("js_multiselect_to_1");
            var selectedOptions = selectElement.options;
            angular.forEach(selectedOptions, function(option){
                    vm.displayNewlySelectedSites.push(vm.selectedStudyInformation + " - " + option.value);
                    vm.selectedSites.push(option.value)
            });
            $rootScope.displayNewlySelectedSites = vm.displayNewlySelectedSites;
            $rootScope.newlySelectedSites = vm.selectedSites;
            $uibModalInstance.close(true);
        };

        vm.cancel = function () {
            //$rootScope.newlySelectedSites = null;
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
