(function () {
    'use strict';

    angular
        .module('passApp')
        .controller('UserManagementController', UserManagementController);

    UserManagementController.$inject = ['$scope', '$state', 'Principal', 'User', 'ParseLinks', 'pagingParams', 'paginationConstants', 'JhiLanguageService', '$rootScope', '_'];

    function UserManagementController($scope, $state, Principal, User, ParseLinks, pagingParams, paginationConstants, JhiLanguageService, $rootScope, _) {
        var vm = this;
        vm.authorities = ['ROLE_USER', 'ROLE_ORGADMIN', 'ROLE_ADMIN'];
        vm.languages = null;
        vm.links = null;
        vm.currentCompany = Principal.currentCompany;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.advSearchPagination = false;
        vm.loadAll();

        vm.searchModelOptions = {updateOn: 'default blur', debounce: {'default': 500, 'blur': 0}};
        Principal.identity().then(function (account) {
            Principal.hasAuthority('ROLE_ADMIN')
                .then(function (result) {
                    vm.isProductSupport = result;
                });
        });


        JhiLanguageService.getAll().then(function (languages) {
            vm.languages = languages;
        });

        function loadAll() {
            //advance Search options for second page
            if ($rootScope.advSearch) {
                $scope.searchModel = {};
                $scope.advancedSearchCollapsed = !$rootScope.advSearch;  //expand search toggle
                //set values for advance search
                if ($rootScope.searchValues != undefined || $rootScope.searchValues != null) {
                    $scope.searchModel.company = ($rootScope.searchValues.company != undefined || $rootScope.searchValues.company != null)
                        ? $rootScope.searchValues.company : '';
                    $scope.searchModel.email = ($rootScope.searchValues.email != undefined || $rootScope.searchValues.email != null)
                        ? $rootScope.searchValues.email : '';
                    $scope.searchModel.lastName = ($rootScope.searchValues.lastName != undefined || $rootScope.searchValues.lastName != null)
                        ? $rootScope.searchValues.lastName : '';
                    $scope.searchModel.status = ($rootScope.searchValues.status != undefined || $rootScope.searchValues.status != null)
                        ? $rootScope.searchValues.status : '';
                }
            }

            User.query({
                //company Id present only for non-admin users
                cid: Principal.currentCompany ? Principal.currentCompany.id : null,
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                search: search()
            }, function (result, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;  //result.length;
                vm.page = pagingParams.page;
                //hide anonymous user from user management: it's a required user for Spring Security
                for (var i in result) {
                    if (result[i]['login'] === 'anonymoususer') {
                        result.splice(i, 1);
                    }
                }
                vm.users = result;
                angular.forEach(vm.users, function(user){
                    vm.roles = _.uniqBy(user, 'authorities');
                })
            });

            function search() {
                var searchParams = {};
                $scope.searchModel = $rootScope.searchValues;
                angular.forEach($scope.searchModel, function (value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                return angular.toJson(searchParams);
            }
        }

        vm.advancedSearch = function () {
            $rootScope.searchValues = $scope.searchModel;  //initialize value for next screen
            $rootScope.advSearch = true;
            vm.page = 1; //as advanced search reset to zero - start page
            vm.loadAll();
        }

        vm.reverse = true;
        vm.predicate = "createdDate";

        vm.sort = function sort(sortBy, response) {
            vm.reverse = response;
            vm.predicate = sortBy;
        }

        function loadPage(page) {
            vm.page = page;
            vm.loadAll();
            vm.sort();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: $rootScope.searchValues
            });

        }

        vm.clearAdvSearch = function () {
            $rootScope.searchValues = null;
            vm.loadAll();
            $scope.searchModel = {};
            $scope.searchModel.status = "ACTIVE";

        }

        vm.toggleAdvancedSearch = function () {
            $scope.searchModel = {};
            $scope.advancedSearchCollapsed = !$scope.advancedSearchCollapsed;
            if ($scope.advancedSearchCollapsed && angular.isDefined($scope.searchModel)) {
                $scope.searchModel.status = "";
                $scope.searchModel = {'$': $scope.searchModel.$};
            }
            $rootScope.searchValues = null;  //initially set to null
            $scope.searchModel.status = "ACTIVE";
        };
    }
})();
