(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('UserManagementDetailController', UserManagementDetailController);

    UserManagementDetailController.$inject = ['$stateParams', 'User', 'Principal'];

    function UserManagementDetailController ($stateParams, User, Principal) {
        var vm = this;

        vm.load = load;
        vm.user = {};
        vm.currentCompany = Principal.currentCompany;
        vm.currentAccount = null;
        vm.getAccessLevelTooltip = getAccessLevelTooltip;

        Principal.identity().then(function(account) {
            vm.currentAccount = account;
        });

        vm.load($stateParams.login);

        function load (login) {
            User.get({login: login}, function(result) {
                vm.user = result;
                vm.accessLevels = _.uniqBy(vm.user.memberships[0].accessLevels, 'accessLevel');
            });
        }

        function getAccessLevelTooltip (membership, accessLevel) {
            var tooltip = membership.company.name;
            if (accessLevel.studyName) {
                tooltip += ' - ' + accessLevel.studyName;
            }
            if (accessLevel.siteReference) {
                tooltip += ' - ' + accessLevel.siteReference;
            }
            return tooltip;
        }
    }
})();
