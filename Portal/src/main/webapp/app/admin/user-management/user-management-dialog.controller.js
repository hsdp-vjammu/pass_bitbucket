(function () {
    'use strict';

    angular
        .module('passApp')
        .controller('UserManagementDialogController', UserManagementDialogController);

    UserManagementDialogController.$inject = ['$scope', '$uibModalInstance', 'Principal', 'entity', 'User', 'Study', 'Site', '_', '$rootScope', '$uibModal'];

    function UserManagementDialogController($scope, $uibModalInstance, Principal, entity, User, Study, Site, _, $rootScope, $uibModal) {
        var vm = this;
        vm.clear = clear;
        vm.save = save;
        vm.isChecked = isChecked;
        vm.studyChanged = studyChanged;
        vm.user = null;
        vm.active = true;
        vm.accesslevels = [];
        vm.sitecoord = {study: null, sites: [], siteReference: [], siteRefs: []};
        vm.userNotAllowed = false;
        vm.allSites = [];
        vm.sitesList = [];
        vm.studies = [];
        vm.sites = [];
        vm.selectedSites = [];
        vm.preSelectedSites = [];
        vm.checkedSites = [];
        vm.sitesLoaded = true;

        $scope.buttonText = {buttonDefaultText: 'Select site'};
        $scope.dropdownLabel = {template: '{{option.externalReference}}', buttonClasses: 'form-control'};

        if (angular.isDefined(entity.$promise)) {
            entity.$promise.then(function (user) {
                vm.user = user;
                parseCurrentCompanyAccessLevels(user);
            });
        } else {
            vm.user = entity;
            parseCurrentCompanyAccessLevels(entity);
        }

        var emailListener = $scope.$watch('vm.user.email', function (value) {
            vm.userNotAllowed = false;
            if (vm.user && vm.user.email) {
                User.get({
                    login: value
                }, function (result) {
                    var isProductSupport = false;
                    angular.forEach(result.authorities, function (authority) {
                        if (authority === 'ROLE_ADMIN') {
                            isProductSupport = true;
                        }
                    });

                    if (isProductSupport) {
                        vm.userNotAllowed = true;
                    } else {
                        vm.user = result;
                        parseCurrentCompanyAccessLevels(result);
                    }
                });
            }
        });

        function studyChanged() {
            vm.sitecoord.site = null;
            loadSites();
        }

        function loadSites() {
            vm.sitesLoaded = false;
            vm.sites = [];
            if (vm.sitecoord.study) {
                Site.list(
                    {studyId: vm.sitecoord.study.id}
                ).$promise.then(function (sites) {
                    vm.sites = sites;
                    vm.sitesLoaded = true;
                    vm.listSites = listSites(sites);
                });
            } else {
                vm.sitesLoaded = true;
            }
        };

        vm.toggleSiteCoord = function () {
            if (!isChecked('ACL_SITECOORD')) vm.sitecoord = {study: null, site: null};
        };

        function clear() {
            $uibModalInstance.dismiss('cancel');
            $rootScope.selectedsites = undefined;
            $rootScope.checkedSites = undefined;
        }

        /*function clearSiteSelection() {
            $uibModalInstance.dismiss('cancel');
            //$uibModalStack.dismissAll('cancel');
            //$rootScope.selectedsites=undefined;
            //$rootScope.checkedSites=undefined;
        }*/

        function onSaveSuccess(result) {
            vm.isSaving = false;
            $uibModalInstance.close(result);
        }

        function onSaveSelectedSuccess(result) {
            vm.isSaving = false;
        }

        function onSaveError() {
            vm.isSaving = false;
        }

        function save() {
            vm.isSaving = true;
            var membership = getCurrentCompanyMembership(vm.user);
            if (!membership) {
                membership = {};
                membership.company = {id: Principal.currentCompany.id};
            }
            membership.enabled = vm.active;
            membership.accessLevels = [];
            membership.accessLevels.sites = [];
            var sites = [];
            for (var i = 0; i < vm.accesslevels.length; i++) {
                var accessLevel = {accessLevel: vm.accesslevels[i]};
                if (accessLevel.accessLevel === 'ACL_SITECOORD') {
                    accessLevel.studyId = vm.sitecoord.study.id;
                    //if no sites got selected or user click on save - No sites added
                    if (vm.newlySelectedSites != undefined) {
                        for (var j = 0; j < vm.newlySelectedSites.length; j++) {
                            membership.accessLevels.sites.push(vm.newlySelectedSites[j]);
                            sites.push(vm.newlySelectedSites[j]);
                        }
                    }
                }
                membership.accessLevels.push(accessLevel);
            }
            vm.user.memberships = [];
            vm.user.memberships.push(membership);
            vm.user.sites = _.uniqBy(sites);
            if (vm.user.id !== null) {
                User.update(vm.user, onSaveSuccess, onSaveError);
            } else {
                User.save(vm.user, onSaveSuccess, onSaveError);
            }
        }

        vm.openSiteSelection = function () {
            $rootScope.sites = vm.sites;
            $uibModal.open({
                templateUrl: 'app/admin/user-management/user-management-siteselection-dialog.html',
                controller: 'SiteselectioDialogController',
                controllerAs: 'vm',
                backdrop: 'static'
            }).result.then(function (choice) {
                if (choice === true) {
                    if ($rootScope.checkedSites != undefined) {
                        vm.checkedSites = $rootScope.checkedSites;
                    }
                    if ($rootScope.newlySelectedSites != undefined) {
                        vm.newlySelectedSites = $rootScope.newlySelectedSites;
                        vm.displayNewlySelectedSites = $rootScope.displayNewlySelectedSites;
                        reloadSites(vm.displayNewlySelectedSites);
                    }
                }
            }, function () {
                console.log("Please load date, before export process...");
            });
        };

        function reloadSites(selSites) {
            angular.forEach(selSites, function (newSite) {
                vm.selectedSites.push(newSite);
                vm.preSelectedSites.push(newSite);
            });
        }

        function getCurrentCompanyMembership(user) {
            var companyMembership = null;
            var currentCompany = Principal.currentCompany;
            if (currentCompany) {
                angular.forEach(user.memberships, function (membership) {
                    if (membership.company.id === currentCompany.id) {
                        companyMembership = membership;
                    }
                });
            }
            return companyMembership;
        }

        function parseCurrentCompanyAccessLevels(user) {
            var levels = [];
            var i = 0;
            var membership = getCurrentCompanyMembership(user);
            if (membership) {
                vm.active = membership.enabled;
                angular.forEach(membership.accessLevels, function (accessLevel) {
                    levels.push(accessLevel.accessLevel);
                    if (accessLevel.accessLevel === 'ACL_SITECOORD') {
                        vm.sitecoord.study = {id: accessLevel.studyId};
                        if (accessLevel.siteId != null) {
                            vm.sitecoord.sites[i] = {id: accessLevel.siteId};
                            vm.sitecoord.siteRefs[i] = {site: accessLevel.siteReference};
                            vm.sitecoord.siteReference[i] = {site: accessLevel.studyName + " - " + accessLevel.siteReference};
                        }
                        i++;
                    }
                });
            }
            vm.accesslevels = levels;
            vm.sitecoord.sites = _.uniqBy(vm.sitecoord.sites, 'id');  //list of ALL sites previously selected
            vm.sitecoord.siteRefs = _.uniqBy(vm.sitecoord.siteRefs, 'site');
            vm.sitecoord.siteReferences = _.uniqBy(vm.sitecoord.siteReference, 'site');

            Study.list(
                {cid: Principal.currentCompany.id}
            ).$promise.then(function (studies) {
                vm.studies = studies;
                if (vm.sitecoord.study) {
                    if (vm.sitecoord.study.id != null) {
                        angular.forEach(studies, function (study) {
                            if (study.id === vm.sitecoord.study.id) {
                                vm.sitecoord.study = study;
                            }
                        });
                        Site.list(
                            {studyId: vm.sitecoord.study.id}
                        ).$promise.then(function (sites) {
                            vm.sites = sites;
                            vm.selectedSites = [];
                            vm.listSites = listSites(sites);  // List of sites for given study id
                        });
                    }
                }
            });
        }


        function listSites(sites) {
            if (vm.sitecoord.sites) {
                var siteSelection = null;
                vm.selectedSites = [];
                vm.preSelectedSites = [];
                angular.forEach(vm.sitecoord.siteReferences, function (obj) {
                    if (obj != undefined) {
                        vm.preSelectedSites.push(obj.site);
                    }
                });

                vm.selectedSiteRefs = [];
                angular.forEach(vm.sitecoord.siteRefs, function (obj) {
                    if (obj != undefined) {
                        vm.selectedSiteRefs.push(obj.site);
                    }
                });

                angular.forEach(sites, function (obj) {
                    siteSelection = _.filter(vm.sitecoord.sites, {'id': obj.id});
                    if (siteSelection != 0) {
                        if (obj.id === siteSelection[0].id) {
                            vm.selectedSites.push(obj.externalReference);
                            //vm.selectedSites = _.uniqBy(vm.selectedSites, 'externalReference');
                            var index = vm.sites.indexOf(obj);
                            vm.sites[index].selected = true;  //previously selected sites marked as selected
                            angular.copy(vm.sites, vm.sitesList);
                            $rootScope.sitesList = vm.sitesList;
                            vm.checkedSites = vm.selectedSites;
                        }
                    }
                });
                $rootScope.studySelected = vm.sitecoord.study.name;
                $rootScope.selectedSiteRefs = vm.selectedSiteRefs;
                $rootScope.preSelectedSites = vm.preSelectedSites;
                $rootScope.checkedSites = vm.selectedSites;
            }

        }

        function isChecked(value) {
            return vm.accesslevels.indexOf(value) !== -1;
        }

        $scope.$on('$destroy', function () {
            if (angular.isDefined(emailListener) && emailListener !== null) {
                emailListener();
            }
        });

    }
})();
