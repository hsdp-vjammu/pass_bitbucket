(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('UserManagementDeleteController', UserManagementDeleteController);

    UserManagementDeleteController.$inject = ['$uibModalInstance', 'entity', 'User', 'Principal'];

    function UserManagementDeleteController ($uibModalInstance, entity, User, Principal) {
        var vm = this;

        vm.user = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete () {
            User.delete({login: vm.user.login, cid: Principal.currentCompany.id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
