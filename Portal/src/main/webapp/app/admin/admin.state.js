(function () {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig ($stateProvider) {
        $stateProvider.state('admin', {
            abstract: true,
            parent: 'app'
        })
        .state('admin-dashboard', {
            parent: 'admin',
            url: '/admin',
            data: {
                pageTitle: 'passApp.admin.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/admin.html'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('admin');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
