(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('password-policy', {
            parent: 'admin',
            url: '/password-policy',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'passwordpolicy.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/admin/password-policy/password-policy.html',
                    controller: 'PasswordPolicyController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('passwordPolicy');
                    return $translate.refresh();
                }],
                policy: ['$stateParams', 'PasswordPolicyService', function($stateParams, PasswordPolicyService) {
                    return PasswordPolicyService.get();
                }]
            }
        });
    }
})();
