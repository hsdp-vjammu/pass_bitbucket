(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('PasswordPolicyController', PasswordPolicyController);

    PasswordPolicyController.$inject = ['$scope', 'PasswordPolicyService', 'policy', 'AlertService'];

    function PasswordPolicyController ($scope, PasswordPolicyService, policy, AlertService) {
        var vm = this;

        vm.save = save;
        vm.isSaving = false;

        policy.$promise.then(function(result){
            vm.sessionTimeout = result.sessionTimeout;
            vm.passwordLength = result.passwordMinLength;
            vm.passwordLifetime = result.maxPasswordAge;
            vm.passwordExpirationThreshold = result.passwordExpirationThreshold;
            vm.failedLoginAttempts = result.failedLoginAttempts;
            vm.failedLoginAttemptsPeriod = result.failedLoginAttemptsPeriod;
        });

        $scope.defaultStep = 1;

        $scope.sessionTimeoutStep = 5;
        $scope.minSessionTimeout = 15;
        $scope.maxSessionTimeout = 360;

        $scope.minPasswordLength = 8;
        $scope.maxPasswordLength = 20;

        $scope.minPasswordLifetime = 5;
        $scope.maxPasswordLifetime = 180;

        $scope.minPasswordExpirationThreshold = 2;
        $scope.maxPasswordExpirationThreshold = 5;

        $scope.failedLoginAttemptsValues = [];
        for (var i = 3; i <= 5; i = i + 1) {
            $scope.failedLoginAttemptsValues.push(i)
        }

        $scope.failedLoginAttemptsPeriodValues = [];
        for (var i = 5; i <= 60; i = i + 5) {
            $scope.failedLoginAttemptsPeriodValues.push(i)
        }

        $scope.parseFloat = function (value) {
            return parseFloat(value);
        };

        function save () {
            vm.isSaving = true;
            PasswordPolicyService.update({
                passwordMinLength: vm.passwordLength,
                maxPasswordAge: vm.passwordLifetime,
                passwordExpirationThreshold: vm.passwordExpirationThreshold,
                sessionTimeout: vm.sessionTimeout,
                failedLoginAttempts: vm.failedLoginAttempts,
                failedLoginAttemptsPeriod: vm.failedLoginAttemptsPeriod
            }, onSaveSuccess, onSaveError);
        }

        var onSaveSuccess = function (result) {
            //$scope.$emit('passApp:companyUpdate', result);
            //vm.error = true;
            AlertService.success("Password policy changes saved");
            vm.isSaving = false;
        };

        var onSaveError = function () {
            AlertService.error("Failed to save password policy changes");
            vm.isSaving = false;
        };

    }
})();
