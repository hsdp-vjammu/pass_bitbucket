(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('PasswordPolicyService', PasswordPolicyService);

    PasswordPolicyService.$inject = ['$resource'];

    function PasswordPolicyService ($resource) {
        var resourceUrl =  'api/config/password-policy';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
