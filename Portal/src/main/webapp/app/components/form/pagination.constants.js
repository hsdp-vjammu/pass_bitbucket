(function() {
    'use strict';

    angular
        .module('passApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });

})();
