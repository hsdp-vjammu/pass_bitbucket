(function () {
    'use strict';

    var jhiItemCountAnalysis = {
        template: '<div class="info">' +
        'Showing {{(($ctrl.page-1) * 60)==0 ? 1:(($ctrl.page-1) * 60)}} - ' +
        '{{($ctrl.page * 60) < $ctrl.queryCount ? ($ctrl.page * 60) : $ctrl.queryCount}} ' +
        'of {{$ctrl.queryCount}} items.' +
        '</div>',
        bindings: {
            page: '<',
            queryCount: '<total'
        }
    };

    angular
        .module('passApp')
        .component('jhiItemCountAnalysis', jhiItemCountAnalysis);
})();
