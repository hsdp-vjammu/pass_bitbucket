(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$scope', '$state', 'Auth', 'Principal', 'ProfileService', '$uibModal'];

    function NavbarController ($scope, $state, Auth, Principal, ProfileService, $uibModal) {
        var vm = this;

        vm.isNavbarCollapsed = true;
        vm.account = null;
        vm.isAuthenticated = null;
        vm.isProductSupport = null;
        vm.isClinician = null;
        vm.isSiteCoordinator = null;
        vm.hasCurrentCompany = null;
        vm.company = null;

        ProfileService.getProfileInfo().then(function(response) {
            vm.inProduction = response.inProduction;
            vm.swaggerDisabled = response.swaggerDisabled;
        });

        var cleanAuthenticationSuccessListener = $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        vm.logout = logout;
        vm.toggleNavbar = toggleNavbar;
        vm.collapseNavbar = collapseNavbar;
        vm.$state = $state;

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                vm.hasCurrentCompany = Principal.hasCurrentCompany;
                vm.company = Principal.currentCompany;
                Principal.hasAuthority('ROLE_ADMIN')
                    .then(function (result) {
                        vm.isProductSupport = result;
                    });
                Principal.hasAuthority('ACL_CLINICIAN')
                    .then(function (result) {
                        vm.isClinician = result;
                    });
                Principal.hasAuthority('ACL_SITECOORD')
                    .then(function (result) {
                        vm.isSiteCoordinator = result;
                    });
            });
        }

        vm.about = about;

        function about() {
            collapseNavbar();
            $uibModal.open({
                templateUrl: 'app/layouts/about/about-dialog.html',
                controller: 'AboutDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
            }).result.then(function() {
                //no action
            }, function() {
                //no action
            });
        }

        function logout() {
            collapseNavbar();
            Auth.logout();
            $state.go('logout');
        }

        function toggleNavbar() {
            vm.isNavbarCollapsed = !vm.isNavbarCollapsed;
        }

        function collapseNavbar() {
            vm.isNavbarCollapsed = true;
        }

        $scope.$on('$destroy', function () {
            if(angular.isDefined(cleanAuthenticationSuccessListener) && cleanAuthenticationSuccessListener !== null){
                cleanAuthenticationSuccessListener();
            }
        });

    }
})();
