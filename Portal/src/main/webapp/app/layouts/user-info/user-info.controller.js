(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('UserInfoController', UserInfoController);

    UserInfoController.$inject = ['$scope', '$state', 'Auth', 'Principal', 'ProfileService', '$uibModal', '_', '$rootScope'];

    function UserInfoController ($scope, $state, Auth, Principal, ProfileService, $uibModal, _, $rootScope) {
        var vm = this;

        vm.isSiteCoordinator = false;
        vm.membership = null;
        vm.userRoles = null;
        vm.company = null;
        vm.studiesToDisplay = null;
        vm.studyInfo = {name: null, id: null};

        var cleanAuthenticationSuccessListener = $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                if (account) {
                    vm.company = Principal.currentCompany;
                    if (vm.company) {
                        angular.forEach(account.memberships, function (membership) {
                            if (membership.company.id === vm.company.id) {
                                vm.membership = membership;
                                if(vm.membership != null) {
                                    var access = [];
                                    var studies = [];
                                    var scStudies = [];
                                    var studyIds = [];
                                    for(i=0; i< membership.accessLevels.length; i++){
                                        var accessLevel = membership.accessLevels[i];
                                        if (accessLevel.accessLevel === 'ACL_SITECOORD') {
                                            vm.isSiteCoordinator = true;
                                        }
                                        access.push(accessLevel.accessLevel);
                                        vm.studyInfo = {id: accessLevel.studyId, name: accessLevel.studyName};
                                        studyIds.push(accessLevel.studyId);
                                        studies.push(accessLevel.studyName);
                                        scStudies[i]  = vm.studyInfo;
                                    }
                                    vm.userRoles = _.uniq(access).join(",");
                                    vm.studiesToDisplay = _.uniq(studies).join(", ");
                                    $rootScope.loginUserStudyIds = _.uniq(studyIds).join(",");
                                    $rootScope.loginUserRole = vm.userRoles;
                                    $rootScope.loginUserStudies = _.uniqBy(scStudies, function(e) { return e.id });
                                }
                            }
                        });
                    }
                }
            });
        }

        $scope.$on('$destroy', function () {
            if(angular.isDefined(cleanAuthenticationSuccessListener) && cleanAuthenticationSuccessListener !== null){
                cleanAuthenticationSuccessListener();
            }
        });

    }
})();
