(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('AboutDialogController', AboutDialogController);

    AboutDialogController.$inject = ['$uibModalInstance'];

    function AboutDialogController ($uibModalInstance) {
        var vm = this;

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }
})();
