(function () {
    'use strict';

    angular
        .module('passApp')
        .factory('Country', Country);

    Country.$inject = ['$resource'];

    function Country ($resource) {
        var service = $resource('api/public/countries/:language', {}, {
            'query': {method: 'GET', isArray: true},
        });

        return service;
    }
})();
