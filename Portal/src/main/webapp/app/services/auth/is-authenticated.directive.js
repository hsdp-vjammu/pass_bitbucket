(function() {
    'use strict';

    angular
        .module('passApp')
        .directive('isAuthenticated', isAuthenticated);

    function isAuthenticated(Principal) {
        var directive = {
            restrict: 'A',
            link: linkFunc
        };

        return directive;

        function linkFunc(scope, element, attrs) {
            var authenticated = ( attrs.isAuthenticated.replace(/\s+/g, '') === 'true');

             var setVisible = function () {
                    element.removeClass('hidden');
                },
                setHidden = function () {
                    element.addClass('hidden');
                },
                defineVisibility = function (reset) {

                    if (reset) {
                        setVisible();
                    }

                    if ((authenticated && Principal.isAuthenticated() && Principal.hasCurrentCompany()) || (!authenticated && !Principal.isAuthenticated())) {
                        setVisible();
                    } else {
                        setHidden();
                    }
                };

            defineVisibility(true);

            scope.$watch(function() {
                return Principal.isAuthenticated() && Principal.hasCurrentCompany();
            }, function() {
                defineVisibility(true);
            });
        }
    }
})();
