(function() {
    'use strict';

    angular
        .module('passApp')
        .factory('Principal', Principal);

    Principal.$inject = ['$sessionStorage', '$q', 'Account', 'JhiTrackerService'];

    function Principal ($sessionStorage, $q, Account, JhiTrackerService) {
        var _identity,
            _authenticated = false;

        var service = {
            authenticate: authenticate,
            hasAnyAuthority: hasAnyAuthority,
            hasAuthority: hasAuthority,
            identity: identity,
            isAuthenticated: isAuthenticated,
            isIdentityResolved: isIdentityResolved,
            hasCurrentCompany: hasCurrentCompany,

            get currentCompany() {
                return $sessionStorage.currentCompany;
            },
            set currentCompany(company) {
                $sessionStorage.currentCompany = company;
            }
        };

        return service;

        function authenticate (identity) {
            _identity = identity;
            _authenticated = identity !== null;
            if (!_authenticated) {
                $sessionStorage.$reset();
            } else if (_identity.authorities && _identity.authorities.indexOf('ROLE_USER') === -1) {
                service.currentCompany = {id: 0}
            }
        }

        function hasAnyAuthority (authorities) {
            if (!_authenticated || !_identity || !_identity.authorities) {
                return false;
            }

            var matched = false;
            for (var i = 0; i < authorities.length && !matched; i++) {
                matched = matched || _identity.authorities.indexOf(authorities[i]) !== -1;
                if (!matched && angular.isDefined(service.currentCompany)) {
                    matched = hasCompanyAccessLevel(_identity, service.currentCompany.id, authorities[i]);
                }
            }

            return matched;
        }

        function hasAuthority (authority) {
            if (!_authenticated) {
                return $q.when(false);
            }

            return this.identity().then(function(_id) {
                var matched = _id.authorities && _id.authorities.indexOf(authority) !== -1;
                if (!matched && angular.isDefined(service.currentCompany)) {
                    matched = hasCompanyAccessLevel(_id, service.currentCompany.id, authority);
                }
                return matched;
            }, function(){
                return false;
            });
        }

        function hasCompanyAccessLevel(identity, companyId, authority) {
            var matched = false;
            angular.forEach(identity.memberships, function(membership) {
                if (membership.company.id === companyId) {
                    angular.forEach(membership.accessLevels, function(accessLevel) {
                        if (accessLevel.accessLevel === authority) {
                            matched = true;
                        }
                    });
                }
            });
            return matched;
        }

        function identity (force) {
            var deferred = $q.defer();

            if (force === true) {
                _identity = undefined;
            }

            // check and see if we have retrieved the identity data from the server.
            // if we have, reuse it by immediately resolving
            if (angular.isDefined(_identity)) {
                deferred.resolve(_identity);

                return deferred.promise;
            }

            // retrieve the identity data from the server, update the identity object, and then resolve.
            Account.get().$promise
                .then(getAccountThen)
                .catch(getAccountCatch);

            return deferred.promise;

            function getAccountThen (account) {
                _identity = account.data;
                _authenticated = true;
                deferred.resolve(_identity);
               // JhiTrackerService.connect();
            }

            function getAccountCatch () {
                _identity = null;
                _authenticated = false;
                deferred.resolve(_identity);
            }
        }

        function isAuthenticated () {
            return _authenticated;
        }

        function hasCurrentCompany () {
            if (!_identity) {
                return false;
            } else if (_authenticated && _identity.authorities && _identity.authorities.indexOf('ROLE_USER') !== -1) {
                return angular.isDefined(service.currentCompany) && service.currentCompany !== null;
            } else {
                return _authenticated
            }
        }

        function isIdentityResolved () {
            return angular.isDefined(_identity);
        }
    }
})();
