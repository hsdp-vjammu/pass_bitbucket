(function() {
    'use strict';

    angular
        .module('passApp')
        .factory('Configuration', Configuration);

    Configuration.$inject = ['$q', '$http'];

    function Configuration ($q, $http) {

        var _configuration;

        var service = {
            load: load,
        };

        return service;

        function load (force) {
            var deferred = $q.defer();

            if (force === true) {
                _configuration = undefined;
            }

            // check and see if we have retrieved the configuration data from the server.
            // if we have, reuse it by immediately resolving
            if (angular.isDefined(_configuration)) {
                deferred.resolve(_configuration);

                return deferred.promise;
            }

            // retrieve the configuration data from the server, update the identity object, and then resolve.
            $http.get('/api/public/config').then(function(response) {
                if (response.data) {
                    _configuration = {
                        password: {
                            minLength: response.data.passwordMinLength,
                            maxAge: response.data.maxPasswordAge,
                            ageThreshold: response.data.passwordExpirationThreshold
                        },
                        session: {
                            timeout: response.data.sessionTimeout
                        }
                    }
                } else {
                    _configuration = null;
                }
                deferred.resolve(_configuration);
            }, function() {
                _configuration = null;
                deferred.resolve(_configuration);
                console.error("Error getting application configuration from server")
            });

            return deferred.promise;
        }
    }
})();
