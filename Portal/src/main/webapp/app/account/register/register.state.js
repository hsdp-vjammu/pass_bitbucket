(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('register', {
            parent: 'home',
            url: 'register',
            data: {
                authorities: [],
                pageTitle: 'register.title'
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('register');
                    return $translate.refresh();
                }]
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/account/register/register-dialog.html',
                    controller: 'RegisterController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg'
                }).result.then(function() {
                    $state.go('home', null, { reload: true });
                }, function() {
                    $state.go('home');
                });
            }]
        })
        .state('register.identity', {
            url: '/identity',
            data: {
                currentStepIndex: 1
            },
            views: {
                'wizard@': {
                    templateUrl: 'app/account/register/register-identity.html'
                }
            }
        })
        .state('register.security', {
            url: '/security',
            data: {
                currentStepIndex: 2
            },
            views: {
                'wizard@': {
                    templateUrl: 'app/account/register/register-security.html'
                }
            }
        })
        .state('register.company', {
            url: '/company',
            data: {
                currentStepIndex: 3
            },
            views: {
                'wizard@': {
                    templateUrl: 'app/account/register/register-company.html'
                }
            }
        })
        .state('register.agreement', {
            url: '/agreement',
            data: {
                currentStepIndex: 4
            },
            views: {
                'wizard@': {
                    templateUrl: 'app/account/register/register-agreement.html'
                }
            }
        });

    }
})();
