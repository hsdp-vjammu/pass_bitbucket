(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('RegisterController', RegisterController);


    RegisterController.$inject = ['$rootScope', '$scope', '$state', '$uibModalInstance', '$translate', '$timeout', 'Auth', 'LoginService'];

    function RegisterController ($rootScope, $scope, $state, $uibModalInstance, $translate, $timeout, Auth, LoginService) {
        var vm = this;

        var stateChangeListener = $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if (toState.data) {
                    vm.currentStepIndex = toState.data.currentStepIndex;
                }
            }
        );
        $scope.$on('$destroy', stateChangeListener);

        vm.configuration = $rootScope.configuration;
        vm.error = null;
        vm.errorUserExists = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.registerAccount = {};
        vm.success = null;
        vm.checkEula=false;
        vm.login = LoginService.open;

        vm.countries = $rootScope.countries;

        $timeout(function (){angular.element('#login').focus();});

        vm.setWizardState = function (state) {
            $state.go(state);
        };

        vm.validateProceed = function (event, form) {
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function(errorField){
                    errorField.$setTouched();
                })
            });
            if(form.$invalid) {
                event.stopImmediatePropagation();
                event.preventDefault();
                return false;
            }
            return true;
        };

        vm.validateSecurity = function (event, form) {
            var valid = true;
            vm.doNotMatchEmail = null;
            if (vm.registerAccount.email !== vm.confirmEmail) {
                valid = false;
                vm.doNotMatchEmail = 'ERROR';
                event.stopImmediatePropagation();
                event.preventDefault();
            }

            vm.doNotMatchPassword = null;
            if (vm.registerAccount.password !== vm.confirmPassword) {
                valid = false;
                vm.doNotMatchPassword = 'ERROR';
                event.stopImmediatePropagation();
                event.preventDefault();
            }

            return valid & vm.validateProceed(event, form);
        };

        vm.filterNumber = function (event) {
            return event.charCode >= 48 && event.charCode <= 57;
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        function register () {
            vm.isSaving = true;
            vm.registerAccount.langKey = $translate.use();

            Auth.createAccount(vm.registerAccount).then(function () {
                vm.isSaving = false;
                vm.success = 'OK';
            }).catch(function (response) {
                vm.isSaving = false;
                vm.success = null;
                if (response.status === 400 && response.data === 'e-mail address already in use') {
                    vm.errorEmailExists = 'ERROR';
                } else {
                    vm.error = 'ERROR';
                }
            });
        }
    }

    angular
        .module('passApp')
        .directive('emailExistsValidator', EmailExistsValidator);

    EmailExistsValidator.$inject = ['$q', '$http', '$timeout'];

    function EmailExistsValidator ($q, $http, $timeout) {
        var vm = this;
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.emailExists = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();

                    $http.get('/api/public/validate/email', {params: { email: viewValue }}).then(function() {
                        // Email not existing.
                        deferred.resolve();
                    }, function() {
                        // Email found, therefore not unique!
                        deferred.reject();
                    });

                    return deferred.promise;
                };
            }
        }
    }

    angular
        .module('passApp')
        .directive('popAvailableValidator', PopAvailableValidator);

    PopAvailableValidator.$inject = ['$q', '$http', '$timeout'];

    function PopAvailableValidator ($q, $http, $timeout) {
        var vm = this;
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.popAvailable = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();

                    var popNumber = "";
                    ngModel.$setValidity('popUnknown', true);
                    if (viewValue) {
                        popNumber = viewValue.replace(/-/g, "");
                    }
                    if (popNumber && popNumber.length == 32) {
                        $http.get('/api/public/validate/pop', {params: { number: popNumber.toUpperCase() }}).then(function() {
                            // POP number available.
                             deferred.resolve();
                        }, function() {
                            // No valid available POP number found
                            ngModel.$setValidity('popUnknown', false);
                            deferred.resolve();
                        });
                    } else {
                        deferred.resolve();
                    }

                    return deferred.promise;
                };
            }
        }
    }

})();
