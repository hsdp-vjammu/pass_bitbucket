(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('PasswordController', PasswordController);

    PasswordController.$inject = ['$rootScope', '$state', 'Auth', 'Principal'];

    function PasswordController ($rootScope, $state, Auth, Principal) {
        var vm = this;

        vm.configuration = $rootScope.configuration;

        vm.changePassword = changePassword;
        vm.error = null;
        vm.success = null;

        Principal.identity().then(function(account) {
            vm.account = account;
            vm.hasExpired = (account.passwordAge >= vm.configuration.password.maxAge);
        });

        function changePassword () {
            if (vm.password === vm.confirmPassword) {
                Auth.changePassword(vm.password).then(function () {
                    vm.error = null;
                    vm.success = 'OK';
                    Principal.identity(true).then(function() {
                        $state.go('home');
                    });
                }).catch(function () {
                    vm.success = null;
                    vm.error = 'ERROR';
                });
            }
        }
    }
})();
