(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('ActivationController', ActivationController);

    ActivationController.$inject = ['$stateParams', 'Auth', 'LoginService'];

    function ActivationController ($stateParams, Auth, LoginService) {
        var vm = this;
        vm.showPasswordFields = false;
        vm.activationKey = null;
        vm.password = null;
        vm.confirmPassword = null;
        vm.activate = activate;

        if ($stateParams.key && $stateParams.key.indexOf(":") !== -1) {
            //Show password reset
            var activationKeyComponents = $stateParams.key.split(':');
            if (activationKeyComponents.length === 2) {
                vm.activationKey = activationKeyComponents[0];
                switch(activationKeyComponents[1]) {
                    case "X":
                        vm.showPasswordFields = true;
                        break;
                    default:
                        activate();
                }
            } else {
                vm.success = null;
                vm.error = 'ERROR';
            }
        } else {
            //Do a direct activation
            vm.activationKey = $stateParams.key;
            activate();
        }

        function activate () {
            var data = {key: vm.activationKey}
            if (vm.password) {
                data.newPassword = vm.password
            }
            Auth.activateAccount(data).then(function () {
                vm.error = null;
                vm.success = 'OK';
            }).catch(function () {
                vm.success = null;
                vm.error = 'ERROR';
            });
        }

        vm.login = LoginService.open;
    }
})();
