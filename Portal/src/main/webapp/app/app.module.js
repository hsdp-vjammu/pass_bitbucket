(function () {
    'use strict';
    agGrid.initialiseAgGridWithAngular1(angular);
    angular
        .module('passApp', [
            'ngAnimate',
            'ngPopNumber',
            //'ngPasswordExpirationCheck',
            'ngStorage',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ngResource',
            'ngCookies',
            'ngAria',
            'ngCacheBuster',
            'ngFileUpload',
            'ui.bootstrap',
            'ui.router',
            'infinite-scroll',
            // jhipster-needle-angularjs-add-module JHipster will add new module here
            'angular-loading-bar',
            'ngIdle',
            'angular-timezone-selector',
            'smart-table',
            'pt.directives',
            'checklist-model',
            'bootstrap.fileField',
            'customFilters',
            'customDirectives',
            'agGrid',
            'customValidators'
        ])
        .constant('_', window._)
        .run(function ($rootScope) {
            $rootScope._ = window._;
        })
        .constant("PATTERNS", {
            "EMAIL": /^(?=[A-Za-z0-9@._%+-]{5,60}$)[A-Za-z0-9._%+-]{1,50}@(?:(?=[A-Za-z0-9-]{1,50}\.)[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*\.){1,8}[A-Za-z0-9]{2,50}$/
        })
        .controller('SessionTimeoutCtrl', ['$rootScope', '$scope', '$state', 'Idle', 'Keepalive', '$uibModal', 'Auth', '$uibModalStack', function ($rootScope, $scope, $state, Idle, Keepalive, $uibModal, Auth, $uibModalStack) {
            $scope.started = false;

            function closeModals() {

                if ($scope.warning) {
                    $scope.warning.close();
                    $scope.warning = null;
                }

                if ($scope.timedout) {
                    $scope.timedout.close();
                    $scope.timedout = null;
                }
            }

            $scope.$on('IdleStart', function () {
                closeModals();

                $scope.warning = $uibModal.open({
                    templateUrl: 'warning-dialog.html',
                    windowClass: 'modal-danger'
                });
            });

            $scope.$on('IdleEnd', function () {
                closeModals();
            });

            $scope.$on('IdleTimeout', function () {
                closeModals();

                $uibModalStack.dismissAll();

                Auth.logout();
                $state.go('logout');

            });

        }])
        .controller('ConfirmDialogController', ['$uibModalInstance', function ($uibModalInstance) {
            var vm = this;

            vm.ok = function () {
                $uibModalInstance.close(true);
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }])
        .config(['KeepaliveProvider', 'IdleProvider', '$httpProvider', function (KeepaliveProvider, IdleProvider, $httpProvider) {
            IdleProvider.idle(5);
            IdleProvider.timeout(5);
            KeepaliveProvider.http('api/account');
            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];
            //KeepaliveProvider.interval(10);
        }])
        .run(run);

    run.$inject = ['$rootScope', 'stateHandler', 'translationHandler', '$translate', 'Configuration', 'Idle', 'Country', 'orderByFilter'];

    function run($rootScope, stateHandler, translationHandler, $translate, Configuration, Idle, Country, orderByFilter) {
        stateHandler.initialize();
        translationHandler.initialize();
        $rootScope.returnState = {};
        $rootScope.search = {
            subjects: {
                model: {},
                options: {advancedSearchOpen: false}
            }
        };

        Configuration.load().then(function (configuration) {
            if (configuration) {
                console.log("Application configuration loaded");
                $rootScope.configuration = configuration;

                Idle.setIdle(configuration.session.timeout * 60);
            }
        });

        Country.query(
            {language: $translate.proposedLanguage()}
        ).$promise.then(function (response) {
            var countries = [];
            response = orderByFilter(response, 'name');
            for (var i = 0, l = response.length; i < l; i++) {
                countries[response[i].alpha3] = response[i].name;
            }

            $rootScope.countries = countries;
            console.log("Countries loaded for language: " + $translate.use());
        });

    }

})();
