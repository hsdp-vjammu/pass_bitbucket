(function () {
    'use strict';
    // DO NOT EDIT THIS FILE, EDIT THE GULP TASK NG CONSTANT SETTINGS INSTEAD WHICH GENERATES THIS FILE
    angular
        .module('passApp')
        .constant('VERSION', "2.0.3-beta69")
        .constant('DEBUG_INFO_ENABLED', true)
    ;
})();
