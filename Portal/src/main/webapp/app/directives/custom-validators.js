(function() {
    'use strict';

    angular
        .module('customValidators', [])
            .directive('lowerThan', LowerThan)
            .directive('higherThan', HigherThan)
            .directive('passwordStrengthValidator', PasswordStrengthValidator)
            .directive('passwordHistoryValidator', PasswordHistoryValidator)
            .directive('compareTo', CompareToValidator);

    LowerThan.$inject = [];
    function LowerThan() {
        return {
            require: 'ngModel',
            link: function($scope, $element, $attrs, ctrl) {

                var validate = function(viewValue) {
                    var comparisonModel = $attrs.lowerThan;

                    if(!viewValue || !comparisonModel){
                        // It's valid because we have nothing to compare against
                        ctrl.$setValidity('lowerThan', true);
                    } else {
                        // It's valid if model is lower than the model we're comparing against
                        ctrl.$setValidity('lowerThan', parseInt(viewValue, 10) < parseInt(comparisonModel, 10) );
                    }

                    return viewValue;
                };

                ctrl.$parsers.unshift(validate);
                ctrl.$formatters.push(validate);

                $attrs.$observe('lowerThan', function(comparisonModel){
                    // Whenever the comparison model changes we'll re-validate
                    return validate(ctrl.$viewValue);
                });

            }
        }
    }

    HigherThan.$inject = [];
    function HigherThan() {
        return {
            require: 'ngModel',
            link: function($scope, $element, $attrs, ctrl) {

                var validate = function(viewValue) {
                    var comparisonModel = $attrs.higherThan;

                    if(!viewValue || !comparisonModel){
                        // It's valid because we have nothing to compare against
                        ctrl.$setValidity('higherThan', true);
                    } else {
                        // It's valid if model is lower than the model we're comparing against
                        ctrl.$setValidity('higherThan', parseInt(viewValue, 10) > parseInt(comparisonModel, 10) );
                    }

                    return viewValue;
                };

                ctrl.$parsers.unshift(validate);
                ctrl.$formatters.push(validate);

                $attrs.$observe('higherThan', function(comparisonModel){
                    // Whenever the comparison model changes we'll re-validate
                    return validate(ctrl.$viewValue);
                });

            }
        }
    }

    PasswordStrengthValidator.$inject = ['$q', '$http'];

    function PasswordStrengthValidator ($q, $http) {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.passwordStrength = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();

                    $http.get('/api/public/validate/password', {params: { pw: viewValue }}).then(function() {
                        // Password strength ok.
                        deferred.resolve();
                    }, function() {
                        // Password too weak
                        deferred.reject();
                    });

                    return deferred.promise;
                };
            }
        }
    }

    PasswordHistoryValidator.$inject = ['$q', '$http'];

    function PasswordHistoryValidator ($q, $http) {
        return {
            require: 'ngModel',
            scope: {
                login: "=login",
                key: "=resetkey"
            },
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.passwordHistory = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();

                    $http.get('/api/public/validate/passwordhistory', {params: { pw: viewValue, login: scope.login, key: scope.key }}).then(function() {
                        // Password strength ok.
                        deferred.resolve();
                    }, function() {
                        // Password too weak
                        deferred.reject();
                    });

                    return deferred.promise;
                };
            }
        }
    }

    CompareToValidator.$inject = [];

    function CompareToValidator () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
    };

})();
