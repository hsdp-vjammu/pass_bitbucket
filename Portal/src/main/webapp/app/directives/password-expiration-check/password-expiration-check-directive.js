(function() {
    'use strict';

    angular.module('passApp')
        .directive('passwordExpirationCheck', ['$rootScope', '$uibModal', '$state', 'Principal','Configuration', function($rootScope, $uibModal, $state, Principal, Configuration) {

            var test = null;

            function link(scope, element, attributes, ngModel) {

                Configuration.load().then(function(configuration) {
                    var maxPasswordAge = configuration.password.maxAge;
                    var expirationWarningDays = configuration.password.ageThreshold;

                    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                        if (toState.name !== 'password') {
                            Principal.identity().then(function(account) {
                                if (Principal.isAuthenticated() && Principal.hasCurrentCompany() && account && account.passwordAge >= maxPasswordAge) {
                                    event.preventDefault();
                                    $state.go('password');
                                }
                            });
                        }
                    });

                    $rootScope.$on('userContextChanged', function() {
                        Principal.identity().then(function(account) {
                            if (Principal.isAuthenticated() && Principal.hasCurrentCompany() && account) {
                                if (account.passwordAge >= maxPasswordAge) {
                                    event.preventDefault();
                                    $state.transitionTo('password', null, { reload: true, inherit: false, notify: true});
                                } else if (account.passwordAge >= (maxPasswordAge - expirationWarningDays)) {
                                    $uibModal.open({
                                        templateUrl: 'app/directives/password-expiration-check/password-expiration-warning.html',
                                        controller: 'PasswordExpirationDialogController',
                                        controllerAs: 'vm',
                                        backdrop: 'static',
                                        size: 'md',
                                        resolve: {
                                            context: function () {
                                                return {
                                                    passwordAge: maxPasswordAge - account.passwordAge
                                                };
                                            }
                                        }
                                    }).result.then(function() {
                                        //Do nothing
                                    }, function() {
                                        //Do nothing
                                    });
                                }
                            }
                        });
                    });
                });
            }

            return {
                link: link,
                restrict: 'E'
            };
        }])
        .controller('PasswordExpirationDialogController', PasswordExpirationDialogController);

    PasswordExpirationDialogController.$inject = ['$rootScope', '$scope', '$uibModalInstance', 'context'];

    function PasswordExpirationDialogController ($rootScope, $scope, $uibModalInstance, context) {
        var vm = this;

        vm.passwordAge = context.passwordAge;

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }

})();
