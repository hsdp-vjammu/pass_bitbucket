(function() {
    'use strict';

    angular.module('customFilters', [])
        .filter('country', ['$rootScope', function ($rootScope) {
            return function(langCode) {
                if ($rootScope.countries && langCode in $rootScope.countries) {
                    return $rootScope.countries[langCode];
                } else {
                    return langCode;
                }
            }
        }])
        .filter('default', ['$translate', function ($translate) {
            return function(input, key) {
                if (input) {
                    return input;
                } else {
                    return $translate.instant(key);
                }
            }
        }]);

})();
