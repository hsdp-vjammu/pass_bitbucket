(function() {
    'use strict';

    angular.module('customDirectives', [])
        .directive('numbersOnly', [function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attr, ngModelCtrl) {
                    function fromUser(text) {
                        if (text) {
                            var transformedInput = text.replace(/[^0-9]/g, '');

                            if (transformedInput !== text) {
                                ngModelCtrl.$setViewValue(transformedInput);
                                ngModelCtrl.$render();
                            }
                            return transformedInput;
                        }
                        return null;
                    }
                    ngModelCtrl.$parsers.push(fromUser);
                }
            };
        }])
        .directive('blur', [function () {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    element.on('click', function () {
                        element.blur();
                    });
                }
            };
        }])
        .directive('staticInclude', [function() {
            return {
                restrict: 'AE',
                templateUrl: function(ele, attrs) {
                    return attrs.templatePath;
                }
            };
        }])
        .directive('capitalize', [function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, modelCtrl) {
                    var capitalize = function(inputValue) {
                        if (inputValue == undefined) inputValue = '';
                        var capitalized = inputValue.toUpperCase();
                        if (capitalized !== inputValue) {
                            modelCtrl.$setViewValue(capitalized);
                            modelCtrl.$render();
                        }
                        return capitalized;
                    }
                    modelCtrl.$parsers.push(capitalize);
                    capitalize(scope[attrs.ngModel]); // capitalize initial value
                }
            };
        }])
        .directive('typeaheadShowOnFocus', [function () {
            return {
                require: 'ngModel',
                link: function ($scope, element, attrs, ngModel) {
                    element.bind('focus', function () {
                        ngModel.$setViewValue();
                        $(element).trigger('input');
                        $(element).trigger('change');
                    });
                }
            };
        }])
        .directive('checkRequired', [function(){
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$validators.checkRequired = function (modelValue, viewValue) {
                        var value = modelValue || viewValue;
                        var match = scope.$eval(attrs.ngTrueValue) || true;
                        return value && match === value;
                    };
                }
            };
        }])
        .directive('authenticatedClass', ['Principal', function (Principal) {
            var directive = {
                restrict: 'A',
                link: linkFunc
            };

            return directive;

            function linkFunc(scope, element, attrs) {
                var cssClass = attrs.authenticatedClass.replace(/\s+/g, '');

                var removeClass = function () {
                        element.removeClass(cssClass);
                    },
                    setClass = function () {
                        element.addClass(cssClass);
                    },
                    defineClass = function (reset) {

                        if (reset) {
                            removeClass();
                        }

                        if (Principal.isAuthenticated()) {
                            setClass();
                        } else {
                            removeClass();
                        }
                    };

                defineClass(true);

                scope.$watch(function() {
                    return Principal.isAuthenticated();
                }, function() {
                    defineClass(true);
                });
            }
        }]);

})();
