(function() {
    'use strict';

    angular.module('ngPopNumber', [])
        .directive('popNumber', ['$filter', function($filter) {

            function link(scope, element, attributes, ngModel) {

                // scope.inputValue is the value of input element used in template
                scope.inputValue = scope.popnumberModel;

                var listener = scope.$watch('inputValue', function(value, oldValue) {
                    var number = '';
                    if (angular.isDefined(value)) {
                        var number = String(value).replace(/[^0-9a-fA-F]+/g, '');

                        scope.inputValue = $filter('popnumber')(number);

                    } else {
                        scope.inputValue = number;
                    }

                    var valid = (!angular.isDefined(number) || number.length == 0 || number.length == 32);
                    ngModel.$setValidity('popnumber', valid);
                    if (valid) {
                        scope.popnumberModel = number
                    } else {
                        scope.popnumberModel = '';
                    }
                });

            }

            return {
                require: 'ngModel',
                link: link,
                restrict: 'E',
                replace: true,
                scope: {
                    popnumberModel: '=popnumber'
                },
                templateUrl: 'app/directives/popnumber/popnumber-input.html'
            };
        }])
        .filter('popnumber', function() {
            /*
             Format pop number as: xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx-xxxx (x = alphanumeric character)
             */

            return function (number) {
                if (!number) { return ''; }

                number = String(number);

                var formattedNumber = number;

                if (number) {
                    var part1 = number.substring(0,4);
                    if (part1) {
                        formattedNumber = part1.toUpperCase();
                    }
                    var part2 = number.substring(4,8);
                    if (part2) {
                        formattedNumber += ("-" + part2.toUpperCase());
                    }
                    var part3 = number.substring(8,12);
                    if (part3) {
                        formattedNumber += ("-" + part3.toUpperCase());
                    }
                    var part4 = number.substring(12,16);
                    if (part4) {
                        formattedNumber += ("-" + part4.toUpperCase());
                    }
                    var part5 = number.substring(16,20);
                    if (part5) {
                        formattedNumber += ("-" + part5.toUpperCase());
                    }
                    var part6 = number.substring(20,24);
                    if (part6) {
                        formattedNumber += ("-" + part6.toUpperCase());
                    }
                    var part7 = number.substring(24,28);
                    if (part7) {
                        formattedNumber += ("-" + part7.toUpperCase());
                    }
                    var part8 = number.substring(28,32);
                    if (part8) {
                        formattedNumber += ("-" + part8.toUpperCase());
                    }

                    return formattedNumber
                } else {
                    return number;
                }

            };
        });

})();
