(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('home', {
            parent: 'app',
            url: '/',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                },
                'auth@': {
                    templateUrl: 'app/home/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('home');
                    $translatePartialLoader.addPart('login');
                    $translatePartialLoader.addPart('study');
                    $translatePartialLoader.addPart('site');
                    $translatePartialLoader.addPart('monitor');
                    $translatePartialLoader.addPart('configuration');
                    $translatePartialLoader.addPart('studyStatus');
                    return $translate.refresh();
                }]
            }
        })
        .state('register-company', {
            parent: 'home',
            url: '/register/company',
            data: {
                authorities: [],
                pageTitle: 'register.title'
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('register');
                    return $translate.refresh();
                }]
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/home/register-company-dialog.html',
                    controller: 'RegisterCompanyController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg'
                }).result.then(function() {
                    $state.go('home', null, { reload: true });
                }, function() {
                    $state.go('home');
                });
            }]
        })
        .state('register-company.info', {
            parent: 'register-company',
            url: '/info',
            data: {
                currentStepIndex: 1
            },
            views: {
                'wizard@': {
                    templateUrl: 'app/home/register-company-info.html'
                }
            }
        })
        .state('register-company.agreement', {
            parent: 'register-company',
            url: '/agreement',
            data: {
                currentStepIndex: 2
            },
            views: {
                'wizard@': {
                    templateUrl: 'app/home/register-company-agreement.html'
                }
            }
        })
        .state('logout', {
            parent: 'app',
            url: '/logout',
            data: {
                authorities: []
            },
            views: {
                'auth@': {
                    templateUrl: 'app/home/logout.html'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('login');
                    return $translate.refresh();
                }]
            }
        });

    }
})();
