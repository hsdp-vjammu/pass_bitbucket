(function() {
    'use strict';

    angular
        .module('passApp')
        .factory('LoginService', LoginService);

    LoginService.$inject = ['$state'];

    function LoginService ($state) {
        var service = {
            open: open
        };

        return service;

        function open () {
            $state.go('home');
        }
    }
})();
