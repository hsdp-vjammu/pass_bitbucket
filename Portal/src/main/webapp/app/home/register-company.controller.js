(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('RegisterCompanyController', RegisterCompanyController);


    RegisterCompanyController.$inject = ['$rootScope', '$scope', '$state', '$uibModalInstance', '$timeout', 'Principal', 'Company'];

    function RegisterCompanyController ($rootScope, $scope, $state, $uibModalInstance, $timeout, Principal, Company) {
        var vm = this;

        var stateChangeListener = $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams){
                if (toState.data) {
                    vm.currentStepIndex = toState.data.currentStepIndex;
                }
            }
        );
        $scope.$on('$destroy', stateChangeListener);

        vm.register = register;
        vm.company = {};
        vm.error = null;
        vm.success = null;
        vm.checkEula=false;

        vm.countries = $rootScope.countries;

        $timeout(function (){angular.element('#login').focus();});

        vm.setWizardState = function (state) {
            $state.go(state);
        };

        vm.validateProceed = function (event, form) {
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function(errorField){
                    errorField.$setTouched();
                })
            });
            if(form.$invalid) {
                event.stopImmediatePropagation();
                event.preventDefault();
                return false;
            }
            return true;
        };

        vm.filterNumber = function (event) {
            return event.charCode >= 48 && event.charCode <= 57;
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.setWizardState = function (state) {
            $state.go(state);
        };

        function register () {
            vm.isSaving = true;
            Company.register(vm.company, onSaveSuccess, onSaveError);
        }

        var onSaveSuccess = function (result) {
            $rootScope.$broadcast('reloadPrincipal');
            vm.isSaving = false;
            vm.success = 'OK';
        };

        var onSaveError = function () {
            vm.isSaving = false;
            vm.success = null;
            vm.error = 'ERROR';
        };

    }

})();
