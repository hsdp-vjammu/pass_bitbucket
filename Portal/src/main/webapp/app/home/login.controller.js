(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$rootScope', '$scope', 'Principal', '$state', '$timeout', 'Auth', '$translate', '_'];

    function LoginController ($rootScope, $scope, Principal, $state, $timeout, Auth, $translate, _) {
        var vm = this;

        vm.authenticationError = null;
        vm.credentials = {};
        vm.password = null;
        vm.username = null;
        vm.account = null;
        vm.membership = null;

        vm.isAuthenticated = Principal.isAuthenticated;
        if (vm.isAuthenticated) {

        }

        $timeout(function (){angular.element('#username').focus();});

        vm.login = login;
        vm.logout = logout;
        vm.clearError = clearError;
        vm.finishLogin = finishLogin;
        vm.registerCompany = registerCompany;
        vm.getMembershipDisplay = getMembershipDisplay;

        if (Principal.isAuthenticated() && !Principal.hasCurrentCompany()) {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.username = account.login;
                vm.password = '          ';
                vm.isAuthenticated = Principal.isAuthenticated;

                if (vm.account.memberships && vm.account.memberships.length === 1) {
                    // Preselect the company if there is only 1
                    vm.membership = vm.account.memberships[0];
                }
            });
        }

        var userContextChangeListener = $rootScope.$on('reloadPrincipal',
            function(event, toState, toParams, fromState, fromParams){
                Principal.identity(true).then(function(account) {
                    vm.account = account;

                    if (vm.account.memberships && vm.account.memberships.length === 1) {
                        // Preselect the company if there is only 1
                        vm.membership = vm.account.memberships[0];
                    }
                });
            }
        );
        $scope.$on('$destroy', userContextChangeListener);

        function login (event) {
            event.preventDefault();
            Auth.login({
                username: vm.username,
                password: vm.password
            }).then(function () {
                vm.authenticationError = null;
                vm.password = '          ';

                if ($state.current.name === 'register' || $state.current.name === 'activate' ||
                    $state.current.name === 'finishReset' || $state.current.name === 'requestReset') {
                    $state.go('home');
                }

                $rootScope.$broadcast('authenticationSuccess');

                Principal.identity().then(function(account) {
                    vm.account = account;
                    vm.isAuthenticated = Principal.isAuthenticated;

                    if (Principal.hasCurrentCompany()) {
                        redirect(account);
                    } else {
                        if (vm.account.memberships && vm.account.memberships.length === 1) {
                            // Preselect the company if there is only 1
                            vm.membership = vm.account.memberships[0];
                        }
                    }
                });
            }).catch(function (error) {
                vm.authenticationError = error.data.message;
            });
        }

        function clearError () {
            vm.authenticationError = null;
        }

        function finishLogin() {
            if (vm.membership.enabled) {
                Principal.currentCompany = vm.membership.company;
                Principal.identity(true).then(function(account) {
                    redirect(account);
                    $rootScope.$broadcast('userContextChanged');
                });
            }
         }

        function redirect(account) {
            // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // since login is succesful, go to stored previousState and clear previousState
            if ($state.current.name != 'password' && Principal.isAuthenticated() && Principal.hasCurrentCompany() && account && account.passwordAge >= $rootScope.configuration.password.maxAge) {
                event.preventDefault();
                $state.transitionTo('password', null, { reload: true, inherit: false, notify: true});
            } else {
                if (Auth.getPreviousState()) {
                    var previousState = Auth.getPreviousState();
                    Auth.resetPreviousState();
                    $state.transitionTo(previousState.name, previousState.params, { reload: true, inherit: false, notify: true});
                } else {
                    $state.transitionTo('home', null, { reload: true, inherit: false, notify: true});
                }
            }
        }

        function registerCompany() {
            $state.go('register-company');
        }

        function logout() {
            vm.password = null;
            vm.username = null;
            vm.account = null;
            vm.membership = null;
            Auth.logout();
            $timeout(function (){angular.element('#username').focus();});
        }

        function getMembershipDisplay(membership) {
            var access = [];
            angular.forEach( membership.accessLevels, function (accessLevel) {
                access.push($translate.instant('Authority.' + accessLevel.accessLevel));
            });
            return membership.company.name + ' (' + _.uniq(access).join(' & ') + ')';
        }
    }
})();
