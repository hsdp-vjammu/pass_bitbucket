(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$rootScope', '$scope', 'Principal', '$state','ParseLinks', 'AlertService', 'pagingParams', 'Study','Site'];

    function HomeController ($rootScope, $scope, Principal, $state, ParseLinks, AlertService, pagingParams, Study,Site) {
        var vm = this;
        vm.account = null;
        vm.loadPage = loadPage;
        vm.getStudies=getStudies;
        vm.checkMultipleRoles = checkMultipleRoles;
        vm.isAuthenticated = Principal.isAuthenticated;
        vm.isProductSupport = null;
        vm.isSiteCoordinator = false;
        vm.company = null;
        vm.membership=null;
        vm.sitecoord = {study: null, site: null};
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };
        $scope.searchModel = {};

        vm.getSite=getSite;
        vm.site = null;
        var subjs = [];
        vm.numberOfSubjs = null;

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        getAccount();


        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                vm.company = Principal.currentCompany;
                Principal.hasAuthority('ROLE_ADMIN')
                    .then(function (result) {
                        vm.isProductSupport = result;
                        if (result) {
                            $state.go('admin-dashboard');
                        }

                        if(Principal.currentCompany!==undefined){
                            if (vm.account) {
                                angular.forEach(vm.account.memberships, function(membership) {
                                    if (membership.company.id === Principal.currentCompany.id) {
                                        vm.userRole = vm.checkMultipleRoles(membership.accessLevels);
                                        angular.forEach(membership.accessLevels, function(accessLevel) {
                                            if(vm.userRole === "ADMIN"){
                                                $state.go('admin-dashboard');
                                            } else if (vm.userRole === "CLINICIAN") {
                                                //do nothing take user to dashboard
                                            }
                                            if (accessLevel.accessLevel === 'ACL_SITECOORD') {
                                                vm.isSiteCoordinator =true;
                                                vm.membership = membership;
                                                vm.sitecoord.study = {id: accessLevel.studyId};
                                                vm.sitecoord.site = {id: accessLevel.siteId};
                                                subjs.push(vm.sitecoord.study.id);
                                                vm.numberOfSubjs = _.uniq(subjs).length;
                                                vm.getSite(accessLevel.siteId);
                                            }
                                        });
                                    }
                                });
                            }

                            if (!vm.isSiteCoordinator) {
                                vm.getStudies();
                            }
                        }

                    });
            });
        }

        function checkMultipleRoles(accessLevels) {
            vm.userRole = "";
            angular.forEach(accessLevels, function(accessLevel) {
                if (accessLevel.accessLevel === 'ACL_COMPADMIN') {
                    vm.admin = true;
                } else if (accessLevel.accessLevel === 'ACL_CLINICIAN') {
                    vm.clinician = true;
                }
            });
            if(vm.admin && vm.clinician){
                vm.userRole =  "CLINICIAN";
            } else if (vm.admin) {
                vm.userRole = "ADMIN";
            }
            return vm.userRole;
        }


        function getStudies() {
            Study.query({
                cid: Principal.currentCompany.id,
                page: pagingParams.page - 1,
                size: 3,
                sort: sort(),
                search: search()
            }, onSuccess, onError);

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                return angular.toJson(searchParams);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                //vm.itemsPerPage=3;
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.studies = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }


        }

        function getSite(siteId) {
            Site.get({id : siteId}).$promise.then(function(site){
                vm.site = site;
            });
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')
            });
        }
        vm.navigateToStudyDetails = function (id) {
            $rootScope.returnState.study = $state.current.name;
            $state.go('study-detail', {id:id});
        };
    }
})();
