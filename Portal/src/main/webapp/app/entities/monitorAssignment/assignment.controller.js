(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorAssignmentController', MonitorAssignmentController);

    MonitorAssignmentController.$inject = ['$rootScope', '$scope', '$state', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Principal', 'MonitorAssignment'];

    function MonitorAssignmentController ($rootScope, $scope, $state, ParseLinks, AlertService, pagingParams, paginationConstants, Principal, MonitorAssignment)  {
        var vm = this;

        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.loadAll();

        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };

        $scope.searchModel = {};
        $scope.$watchCollection('searchModel', function (newVal, oldVal) {
            loadAll ();
        });

        function loadAll () {

            MonitorAssignment.query({
                cid: Principal.currentCompany.id,
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort(),
                search: search()
            }, onSuccess, onError);

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                //for SC data manipulation changes
                /*if($rootScope.loginUserRole === "ACL_SITECOORD") {
                    searchParams = {siteIds: $rootScope.loginUserStudyIds}
                }*/
                return angular.toJson(searchParams);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.assignments = data;
                //replaceSubjectsForSC();  //replace data list for SC
                vm.page = pagingParams.page;
            }

            function replaceSubjectsForSC() {
                //display site coordinator related studies
                vm.newStudiesCollection = [];
                if($rootScope.loginUserRole === "ACL_SITECOORD") {
                    angular.forEach(vm.assignments, function(assignment){
                        var scAssignedStudies = $rootScope.loginUserStudies;
                        var studyFound = _.find(scAssignedStudies, function(gsc) {
                            return gsc.id === assignment.studyId;
                        } );
                        if(studyFound) {
                            vm.newStudiesCollection.push(assignment);
                        }
                    });
                    vm.assignments = vm.newStudiesCollection;  //display list for SC
                }
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });

        }

        vm.toggleAdvancedSearch = function () {
            $scope.advancedSearchCollapsed = !$scope.advancedSearchCollapsed;
            if ($scope.advancedSearchCollapsed && angular.isDefined($scope.searchModel)) {
                $scope.searchModel = {'$': $scope.searchModel.$};
            }
        };

        vm.navigateToConfigurationDetails = function (id) {
            $rootScope.returnState.configuration = $state.current.name;
            $state.go('monitor-configuration-detail', {id:id});
        };

        vm.navigateToStudyDetails = function (id) {
            $rootScope.returnState.study = $state.current.name;
            $state.go('study-detail', {id:id});
        };

        vm.navigateToSubjectDetails = function (id) {
            $rootScope.returnState.subject = $state.current.name;
            $state.go('subject-detail.analysis', {id:id});
        };

    }

})();
