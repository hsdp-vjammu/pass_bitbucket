(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('monitor-assignment', {
            parent: 'entity',
            url: '/monitor-assignment?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.monitorAssignment.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitorAssignment/assignments.html',
                    controller: 'MonitorAssignmentController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'monitorConfigurationName,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitorAssignment');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('study');
                    return $translate.refresh();
                }]
            }
        })
        .state('monitor-assignment.new', {
            parent: 'monitor-assignment',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitorAssignment/assignment-dialog.html',
                    controller: 'MonitorAssignmentDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                serialNumber: null,
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('monitor-assignment', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('monitor-assignment.delete', {
            parent: 'monitor-assignment',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitorAssignment/assignment-delete-dialog.html',
                    controller: 'MonitorAssignmentDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MonitorAssignment', function(MonitorAssignment) {
                            return MonitorAssignment.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitor-assignment', {id: $stateParams.id}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
