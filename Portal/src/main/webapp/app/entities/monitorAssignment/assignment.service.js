(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('MonitorAssignment', MonitorAssignment);

    MonitorAssignment.$inject = ['$resource'];

    function MonitorAssignment ($resource) {
        var resourceUrl =  'api/monitorAssignments/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.initialMonitorId = data.monitorId,
                        data.subject = {
                            id: data.subjectId,
                            externalReference: data.subjectExternalReference,
                            firstName: data.subjectFirstName,
                            lastName: data.subjectLastName,
                            siteId: data.siteId,
                            study: {
                                id: data.studyId,
                                externalReference: data.studyExternalReference,
                                name: data.studyName
                            }
                        }
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
