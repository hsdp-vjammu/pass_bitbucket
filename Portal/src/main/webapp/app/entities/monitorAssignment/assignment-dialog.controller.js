(function () {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorAssignmentDialogController', MonitorAssignmentDialogController);

    MonitorAssignmentDialogController.$inject = ['$timeout', '$scope', '$state', '$uibModalInstance', 'Principal', 'entity', 'MonitorAssignment', 'Subject', 'Monitor', 'MonitorConfiguration', 'Site', 'Study', '$rootScope'];

    function MonitorAssignmentDialogController($timeout, $scope, $state, $uibModalInstance, Principal, entity, MonitorAssignment, Subject, Monitor, MonitorConfiguration, Site, Study, $rootScope) {
        var _selectedSubjectName;
        var _selectedMonitorSerialNumber;

        var vm = this;
        vm.monitors = [];
        vm.sites = [];
        vm.sitesLoaded = false;
        vm.isSiteCoordinator = false;
        vm.siteReferences = [];
        vm.file = null;
        vm.isStudyActive = true;

        Principal.hasAuthority('ACL_SITECOORD')
            .then(function (result) {
                vm.isSiteCoordinator = result;
            });

        if (angular.isDefined(entity.$promise)) {
            entity.$promise.then(function (assignment) {
                vm.assignment = assignment;
                Subject.queryUnassigned(
                    {cid: Principal.currentCompany.id, subjectId: assignment.subjectId}
                ).$promise.then(function (subjects) {
                    vm.subjects = subjects;
                    replaceSubjectsForSC();
                });
                Monitor.queryUnallocated(
                    {cid: Principal.currentCompany.id, subjectId: assignment.subjectId}
                ).$promise.then(function (monitors) {
                    vm.monitors = monitors;
                });
                MonitorConfiguration.list(
                    {cid: Principal.currentCompany.id}
                ).$promise.then(function (configurations) {
                    vm.configurations = configurations;
                });
                if (vm.assignment.subject.studyId) {
                    vm.loadSites();
                }
            });
        } else {
            vm.assignment = entity;
            Subject.queryUnassigned(
                {cid: Principal.currentCompany.id}
            ).$promise.then(function (subjects) {
                vm.subjects = subjects;
                replaceSubjectsForSC();
            });
            Monitor.queryUnallocated(
                {cid: Principal.currentCompany.id}
            ).$promise.then(function (monitors) {
                vm.monitors = monitors;
            });
            MonitorConfiguration.list(
                {cid: Principal.currentCompany.id}
            ).$promise.then(function (configurations) {
                vm.configurations = configurations;
            });
        }

        function replaceSubjectsForSC() {
            //display site coordinator related studies
            vm.newStudiesCollection = [];
            if (vm.isSiteCoordinator) {
                angular.forEach(vm.subjects, function (subject) {
                    var scAssignedStudies = $rootScope.loginUserStudies;
                    var studyFound = _.find(scAssignedStudies, function (gsc) {
                        return gsc.id === subject.studyId;
                    });
                    if (studyFound) {
                        vm.newStudiesCollection.push(subject);
                    }
                });
                vm.subjects = vm.newStudiesCollection;  //display list for SC
            }
        }

        $timeout(function () {
            angular.element('.form-group:eq(0)>input').focus();
        });
        var onSaveSuccessSite = function (result) {
            $scope.$emit('passApp:siteUpdate', result);
            if (vm.assignment.initialMonitorId) {
                MonitorAssignment.update(vm.assignment, onSaveSuccess, onSaveError);
            } else {
                MonitorAssignment.save(vm.assignment, onSaveSuccess, onSaveError);
            }
            vm.isSaving = false;
        };
        var onSaveSuccess = function (result) {
            $scope.$emit('passApp:monitorAssignmentUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;

            vm.assignment.subjectId = vm.assignment.subject.id;
            vm.assignment.siteId = vm.assignment.subject.siteId;
            vm.assignment.siteReference = vm.assignment.subject.siteReference;
            vm.siteReferences.push(vm.assignment.siteReference);
            vm.saveSite(vm.assignment.subject.studyId, vm.assignment.siteReference);
        };
        vm.saveSite = function (studId, siteReference) {
            Site.save({
                studyId: studId,
                siteReferences: vm.siteReferences,
                file: vm.file
            }, onSaveSuccessSite, onSaveError);

        };
        vm.clear = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.loadSites = function () {
            vm.sitesLoaded = false;
            vm.sites = [];
            Site.list(
                {studyId: vm.assignment.subject.studyId}
            ).$promise.then(function (sites) {
                vm.sites = sites;
                vm.sitesLoaded = true;
            });
        };
        vm.checkStudyStatus = function () {
            vm.isStudyActive = false;
            vm.study = null;
            Study.get({id: vm.assignment.subject.studyId}, function (study) {
                vm.study = study;
                if (vm.study.status === 'ACTIVE') {
                    vm.isStudyActive = true;
                } else {
                    vm.isStudyActive = false;
                }
            });
        };

        $scope.ngModelSubjectSelected = function (value) {
            if (arguments.length) {
                if (value && angular.isDefined(value.id)) {
                    vm.assignment.subject = value;
                    vm.checkStudyStatus();
                    vm.loadSites();
                    _selectedSubjectName = value.displayName;
                } else {
                    _selectedSubjectName = value;
                    vm.assignment.subject = {};
                    vm.sites = [];
                    vm.sitesLoaded = false;
                    vm.isStudyActive = true;
                }
            } else {
                return _selectedSubjectName;
            }
        };

        $scope.ngModelMonitorSelected = function (value) {
            if (arguments.length) {
                if (value && angular.isDefined(value.id)) {
                    vm.assignment.monitorId = value.id;
                    vm.assignment.monitorSerialNumber = value.serialNumber;
                    _selectedMonitorSerialNumber = value.serialNumber;
                } else {
                    vm.assignment.monitorId = null;
                    vm.assignment.monitorSerialNumber = null;
                    _selectedMonitorSerialNumber = value;
                }
            } else {
                return _selectedMonitorSerialNumber;
            }
        };

        $scope.modelOptions = {
            debounce: {
                default: 250,
                blur: 50
            },
            getterSetter: true
        };
    }

})();
