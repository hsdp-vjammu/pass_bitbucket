(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorAssignmentDeleteController', MonitorAssignmentDeleteController);

    MonitorAssignmentDeleteController.$inject = ['$uibModalInstance', 'entity', 'MonitorAssignment'];

    function MonitorAssignmentDeleteController($uibModalInstance, entity, MonitorAssignment) {
        var vm = this;
        vm.assignment = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            MonitorAssignment.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
