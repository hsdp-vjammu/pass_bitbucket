(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('FeedbackData', FeedbackData);
    FeedbackData.$inject = ['$resource'];
    function FeedbackData($resource) {
        var resourceUrl =  'api/feedback';
        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                transformResponse: function (data, headers, status) {
                    var response = {}
                    response.error = false;
                    try {
                        response.data = angular.fromJson(data)
                    } catch (e) {
                        response.data = []
                        response.error = true
                    }
                    response.headers = headers();
                    response.status = status;
                    return response;
                },
                isArray: false
            }
        });
    }
})();
