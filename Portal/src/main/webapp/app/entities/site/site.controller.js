(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('SiteController', SiteController);

    SiteController.$inject = ['$rootScope', '$scope', '$state', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Site', 'entity', '$uibModal'];

    function SiteController ($rootScope, $scope, $state, ParseLinks, AlertService, pagingParams, paginationConstants, Site, entity, $uibModal) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.study = null;
        vm.isSaving = false;

        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };

        $scope.searchModel = {};
        entity.$promise.then(function(study){
            vm.study = study;

            var unsubscribe = $scope.$watchCollection('searchModel', function (newVal, oldVal) {
                loadAll ();
            });
            $scope.$on('$destroy', unsubscribe);

        });


        function loadAll () {
            if (vm.study) {
                Site.query({
                    studyId: vm.study.id,
                    page: pagingParams.page - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort(),
                    search: search()
                }, onSuccess, onError);
            }

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                return angular.toJson(searchParams);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.sites = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')
            });
        }

        vm.toggleAdvancedSearch = function () {
            $scope.advancedSearchCollapsed = !$scope.advancedSearchCollapsed;
            if ($scope.advancedSearchCollapsed && angular.isDefined($scope.searchModel)) {
                $scope.searchModel = {'$': $scope.searchModel.$};
            }
        };

        vm.selectSite = function(site) {
            vm.navigateToSiteDetails(site.id);
        }

        vm.selectedSites = [];
        vm.toggleSiteSelection = function(site) {
            var checked = !site.selected;
            if (checked && vm.selectedSites.indexOf(site) === -1) {
                vm.selectedSites.push(site);
            } else if (!checked && vm.selectedSites.indexOf(site) !== -1) {
                vm.selectedSites.splice(vm.selectedSites.indexOf(site), 1);
            }
            site.selected = checked;
        }

        vm.delete = function () {
            $uibModal.open({
                templateUrl: 'app/entities/site/site-delete-confirm-dialog.html',
                controller: 'ConfirmDialogController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'md'
            }).result.then(function(choice) {
                if (choice === true) {
                    vm.isSaving = true;
                    var siteIDs = vm.selectedSites.map(function(site) {
                        return site.id;
                    });
                    Site.delete({
                        studyId: vm.study.id,
                        ids: siteIDs
                    }, onDeleteSuccess, onDeleteError);
                };
            }, function() {
                //no action
            });
        }

        var onDeleteSuccess = function (result) {
            vm.selectedSites = [];
            loadAll();
            vm.isSaving = false;
        };

        var onDeleteError = function () {
            vm.isSaving = false;
        };

        vm.navigateToSiteDetails = function (id) {
            $rootScope.returnState.study = $state.current.name;
            $state.go('site-detail', {id:id});
        };

    }

})();
