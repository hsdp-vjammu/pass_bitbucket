(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('SiteDetailController', SiteDetailController);

    SiteDetailController.$inject = ['$scope', '$state', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Site', 'entity'];

    function SiteDetailController($scope, $state, ParseLinks, AlertService, pagingParams, paginationConstants, Site, entity) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.subjects = [];

        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };

        $scope.searchModel = {};
        entity.$promise.then(function(site){
            vm.site = site;

            var unsubscribe = $scope.$watchCollection('searchModel', function (newVal, oldVal) {
                loadAll ();
            });
            $scope.$on('$destroy', unsubscribe);
        });

        function loadAll () {
            if (vm.site) {
                Site.querySubjects({
                    id: vm.site.id,
                    page: pagingParams.page - 1,
                    size: paginationConstants.itemsPerPage,
                    sort: sort(),
                    search: search()
                }, onSuccess, onError);
            }

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                return angular.toJson(searchParams);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.subjects = data;
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')
            });
        }

    }
})();
