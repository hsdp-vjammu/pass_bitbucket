(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('Site', Site);

    Site.$inject = ['$resource'];

    function Site ($resource) {
         var resourceUrl =  'api/sites/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'list': {
                url: 'api/sites/study/:studyId',
                method: 'GET',
                isArray: true
            },
            'querySubjects': {
                url: 'api/sites/:id/subjects',
                method: 'GET',
                isArray: true
            },
            'update': { method:'PUT' },
            'save': {
                method: 'POST',
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();

                    formData.append("data", angular.toJson({studyId: data.studyId, references: data.siteReferences}));
                    formData.append("file", data.file);

                    return formData;
                }
            }
        });
    }
})();
