(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('SiteDialogController', SiteDialogController);

    SiteDialogController.$inject = ['$timeout', '$stateParams', '$state', '$scope', '$http', '$uibModalInstance', 'Principal', 'Site', 'Study'];

    function SiteDialogController ($timeout, $stateParams, $state, $scope, $http, $uibModalInstance, Principal, Site, Study) {
        var vm = this;

        vm.study = null;
        Study.get( {id : $stateParams.studyId}, function( study ) {
            vm.study = study;
        });

        vm.siteReference = null;
        vm.siteReferences = [];
        vm.validationMessages = [];
        vm.file = null;

        vm.fileValidationError = {}

        $scope.$watch('vm.file', function() {
            vm.clearValidationMessages();
        });

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('passApp:siteUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function (error) {
            vm.fileValidationError.message = 'passApp.site.error.' + error.data.error;
            vm.fileValidationError.siteReferences = [];
            angular.forEach(error.data.siteReferences, function(value, key) {
                vm.fileValidationError.siteReferences.push({reference: key, error: 'passApp.site.validate.siteReference.' + value});
            });
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;

            Site.save({
                studyId: vm.study.id,
                siteReferences: vm.siteReferences,
                file: vm.file}, onSaveSuccess, onSaveError);

        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.clearValidationMessages = function() {
            vm.validationMessages = [];
        };

        vm.addSiteReference = function () {

            var siteReference = vm.siteReference;
            if (siteReference && vm.siteReferences.indexOf(siteReference) === -1) {
                vm.validateSiteReference(vm.study, siteReference);
            } else {
                vm.validationMessages = [];

                vm.validationMessages.push('passApp.site.validate.siteReference.DUPLICATE');
            }
        };

        vm.removeSiteReference = function (siteReference) {
            var index = vm.siteReferences.indexOf(siteReference);
            vm.siteReferences.splice(index, 1);
        };

        vm.validateSiteReference = function (study, siteReference) {
            vm.validationMessages = [];
            vm.isValidating = true;

            $http.get('/api/sites/validate/' + study.id + '/' + siteReference).then(function() {
                // Validation passed
                vm.siteReferences.push(siteReference);
                vm.siteReference = null;
                vm.isValidating = false;
            }, function(response) {
                // Validation failed
                //var messages = angular.fromJson(response.data);
                angular.forEach(response.data, function(message) {
                    vm.validationMessages.push('passApp.site.validate.siteReference.' + message);
                });
                vm.isValidating = false;
            });
        };

    }
})();
