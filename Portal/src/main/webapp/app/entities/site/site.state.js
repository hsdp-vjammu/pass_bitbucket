(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('site', {
            parent: 'setup',
            url: '/study/{studyId}/site?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.site.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site/sites.html',
                    controller: 'SiteController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'externalReference,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('site');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Study', function($stateParams, Study) {
                    return Study.get({id : $stateParams.studyId});
                }]
            }
        })
        .state('site-detail', {
            parent: 'setup',
            url: '/site/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.study.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/site/site-detail.html',
                    controller: 'SiteDetailController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'externalReference,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('site');
                    $translatePartialLoader.addPart('subject');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Site', function($stateParams, Site) {
                    return Site.get({id : $stateParams.id});
                }]
            }
        })
        .state('site.new', {
            parent: 'site',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/site/site-dialog.html',
                    controller: 'SiteDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg'
                }).result.then(function() {
                    $state.go('site', null, { reload: true });
                }, function() {
                    $state.go('site');
                });
            }]
        });
    }

})();
