(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorDeleteController',MonitorDeleteController);

    MonitorDeleteController.$inject = ['$state', '$stateParams', '$uibModalInstance', 'entity', 'Monitor'];

    function MonitorDeleteController($state, $stateParams, $uibModalInstance, entity, Monitor) {
        var vm = this;
        vm.monitor = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.confirmRemove = function (monitor) {
            var changedMonitor = angular.copy(vm.monitor);
            changedMonitor.status = "REMOVED";

            Monitor.update(changedMonitor,
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
