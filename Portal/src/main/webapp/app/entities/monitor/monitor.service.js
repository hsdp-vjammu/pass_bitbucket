(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('Monitor', Monitor);

    Monitor.$inject = ['$resource'];

    function Monitor ($resource) {
        var resourceUrl =  'api/monitors/:status:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'queryUnallocated': {
                method: 'GET',
                params: {'status': "unallocated"},
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'updateBulk': {
                url: 'api/monitors/changeStatus',
                method:'PUT'
            },
            'save': {
                method: 'POST',
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();

                    formData.append("data", angular.toJson({companyId: data.companyId, type: data.type, serialNumbers: data.serialNumbers}));
                    formData.append("file", data.file);

                    return formData;
                }
            }
        });
    }
})();
