(function () {
    'use strict';
    angular
        .module('passApp')
        .factory('MonitorHistory', MonitorHistory);

    MonitorHistory.$inject = ['$resource'];

    function MonitorHistory($resource) {
        var resourceUrl = 'api/monitors/history/:id';
        return $resource(resourceUrl, {}, {
            'query': {method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'getByAssignedSubject': {
                url: 'api/monitors/history/subject/:assignedSubject',
                method: 'GET',
                isArray: true
            },
            'getByAssignedSubjectAndDeviceSN': {
                url: 'api/monitors/history/subject/:assignedSubject/sn/:serialNumber',
                method: 'GET',
                isArray: true
            }
        });
    }
})();
