(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorDialogController', MonitorDialogController);

    MonitorDialogController.$inject = ['$timeout', '$scope', '$http', '$uibModalInstance', 'Principal', 'Monitor'];

    function MonitorDialogController ($timeout, $scope, $http, $uibModalInstance, Principal, Monitor) {
        var vm = this;

        vm.monitorType = "PHb";
        vm.serialNumber = null;
        vm.serialNumbers = [];
        vm.validationMessages = [];
        vm.fileTypeValidationMessages = [];
        vm.messageForUser = [];
        vm.file = null;
        vm.isSaving = true;

        vm.fileValidationError = {};

        $scope.$watch('vm.file', function() {
            vm.clearValidationMessages();
        });

        vm.companyId = Principal.currentCompany.id;

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function (error) {
            vm.fileValidationError.message = 'passApp.monitor.error.' + error.data.error;
            vm.messageForUser.message = 'passApp.monitor.error.messageForUser'

            vm.fileValidationError.serialNumbers = [];
            angular.forEach(error.data.serialNumbers, function(value, key) {
                if(value.length>1) {
                    vm.fileValidationError.serialNumbers.push({
                        serialNumber: key,
                        error: 'passApp.monitor.validate.serialNumber.INVALID_SERIALNUMBER'
                    });
                }
                else{
                    vm.fileValidationError.serialNumbers.push({
                        serialNumber: key,
                        error: 'passApp.monitor.validate.serialNumber.' + value
                    });
                }
            });

            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;

            Monitor.save({
                companyId: vm.companyId,
                type: vm.monitorType,
                serialNumbers: vm.serialNumbers,
                file: vm.file}, onSaveSuccess, onSaveError);

        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.clearValidationMessages = function() {
            vm.validationMessages = [];
           // vm.fileTypeValidationMessages = [];
        };

        vm.addSerialNumber = function () {

            var serialNumber = vm.serialNumber;
            if (serialNumber && vm.serialNumbers.indexOf(serialNumber) === -1) {
                vm.validateSerialNumber(vm.companyId, vm.monitorType, serialNumber);
            } else {
                vm.validationMessages = [];

                vm.validationMessages.push('passApp.monitor.validate.serialNumber.DUPLICATE');
            }
        };

        vm.removeSerialNumber = function (serialNumber) {
            var index = vm.serialNumbers.indexOf(serialNumber);
            vm.serialNumbers.splice(index, 1);
        };
        vm.fileTypeCheck= function(){
            vm.fileTypeValidationMessages = [];
            //vm.isValidating = true;
            var fileExtn = vm.file.name.substr(vm.file.name.lastIndexOf('.')+1).toLowerCase();
            if(fileExtn!=="txt"){
                vm.fileTypeValidationMessages.push('passApp.monitor.error.invalidFileType');
                vm.isSaving=false;
            }
            else {
                vm.isSaving=true;
            }

        };
        vm.validateSerialNumber = function (companyId, type, serialNumber) {
            vm.validationMessages = [];
            vm.isValidating = true;

            $http.get('/api/monitors/validate/' + companyId + '/' + type + '/' + serialNumber).then(function() {
                // Validation passed
                vm.serialNumbers.push(serialNumber);
                vm.serialNumber = null;
                vm.isValidating = false;
            }, function(response) {
                // Validation failed
                //var messages = angular.fromJson(response.data);
                angular.forEach(response.data, function(message) {
                    vm.validationMessages.push('passApp.monitor.validate.serialNumber.' + message);
                });
                vm.isValidating = false;
            });
        };

    }
})();
