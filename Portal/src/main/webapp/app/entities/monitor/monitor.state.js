(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('monitor', {
            parent: 'setup',
            url: '/monitor?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.monitor.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitor/monitors.html',
                    controller: 'MonitorController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'serialNumber,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitor');
                    $translatePartialLoader.addPart('monitorType');
                    $translatePartialLoader.addPart('global');
                    $translatePartialLoader.addPart('monitorStatus');
                    $translatePartialLoader.addPart('study');
                    $translatePartialLoader.addPart('studyStatus');
                    $translatePartialLoader.addPart('monitorAction');
                    return $translate.refresh();
                }]
            }
        })
        .state('monitor-detail', {
            parent: 'setup',
            url: '/monitor/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.monitor.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitor/monitor-detail.html',
                    controller: 'MonitorDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitor');
                    $translatePartialLoader.addPart('monitorType');
                    $translatePartialLoader.addPart('monitorStatus');
                    $translatePartialLoader.addPart('monitorStatusChangeAction');
                    $translatePartialLoader.addPart('study');
                    $translatePartialLoader.addPart('studyStatus');
                    $translatePartialLoader.addPart('monitorAction');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Monitor', function($stateParams, Monitor) {
                    return Monitor.get({id : $stateParams.id});
                }]
            }
        })
        .state('monitor.new', {
            parent: 'monitor',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitor/monitor-dialog.html',
                    controller: 'MonitorDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                serialNumber: null,
                                type: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('monitor', null, { reload: true });
                }, function() {
                    $state.go('monitor');
                });
            }]
        })
        .state('monitor-detail.delete', {
            parent: 'monitor-detail',
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitor/monitor-delete-dialog.html',
                    controller: 'MonitorDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Monitor', function(Monitor) {
                            return Monitor.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitor-detail', {id: $stateParams.id}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
