(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorDetailController', MonitorDetailController);

    MonitorDetailController.$inject = ['$scope', '$state', '$rootScope', 'entity', 'Monitor', 'MonitorHistory'];

    function MonitorDetailController($scope, $state, $rootScope, entity, Monitor, MonitorHistory) {
        var vm = this;
        vm.monitor = entity;

        entity.$promise.then(function(monitor){
            vm.monitor = monitor;

            vm.monitorHistory = MonitorHistory.query({id: monitor.id});
        });

        var unsubscribe = $rootScope.$on('passApp:monitorUpdate', function(event, result) {
            result.$promise.then(function(monitor){
                vm.monitor = monitor;

                vm.monitorHistory = MonitorHistory.query({id: monitor.id});
            });
        });

        $scope.$on('$destroy', unsubscribe);

        vm.changeStatus = function (status) {
            if (status === "REMOVED") {
                $state.go('monitor-detail.delete', {id: vm.monitor.id}, { location: 'replace' })
            } else {
                vm.isSaving = true;
                var changedMonitor = angular.copy(vm.monitor);
                changedMonitor.status = status;
                Monitor.update(changedMonitor, onSaveSuccess, onSaveError);
            }
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('passApp:monitorUpdate', result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.navigateBack = function () {
            if ($rootScope.returnState.monitor) {
                $state.go($rootScope.returnState.monitor);
            } else {
                $state.go('monitor');
            }
        };

    }
})();
