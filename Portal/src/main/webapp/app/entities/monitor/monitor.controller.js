(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorController', MonitorController);

    MonitorController.$inject = ['$rootScope', '$scope', '$state', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', '$uibModal', 'Principal', 'Monitor'];

    function MonitorController ($rootScope, $scope, $state, ParseLinks, AlertService, pagingParams, paginationConstants, $uibModal, Principal, Monitor)  {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.loadAll();

        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };

        $scope.searchModel = {};
        //Set default values
        $scope.searchModel.type = "PHb";
        $scope.searchModel.study_status = "ALL";

        $scope.$watchCollection('searchModel', function (newVal, oldVal) {
            loadAll ();
        });

        vm.toggleAdvancedSearch = function () {
            $scope.advancedSearchCollapsed = !$scope.advancedSearchCollapsed;
            if ($scope.advancedSearchCollapsed && angular.isDefined($scope.searchModel)) {
                $scope.searchModel = {'$': $scope.searchModel.$};
            }
        };

        function loadAll () {
            $scope.selectedStatus="Select";
            Monitor.query({
                cid: Principal.currentCompany.id,
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort(),
                search: search()
            }, onSuccess, onError);

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                return angular.toJson(searchParams);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.monitors = data;
                vm.page = pagingParams.page;
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        vm.allowUnassign = false;
        vm.unassign = unassign;
        function unassign () {
            if (vm.selectedMonitors && vm.selectedMonitors.length > 0) {
                var monitorStatus = vm.selectedMonitors[0].status.toUpperCase();
                if (monitorStatus === "ASSIGNED") {
                    $uibModal.open({
                        templateUrl: 'app/entities/monitor/monitor-unassign-confirm-dialog.html',
                        controller: 'ConfirmDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'md'
                    }).result.then(function(choice) {
                        if (choice === true) {
                            performStatusChange("UNASSIGNED");
                        };
                    }, function() {
                        //no action
                    });
               } else if (monitorStatus === "DISABLED") {
                    performStatusChange("UNASSIGNED");
                }
            }
        }

        vm.allowRemove = false;
        vm.remove = remove;
        function remove () {
            if (vm.selectedMonitors && vm.selectedMonitors.length > 0) {
                var monitorStatus = vm.selectedMonitors[0].status.toUpperCase();
                if (monitorStatus === "UNASSIGNED" || monitorStatus === "DISABLED") {
                    performStatusChange("REMOVED");
                }
            }
        }

        vm.allowDisable = false;
        vm.disable = disable;
        function disable () {
            if (vm.selectedMonitors && vm.selectedMonitors.length > 0) {
                var monitorStatus = vm.selectedMonitors[0].status.toUpperCase();
                if (monitorStatus === "UNASSIGNED") {
                    performStatusChange("DISABLED");
                }
            }
        }

        vm.allowRegister = false;
        vm.register = register;
        function register () {
            if (vm.selectedMonitors && vm.selectedMonitors.length > 0) {
                var monitorStatus = vm.selectedMonitors[0].status.toUpperCase();
                if (monitorStatus === "REMOVED") {
                    performStatusChange("UNASSIGNED");
                }
            }
        }
        function performStatusChange (status) {
            vm.isSaving = true;
            var monitorIDs = vm.selectedMonitors.map(function(monitor) {
                return monitor.id;
            });
            Monitor.updateBulk({
                companyId: Principal.currentCompany.id,
                status: status,
                ids: monitorIDs,
            }, onUpdateSuccess, onUpdateError);
        }

        var onUpdateSuccess = function (result) {
            vm.selectedMonitors = [];
            loadAll();
            vm.isSaving = false;
        };

        var onUpdateError = function () {
            vm.isSaving = false;
        };

        vm.errorMultipleStatusses = false;

        vm.selectedMonitors = [];
        vm.toggleMonitorSelection = function(monitor) {
            var checked = !monitor.selected;
            if (checked && vm.selectedMonitors.indexOf(monitor) === -1) {
                vm.selectedMonitors.push(monitor);
            } else if (!checked && vm.selectedMonitors.indexOf(monitor) !== -1) {
                vm.selectedMonitors.splice(vm.selectedMonitors.indexOf(monitor), 1);
            }
            monitor.selected = checked;
            vm.updateButtonStatus();
        };

        vm.updateButtonStatus = function() {
            vm.allowUnassign = false;
            vm.allowRemove = false;
            vm.allowDisable = false;
            vm.allowRegister= false;

            vm.errorMultipleStatusses = false;

            if (vm.selectedMonitors && vm.selectedMonitors.length > 0) {
                var monitorsByStatus = vm.selectedMonitors.reduce(function(collection, item) {
                    if (collection.indexOf(item.status) === -1) {
                        collection.push(item.status);
                    }
                    return collection;
                }, []);

                if (monitorsByStatus.length === 1) {
                    var monitorStatus = monitorsByStatus[0].toUpperCase();

                    vm.allowUnassign = monitorStatus === "ASSIGNED" || monitorStatus === "DISABLED";
                    vm.allowRemove = monitorStatus === "UNASSIGNED" || monitorStatus === "DISABLED";
                    vm.allowDisable = monitorStatus === "UNASSIGNED";
                    vm.allowRegister = monitorStatus == "REMOVED";
                } else {
                    vm.errorMultipleStatusses = true;
                }

            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

        vm.navigateToStudyDetails = function (id) {
            $rootScope.returnState.study = $state.current.name;
            $state.go('study-detail', {id:id});
        };

        vm.navigateToSubjectDetails = function (id) {
            $rootScope.returnState.subject = $state.current.name;
            $state.go('subject-detail.analysis', {id:id});
        };

        vm.navigateToMonitorDetails = function (id) {
            $rootScope.returnState.monitor = $state.current.name;
            $state.go('monitor-detail', {id:id});
        };

    }

})();
