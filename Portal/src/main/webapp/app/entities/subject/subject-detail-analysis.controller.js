(function () {
    'use strict';
    angular
        .module('passApp')
        .controller('SubjectAnalysisController', SubjectAnalysisController);
    SubjectAnalysisController.$inject = ['$scope', '$http', 'entity', 'Analysis', 'TDRData', 'Monitor', 'Principal', 'AlertService', 'MonitorHistory', '_', 'subjectJsonData', '$uibModal',
        '$rootScope', '$filter', 'moment', 'PagerService', '$state', 'FeedbackData'];

    function SubjectAnalysisController($scope, $http, entity, Analysis, TDRData, Monitor, Principal, AlertService, MonitorHistory, _, subjectJsonData,
                                       $uibModal, $rootScope, $filter, moment, PagerService, $state, feedbackData) {
        var vm = this;
        vm.subject = entity;
        vm.dataLoadedCheck = false;
        vm.subjectData = subjectJsonData.data.passApp.subject;
        vm.predicate = "serialNumber";
        vm.loadMonitors = loadMonitors;
        vm.analysis = {};
        vm.datePickerOpenStatus = {};
        vm.totalItems = {};
        vm.currentPage = {};
        vm.timeSeriesData = [];
        vm.currentPage.Series = 1;
        vm.totalItems.TimeSeries = 0;
        vm.errorMessage = "";
        vm.successMessage = "";
        vm.date = new Date();
        vm.reportType = "DATA_LIST";
        vm.loadMonitors();
        $scope.data = [];

        //pagination
        vm.pager = {};
        vm.dataCount = 0;
        vm.setPage = setPage;
        vm.setFeedbackPage = setFeedbackPage;

        function loadMonitors() {
            $rootScope.$on('monitorSubjectData', function (event, response) {
                vm.selectedMonitor = response.selectedMonitor;
                vm.monitors = response.monitors;
                vm.subject = response.subject;
                //preset values
                vm.timeZone = vm.subject.timeZone;
                vm.assignedSubject = vm.subject.id;
                vm.subjectReference = vm.subject.externalReference;
            });
        }

        vm.deviceSerialNumberChange = function (selMonitor) {
            vm.monitorHistoryData = MonitorHistory.getByAssignedSubjectAndDeviceSN({
                assignedSubject: vm.assignedSubject,
                serialNumber: selMonitor
            });
            vm.monitorHistoryData.$promise.then(function (response) {
                if (response.length > 0) {
                    vm.subjectConfigStatus = true; //configuration already modified, no edits are allowed
                } else {
                    vm.subjectConfigStatus = false; //not modified before, so allow EDIT for identity
                }
            }, function (error) {
                $scope.error = error.status + " - " + error.data.message;
                vm.errorMessage = "No Monitor History records found for given Subject and serial number  - " + $scope.error;
            });
        };

        vm.identityStateChange = function () {
            $state.go('subject-edit.identity', {id: vm.subject.id}); //   $state.go('study-detail', {id:id});
        };


        vm.set_offWristColor = function (timeData) {
            if (timeData.wristStatus == 0) {
                return {color: "red"};
            }
        };
        var now = new Date();
        now.setDate(now.getDate() - 30);
        vm.startDate = now;
        vm.endDate = vm.date;
        vm.startDatePicker = {
            format: 'MM/dd/yyyy',
            dateOptions: {
                //datepickerMode: 'month',
                maxDate: new Date(),
                showWeeks: false,
                showButtonBar: true,
                startingDay: 1
            }
        };
        vm.endDatePicker = {
            format: 'MM/dd/yyyy',
            dateOptions: {
                //datepickerMode: 'month',
                maxDate: new Date(),
                showWeeks: false,
                showButtonBar: true,
                startingDay: 1
            }
        };

        vm.validateEndDate = function validateEndDate(startDate, endDate) {
            vm.errorMessage = "";
            if (vm.formatDate(endDate) < vm.formatDate(startDate)) {
                vm.errorMessage = 'End Date should not be less than start date.';
                //AlertService.error(vm.errorMessage);
                return false;
            }
        };

        vm.formatDate = function (date) {
            return Analysis.formattedDate(date);
        };

        vm.formatDateTime = function (date) {
            date = date.replace("+0000", "");
            date = date.trim();
            return Analysis.formattedDateTime(date);
        };
        vm.openStartDateCalendar = function () {
            vm.startDatePickerOpenStatus = true;
        };
        vm.openEndDateCalendar = function () {
            vm.endDatePickerOpenStatus = true;
        };

        $scope.setPage = function (pageNo) {
            vm.currentPage.Series = pageNo;
        };

        $scope.pageChanged = function () {
            console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.setItemsPerPage = function (num) {
            $scope.itemsPerPage = num;
            vm.currentPage.Step = 1;
            vm.currentPage.Activity = 1;
            vm.currentPage.Heart = 1;
            vm.currentPage.Respiration = 1;
            vm.currentPage.ActivitySession = 1;
            vm.currentPage.Questions = 1;
            vm.currentPage.Series = 1;
        };


        var offWrist_Rules = {
            'off-wrist': function (params) {
                return params.data.wristStatus === '1';
            }
        };

        var run_Rules = {
            'run': function (params) {
                return params.data.runStatus === '0';
            }
        };

        var walk_Rules = {
            'walk': function (params) {
                return params.data.walkStatus === '1';
            }
        };

        var cycle_Rules = {
            'cycle': function (params) {
                return params.data.cycleStatus === '1';
            }
        };
        var sleepIntent_Rules = {
            'sleep-intent': function (params) {
                return params.data.sleepIntent === '1';
            }
        };
        var activeMins_Rules = {
            'active-mins': function (params) {
                return parseInt(params.data.activeMinutes) > 0;
            }
        };

        var datalistColumnDefs = [
            {
                headerName: vm.subjectData.dataListHeaders.timeStamp,
                field: "timeStamp",
                lockPosition: true,
                pinned: true,
                width: 175,
                suppressResize: true
            },
            {
                headerName: vm.subjectData.dataListHeaders.activityCounts,
                field: "activityCounts",
                width: 140
            },
            {
                headerName: vm.subjectData.dataListHeaders.heartRateBPM,
                field: "heartRate",
                width: 150,
                valueFormatter: function (params) {
                    return (params.value === "0" ? params.value = "NaN" : params.value);
                },
                headerTooltip: vm.subjectData.tooltips.heartRateBPM
            },
            {
                headerName: vm.subjectData.dataListHeaders.respirationRate,
                field: "respirationRate",
                width: 150,
                valueFormatter: function (params) {
                    return (params.value === "0" ? params.value = "NaN" : params.value);
                },
                headerTooltip: vm.subjectData.tooltips.respirationRate
            },
            {
                headerName: vm.subjectData.dataListHeaders.totalEnergyExpenditure,
                width: 120,
                field: "totalEnergyExpenditure"
            },
            {
                headerName: vm.subjectData.dataListHeaders.energyExpenditure,
                width: 160,
                field: "activeEnergyExpenditure"
                /*valueFormatter: function(params){
                    return (params.value === "0.0" ? params.value : parseFloat(params.value).toFixed(4));
                }*/
            },
            {
                headerName: vm.subjectData.dataListHeaders.steps,
                field: "steps",
                width: 80
            },
            {
                headerName: vm.subjectData.dataListHeaders.notWorn,
                field: "offWrist",
                width: 120,
                cellClassRules: offWrist_Rules,
                headerTooltip: vm.subjectData.tooltips.notWorn
            },
            {
                headerName: vm.subjectData.dataListHeaders.run,
                field: "run",
                width: 80,
                cellClassRules: run_Rules,
                headerTooltip: vm.subjectData.tooltips.run
            },
            {
                headerName: vm.subjectData.dataListHeaders.walk,
                width: 80,
                field: "walk",
                cellClassRules: walk_Rules,
                headerTooltip: vm.subjectData.tooltips.walk
            },
            {
                headerName: vm.subjectData.dataListHeaders.cycle,
                width: 80,
                field: "cycle",
                cellClassRules: cycle_Rules,
                headerTooltip: vm.subjectData.tooltips.cycle
            },
            {
                headerName: vm.subjectData.dataListHeaders.activeMinutes,
                width: 140,
                field: "activeMinutes",
                cellClassRules: activeMins_Rules,
                headerTooltip: vm.subjectData.tooltips.activeMinutes
            },
            {
                headerName: vm.subjectData.dataListHeaders.restingHeartRate,
                width: 160,
                field: "restingHeartRate",
                valueFormatter: function (params) {
                    return (params.value === "0" ? params.value = "NaN" : params.value);
                },
                headerTooltip: vm.subjectData.tooltips.restingHeartRate
            },
            {
                headerName: vm.subjectData.dataListHeaders.recoveryHeartRate,
                width: 170,
                field: "recoveryHeartRate",
                valueFormatter: function (params) {
                    return (params.value === "0" ? params.value = "NaN" : params.value);
                },
                headerTooltip: vm.subjectData.tooltips.recoveryHeartRate
            },
            {
                headerName: vm.subjectData.dataListHeaders.cardioFitnessIndex,
                width: 170,
                field: "cardioFitnessIndex",
                valueFormatter: function (params) {
                    return (params.value === "0" ? params.value = "NaN" : params.value);
                },
                headerTooltip: vm.subjectData.tooltips.cardioFitnessIndex
            },
            {
                headerName: vm.subjectData.dataListHeaders.vo2Max,
                width: 100,
                field: "vo2Max",
                valueFormatter: function (params) {
                    return (params.value === "0" ? params.value = "NaN" : params.value);
                },
                headerTooltip: vm.subjectData.tooltips.vo2Max
            }
        ];

        $scope.gridOptions = {
            columnDefs: datalistColumnDefs,
            rowData: [],

            suppressPaginationPanel: true,
            pagination: true,
            paginationPageSize: 60,

            enableColResize: true,
            suppressColumnMoveAnimation: true,
            suppressAutoSize: true,
            //pinnedTopRowData: true
            /*rowDragManaged: true*/ //works only if no pagination
            //angularCompileRows: true,
            //enableFilter: true,
            //enableSorting: true,
            rowSelection: 'multiple'
            //suppressPaginationPanel: true
        };

        function disableExportButton() {
            vm.successMessage = "";
            vm.errorMessage = "";
            vm.pager.pages = 0;
            $scope.gridOptions.api.setRowData(null);
            vm.dataLoadedCheck = false;  //dates refreshed for data load, disable export button
        }

        $scope.$watch('vm.startDate', function () {
            disableExportButton();
        });

        $scope.$watch('vm.endDate', function () {
            disableExportButton();
        });

        $scope.$watch('vm.selectedMonitor', function () {
            vm.deviceSN = vm.selectedMonitor;
            disableExportButton();
        });

        //generate file name
        function getExportFilePathAndName() {
            //get last FIVE char's of Subject ID
            vm.subjectId = vm.subject.externalReference.length > 5 ? vm.subject.externalReference.substring(vm.subject.externalReference.length - 5) : vm.subject.externalReference;
            vm.fileName = "DataList_" + vm.subjectId + "_" + Analysis.formattedDateTimeForExport(new Date());
            //remove : from time stamp
            vm.fileName = _.replace(vm.fileName, new RegExp(":", "g"), ""); //replace : with blank
            vm.fileName = _.replace(vm.fileName, new RegExp("-", "g"), "_"); //replace -
            vm.fileName = _.replace(vm.fileName, new RegExp(" ", "g"), "");  //remove space
            //vm.fileName = vm.filePath + vm.fileName + ".csv";
            vm.fileName = vm.fileName + ".xlsx";
        }

        //export Excel - process user input dialog box
        vm.exportFile = function () {
            vm.errorMsg = "";
            vm.successMessage = "";
            $uibModal.open({
                templateUrl: 'app/entities/subject/subject-export.html',
                controller: 'SubjectExportController',
                controllerAs: 'vm',
                backdrop: 'static',
                size: 'lg'
            }).result.then(function (choice) {
                if (choice === true) {
                    $rootScope.$on("ExportDialogData", function (evt, data) {
                        vm.checkForPwd = data.checkForPwd;
                        vm.password = data.password;
                        vm.repeatPwd = data.repeatPwd;
                    });
                    //if rootScope.$on does not work use these values
                    if (vm.checkForPwd === undefined) {
                        vm.exportDialogData = $rootScope.exportDialogData;
                        vm.checkForPwd = vm.exportDialogData.checkForPwd;
                        vm.password = vm.exportDialogData.password;
                        vm.repeatPwd = vm.exportDialogData.repeatPwd;
                    }
                    vm.excelFileDownloadProcess();
                }
            }, function () {
                console.log("Please load date, before export process...");
            });
        };

        //Excel file download function
        vm.excelFileDownloadProcess = function () {
            vm.errorMsg = "";
            vm.successMessage = "";
            if (vm.dataLoadedCheck) {
                //disable export button to prevent double click
                vm.dataLoadedCheck = false;
                //Data needed for export process
                getExportFilePathAndName();
                //data yet to be populated for these fields
                vm.healthBandFirmwareVer = "";
                vm.mobileAppSWVer = "";
                vm.paswebAppSWVer = $rootScope.VERSION;

                vm.studyName = vm.subject.studyName;
                vm.configurationName = vm.subject.assignedMonitors[0].studyName;

                vm.zipData = {
                    serialNo: vm.sMonitor,
                    subjectUUID: vm.subjectUUID,
                    requestTimeStampStart: vm.sDate,
                    requestTimeStampEnd: vm.eDate,
                    fileName: vm.fileName,
                    checkForPwd: vm.checkForPwd,
                    password: vm.password,
                    repeatPwd: vm.repeatPwd,
                    healthBandFirmwareVer: vm.healthBandFirmwareVer,
                    mobileAppSWVer: vm.mobileAppSWVer,
                    paswebAppSWVer: vm.paswebAppSWVer,
                    studyName: vm.studyName,
                    timeZone: vm.timeZone,
                    configurationName: vm.configurationName
                };
                //TDRData.exportToExcel(vm.zipData, onSuccess, onError);
                $http({
                    method: 'POST',
                    url: "api/file-download",
                    data: vm.zipData,
                    responseType: 'arraybuffer',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function (data, status, headers, config) {
                    // when WS success
                    var file = new Blob([data], {
                        type: 'application/json'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = vm.fileName;
                    document.body.appendChild(a);

                    if (document.createEvent) {
                        if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) { // IE
                            //blobObject = new Blob(['\ufeff'+value]);
                            window.navigator.msSaveBlob(file, vm.fileName);
                        } else { // FF, Chrome
                            var event = document.createEvent('MouseEvents');
                            event.initEvent('click', true, true);
                            a.dispatchEvent(event);
                        }
                    } else if (document.createEventObject) { // Have No Idea
                        var evObj = document.createEventObject();
                        a.fireEvent('onclick', evObj);
                    } else { // For Any Case
                        a.click();
                    }
                    //a.click();  //works in chrome
                    vm.successMessage = "Successfully exported excel file - " + vm.fileName;
                    vm.dataLoadedCheck = true;

                }).error(function (data, status, headers, config) {
                    vm.successMessage = "Unable to export excel file - " + vm.fileName + data;
                });
            } else {
                vm.errorMsg = "Please process date range specified above to export. ";
                AlertService.warning(vm.errorMsg);
                vm.errorMessage = vm.errorMsg;
            }
        };


        //generate csv file name
        function getCSVExportFilePathAndName() {
            //get last FIVE char's of Subject ID
            vm.subjectId = vm.subject.externalReference.length > 5 ? vm.subject.externalReference.substring(vm.subject.externalReference.length - 5) : vm.subject.externalReference;
            vm.fileName = "DataList_" + vm.subjectId + "_" + Analysis.formattedDateTimeForExport(new Date());
            //remove : from time stamp
            vm.fileName = _.replace(vm.fileName, new RegExp(":", "g"), ""); //replace : with blank
            vm.fileName = _.replace(vm.fileName, new RegExp("-", "g"), "_"); //replace -
            vm.fileName = _.replace(vm.fileName, new RegExp(" ", "g"), "");  //remove space
            //vm.fileName = vm.filePath + vm.fileName + ".csv";
            vm.csvFileName = vm.fileName + ".csv";
        }

        //export functionality using agGrid for csv
        vm.exportToCsv = function () {
            vm.errorMsg = "";
            vm.successMessage = "";
            //get ConfigurationName and DeviceSN, when no assignment
            if (vm.subject.studyName != "") {
                vm.configurationName = vm.subject.studyName;
            } else {
                vm.configurationName = "";
            }

            if (vm.selectedMonitor != "") {
                vm.deviceSN = vm.selectedMonitor; //set to device SN selected on screen
            } else {
                vm.deviceSN = "";
            }

            if (vm.dataLoadedCheck) {
                getCSVExportFilePathAndName();
                var headerStr = "";
                vm.AnalysisDateRange = Analysis.formattedDate(new Date(vm.startDate)) + " - " + Analysis.formattedDate(new Date(vm.endDate));
                headerStr =
                    //"Subject ID:                " + vm.subject.externalReference + "\n" +
                    "Subject UUID:                " + vm.subject.uniqueIdentifier + "\n" +
                    "Analysis range:            " + vm.AnalysisDateRange + "\n" +
                    "Health band firmware version : " + "" + "\n" +
                    "Mobile application version :   " + "" + "\n" +
                    "PAS Web application version :  " + $rootScope.VERSION + "\n" +
                    "Study Name:                " + vm.subject.studyName + "\n" +
                    "Configuration name:        " + vm.configurationName + "\n" +
                    "Device SN:                 " + vm.deviceSN + "\n";
                /*var footerStr = "";
                footerStr = "\n" + "FOOTER INFORMATION: " + "\n" + " Data to be displayed on FOOTER...."*/
                var params = {
                    skipHeader: false,
                    skipFooters: true,
                    skipGroups: true,
                    customHeader: headerStr,
                    //customFooter: footerStr,
                    fileName: vm.fileName
                };
                //process CSV data and export file
                processDataForCSVExport(params);
            }
        };

        function processDataForCSVExport(params) {
            //requestBody data
            vm.requestData = {
                serialNo: vm.sMonitor,
                subjectUUID: vm.subjectUUID,
                requestTimeStampStart: vm.sDate,
                requestTimeStampEnd: vm.eDate,
                fileName: vm.fileName,
                healthBandFirmwareVer: vm.healthBandFirmwareVer,
                mobileAppSWVer: vm.mobileAppSWVer,
                paswebAppSWVer: vm.paswebAppSWVer,
                studyName: vm.studyName,
                timeZone: vm.timeZone,
                configurationName: vm.configurationName
            };

            $http({
                method: 'POST',
                url: "api/processCSVData",
                data: vm.requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (data) {
                $scope.data = data;
                vm.timeSeriesData = angular.fromJson($scope.data);
                $scope.gridOptions.api.setRowData(vm.timeSeriesData);
                vm.successMessage = "Successfully processed data for CSV file - " + vm.fileName;
                $scope.gridOptions.api.exportDataAsCsv(params);
                vm.dataLoadedCheck = true;
            }).error(function (data) {
                vm.successMessage = "Unable to process data for CSV Export file - " + vm.fileName + data;
            });
        }

        vm.getDataForDatalistPageDisplay = function (startDate, endDate, selectedMonitor, page) {
            $scope.data = null;
            vm.totalItems = {};
            vm.currentPage = {};
            vm.timeSeriesData = [];
            vm.sMonitor = selectedMonitor;
            vm.sDate = vm.formatDate(startDate); // vm.calculateDates(vm.formatDate(startDate), false);
            vm.eDate = vm.formatDate(endDate);// vm.calculateDates(vm.formatDate(endDate), true);

            //for Export to Excel format
            vm.excelStartDate = vm.formatDate(startDate);
            vm.excelEndDate = vm.formatDate(endDate);
            vm.page = page;
            //extract subjectId from parent
            if (vm.subject != null) {
                vm.subjectUUID = vm.subject.uniqueIdentifier;
            }
            vm.setPage(1, "DATA_LIST");
        };

        function setPage(page, reportType) {
            if (reportType === "DATA_LIST") {
                if (page < 1 || page > vm.pager.totalPages) {
                    return;
                }
                if (page >= 1) {
                    TDRData.query({
                            serialNo: vm.selectedMonitor,
                            subjectUUID: vm.subjectUUID,
                            requestTimeStampStart: vm.sDate,
                            requestTimeStampEnd: vm.eDate,
                            timeZone: vm.timeZone,
                            page: page - 1
                        }, function onSuccess(data) {
                            $scope.data = data.data;
                            vm.timeSeriesData = angular.fromJson($scope.data);

                            $scope.gridOptions.api.setRowData(vm.timeSeriesData);
                            if ($scope.data.length === 0 && data.error === true) {
                                vm.errorMessage = "Page loading json error.";
                            } else if ($scope.data.length === 0) {
                                vm.errorMessage = "No data for given dates.";
                                vm.dataLoadedCheck = false;
                            } else {
                                vm.dataLoadedCheck = true;  //verify data loaded before export
                                // get current page of items
                                vm.items = vm.timeSeriesData.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
                                vm.dataCount = page * 60;
                                vm.dataListTotal = data.headers.totalelements;
                                //display pagination controls
                                vm.pager = PagerService.GetPager(vm.dataListTotal, page);
                            }

                        }
                    ), function (data) {  //error criteria
                        if (data != null) {
                            $scope.error = data.status + " - " + data.data.message;
                            vm.errorMessage = "Unable to load data from TDR - " + $scope.error;
                        } else {
                            vm.errorMessage = "Unable to load data from TDR.";
                        }
                    };

                }
            } else { //call feedback
                setFeedbackPage(page);
            }
        }

        //get data for Feedback report
        var feedbackColumnDefs = [
            /*{
                headerName: vm.subjectData.feedbackHeaders.timeStamp,
                field: "timesStamp",
                lockPosition: true,
                pinned: true,
                width: 200,
                suppressResize: true
            },*/
            {
                headerName: vm.subjectData.feedbackHeaders.scheduledQuestion,
                field: "question",
                width: 450
            },
            {
                headerName: vm.subjectData.feedbackHeaders.feedback,
                field: "feedback",
                width: 250,
                valueFormatter: function (params) {
                    return (params.value === "" ? params.value = "NO RESPONSE" : params.value);
                }
                //, headerTooltip: vm.subjectData.tooltips.respirationRate*
            },
            {
                headerName: vm.subjectData.feedbackHeaders.feedbackTime,
                field: "feedbackTimestamp",
                width: 250
            },
            {
                headerName: vm.subjectData.feedbackHeaders.notificationTime,
                field: "notificationFireTimeStamp",
                width: 250
            }
        ];

        vm.getFeedbackData = function (startDate, endDate, selectedMonitor, page) {
            $scope.data = null;
            vm.totalItems = {};
            vm.currentPage = {};
            vm.timeSeriesData = [];
            vm.sMonitor = selectedMonitor;
            vm.sDate = vm.formatDate(startDate); // vm.calculateDates(vm.formatDate(startDate), false);
            vm.eDate = vm.formatDate(endDate);// vm.calculateDates(vm.formatDate(endDate), true);

            //for Export to Excel format
            vm.excelStartDate = vm.formatDate(startDate);
            vm.excelEndDate = vm.formatDate(endDate);
            vm.page = page;
            //extract subjectId from parent
            if (vm.subject != null) {
                vm.subjectUUID = vm.subject.uniqueIdentifier;
            }
            vm.setFeedbackPage(1);
        };

        function setFeedbackPage(page) {
            if (page < 1 || page > vm.pager.totalPages) {
                return;
            }
            if (page >= 1) {
                feedbackData.query({
                        serialNo: vm.selectedMonitor, //'00-7417-2200-0000-03', //
                        subjectId: vm.subject.uniqueIdentifier, //'5fdaa1a4-4f53-42b8-899c-77f41c21fa05', //
                        feedbackTimeStampStart: vm.sDate,
                        feedbackTimeStampEnd: vm.eDate,
                        timeZone: vm.timeZone,
                        page: page - 1
                    }, function onSuccess(data) {
                        $scope.data = data.data;
                        vm.timeSeriesData = angular.fromJson($scope.data);

                        $scope.gridOptions.api.setRowData(vm.timeSeriesData);
                        if ($scope.data.length === 0 && data.error === true) {
                            vm.errorMessage = "Page loading json error.";
                        } else if ($scope.data.length === 0) {
                            vm.errorMessage = "No data for given dates.";
                            vm.dataLoadedCheck = false;
                        } else {
                            vm.dataLoadedCheck = true;  //verify data loaded before export
                            // get current page of items
                            vm.items = vm.timeSeriesData.slice(vm.pager.startIndex, vm.pager.endIndex + 1);
                            vm.dataCount = page * 60;
                            vm.dataListTotal = data.headers.totalelements;
                            //display pagination controls
                            vm.pager = PagerService.GetPager(vm.dataListTotal, page);
                        }

                    }
                ), function (data) {  //error criteria
                    if (data != null) {
                        $scope.error = data.status + " - " + data.data.message;
                        vm.errorMessage = "Unable to load data from Monitor Configuration - " + $scope.error;
                    } else {
                        vm.errorMessage = "Unable to load data from Monitor Configuration.";
                    }
                };
            }
        }

        vm.reportChange = function (reportType) {
            //switch view
            disableExportButton();
            if (reportType === "DATA_LIST") {
                vm.errorMessage = "";
                vm.page = 0;  //calling first page first time
                vm.dataListTotal = 0;
                vm.reportType = "DATA_LIST";
                $scope.gridOptions.api.setColumnDefs(datalistColumnDefs);
                $scope.gridOptions.api.setRowData(null);
            } else if (reportType === "FEEDBACK") {
                vm.errorMessage = "";
                $scope.data = null;
                vm.reportType = "FEEDBACK";
                vm.totalItems = {};
                vm.currentPage = {};
                vm.timeSeriesData = [];
                vm.dataLoadedCheck = false;
                vm.page = 0;  //calling first page first time
                vm.dataListTotal = 0;
                $scope.gridOptions.api.setColumnDefs(feedbackColumnDefs);
                $scope.gridOptions.api.setRowData(null);
            }
        };

        vm.processDataList = function (startDate, endDate, reportType, selectedMonitor) {
            disableExportButton();
            if (selectedMonitor === undefined) {
                vm.errorMessage = "Device serial number required for search....";
            } else if (reportType === "DATA_LIST") {
                vm.getDataForDatalistPageDisplay(startDate, endDate, selectedMonitor, vm.page);
                return true;
            } else if (reportType === "FEEDBACK") {
                vm.getFeedbackData(startDate, endDate, selectedMonitor, vm.page);
                return true;
            } else {
                vm.errorMessage = "Under Construction....";
                $scope.data = null;
                vm.totalItems = {};
                vm.currentPage = {};
                vm.timeSeriesData = [];
                $scope.gridOptions.api.setRowData(vm.timeSeriesData);
                vm.dataLoadedCheck = false;  //No Data load for this Analysis type
                vm.currentPage.Series = 1;
                vm.totalItems.TimeSeries = 0;
                return true;
            }
        };
    }
})();
