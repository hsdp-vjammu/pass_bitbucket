(function () {
    'use strict';

    angular
        .module('passApp')
        .controller('SubjectDialogController', SubjectDialogController);

    SubjectDialogController.$inject = ['$state', '$timeout', '$rootScope', '$scope', 'Principal', '$uibModalInstance', '$translate', 'entity', 'Subject', 'Study', 'Monitor', 'Site'];

    function SubjectDialogController($state, $timeout, $rootScope, $scope, Principal, $uibModalInstance, $translate, entity, Subject, Study, Monitor, Site) {
        var vm = this;

        var stateChangeListener = $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (toState.data) {
                    vm.currentStepIndex = toState.data.currentStepIndex;
                }
            }
        );
        $scope.$on('$destroy', stateChangeListener);

        vm.studies = [];
        vm.sites = [];
        vm.sitesLoaded = false;
        vm.monitors = [];
        vm.siteReferences = [];
        vm.file = null;
        //vm.study = null;

        vm.studiesToDisplay = [];

        //display site coordinator related studies
        //$rootScope.loginUserRole = vm.userRoles;
        angular.forEach($rootScope.loginUserStudies, function(studyInfo){
            vm.studiesToDisplay.push(Object.assign(studyInfo));
        });

        if (angular.isDefined(entity.$promise)) {
            entity.$promise.then(function (subject) {
                vm.subject = subject;
                vm.monitors = vm.monitors.concat(subject.assignedMonitors);
                vm.monitors.sort(arraySort("serialNumber"));
                if (subject.studyId) {
                    vm.loadSites();
                }
            });
        } else {
            vm.subject = entity;
            Principal.identity().then(function (account) {
                angular.forEach(account.memberships, function (membership) {
                    if (membership.company.id === Principal.currentCompany.id) {
                        angular.forEach(membership.accessLevels, function (accessLevel) {
                            if (accessLevel.accessLevel === "ACL_SITECOORD") {
                                vm.subject.siteReference = accessLevel.siteReference;
                                vm.subject.studyId = accessLevel.studyId;
                                vm.subject.studyName = accessLevel.studyName;
                            }
                        })
                    }
                });
            });
        }

        vm.countries = $rootScope.countries;

        vm.studies = Study.list({cid: Principal.currentCompany.id});
        Monitor.queryUnallocated(
            {cid: Principal.currentCompany.id}
        ).$promise.then(function (monitors) {
            vm.monitors = vm.monitors.concat(monitors);
            vm.monitors.sort(arraySort("serialNumber"));
        });

        vm.dateOfBirthPicker = {
            format: 'dd/MM/yyyy',
            dateOptions: {
                datepickerMode: 'year',
                maxDate: new Date(),
                showWeeks: false,
                startingDay: 1
            }
        };


        $timeout(function () {
            angular.element('.form-group:eq(1)>input').focus();
        });
        var onSaveSuccessSite = function (result) {
            $scope.$emit('passApp:siteUpdate', result);
            //vm.loadSites();
            if (vm.subject.id == null) {
                if (vm.isSaveNew == true) {
                    Subject.save(vm.subject, onSaveAndNewSuccess, onSaveError);
                    vm.isSaveNew = false;
                }
                else {
                    Subject.save(vm.subject, onSaveSuccess, onSaveError);
                }
            }
            else {
                if (vm.isSaveNew == true) {
                    Subject.update(vm.subject, onSubjectUpdateSaveAndNewSuccess, onSaveError);
                    vm.isSaveNew = false;
                }
                else {
                    Subject.update(vm.subject, onSubjectUpdateSaveSuccess, onSaveError);
                }
            }
            vm.isSaving = false;
        };

        var updateMonitorForSmartConfig = function (subject) {
            //if no assigned monitors nothing to update
            if (subject.assignedMonitors.length != 0) {
                var changedMonitor = angular.copy(subject.assignedMonitors[0]);
                changedMonitor.assignment_status = 1;
                Monitor.update(changedMonitor, onMonitorSaveSuccess, onMonitorSaveError);
            }
        }

        var onMonitorSaveSuccess = function (result) {
            $scope.$emit('passApp:monitorUpdate', result);
            vm.isSaving = false;
        };

        var onMonitorSaveError = function () {
            vm.isSaving = false;
        };


        var processSuccess = function (result) {
            $scope.$emit('passApp:subjectUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        var processNewSaveSuccess = function (result) {
            $scope.$emit('passApp:subjectUpdate', result);
            vm.isSaving = false;
            // Reset entity properties
            for (var property in vm.subject) {
                if (property != "studyId") {
                    vm.subject[property] = null;
                }
            }
            // Set entity defaults
            vm.subject.heightUnit = 'CM';
            vm.subject.weightUnit = 'KG';
            vm.subject.wearingPosition = 'LEFT';
            vm.subject.dominantHand = 'RIGHT';
            $state.go("subject.new.identity");
        }

        var onSubjectUpdateSaveSuccess = function (result) {
            //update  Monitor assignment_status for smart config
            updateMonitorForSmartConfig(vm.subject);
            processSuccess(result);
        };

        var onSubjectUpdateSaveAndNewSuccess = function (result) {
            //update  Monitor assignment_status for smart config
            updateMonitorForSmartConfig(vm.subject);
            processNewSaveSuccess(result);
        };

        var onSaveSuccess = function (result) {
            processSuccess(result);
        };

        var onSaveAndNewSuccess = function (result) {
            processNewSaveSuccess(result);
        };

        $scope.$watch('vm.subject.heightUnit', function (newVal, oldVal) {
            if (angular.isDefined(vm.subject) && vm.subject != null && isNumeric(vm.subject.height)) {
                if (angular.equals(oldVal, 'CM') && angular.equals(newVal, 'IN')) {
                    vm.subject.height = Math.round(parseFloat(vm.subject.height) * 0.39370);
                } else if (angular.equals(oldVal, 'IN') && angular.equals(newVal, 'CM')) {
                    vm.subject.height = Math.round(parseFloat(vm.subject.height) / 0.39370);
                }
            }
        });

        $scope.$watch('vm.subject.weightUnit', function (newVal, oldVal) {
            if (angular.isDefined(vm.subject) && vm.subject != null && isNumeric(vm.subject.weight)) {
                if (angular.equals(oldVal, 'KG') && angular.equals(newVal, 'LB')) {
                    vm.subject.weight = Math.round(parseFloat(vm.subject.weight) * 2.2046);
                } else if (angular.equals(oldVal, 'LB') && angular.equals(newVal, 'KG')) {
                    vm.subject.weight = Math.round(parseFloat(vm.subject.weight) / 2.2046);
                }
            }
        });

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.setWizardState = function (state) {
            $state.go(state);
        };

        vm.validateProceed = function (event, form) {
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    errorField.$setTouched();
                })
            });
            if (form.$invalid) {
                event.stopImmediatePropagation();
                event.preventDefault();
                return false;
            }
            return true;
        };

        vm.save = function () {
            vm.isSaving = true;
            vm.isSaveNew = false;
            if (vm.subject.studyId != null) {
                vm.siteReferences.push(vm.subject.siteReference);
                vm.saveSite(vm.subject.studyId, vm.subject.siteReference);

            }
        };

        vm.saveAndNew = function () {
            vm.isSaving = true;
            vm.isSaveNew = true;
            vm.siteReferences.push(vm.subject.siteReference);
            vm.saveSite(vm.subject.studyId, vm.subject.siteReference);
            /*if (vm.subject.id !== null) {
                Subject.update(vm.subject, onSaveAndNewSuccess, onSaveError);
            } else {
                Subject.save(vm.subject, onSaveAndNewSuccess, onSaveError);
            }*/
        };

        vm.clear = function () {
            /*if ( vm.isSaveNew==true) {
                $state.go('passApp.subject.home.title');
                //$scope.$emit('passApp:subjectUpdate', vm.result);
                //vm.result=null;
                vm.isSaveNew=false;
            }
            else {*/
            $uibModalInstance.dismiss('cancel');
            //}
            vm.isSaving = false;
        };

        vm.studyChanged = function () {
            vm.subject.siteReference = null;
            //vm.subject.externalReference = null;
            vm.loadSites();

        };
        vm.saveSite = function (studId, siteReference) {
            Site.save({
                studyId: studId,
                siteReferences: vm.siteReferences,
                file: vm.file
            }, onSaveSuccessSite, onSaveError);

        };

        vm.loadSites = function () {
            vm.sitesLoaded = false;
            vm.sites = [];

            Site.list(
                {studyId: vm.subject.studyId}
            ).$promise.then(function (sites) {
                vm.sites = sites;
                vm.sitesLoaded = true;
            });
            /*vm.study.studyId=6;
            vm.study.studyName="Test Study 5";*/
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.dateOfBirth = false;

        vm.openCalendar = function (date) {
            vm.datePickerOpenStatus[date] = true;
        };

        function arraySort(property) {
            var sortOrder = 1;
            if (property[0] === "-") {
                sortOrder = -1;
                property = property.substr(1);
            }
            return function (a, b) {
                var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
                return result * sortOrder;
            }
        }

        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    }

    angular
        .module('passApp')
        .directive('subjectidValidator', SubjectIdValidator);

    SubjectIdValidator.$inject = ['$q', '$http', '$timeout'];

    function SubjectIdValidator($q, $http, $timeout) {
        var vm = this;
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.subjectId = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();

                    if (isNaN(parseInt(attrs.validStudy))) {
                        deferred.resolve();
                    } else {
                        var subjectId = 0;
                        if (!isNaN(parseInt(attrs.subjectid))) {
                            subjectId = parseInt(attrs.subjectid)
                        }

                        $http.get('/api/subjects/' + parseInt(attrs.validStudy) + '/' + viewValue + '/' + subjectId).then(function () {
                            // Found the user, therefore not unique.
                            deferred.reject();
                        }, function () {
                            // User not found, therefore unique!
                            deferred.resolve();
                        });
                    }

                    return deferred.promise;
                };
            }
        }
    }
})();
