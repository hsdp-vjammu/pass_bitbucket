(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('SubjectController', SubjectController);

    SubjectController.$inject = ['$rootScope', '$scope', '$state', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants', 'Principal', 'Subject'];

    function SubjectController ($rootScope, $scope, $state, ParseLinks, AlertService, pagingParams, paginationConstants, Principal, Subject) {
        $scope.advancedSearchCollapsed = !$rootScope.search.subjects.options.advancedSearchOpen;
        $scope.searchModel = $rootScope.search.subjects.model;

        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;

        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };

        var searchModelListener = $scope.$watchCollection('searchModel', function (newVal, oldVal) {
            loadAll ();
        });
        $scope.$on('$destroy', searchModelListener);



        function loadAll () {
            vm.queryCount = 0;
            $rootScope.search.subjects.model = $scope.searchModel;
            $rootScope.search.subjects.options.advancedSearchOpen = !$scope.advancedSearchCollapsed;
            $rootScope.search.subjects.options.page = pagingParams.page;
            $rootScope.search.subjects.options.sort = vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc');

            Subject.query({
                cid: Principal.currentCompany.id,
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort(),
                search: search()
            }, onSuccess, onError);

            function is_empty(obj) {
                for(var i in obj) {
                    if(obj.hasOwnProperty(i))
                        return false;
                }
                return true;
            }

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                //for SC data manipulation changes
                if($rootScope.loginUserRole === "ACL_SITECOORD") {
                    var searchP = {name: null, id: null};
                    var sParams = [];
                    var searchSCRecords = is_empty(searchParams) ? 'true' : 'false';
                    sParams = {'searchParams': searchParams, 'siteIds': $rootScope.loginUserStudyIds, 'searchSCRecords': searchSCRecords};
                    if(is_empty(searchParams)) {
                        searchParams = sParams;
                    } else {
                        searchParams = searchParams;
                    }
                }
                return angular.toJson(searchParams);
            }


            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'externalReference') {
                    result.push('externalReference');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.subjects = data;
                //replaceSubjectsForSC();
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function replaceSubjectsForSC() {
            //display site coordinator related studies
            vm.newStudiesCollection = [];
            if($rootScope.loginUserRole === "ACL_SITECOORD") {
                angular.forEach(vm.subjects, function(subject){
                    var scAssignedStudies = $rootScope.loginUserStudies;
                    var studyFound = _.find(scAssignedStudies, function(gsc) {
                        return gsc.id === subject.studyId;
                    } );
                    if(studyFound) {
                        vm.newStudiesCollection.push(subject);
                    }
                });
                //vm.queryCount = vm.newStudiesCollection.length;
                vm.subjects = vm.newStudiesCollection;  //display list for SC
            }
        }

        function loadPage (page) {
            vm.page = page;
            vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')
            });
        }


        vm.toggleAdvancedSearch = function () {
            $scope.advancedSearchCollapsed = !$scope.advancedSearchCollapsed;
            if ($scope.advancedSearchCollapsed && angular.isDefined($scope.searchModel)) {
                $scope.searchModel = {'$': $scope.searchModel.$};
            }
        };

        vm.clearSearch = function () {
            $scope.advancedSearchCollapsed = true;
            $scope.searchModel = {}
        };

        vm.selectSubject = function(subject) {
            vm.navigateToSubjectDetails(subject.id);
        }

        vm.navigateToStudyDetails = function (id) {
            $rootScope.returnState.study = $state.current.name;
            $state.go('study-detail', {id:id});
        };

        vm.navigateToSubjectDetails = function (id) {
            $rootScope.returnState.subject = $state.current.name;
            $state.go('subject-detail.analysis', {id:id});
        };

    }

})();
