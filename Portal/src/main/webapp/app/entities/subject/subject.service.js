(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('Subject', Subject);

    Subject.$inject = ['$resource', 'DateUtils'];

    function Subject ($resource, DateUtils) {
        var resourceUrl =  'api/subjects/:status:id';

        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                transformResponse: function (data) {
                    data = data.replace(/null/gi, "\"\"");
                    var collection = angular.fromJson(data);
                    angular.forEach(collection, function(object) {
                   //  object.dateOfBirth = DateUtils.convertDateTimeFromServer(object.dateOfBirth);
                        object.dateOfBirth = DateUtils.convertLocalDateFromServer(object.dateOfBirth);
                        if (object.assignedMonitors && object.assignedMonitors.length != 0) {
                            object.monitorSerialNumber = object.assignedMonitors[0].serialNumber;
                        } else {
                            object.monitorSerialNumber = '';
                        }
                    }, collection);

                    return collection;
                },
                isArray: true
            },
            'queryUnassigned': {
                method: 'GET',
                params: {'status': "unassigned"},
                transformResponse: function (data) {
                    data = data.replace(/null/gi, "\"\"");
                    var collection = angular.fromJson(data);
                    angular.forEach(collection, function(object) {
                  //    object.dateOfBirth = DateUtils.convertDateTimeFromServer(object.dateOfBirth);
                        object.dateOfBirth = DateUtils.convertLocalDateFromServer(object.dateOfBirth);
                        if (object.assignedMonitors && object.assignedMonitors.length != 0) {
                            object.monitorSerialNumber = object.assignedMonitors[0].serialNumber;
                        } else {
                            object.monitorSerialNumber = '';
                        }
                        object.displayName = (object.firstName + ' ' + object.lastName + ' (' + object.externalReference + ')').trim();
                    }, collection);

                    return collection;
                },
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                     //   data.dateOfBirth = DateUtils.convertDateTimeFromServer(data.dateOfBirth);
                        data.dateOfBirth = DateUtils.convertLocalDateFromServer(data.dateOfBirth);

                        if (data.assignedMonitors && data.assignedMonitors.length != 0) {
                            data.monitorSerialNumber = data.assignedMonitors[0].serialNumber;
                        } else {
                            data.monitorSerialNumber = '';
                        }
                    }
                    return data;
                }
            },
            'update': {
                method:'PUT',
                transformResponse: function (data) {
                    if (data){
                        data = angular.fromJson(data);
                        data.dateOfBirth = DateUtils.convertLocalDateToServer(data.dateOfBirth)
                    }

                }


            }
        });
    }
})();
