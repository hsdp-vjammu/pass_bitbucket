(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('SubjectExportController', SubjectExportController);

    SubjectExportController.$inject = ['$scope','$uibModalInstance','$rootScope'];

    function SubjectExportController ($scope,$uibModalInstance, $rootScope) {
        var vm = this;

        vm.pwdMismatch = function() {
            vm.response = false;
            if ((vm.password != undefined) && (vm.repeatPwd != undefined) && (vm.repeatPwd != vm.password) ) {
                vm.response = true;
            }
            return vm.response;
        }
        vm.ok = function () {
                //passing dialog box data to Subject Ctrl
                vm.dialogBoxData = {
                    checkForPwd: vm.checkForPwd,
                    password: vm.password,
                    repeatPwd: vm.repeatPwd
                }
                $rootScope.$broadcast("ExportDialogData", vm.dialogBoxData);
                $rootScope.$emit("ExportDialogData", vm.dialogBoxData);
                $rootScope.exportDialogData = vm.dialogBoxData;
                $uibModalInstance.close(true);
        };

        vm.cancel = function () {
                $uibModalInstance.dismiss('cancel');
        };
       }
})();
