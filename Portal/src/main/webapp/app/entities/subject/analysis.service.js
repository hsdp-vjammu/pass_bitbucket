(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('Analysis', Analysis);

    Analysis.$inject = ['$http', 'DateUtils','$filter'];

    function Analysis ($http, DateUtils,$filter) {


        return {
            formattedDate : function (dateValue) {
                return $filter('date')(new Date(dateValue),'yyyy-MM-dd')
            },
            formattedDateTime : function (dateValue) {
                return $filter('date')(new Date(dateValue),'dd-MMM-yyyy hh:mm:ss a')
            },
            formattedDateTimeNoSecs : function (dateValue) {
                return $filter('date')(new Date(dateValue),'dd-MMM-yyyy hh:mm a')
            },
            formattedDateTimeForExport : function (dateValue) {
                return $filter('date')(new Date(dateValue),'dd-MMM-yyyy hh:mm:ss')
            },
            convertDateTimeToLocalDateTime : function (utcDateString, format) {
                if (!utcDateString) {
                    return;
                }
                if (utcDateString.indexOf('Z') === -1 && utcDateString.indexOf('+') === -1) {
                    utcDateString += 'Z';
                }


                return $filter('date')(utcDateString, format);
            }
        }



    }

     /*   var promise = $http.get('test.json').then(function (response) {
            // The then function here is an opportunity to modify the response
            console.log(response);
            // The return value gets picked up by the then in the controller.
            return response.data;
        });
        // Return the promise to the controller
        return promise;







        };*/







})();
