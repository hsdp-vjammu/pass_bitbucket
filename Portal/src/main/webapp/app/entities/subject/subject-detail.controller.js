(function () {
    'use strict';

    angular
        .module('passApp')
        .controller('SubjectDetailController', SubjectDetailController);

    SubjectDetailController.$inject = ['$rootScope', '$scope', '$state', 'entity', 'MonitorHistory'];

    function SubjectDetailController($rootScope, $scope, $state, entity, MonitorHistory) {
        var vm = this;
        vm.subject = entity;
        vm.messageToDisplay = "This subject is currently assigned or was assigned to a study. Please setup the subject again with a new subject ID in order to use the subject in another study.";
        vm.subjectConfigStatus = false;
        vm.loadMonitors = loadMonitors;
        vm.loadMonitors();

        if (typeof $state.params.activeTab === "undefined") {
            vm.subjectDetailsCollapsed = true;
            vm.activeTab = null;
        } else {
            vm.subjectDetailsCollapsed = false;
            vm.activeTab = $state.params.activeTab;
        }
        var unsubscribe = $rootScope.$on('passApp:subjectUpdate', function (event, result) {
            vm.subject = result;
        });
        $scope.$on('$destroy', unsubscribe);

        //pre-loading monitors for config status and drop down display
        function loadMonitors() {
            entity.$promise.then(function (subject) {
                vm.subject = subject;
                vm.timeZone = subject.timeZone;
                vm.displayUUID = (vm.subject.uniqueIdentifier).toString().slice(-16);  //show only last 16 digits
                vm.assignedSubject = vm.subject.id;
                vm.subjectReference = vm.subject.externalReference === ''? 'anonymous':vm.subject.externalReference;
                vm.monitorsData = MonitorHistory.getByAssignedSubject({
                    assignedSubject: vm.assignedSubject
                });
                vm.monitors = [];
                vm.monitorsData.$promise.then(function (monitors) {
                    angular.forEach(monitors, function (monitor) {
                        //set default serial number for drop down
                        if (vm.subject.assignedMonitors.length != 1) {
                            //AlertService.info("There are no assigned monitors for given Subject.");
                            console.log("There are no assigned monitors for given Subject.");
                        } else if (monitor.serialNumber === vm.subject.assignedMonitors[0].serialNumber) {
                            vm.selectedMonitor = monitor.serialNumber;
                        }
                        vm.monitors.push({
                            'serialNumber': monitor.serialNumber,
                            'lastModifiedDate': monitor.lastModifiedDate
                        });
                    }, vm.monitors);
                    vm.monitors = _.uniqBy(vm.monitors, 'serialNumber');
                    if (vm.selectedMonitor === undefined) {
                        vm.selectedMonitor = vm.monitors.length === 0 ? null : vm.monitors[0].serialNumber;
                    }

                    //emit subject, monitors and selected monitor information to subject details page for drop down display
                    $scope.$watch('vm.selectedMonitor', function (newValue, oldValue) {
                        $scope.$emit('monitorSubjectData', {
                            monitors: vm.monitors,
                            selectedMonitor: vm.selectedMonitor,
                            subject: vm.subject
                        });
                    });
                    //find out Configuration Status
                    if(vm.selectedMonitor === null) { //no device assigned so far
                        vm.subjectConfigStatus = false; //not modified before, so allow EDIT for identity
                    } else {
                        setConfigStatus();
                    }
                }, function (error) {
                    $scope.error = error.status + " - " + error.data.message;
                    vm.errorMessage = "No device serial numbers found for given Subject - " + $scope.error;
                });
            }, function (error) {
                $scope.error = error.status + " - " + error.data.message;
                vm.errorMessage = "Unable to find assigned Subject - " + $scope.error;
            });
        }

        //verify configuration previously assigned, by checking monitor history
        function setConfigStatus() {
            //make a call to monitorHistory to find out number of records for given subjectID + monitor S/N
            vm.monitorHistoryData = MonitorHistory.getByAssignedSubjectAndDeviceSN({
                assignedSubject: vm.assignedSubject,
                serialNumber: vm.selectedMonitor
            });
            vm.monitorHistoryData.$promise.then(function (response) {
                if (response.length > 0) {
                    vm.subjectConfigStatus = true; //configuration already modified, no edits are allowed
                } else {
                    vm.subjectConfigStatus = false; //not modified before, so allow EDIT for identity
                }
            }, function (error) {
                $scope.error = error.status + " - " + error.data.message;
                vm.errorMessage = "No Monitor History records found for given Subject and serial number  - " + $scope.error;
            });
        }

        vm.identityStateChange = function () {
            $state.go('subject-edit.identity', {id: vm.subject.id});
        };

        vm.selectTab = function (tab) {
            tab !== vm.activeTab ? vm.activeTab = tab : vm.activeTab = null;
        };

        vm.navigateBack = function () {
            if ($rootScope.returnState.subject) {
                $state.go($rootScope.returnState.subject);
            } else {
                $state.go('subject');
            }
        };
    }
})();
