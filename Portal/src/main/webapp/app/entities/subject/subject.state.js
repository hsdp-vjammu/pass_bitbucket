(function () {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('subject', {
                parent: 'entity',
                url: '/subject?page&sort&search',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'passApp.subject.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/subject/subjects.html',
                        controller: 'SubjectController',
                        controllerAs: 'vm'
                    }
                },
                params: {
                    defaultPage: {
                        value: '1',
                        squash: true
                    },
                    defaultSort: {
                        value: 'externalReference,asc',
                        squash: true
                    },
                    search: null
                },
                resolve: {
                    pagingParams: ['$stateParams', '$rootScope', 'PaginationUtil', function ($stateParams, $rootScope, PaginationUtil) {
                        var page = PaginationUtil.parsePage($stateParams.defaultPage);
                        if ($stateParams.page) {
                            page = PaginationUtil.parsePage($stateParams.page);
                        } else if ($rootScope.search.subjects.options.page) {
                            page = $rootScope.search.subjects.options.page;
                        }
                        var sortPredicate = PaginationUtil.parsePredicate($stateParams.defaultSort);
                        var sortAscending = PaginationUtil.parseAscending($stateParams.defaultSort);
                        if ($stateParams.sort) {
                            sortPredicate = PaginationUtil.parsePredicate($stateParams.sort);
                            sortAscending = PaginationUtil.parseAscending($stateParams.sort);
                        } else if ($rootScope.search.subjects.options.sort) {
                            sortPredicate = PaginationUtil.parsePredicate($rootScope.search.subjects.options.sort);
                            sortAscending = PaginationUtil.parseAscending($rootScope.search.subjects.options.sort);
                        }
                        return {
                            page: page,
                            sort: $stateParams.sort,
                            predicate: sortPredicate,
                            ascending: sortAscending,
                            search: $stateParams.search
                        };
                    }],
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('subject');
                        $translatePartialLoader.addPart('gender');
                        $translatePartialLoader.addPart('heightUnit');
                        $translatePartialLoader.addPart('weightUnit');
                        $translatePartialLoader.addPart('attributes');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('subject-detail', {
                parent: 'entity',
                url: '/subject/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'passApp.subject.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/subject/subject-detail.html',
                        controller: 'SubjectDetailController',
                        controllerAs: 'vm'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('subject');
                        $translatePartialLoader.addPart('gender');
                        $translatePartialLoader.addPart('heightUnit');
                        $translatePartialLoader.addPart('weightUnit');
                        $translatePartialLoader.addPart('attributes');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'Subject', function ($stateParams, Subject) {
                        return Subject.get({id: $stateParams.id});
                    }]
                },
                abstract: true
            })
            .state('subject-detail.analysis', {
                url: '/analysis',
                resolve: {
                    subjectJsonData: ['JhiLanguageService', '$http', '$q', function (JhiLanguageService, $http, $q) {
                        var deferred = $q.defer();
                        JhiLanguageService.getCurrent().then(function (current) {
                            //get Subject Json to extract headers and tooltips
                            return $http.get('i18n/' + current + '/subject.json').then(function (data) {
                                deferred.resolve(data);
                            });
                        });
                        return deferred.promise;
                    }]
                },
                views: {
                    'information': {
                        templateUrl: 'app/entities/subject/subject-detail-information.html'
                    },
                    'analysis': {
                        templateUrl: 'app/entities/subject/subject-detail-analysis.html',
                        controller: 'SubjectAnalysisController',
                        controllerAs: 'vm'
                    },
                    entity: ['$stateParams', 'Subject', function ($stateParams, Subject) {
                        return Subject.get({id: $stateParams.id});
                    }]

                }
            })
            .state('subject-detail.analysis.identity', {
                url: '/identity',
                onEnter: ['$stateParams', function ($stateParams) {
                    $stateParams.activeTab = 'identity';
                }]
            })
            .state('subject-detail.analysis.demographics', {
                url: '/demographics',
                onEnter: ['$stateParams', function ($stateParams) {
                    $stateParams.activeTab = 'demographics';
                }]
            })
            .state('subject-detail.analysis.hstats', {
                url: '/hstats',
                onEnter: ['$stateParams', function ($stateParams) {
                    $stateParams.activeTab = 'hstats';
                }]
            })
            .state('subject.new', {
                parent: 'subject',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/subject/subject-dialog-wizard.html',
                        controller: 'SubjectDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    id: null,
                                    studyId: null,
                                    uniqueIdentifier: null,
                                    externalReference: null,
                                    firstName: null,
                                    lastName: null,
                                    gender: null,
                                    dateOfBirth: null,
                                    phoneNumber: null,
                                    email: null,
                                    streetAddress: null,
                                    zipCode: null,
                                    city: null,
                                    state: null,
                                    country: null,
                                    timeZone: null,
                                    weight: null,
                                    weightUnit: 'KG',
                                    height: null,
                                    heightUnit: 'CM',
                                    wearingPosition: "LEFT",
                                    dominantHand: "RIGHT"
                                };
                            }
                        }
                    }).result.then(function () {
                        $state.go('subject', null, {reload: true});
                    }, function () {
                        $state.go('subject');
                    });
                }]
            })
            .state('subject.new.identity', {
                url: '/identity',
                data: {
                    currentStepIndex: 1
                },
                views: {
                    'wizard@': {
                        templateUrl: 'app/entities/subject/subject-new-identity.html'
                    }
                }
            })
            .state('subject.new.demographics', {
                url: '/demographics',
                data: {
                    currentStepIndex: 2
                },
                views: {
                    'wizard@': {
                        templateUrl: 'app/entities/subject/subject-new-demographics.html'
                    }
                }
            })
            .state('subject.new.hstats', {
                url: '/hstats',
                data: {
                    currentStepIndex: 3
                },
                views: {
                    'wizard@': {
                        templateUrl: 'app/entities/subject/subject-new-hstats.html'
                    }
                }
            })
            .state('subject-edit', {
                parent: 'subject-detail.analysis',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER']
                },
                abstract: true
            })
            .state('subject-edit.identity', {
                url: '/identity',
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/subject/subject-edit-identity.html',
                        controller: 'SubjectDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Subject', function (Subject) {
                                return Subject.get({id: $stateParams.id});
                            }]
                        }
                    }).result.then(function () {
                        $state.go('subject-detail.analysis.identity', {id: $stateParams.id}, {reload: true});
                    }, function () {
                        $state.go('subject-detail.analysis.identity', {id: $stateParams.id}, {reload: true});
                    });
                }]
            })
            .state('subject-edit.demographics', {
                url: '/demographics',
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/subject/subject-edit-demographics.html',
                        controller: 'SubjectDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Subject', function (Subject) {
                                return Subject.get({id: $stateParams.id});
                            }]
                        }
                    }).result.then(function () {
                        $state.go('subject-detail.analysis.demographics', {id: $stateParams.id}, {reload: true});
                    }, function () {
                        $state.go('subject-detail.analysis.demographics', {id: $stateParams.id}, {reload: true});
                    });
                }]
            })
            .state('subject-edit.hstats', {
                url: '/hstats',
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/subject/subject-edit-hstats.html',
                        controller: 'SubjectDialogController',
                        controllerAs: 'vm',
                        backdrop: 'static',
                        size: 'lg',
                        resolve: {
                            entity: ['Subject', function (Subject) {
                                return Subject.get({id: $stateParams.id});
                            }]
                        }
                    }).result.then(function () {
                        $state.go('subject-detail.analysis.hstats', {id: $stateParams.id}, {reload: true});
                    }, function () {
                        $state.go('subject-detail.analysis.hstats', {id: $stateParams.id}, {reload: true});
                    });
                }]
            })
            .state('subject.delete', {
                parent: 'subject',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER']
                },
                onEnter: ['$stateParams', '$state', '$uibModal', function ($stateParams, $state, $uibModal) {
                    $uibModal.open({
                        templateUrl: 'app/entities/subject/subject-delete-dialog.html',
                        controller: 'SubjectDeleteController',
                        controllerAs: 'vm',
                        size: 'md',
                        resolve: {
                            entity: ['Subject', function (Subject) {
                                return Subject.get({id: $stateParams.id});
                            }]
                        }
                    }).result.then(function () {
                        $state.go('subject', null, {reload: true});
                    }, function () {
                        $state.go('^');
                    });
                }]
            })
            .state('subject-export', {
                parent: 'subject-detail.analysis',
                url: '/new'
            })
        ;
    }

})();
