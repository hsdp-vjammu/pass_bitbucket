(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('TDRData', TDRData);
    TDRData.$inject = ['$resource'];
    function TDRData($resource) {
        var resourceUrl =  'api/dataitem';
        return $resource(resourceUrl, {}, {
            'query': {
                method: 'GET',
                transformResponse: function (data, headers, status) {
                    var response = {}
                    response.error = false;
                    try {
                        response.data = angular.fromJson(data)
                    } catch (e) {
                        response.data = []
                        response.error = true
                    }
                    response.headers = headers();
                    response.status = status;
                    return response;
                },
                isArray: false
            }
        });
    }

})();
