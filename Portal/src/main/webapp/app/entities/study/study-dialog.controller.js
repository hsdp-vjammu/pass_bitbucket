(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('StudyDialogController', StudyDialogController);

    StudyDialogController.$inject = ['$timeout', '$scope', '$uibModalInstance', 'Principal', 'entity', 'Study'];

    function StudyDialogController ($timeout, $scope, $uibModalInstance, Principal, entity, Study) {
        var vm = this;

        entity.companyId = Principal.currentCompany.id;
        vm.study = entity;

        $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
        });

        var onSaveSuccess = function (result) {
            $scope.$emit('passApp:studyUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;
            if (vm.study.id !== null) {
                Study.update(vm.study, onSaveSuccess, onSaveError);
            } else {
                Study.save(vm.study, onSaveSuccess, onSaveError);
            }
        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.dateCreated = false;
        vm.datePickerOpenStatus.dateClosed = false;

        vm.openCalendar = function(date) {
            vm.datePickerOpenStatus[date] = true;
        };
    }

    angular
        .module('passApp')
        .directive('studyNameValidator', StudyNameValidator);

    StudyNameValidator.$inject = ['$q', '$http', '$timeout'];

    function StudyNameValidator ($q, $http, $timeout) {
        var vm = this;
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.studyName = function (modelValue, viewValue) {
                    if (!viewValue) {
                        return $q.when(true);
                    }
                    var deferred = $q.defer();

                    if (isNaN(parseInt(attrs.validCompany))) {
                        deferred.reject();
                    } else {
                        var studyid = 0;
                        if (!isNaN(parseInt(attrs.studyid))) {
                            studyid = parseInt(attrs.studyid)
                        }

                        $http.get('/api/studies/' + parseInt(attrs.validCompany) + '/' + viewValue  + '/' + studyid).then(function() {
                            // Found the user, therefore not unique.
                            deferred.resolve();
                        }, function() {
                            // User not found, therefore unique!
                            deferred.reject();
                        });
                    }

                    return deferred.promise;
                };
            }
        }
    }

})();
