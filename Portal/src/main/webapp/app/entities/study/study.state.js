(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('study', {
            parent: 'setup',
            url: '/study?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.study.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/study/studies.html',
                    controller: 'StudyController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('study');
                    $translatePartialLoader.addPart('studyStatus');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('study-detail', {
            parent: 'setup',
            url: '/study/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.study.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/study/study-detail.html',
                    controller: 'StudyDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('study');
                    $translatePartialLoader.addPart('studyStatus');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Study', function($stateParams, Study) {
                    return Study.get({id : $stateParams.id});
                }]
            }
        })
        .state('study.new', {
            parent: 'study',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/study/study-dialog.html',
                    controller: 'StudyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        account: ['Principal', function (Principal) {
                            return Principal.identity();
                        }],
                        entity: [function () {
                            return {
                                uniqueIdentifier: null,
                                externalReference: null,
                                name: null,
                                description: null,
                                comments: null,
                                status: "ACTIVE",
                                dateCreated: null,
                                dateClosed: null,
                                id: null
                            };
                        }]
                    }
                }).result.then(function() {
                    $state.go('study', null, { reload: true });
                }, function() {
                    $state.go('study');
                });
            }]
        })
        .state('study-detail.edit', {
            parent: 'study-detail',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/study/study-dialog.html',
                    controller: 'StudyDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Study', 'Principal', '$q', function(Study, Principal, $q) {
                            var defer = $q.defer();
                            Study.get( {id : $stateParams.id}, function( study ) {
                                study.companyId = Principal.currentCompany.id;
                                defer.resolve( study );
                            });
                            return defer.promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('study-detail', {id: $stateParams.id}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('study-detail.delete', {
            parent: 'study-detail',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/study/study-delete-dialog.html',
                    controller: 'StudyDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Study', function(Study) {
                            return Study.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('study', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
