(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('StudyDetailController', StudyDetailController);

    StudyDetailController.$inject = ['$rootScope', '$scope', '$state', '$uibModal', 'entity', 'Study'];

    function StudyDetailController($rootScope, $scope, $state, $uibModal, entity, Study) {
        var vm = this;
        vm.study = entity;

        var unsubscribe = $rootScope.$on('passApp:studyUpdate', function(event, result) {
            vm.study = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.toggleStudyStatus = function () {
            if (vm.study.status === 'ACTIVE') {
                $uibModal.open({
                    templateUrl: 'app/entities/study/study-inactivate-confirm-dialog.html',
                    controller: 'ConfirmDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md'
                }).result.then(function(choice) {
                    if (choice === true) {
                        changeStatus('INACTIVE');
                    }
                }, function() {
                    //no action
                });
            } else {
                changeStatus('ACTIVE');
            }
        }

        var changeStatus = function (status) {
            vm.isSaving = true;
            Study.updateStatus({id: vm.study.id, status: status}, onUpdateSuccess, onUpdateError);
        }

        var onUpdateSuccess = function (result) {
            $scope.$emit('passApp:studyUpdate', result);
            vm.isSaving = false;
        };

        var onUpdateError = function () {
            vm.isSaving = false;
        };

        vm.navigateBack = function () {
            if ($rootScope.returnState.study) {
                $state.go($rootScope.returnState.study);
            } else {
                $state.go('study');
            }
        };

    }
})();
