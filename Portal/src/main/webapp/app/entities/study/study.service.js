(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('Study', Study);

    Study.$inject = ['$resource', 'DateUtils'];

    function Study ($resource, DateUtils) {
        var resourceUrl =  'api/studies/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'list': {
                url: 'api/studies/list',
                method: 'GET',
                isArray: true
            },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateCreated = DateUtils.convertDateTimeFromServer(data.dateCreated);
                        data.dateClosed = DateUtils.convertDateTimeFromServer(data.dateClosed);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' },
            'updateStatus': {
                url: 'api/studies/changeStatus',
                method:'PUT'
            }
        });
    }
})();
