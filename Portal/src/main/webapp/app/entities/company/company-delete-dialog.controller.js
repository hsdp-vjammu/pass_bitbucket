(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('CompanyDeleteController',CompanyDeleteController);

    CompanyDeleteController.$inject = ['$uibModalInstance', 'entity', 'Company','$scope','Study','AlertService','Principal','$location'];

    function CompanyDeleteController($uibModalInstance, entity, Company,scope,Study,AlertService,Principal,location) {
        var vm = this;
        vm.company = entity;
        vm.studies = [];
        //vm.studyCount=studyCount;
        //vm.studyCount();
        scope.searchModel = {};
        var searchObject = location.$$path;
        var arr=searchObject.split('/');
        var cid=arr[2];

        scope.load=function () {
            Study.list(
                {cid: cid}
            ).$promise.then(function(studies){
                vm.studies = studies;
                if(vm.studies.length>0){
                    vm.hasStudy=true;
                }
                else {
                    vm.hasStudy=false;
                }
            })
                /*.then(function (studies) {
                    if(vm.studies.count()>0){
                        vm.hasStudy=true;
                    }
                    else {
                        vm.hasStudy=false;
                    }
                })
*/
        }
        /*function studyCount(){
            Study.list(
                {cid: 9}
            ).$promise.then(function(studies){
                vm.studies = studies;
                    });
        }*/

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            Company.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
