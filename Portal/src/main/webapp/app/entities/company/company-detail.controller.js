(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('CompanyDetailController', CompanyDetailController);

    CompanyDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'Company', 'User'];

    function CompanyDetailController($scope, $rootScope, $stateParams, entity, Company, User) {
        var vm = this;
        vm.countries = $rootScope.countries;
        vm.company = entity;

        var unsubscribe = $rootScope.$on('passApp:companyUpdate', function(event, result) {
            vm.company = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
