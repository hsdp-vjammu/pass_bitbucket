(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('PopNumber', PopNumber);

    PopNumber.$inject = ['$resource', 'DateUtils'];

    function PopNumber ($resource, DateUtils) {
        var resourceUrl =  'api/pop-numbers/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.importDate = DateUtils.convertDateTimeFromServer(data.importDate);
                    }
                    return data;
                }
            },
            'save': {
                method: 'POST',
                headers: { 'Content-Type': undefined },
                transformRequest: function (data) {
                    var formData = new FormData();

                    formData.append("data", angular.toJson({numbers: data.numbers}));
                    formData.append("file", data.file);

                    return formData;
                }
            }
        });
    }

    angular
        .module('passApp')
        .factory('PopNumberSearch', PopNumberSearch);

    PopNumberSearch.$inject = ['$resource'];

    function PopNumberSearch ($resource) {
        var resourceUrl =  'api/pop-numbers/_search';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }

})();
