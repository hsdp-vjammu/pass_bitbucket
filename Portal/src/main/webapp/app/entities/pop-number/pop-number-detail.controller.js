(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('PopNumberDetailController', PopNumberDetailController);

    PopNumberDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'PopNumber'];

    function PopNumberDetailController($scope, $rootScope, $stateParams, entity, PopNumber) {
        var vm = this;
        vm.popNumber = entity;
        
        var unsubscribe = $rootScope.$on('passApp:popNumberUpdate', function(event, result) {
            vm.popNumber = result;
        });
        $scope.$on('$destroy', unsubscribe);

    }
})();
