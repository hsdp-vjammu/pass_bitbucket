(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('pop-number', {
            parent: 'admin',
            url: '/pop-number',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'passApp.popNumber.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pop-number/pop-numbers.html',
                    controller: 'PopNumberController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'number,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('popNumber');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('pop-number-detail', {
            parent: 'admin',
            url: '/pop-number/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'passApp.popNumber.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/pop-number/pop-number-detail.html',
                    controller: 'PopNumberDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('popNumber');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'PopNumber', function($stateParams, PopNumber) {
                    return PopNumber.get({id : $stateParams.id});
                }]
            }
        })
        .state('pop-number.new', {
            parent: 'pop-number',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pop-number/pop-number-dialog.html',
                    controller: 'PopNumberDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg'
                }).result.then(function() {
                    $state.go('pop-number', null, { reload: true });
                }, function() {
                    $state.go('pop-number');
                });
            }]
        })
        .state('pop-number.delete', {
            parent: 'pop-number',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/pop-number/pop-number-delete-dialog.html',
                    controller: 'PopNumberDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PopNumber', function(PopNumber) {
                            return PopNumber.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('pop-number', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
