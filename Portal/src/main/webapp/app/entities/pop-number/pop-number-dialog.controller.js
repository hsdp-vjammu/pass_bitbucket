(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('PopNumberDialogController', PopNumberDialogController);

    PopNumberDialogController.$inject = ['$timeout', '$scope', '$http', '$uibModalInstance', 'PopNumber'];

    function PopNumberDialogController ($timeout, $scope, $http, $uibModalInstance, PopNumber) {
        var vm = this;

        vm.number = null;
        //vm.invalidInput=null;
        vm.numbers = [];
        vm.validationMessages = [];
        vm.file = null;

        vm.fileValidationError = {}

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        $scope.$watch('vm.file', function() {
            vm.clearValidationMessages();
        });

        var onSaveSuccess = function (result) {
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function (error) {
            vm.fileValidationError.message = 'passApp.popNumber.error.' + error.data.error;
            vm.fileValidationError.numbers = [];
            angular.forEach(error.data.numbers, function(value, key) {
                vm.fileValidationError.numbers.push({number: key, error: 'passApp.popNumber.validate.number.' + value[0]});
            });
            vm.isSaving = false;
        };

        vm.save = function () {
            vm.isSaving = true;

            PopNumber.save({
                numbers: vm.numbers,
                file: vm.file}, onSaveSuccess, onSaveError);

        };

        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };

        vm.clearValidationMessages = function() {
            vm.validationMessages = [];
        };

        vm.addNumber = function () {

            var number = vm.number;
            if(number!=="")
            {

                    if (number && vm.numbers.indexOf(number) === -1) {
                        vm.validateNumber(number);
                    }
                    else {
                        vm.validationMessages = [];

                        vm.validationMessages.push('passApp.popNumber.validate.number.DUPLICATE');
                    }

            }
            else {
                vm.validationMessages = [];

                vm.validationMessages.push('passApp.popNumber.validate.number.TOO_LONG');
            }
        };

        vm.removeNumber = function (number) {
            var index = vm.numbers.indexOf(number);
            vm.numbers.splice(index, 1);
        };

        vm.validateNumber = function (number) {
            vm.validationMessages = [];
            vm.isValidating = true;

            $http.get('/api/pop-numbers/validate/' + number).then(function() {
                // Validation passed
                vm.numbers.push(number);
                vm.number = null;
                vm.isValidating = false;
            }, function(response) {
                // Validation failed
                //var messages = angular.fromJson(response.data);
                angular.forEach(response.data, function(message) {
                    vm.validationMessages.push('passApp.popNumber.validate.number.' + message);
                });
                vm.isValidating = false;
            });
        };
    }
})();
