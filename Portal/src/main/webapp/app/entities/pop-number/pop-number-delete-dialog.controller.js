(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('PopNumberDeleteController',PopNumberDeleteController);

    PopNumberDeleteController.$inject = ['$uibModalInstance', 'entity', 'PopNumber'];

    function PopNumberDeleteController($uibModalInstance, entity, PopNumber) {
        var vm = this;
        vm.popNumber = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            PopNumber.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
