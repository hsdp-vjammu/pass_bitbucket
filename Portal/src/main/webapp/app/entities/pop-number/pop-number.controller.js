(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('PopNumberController', PopNumberController);

    PopNumberController.$inject = ['$scope', '$state', 'PopNumber', 'ParseLinks', 'AlertService', 'pagingParams', 'paginationConstants'];

    function PopNumberController ($scope, $state, PopNumber, ParseLinks, AlertService, pagingParams, paginationConstants) {
        var vm = this;
        vm.loadAll = loadAll;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.loadAll();

        vm.searchModelOptions = { updateOn: 'default blur', debounce: { 'default': 500, 'blur': 0 } };

        $scope.searchModel = {};
        $scope.$watchCollection('searchModel', function (newVal, oldVal) {
            loadAll ();
        });

        function loadAll() {
            PopNumber.query({
                page: pagingParams.page - 1,
                size: paginationConstants.itemsPerPage,
                sort: sort(),
                search: search()
            }, onSuccess, onError);

            function search() {
                var searchParams = {};
                angular.forEach($scope.searchModel, function(value, key) {
                    if (value) {
                        this[key] = value;
                    }
                }, searchParams);
                return angular.toJson(searchParams);
            }

            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'number') {
                    result.push('number');
                }
                return result;
            }

            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.popNumbers = data;
                vm.page = pagingParams.page;
                //vm.transition();
            }

            function onError(error) {
                AlertService.error(error.data.message);
            }
        };

        function loadPage (page) {
            vm.page = page;
            //vm.transition();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }

    }
})();
