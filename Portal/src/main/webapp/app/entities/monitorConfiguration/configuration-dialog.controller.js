(function () {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorConfigurationDialogController', MonitorConfigurationDialogController);

    MonitorConfigurationDialogController.$inject = ['$rootScope', '$state', '$timeout', '$scope', '$uibModalInstance', 'Principal', 'entity', 'MonitorConfiguration'];

    function MonitorConfigurationDialogController($rootScope, $state, $timeout, $scope, $uibModalInstance, Principal, entity, MonitorConfiguration) {
        var vm = this;

        var stateChangeListener = $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if (toState.data) {
                    vm.currentStepIndex = toState.data.currentStepIndex;
                }
            }
        );
        $scope.$on('$destroy', stateChangeListener);

        vm.hasQuestions = false;
        vm.questions = null;
        //vm.allowSedentaryAlert = true;
        vm.selectedQuestion = null;
        vm.toggleQuestions = toggleQuestions;
        //vm.allowSedentaryAlert = allowSedentaryAlert();
        vm.openCalendar = openCalendar;

        $timeout(function () {
            angular.element('.form-group:eq(0)>input').focus();
        });

        vm.baseState = $state.$current.name;
        if ($state.$current.name.startsWith("monitor-configuration.new")) {
            vm.baseState = "monitor-configuration.new";
        } else if ($state.$current.name.startsWith("monitor-configuration-detail.edit")) {
            vm.baseState = "monitor-configuration-detail.edit";
        }

        if (angular.isDefined(entity.$promise)) {
            vm.hasQuestions = entity.questions.length > 0;
            vm.toggleQuestions();
        }

        entity.companyId = Principal.currentCompany.id;
        vm.configuration = entity;

        $scope.days = ['1-MON', '2-TUE', '3-WED', '4-THU', '5-FRI', '6-SAT', '7-SUN'];
        $scope.accordion = {
            oneAtATime: true,
            open: true
        };

        vm.datePicker = {
            format: 'dd-MMM-yyyy',
            dateOptions: {
                datepickerMode: 'day',
                showWeeks: false,
                startingDay: 1
            }
        };

        vm.datePickerOpenStatus = {};
        vm.datePickerOpenStatus.recurrenceStartDate = false;
        vm.datePickerOpenStatus.recurrenceEndDate = false;

        function toggleQuestions() {
            if (vm.hasQuestions) {
                vm.questions = initQuestions();
            } else {
                vm.questions = null;
            }
        };

        function allowSedentaryAlert() {
            if (vm.allowSedentaryAlert) {
                //show Sedentary Alert question range
                vm.sedentaryAlert = true;
            } else {
                vm.sedentaryAlert = false;
            }
        };

        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('passApp:monitorConfigurationUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        };

        var onSaveError = function () {
            vm.isSaving = false;
        };

        vm.save = function (event, form) {
            vm.isSaving = true;
            if (vm.validateProceed(event, form)) {
                vm.configuration.questions = [];
                if (angular.isDefined(vm.questions) && vm.questions !== null) {
                    for (var i = 0; i < vm.questions.length; i++) {
                        if (vm.questions[i].text) {
                            if (vm.questions[i].recurrenceTime1 != null) {
                                vm.questions[i].recTime1 = moment(vm.questions[i].recurrenceTime1).format("hh:mm A"); //.format("hh:mm:ss a")
                            } else {
                                vm.questions[i].recTime1 = "";
                            }
                            if (vm.questions[i].recurrenceTime2 != null) {
                                vm.questions[i].recTime2 = moment(vm.questions[i].recurrenceTime2).format("hh:mm A");
                            } else {
                                vm.questions[i].recTime2 = "";
                            }
                            if (vm.questions[i].recurrenceTime3 != null) {
                                vm.questions[i].recTime3 = moment(vm.questions[i].recurrenceTime3).format("hh:mm A");
                            } else {
                                vm.questions[i].recTime3 = "";
                            }
                            //to store recurrenceTime
                            vm.questions[i].recurrenceTime1 = moment();
                            vm.questions[i].recurrenceTime2 = moment();
                            vm.questions[i].recurrenceTime3 = moment();
                            vm.configuration.questions.push(vm.questions[i]);
                        }
                    }
                }

                if (vm.configuration.id !== null) {
                    MonitorConfiguration.update(vm.configuration, onSaveSuccess, onSaveError);
                } else {
                    MonitorConfiguration.save(vm.configuration, onSaveSuccess, onSaveError);
                }
            } else {
                vm.isSaving = false;
            }
        };

        vm.saveOrNext = function (event, form, state) {
            if (vm.hasQuestions && state) {
                if (vm.validateProceed(event, form)) {
                    $state.go(vm.baseState + '.' + state);
                }
            } else {
                vm.save(event, form);
            }
        };

        vm.clear = function () {
            $uibModalInstance.dismiss('cancel');
        };

        vm.setWizardState = function (state) {
            $state.go(vm.baseState + '.' + state);
        };

        vm.validateProceed = function (event, form) {
            angular.forEach(form.$error, function (field) {
                angular.forEach(field, function (errorField) {
                    if (angular.isDefined(errorField.$setTouched)) {
                        errorField.$setTouched();
                    }
                });
            });

            if (angular.isDefined(form.$error.required)) {
                angular.forEach(form.$error.required, function (field) {
                    angular.forEach(field, function (errorField) {
                        //errorField.$setTouched();
                        if (angular.isDefined(errorField) && errorField != null) {
                            angular.forEach(errorField.required, function (errorChildField) {
                                errorChildField.$setTouched();
                            });
                        }
                    });
                });
            }

            if (form.$invalid) {
                event.stopImmediatePropagation();
                event.preventDefault();
                return false;
            }
            return true;
        };

        vm.setSelectedQuestion = function (question) {
            vm.selectedQuestion = question;
        };

        vm.clearQuestion = function (question) {
            question.text = null;
            question.responseType = null;
            question.responseValue1 = null;
            question.responseValue2 = null;
            question.responseRangeMin = null;
            question.responseRangeMax = null;
        };

        function getFormattedTimeForDisplay(recTime) {
            var timeString = recTime;  //"07:54 PM"
            var n = timeString.indexOf(":");
            var timeHours = timeString.substr(0, 2);
            var timeMins = timeString.substr(n + 1, 2);
            var AMorPM = timeString.substr(n + 4, 2);
            if (AMorPM === "PM") {
                if (parseInt(timeHours) != 12 && parseInt(timeHours) >= 1) {
                    timeHours = (parseInt(timeHours) + 12).toString();
                }
            } else {
                if (parseInt(timeHours) === 12) {
                    timeHours = (parseInt(timeHours) - 12).toString();
                }
            }
            var newDate = moment({h: timeHours, m: timeMins, s: 0, ms: 0});
            var showDate = new Date(newDate);
            return showDate;
        }

        function initQuestions() {
            var questions = [];
            for (var i = 0; i < 5; i++) {
                questions[i] = {text: "", responseType: null};
            }

            if (angular.isDefined(entity.questions)) {
                for (var i = 0; i < entity.questions.length; i++) {
                    if (entity.questions[i].recTime1 != "") {  // required field always value must be there
                        entity.questions[i].recurrenceTime1 = getFormattedTimeForDisplay(entity.questions[i].recTime1); //).format('dddd, MMMM Do, YYYY h:mm:ss A');
                    }
                    if (entity.questions[i].recTime2 != "") {
                        entity.questions[i].recurrenceTime2 = getFormattedTimeForDisplay(entity.questions[i].recTime2);
                    } else {
                        entity.questions[i].recurrenceTime2 = null;
                    }
                    if (entity.questions[i].recTime3 != "") {
                        entity.questions[i].recurrenceTime3 = getFormattedTimeForDisplay(entity.questions[i].recTime3);
                    } else {
                        entity.questions[i].recurrenceTime3 = null;
                    }
                    questions[i] = entity.questions[i];
                }
            }
            return questions;
        }


        vm.clearAppointmentTime = function (question, time) {
            question[time] = null;
            // vm.validateQuestion(question);
        };
    }

})();
