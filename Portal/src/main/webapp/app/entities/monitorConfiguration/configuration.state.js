(function() {
    'use strict';

    angular
        .module('passApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('monitor-configuration', {
            parent: 'setup',
            url: '/monitor-configuration?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.monitorConfiguration.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitorConfiguration/configurations.html',
                    controller: 'MonitorConfigurationController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'name,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitorConfiguration');
                    $translatePartialLoader.addPart('monitorType');
                    $translatePartialLoader.addPart('watchDisplayType');
                    $translatePartialLoader.addPart('questionResponseType');
                    $translatePartialLoader.addPart('recurrencePattern');
                    $translatePartialLoader.addPart('recurrenceWeekday');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('monitor-configuration-detail', {
            parent: 'setup',
            url: '/monitor-configuration/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'passApp.monitorConfiguration.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-detail.html',
                    controller: 'MonitorConfigurationDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('monitorConfiguration');
                    $translatePartialLoader.addPart('monitorType');
                    $translatePartialLoader.addPart('watchDisplayType');
                    $translatePartialLoader.addPart('questionResponseType');
                    $translatePartialLoader.addPart('recurrencePattern');
                    $translatePartialLoader.addPart('recurrenceWeekday');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'MonitorConfiguration', function($stateParams, MonitorConfiguration) {
                    return MonitorConfiguration.get({id : $stateParams.id});
                }]
            }
        })
        .state('monitor-configuration.new', {
            parent: 'monitor-configuration',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog.html',
                    controller: 'MonitorConfigurationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        account: ['Principal', function (Principal) {
                            return Principal.identity();
                        }],
                        entity: [function () {
                            return {
                                id: null,
                                name: null,
                                monitorType: "PHb",
                                displayType: "DIGITAL",
                                displayBlinded: false,
                                mobileAppBlinded: false,
                                palSedentaryMin: 0,
                                palSedentaryMax: 2500,
                                palLightMin: 2501,
                                palLightMax: 10000,
                                palMediumMin: 10001,
                                palMediumMax: 33000,
                                palVigorousMin: 33001,
                                sedentaryAlertEnabled: true,
                                sedentaryAlert: 30,
                                targetSteps: 10000,
                                targetAee: 750
                            };
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitor-configuration', null, { reload: true, location: 'replace' });
                }, function() {
                    $state.go('monitor-configuration', null, { reload: true, location: 'replace' });
                });
            }]
        })
        .state('monitor-configuration.new.settings', {
            url: '/settings',
            views: {
                'wizard@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog-settings.html'

                }
            }
        })
        .state('monitor-configuration.new.questions', {
            url: '/questions',
            views: {
                'wizard@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog-questions.html'

                }
            }
        })
        .state('monitor-configuration.new.schedule', {
            url: '/schedule',
            views: {
                'wizard@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog-schedule.html'

                }
            }
        })
        .state('monitor-configuration-detail.edit', {
            parent: 'monitor-configuration-detail',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog.html',
                    controller: 'MonitorConfigurationDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['MonitorConfiguration', 'Principal', '$q', function(MonitorConfiguration, Principal, $q) {
                            var defer = $q.defer();
                            MonitorConfiguration.get( {id : $stateParams.id}, function( configuration ) {
                                configuration.companyId = Principal.currentCompany.id;
                                defer.resolve( configuration );
                            });
                            return defer.promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitor-configuration-detail', {id: $stateParams.id}, { reload: true });
                }, function() {
                    $state.go('monitor-configuration-detail', {id: $stateParams.id}, { reload: false });
                });
            }]
        })
        .state('monitor-configuration-detail.edit.settings', {
            url: '/settings',
            data: {
                currentStepIndex: 1
            },
            onEnter: ['$stateParams', function($stateParams)
            {
                $stateParams.currentStepIndex = 1;
            }],
            views: {
                'wizard@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog-settings.html'
                }
            }
        })
        .state('monitor-configuration-detail.edit.questions', {
            url: '/questions',
            data: {
                currentStepIndex: 2
            },
            onEnter: ['$stateParams', function($stateParams)
            {
                $stateParams.currentStepIndex = 2;
            }],
            views: {
                'wizard@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog-questions.html'
                }
            }
        })
        .state('monitor-configuration-detail.edit.schedule', {
            url: '/schedule',
            data: {
                currentStepIndex: 3
            },
            onEnter: ['$stateParams', function($stateParams)
            {
                $stateParams.currentStepIndex = 3;
            }],
            views: {
                'wizard@': {
                    templateUrl: 'app/entities/monitorConfiguration/configuration-dialog-schedule.html'
                }
            }
        })
        .state('monitor-configuration-detail.delete', {
            parent: 'monitor-configuration-detail',
            url: '/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/monitorConfiguration/configuration-delete-dialog.html',
                    controller: 'MonitorConfigurationDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['MonitorConfiguration', function(MonitorConfiguration) {
                            return MonitorConfiguration.get({id : $stateParams.id});
                        }]
                    }
                }).result.then(function() {
                    $state.go('monitor-configuration', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
