(function() {
    'use strict';
    angular
        .module('passApp')
        .factory('MonitorConfiguration', MonitorConfiguration);

    MonitorConfiguration.$inject = ['$resource', 'DateUtils', '$filter'];

    function MonitorConfiguration ($resource, DateUtils, $filter) {
        var resourceUrl =  'api/monitorConfigurations/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'list': {
                method: 'GET',
                url: 'api/monitorConfigurations/list',
                isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = data.replace(/null/gi, "\"\"");
                    var configuration = angular.fromJson(data);

                    angular.forEach(configuration.questions, function(object) {
                        object.recurrenceStartDate = DateUtils.convertLocalDateFromServer(object.recurrenceStartDate);
                        object.recurrenceEndDate = DateUtils.convertLocalDateFromServer(object.recurrenceEndDate);
                    });
                    configuration.questions = $filter('orderBy')(configuration.questions, 'id');

                    return configuration;
                },

            },
            'update': { method:'PUT' }
        });
    }
})();
