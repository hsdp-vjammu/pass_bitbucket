(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorConfigurationDeleteController',MonitorConfigurationDeleteController);

    MonitorConfigurationDeleteController.$inject = ['$uibModalInstance', 'entity', 'MonitorConfiguration'];

    function MonitorConfigurationDeleteController($uibModalInstance, entity, MonitorConfiguration) {
        var vm = this;
        vm.configuration = entity;
        vm.clear = function() {
            $uibModalInstance.dismiss('cancel');
        };
        vm.confirmDelete = function (id) {
            MonitorConfiguration.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        };
    }
})();
