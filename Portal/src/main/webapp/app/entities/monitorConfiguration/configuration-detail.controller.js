(function() {
    'use strict';

    angular
        .module('passApp')
        .controller('MonitorConfigurationDetailController', MonitorConfigurationDetailController);

    MonitorConfigurationDetailController.$inject = ['$rootScope', '$scope', '$state', 'entity'];

    function MonitorConfigurationDetailController($rootScope, $scope, $state, entity) {
        var vm = this;
        vm.configuration = entity;

        var unsubscribe = $rootScope.$on('passApp:monitorConfigurationUpdate', function(event, result) {
            vm.configuration = result;
        });
        $scope.$on('$destroy', unsubscribe);

        vm.navigateBack = function () {
            if ($rootScope.returnState.configuration) {
                $state.go($rootScope.returnState.configuration);
            } else {
                $state.go('monitor-configuration');
            }
        };

    }
})();
