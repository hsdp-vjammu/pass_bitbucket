/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.philips.respironics.pass.portal.web.rest.dto;
