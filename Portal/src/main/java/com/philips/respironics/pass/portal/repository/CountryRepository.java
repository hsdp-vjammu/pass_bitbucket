package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Country entity.
 */
@SuppressWarnings("unused")
public interface CountryRepository extends JpaRepository<Country,Long> {

    List<Country> findAllByDisplayLanguage(String language);

    Country findByAlpha3AndDisplayLanguage(String alpha3, String language);

}
