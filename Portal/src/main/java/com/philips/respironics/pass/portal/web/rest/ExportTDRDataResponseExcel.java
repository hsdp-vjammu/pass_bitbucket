package com.philips.respironics.pass.portal.web.rest;

import com.philips.respironics.pass.portal.service.util.ExcelExportUtil;
import com.philips.respironics.pass.portal.web.rest.dto.TDRDataDTO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.util.List;

public class ExportTDRDataResponseExcel extends ExcelExportUtil<TDRDataDTO> {
    @Override
    public void fillData(List<TDRDataDTO> dataList, int dataListStartingRowNum) {
        for (TDRDataDTO rowData : dataList) {
            Row row = sh.createRow(dataListStartingRowNum++);
            row.createCell(0)
                .setCellValue(rowData.getTimeStamp());
            Cell cell_1 = row.createCell(1, CellType.STRING);
                cell_1.setCellValue(rowData.getActivityCounts());
            row.createCell(2)
                .setCellValue(rowData.getHeartRate());
            row.createCell(3)
                .setCellValue(rowData.getRespirationRate());
            row.createCell(4)
                .setCellValue(rowData.getTotalEnergyExpenditure());
            row.createCell(5)
                .setCellValue(rowData.getActiveEnergyExpenditure());
            row.createCell(6)
                .setCellValue(rowData.getSteps());
            row.createCell(7)
                .setCellValue(rowData.getOffWrist());
            row.createCell(8)
                .setCellValue(rowData.getRun());
            row.createCell(9)
                .setCellValue(rowData.getWalk());
            row.createCell(10)
                .setCellValue(rowData.getCycle());
            row.createCell(11)
                .setCellValue(rowData.getActiveMinutes());
            row.createCell(12)
                .setCellValue(rowData.getRestingHeartRate());
            row.createCell(13)
                .setCellValue(rowData.getRecoveryHeartRate());
            row.createCell(14)
                .setCellValue(rowData.getCardioFitnessIndex());
            row.createCell(15)
                .setCellValue(rowData.getVo2Max());
        }
    }
}
