package com.philips.respironics.pass.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.philips.respironics.pass.portal.config.Constants;
import com.philips.respironics.pass.portal.domain.enumeration.Gender;
import com.philips.respironics.pass.portal.domain.enumeration.HeightUnit;
import com.philips.respironics.pass.portal.domain.enumeration.WeightUnit;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * A Subject.
 */
@Entity
@Table(name = "subject")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Subject extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.UUID_REGEX)
    @Size(min = 36, max = 36)
    @Column(name = "unique_identifier", nullable = false, updatable = false)
    private String uniqueIdentifier;

    @Column(name = "external_reference")
    private String externalReference;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "height")
    private Double height;

    @Column(name = "height_unit")
    @Enumerated(EnumType.STRING)
    private HeightUnit heightUnit;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "weight_unit")
    @Enumerated(EnumType.STRING)
    private WeightUnit weightUnit;

    @Column(name ="dominant_hand")
    private  String DominantHand;

    @Column(name="wearing_position")
    private String WearingPosition;

    @NotNull
    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "zipcode")
    private String zipcode;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "timezone")
    private String timeZone;


    @ManyToOne
    @NotNull
    private Study study;

    @ManyToOne
    private Site site;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "assignedSubject")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Monitor> assignedMonitors = new HashSet<>();

    public Subject() {
        this.uniqueIdentifier = UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public String getExternalReference() {
        return externalReference;
    }
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Double getHeight() {
        return height;
    }
    public void setHeight(Double height) {
        this.height = height;
    }

    public HeightUnit getHeightUnit() {
        return heightUnit;
    }
    public void setHeightUnit(HeightUnit unit) {
        this.heightUnit = unit;
    }

    public Double getWeight() {
        return weight;
    }
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }
    public void setWeightUnit(WeightUnit unit) {
        this.weightUnit = unit;
    }

    public String getDominantHand() {
        return DominantHand;
    }

    public void setDominantHand(String dominantHand) {
        DominantHand = dominantHand;
    }

    public String getWearingPosition() {
        return WearingPosition;
    }

    public void setWearingPosition(String wearingPosition) {
        WearingPosition = wearingPosition;
    }

    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getStreetAddress() {
        return streetAddress;
    }
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipcode;
    }
    public void setZipCode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getTimeZone() {
        return timeZone;
    }
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Study getStudy() {
        return study;
    }
    public void setStudy(Study study) {
        this.study = study;
    }

    public Site getSite() {
        return site;
    }
    public void setSite(Site site) {
        this.site = site;
    }

    public Set<Monitor> getAssignedMonitors() {
        return assignedMonitors;
    }
    public void setAssignedMonitors(Set<Monitor> monitors) {
        this.assignedMonitors = monitors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Subject subject = (Subject) o;
        if(subject.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, subject.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Subject{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", externalReference='" + externalReference + "'" +
            ", firstName='" + firstName + "'" +
            ", lastName='" + lastName + "'" +
            ", dateOfBirth='" + dateOfBirth + "'" +
            ", weight='" + weight + "'" +
            ", height='" + height + "'" +
            ", gender='" + gender + "'" +
            ", streetAddress='" + streetAddress + "'" +
            ", city='" + city + "'" +
            ", state='" + state + "'" +
            ", country='" + country + "'" +
            ", phoneNumber='" + phoneNumber + "'" +
            ", dominantHand='" + DominantHand + "'" +
            ", wearingPosition='" + WearingPosition + "'" +
            '}';
    }
}
