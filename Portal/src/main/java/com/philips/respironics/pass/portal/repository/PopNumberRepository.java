package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.PopNumber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.Optional;

/**
 * Spring Data JPA repository for the PopNumber entity.
 */
@SuppressWarnings("unused")
public interface PopNumberRepository extends JpaRepository<PopNumber,Long>, JpaSpecificationExecutor<PopNumber> {

    Optional<PopNumber> findOneByNumber(String number);

    Page<PopNumber> findAllByNumberContainingIgnoreCase(String number, Pageable pageable);

    Long countByNumberAndUserNull(String number);
   PopNumber findPopNumberByUser(String email);
}
