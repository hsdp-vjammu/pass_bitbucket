package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Monitor;
import com.philips.respironics.pass.portal.domain.MonitorHistory;
import com.philips.respironics.pass.portal.domain.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the MonitorHistory entity.
 */

public interface MonitorHistoryRepository extends JpaRepository<MonitorHistory, Long> {
    List<MonitorHistory> findAllByMonitor(Monitor monitor);
    List<MonitorHistory> findDistinctByAssignedSubject(Subject assignedSubject);
    List<MonitorHistory> findDistinctByAssignedSubjectAndSerialNumber(Subject assignedSubject, String serialNumber);
}
