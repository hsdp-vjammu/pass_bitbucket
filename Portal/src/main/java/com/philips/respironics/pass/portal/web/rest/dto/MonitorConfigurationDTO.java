package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Monitor;
import com.philips.respironics.pass.portal.domain.MonitorConfiguration;
import com.philips.respironics.pass.portal.domain.MonitorConfigurationQuestion;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.domain.enumeration.WatchDisplayType;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the MonitorConfiguration entity.
 */
public class MonitorConfigurationDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String uniqueIdentifier;

    @NotNull
    @Size(min = 4, max = 25)
    private String name;

    @NotNull
    private MonitorType monitorType;

    @NotNull
    private WatchDisplayType displayType;

    @NotNull
    private boolean displayBlinded = false;

    @NotNull
    private boolean mobileAppBlinded = false;

    private Integer palSedentaryMin;
    private Integer palSedentaryMax;
    private Integer palLightMin;
    private Integer palLightMax;
    private Integer palMediumMin;
    private Integer palMediumMax;
    private Integer palVigorousMin;
    private Integer sedentaryAlert;
    @NotNull
    private boolean sedentaryAlertEnabled = true;

    private Integer targetSteps;
    private Integer targetAee;

    private Long companyId;

    private Set<MonitorConfigurationQuestionDTO> questions = new HashSet<>();

    private Set<MonitorDTO> monitors = new HashSet<>();

    public MonitorConfigurationDTO() { }

    public MonitorConfigurationDTO(MonitorConfiguration entity) {
        this.id = entity.getId();
        this.uniqueIdentifier = entity.getUniqueIdentifier();
        this.name = entity.getName();
        this.monitorType = entity.getMonitorType();
        this.displayType = entity.getDisplayType();
        this.displayBlinded = entity.getDisplayBlinded();
        this.mobileAppBlinded = entity.getMobileAppBlinded();
        this.palSedentaryMin = entity.getPalSedentaryMin();
        this.palSedentaryMax = entity.getPalSedentaryMax();
        this.palLightMin = entity.getPalLightMin();
        this.palLightMax = entity.getPalLightMax();
        this.palMediumMin = entity.getPalMediumMin();
        this.palMediumMax = entity.getPalMediumMax();
        this.palVigorousMin = entity.getPalVigorousMin();
        this.sedentaryAlert = entity.getSedentaryAlert();
        this.sedentaryAlertEnabled = entity.isSedentaryAlertEnabled();
        this.targetSteps = entity.getTargetSteps();
        this.targetAee = entity.getTargetAee();
        if (entity.getCompany() != null) {
            this.companyId = entity.getCompany().getId();
        }

        for (MonitorConfigurationQuestion monitorConfigurationQuestionEntity : entity.getQuestions()) {
            this.questions.add(new MonitorConfigurationQuestionDTO(monitorConfigurationQuestionEntity));
        }

        for (Monitor monitorEntity : entity.getMonitors()) {
            this.monitors.add(new MonitorDTO(monitorEntity));
        }

        this.setCreatedDate(entity.getCreatedDate());
        this.setCreatedBy(entity.getCreatedBy());
        this.setLastModifiedDate(entity.getLastModifiedDate());
        this.setLastModifiedBy(entity.getLastModifiedBy());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }
    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public MonitorType getMonitorType() {
        return monitorType;
    }
    public void setMonitorType(MonitorType monitorType) {
        this.monitorType = monitorType;
    }

    public WatchDisplayType getDisplayType() {
        return displayType;
    }
    public void setDisplayType(WatchDisplayType displayType) {
        this.displayType = displayType;
    }

    public boolean getDisplayBlinded() {
        return displayBlinded;
    }
    public void setDisplayBlinded(boolean blinded) {
        this.displayBlinded = blinded;
    }

    public boolean getMobileAppBlinded() {
        return mobileAppBlinded;
    }
    public void setMobileAppBlinded(boolean blinded) {
        this.mobileAppBlinded = blinded;
    }

    public Integer getPalSedentaryMax() {
        return palSedentaryMax;
    }
    public void setPalSedentaryMax(Integer value) {
        this.palSedentaryMax = value;
    }

    public Integer getPalSedentaryMin() {
        return palSedentaryMin;
    }
    public void setPalSedentaryMin(Integer value) {
        this.palSedentaryMin = value;
    }

    public Integer getPalLightMin() {
        return palLightMin;
    }
    public void setPalLightMin(Integer value) {
        this.palLightMin = value;
    }

    public Integer getPalLightMax() {
        return palLightMax;
    }
    public void setPalLightMax(Integer value) {
        this.palLightMax = value;
    }

    public Integer getPalMediumMin() {
        return palMediumMin;
    }
    public void setPalMediumMin(Integer value) {
        this.palMediumMin = value;
    }

    public Integer getPalMediumMax() {
        return palMediumMax;
    }
    public void setPalMediumMax(Integer value) {
        this.palMediumMax = value;
    }

    public Integer getPalVigorousMin() {
        return palVigorousMin;
    }
    public void setPalVigorousMin(Integer value) {
        this.palVigorousMin = value;
    }

    public boolean isSedentaryAlertEnabled() { return sedentaryAlertEnabled;   }
    public void setSedentaryAlertEnabled(boolean sedentaryAlertEnabled) { this.sedentaryAlertEnabled = sedentaryAlertEnabled; }

    public Integer getSedentaryAlert() { return sedentaryAlert;  }
    public void setSedentaryAlert(Integer value) {
        this.sedentaryAlert = value;
    }

    public Integer getTargetSteps() {
        return targetSteps;
    }
    public void setTargetSteps(Integer value) {
        this.targetSteps = value;
    }

    public Integer getTargetAee() {
        return targetAee;
    }
    public void setTargetAee(Integer value) {
        this.targetAee = value;
    }

    public Long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Set<MonitorConfigurationQuestionDTO> getQuestions() { return questions; }
    public void setQuestions(Set<MonitorConfigurationQuestionDTO> questions) { this.questions = questions; }

    public Set<MonitorDTO> getMonitors() { return monitors; }
    public void setMonitors(Set<MonitorDTO> monitors) { this.monitors = monitors; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MonitorConfigurationDTO monitorConfigurationDTO = (MonitorConfigurationDTO) o;

        if ( ! Objects.equals(id, monitorConfigurationDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MonitorConfigurationDTO{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", name='" + name + "'" +
            ", monitorType='" + monitorType + "'" +
            '}';
    }

    public static List<MonitorConfigurationDTO> convert(List<MonitorConfiguration> entities)
    {
        List<MonitorConfigurationDTO> dtos = new ArrayList<>();
        for (MonitorConfiguration entity : entities) {
            dtos.add(new MonitorConfigurationDTO(entity));
        }
        return dtos;
    }

}
