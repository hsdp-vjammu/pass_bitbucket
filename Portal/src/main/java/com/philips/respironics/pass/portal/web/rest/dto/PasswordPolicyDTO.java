package com.philips.respironics.pass.portal.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.philips.respironics.pass.portal.domain.Setting;

import java.util.List;

public class PasswordPolicyDTO {

    private int passwordMinLength;

    private int maxPasswordAge;

    private int passwordExpirationThreshold;

    private int sessionTimeout;

    private int failedLoginAttempts;

    private int failedLoginAttemptsPeriod;

    @JsonCreator
    public PasswordPolicyDTO() {
    }

    public int getPasswordMinLength() {
        return passwordMinLength;
    }
    public void setPasswordMinLength(int length) {
        this.passwordMinLength = length;
    }

    public int getMaxPasswordAge() { return maxPasswordAge; }
    public void setMaxPasswordAge(int maxAge) {
        this.maxPasswordAge = maxAge;
    }

    public int getPasswordExpirationThreshold() {
        return passwordExpirationThreshold;
    }
    public void setPasswordExpirationThreshold(int threshold) {
        this.passwordExpirationThreshold = threshold;
    }

    public int getSessionTimeout() {
        return sessionTimeout;
    }
    public void setSessionTimeout(int sessionTimeout) {
        this.sessionTimeout = sessionTimeout;
    }

    public int getFailedLoginAttempts() {
        return failedLoginAttempts;
    }
    public void setFailedLoginAttempts(int failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }

    public int getFailedLoginAttemptsPeriod() {
        return failedLoginAttemptsPeriod;
    }
    public void setFailedLoginAttemptsPeriod(int failedLoginAttemptsPeriod) {
        this.failedLoginAttemptsPeriod = failedLoginAttemptsPeriod;
    }

    public void loadPasswordPolicies(List<Setting> passwordPolicySettings) {
        for (Setting setting : passwordPolicySettings) {
            if (Setting.PASSWORD_POLICY.CATEGORY.equals(setting.getCategory())) {
                switch (setting.getName()) {
                    case Setting.PASSWORD_POLICY.MIN_PASSWORD_LENGTH:
                        this.setPasswordMinLength(Integer.parseInt(setting.getValue()));
                        break;
                    case Setting.PASSWORD_POLICY.MAX_PASSWORD_AGE:
                        this.setMaxPasswordAge(Integer.parseInt(setting.getValue()));
                        break;
                    case Setting.PASSWORD_POLICY.PASSWORD_EXPIRATION_THRESHHOLD:
                        this.setPasswordExpirationThreshold(Integer.parseInt(setting.getValue()));
                        break;
                }
            }
        }
    }

    public void loadSessionManagement(List<Setting> sessionManagementSettings) {
        for (Setting setting : sessionManagementSettings) {
            if (Setting.SESSION_MANAGEMENT.CATEGORY.equals(setting.getCategory())) {
                switch (setting.getName()) {
                    case Setting.SESSION_MANAGEMENT.SESSION_TIMEOUT:
                        this.setSessionTimeout(Integer.parseInt(setting.getValue()));
                        break;
                    case Setting.SESSION_MANAGEMENT.FAILED_LOGIN_ATTEMPTS:
                        this.setFailedLoginAttempts(Integer.parseInt(setting.getValue()));
                        break;
                    case Setting.SESSION_MANAGEMENT.FAILED_LOGIN_ATTEMPTS_PERIOD:
                        this.setFailedLoginAttemptsPeriod(Integer.parseInt(setting.getValue()));
                        break;
                }
            }
        }
    }

}
