package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.PopNumber;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PopNumberSearchSpecificationBuilder {

    public static Specification<PopNumber> build(Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
        }
        return build(criteria);
    }

    public static Specification<PopNumber> build(List<SearchParameter> searchCriteria) {

        List<Specification<PopNumber>> specs = new ArrayList<>();

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else {
                specs.add(PopNumberSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<PopNumber> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<PopNumber> buildGlobalSearch(Object value) {
        List<Specification<PopNumber>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("number", value));
        specs.add(createLikeSpecification("user", value));

        Specification<PopNumber> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<PopNumber> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<PopNumber> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

}
