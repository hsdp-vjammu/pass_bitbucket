package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.Study;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Study entity.
 */
@SuppressWarnings("unused")
public interface StudyRepository extends JpaRepository<Study,Long>, JpaSpecificationExecutor<Study> {

    Long countByExternalReference(String externalReference);

    List<Study> findAllByCompany(Company company);
    List<Study> findAllByCompanyId(long id);

    Page<Study> findAllByCompany(Company company, Pageable var1);

    Long countByCompanyAndNameIgnoreCaseAndIdNot(Company company, String name, Long id);

}
