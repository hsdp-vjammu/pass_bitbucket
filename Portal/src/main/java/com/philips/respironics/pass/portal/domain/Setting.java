package com.philips.respironics.pass.portal.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Setting.
 */
@Entity
@Table(name = "setting")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Setting extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "category", nullable = false, updatable = false)
    private String category;

    @NotNull
    @Column(name = "name", nullable = false, updatable = false)
    private String name;

    @NotNull
    @Column(name = "value", nullable = false)
    private String value;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Setting setting = (Setting) o;
        if(setting.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, setting.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Setting{" +
            "id=" + id +
            ", category='" + category + "'" +
            ", name='" + name + "'" +
            ", value='" + value + "'" +
            '}';
    }

    public static class PASSWORD_POLICY {
        public static final String CATEGORY = "password-policy";

        public static final String MIN_PASSWORD_LENGTH = "min-password-length";
        public static final String MAX_PASSWORD_AGE = "max-password-age";
        public static final String PASSWORD_EXPIRATION_THRESHHOLD = "password-expiration-threshhold";
        public static final String PASSWORD_HISTORY = "password-history";

    }

    public static class SESSION_MANAGEMENT {
        public static final String CATEGORY = "session-management";

        public static final String SESSION_TIMEOUT = "session-timeout";
        public static final String FAILED_LOGIN_ATTEMPTS = "failed-login-attempts";
        public static final String FAILED_LOGIN_ATTEMPTS_PERIOD = "failed-login-attempts-period";
    }
}
