package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.repository.*;
import com.philips.respironics.pass.portal.security.AccountLockService;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.security.SecurityUtils;
import com.philips.respironics.pass.portal.service.util.RandomUtil;
import com.philips.respironics.pass.portal.web.rest.dto.ManagedUserDTO;
import com.philips.respironics.pass.portal.web.rest.dto.MembershipAccessDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Inject
    private SocialService socialService;

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private SettingRepository settingRepository;

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private MembershipAccessRepository membershipAccessRepository;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private AuthorityRepository authorityRepository;

    @Inject
    private MembershipRepository membershipRepository;

    @Inject
    private CompanyRepository companyRepository;

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private AccountLockService accountLockService;

    public Optional<User> activateRegistration(String key, String password) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                if (StringUtils.isNotBlank(password)) {
                    user.setPassword(passwordEncoder.encode(password));
                }
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);

        return userRepository.findOneByResetKey(key)
            .filter(user -> {
                ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
                return user.getResetDate().isAfter(oneDayAgo);
            })
            .map(user -> {
                String encryptedPassword = passwordEncoder.encode(newPassword);

                int passwordHistory = 0;
                Setting passwordHistorySetting = settingRepository.findOneByCategoryAndName(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.PASSWORD_HISTORY);
                try {
                    if (passwordHistorySetting != null) {
                        passwordHistory = Integer.parseInt(passwordHistorySetting.getValue());
                    }
                } catch (NumberFormatException ex) {
                    passwordHistory = 0;
                }

                user.updatePassword(encryptedPassword, passwordHistory);

                user.setResetKey(null);
                user.setResetDate(null);
                user.setLocked(null);
                user = userRepository.save(user);

                accountLockService.clearLock(user.getLogin());
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(ZonedDateTime.now());
                userRepository.save(user);
                return user;
            });
    }

    public User createUserInformation(String firstName, String lastName,
                                      String email, String password, String langKey) {

        User newUser = new User();
        Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<Authority> authorities = new HashSet<>();
        //String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(email);
        // new user gets initially a generated password
        //newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setPassword(encryptedPassword);

        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    public User createUser(ManagedUserDTO managedUserDTO) {
        User user = new User();
        Set<String> sitesList = managedUserDTO.getSites();  //siteList is referring to external referance not site ID

        user.setLogin(managedUserDTO.getLogin());
        user.setFirstName(managedUserDTO.getFirstName());
        user.setLastName(managedUserDTO.getLastName());
        user.setEmail(managedUserDTO.getEmail());
        if (managedUserDTO.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(managedUserDTO.getLangKey());
        }
        if (managedUserDTO.getAuthorities() != null) {
            Set<Authority> authorities = new HashSet<>();
            managedUserDTO.getAuthorities().stream().forEach(
                authority -> authorities.add(authorityRepository.findOne(authority))
            );
            user.setAuthorities(authorities);
        }

        //if sitesList is empty don't persist any company user access data/membership access data
        if (!sitesList.isEmpty()) {
            Set<MembershipAccess> membershipAccesses = new HashSet<>();
            Membership membership = new Membership();
            final Company[] company = {new Company()};

            for (String newSite : sitesList) {
                if (managedUserDTO.getMemberships() != null && !managedUserDTO.getMemberships().isEmpty()) {
                    managedUserDTO.getMemberships().stream().forEach(membershipDTO -> {
                        company[0] = companyRepository.findOne(membershipDTO.getCompany().getId());
                        membership.setCompany(company[0]);
                        membership.setUser(user);

                        for (MembershipAccessDTO accessLevelDTO : membershipDTO.getAccessLevels()) {
                            MembershipAccess accessLevel = new MembershipAccess();
                            accessLevel.setMembership(membership);
                            accessLevel.setAccessLevel(accessLevelDTO.getAccessLevel());  //ACL_SITE COORDINATOR
                            //for sites part of membership access, which have site ID
                            if (accessLevelDTO.getSiteId() != null) {
                                Site site = siteRepository.findOne(accessLevelDTO.getSiteId());
                                accessLevel.setSite(site);
                            }
                            Study study = studyRepository.findOne(accessLevelDTO.getStudyId());  //check study present
                            Optional<Site> site = siteRepository.findOneByStudyAndExternalReference(study, newSite);  //check site present
                            if (site.isPresent()) {
                                accessLevel.setSite(site.get());
                                site.get().getMembershipAccessLevels().add(accessLevel);
                            } else {
                                //no site found - do nothing
                            }
                            //add site only when new one added, not null sites
                            if (accessLevel.getSite() != null) {
                                membershipAccesses.add(accessLevel);
                            }
                        }
                    });
                }
            }  //iterating thru site list and adding them to membership
            membershipAccesses.stream().forEach(
                memberAccessLevel -> membership.getAccessLevels().add(memberAccessLevel)
            );
            user.getMemberships().add(membership);
            company[0].getMemberships().add(membership);
        } else {
            //to handle user roles, admin adding clinician
            handleUserRoleChanges(managedUserDTO, user);
        }
        // new user is not active
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setActivationKey(RandomUtil.generateActivationKey());
        user.setActivated(false);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    private void handleUserRoleChanges(ManagedUserDTO managedUserDTO, User user) {
        if (managedUserDTO.getMemberships() != null && !managedUserDTO.getMemberships().isEmpty()) {
            managedUserDTO.getMemberships().stream().forEach(membershipDTO -> {
                Company company = companyRepository.findOne(membershipDTO.getCompany().getId());
                Membership membership = new Membership();
                membership.setCompany(company);
                membership.setUser(user);

                for (MembershipAccessDTO accessLevelDTO : membershipDTO.getAccessLevels()) {
                    MembershipAccess accessLevel = new MembershipAccess();
                    accessLevel.setMembership(membership);
                    accessLevel.setAccessLevel(accessLevelDTO.getAccessLevel());
                    if (accessLevelDTO.getSiteId() != null) {
                        Site site = siteRepository.findOne(accessLevelDTO.getSiteId());
                        accessLevel.setSite(site);
                    }
                    membership.getAccessLevels().add(accessLevel);
                }

                user.getMemberships().add(membership);
                company.getMemberships().add(membership);
            });
        }
    }


    public void updateUserInformation(String firstName, String lastName, String email, String langKey) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
            u.setFirstName(firstName);
            u.setLastName(lastName);
            u.setEmail(email);
            u.setLangKey(langKey);
            userRepository.save(u);
            log.debug("Changed Information for User: {}", u);
        });
    }

    public void deleteUserInformation(String login) {
        userRepository.findOneByLogin(login).ifPresent(u -> {
            socialService.deleteUserSocialConnection(u.getLogin());

            deleteUser(u);
            log.debug("Deleted User: {}", u);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
            String encryptedPassword = passwordEncoder.encode(password);

            int passwordHistory = 0;
            Setting passwordHistorySetting = settingRepository.findOneByCategoryAndName(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.PASSWORD_HISTORY);
            try {
                if (passwordHistorySetting != null) {
                    passwordHistory = Integer.parseInt(passwordHistorySetting.getValue());
                }
            } catch (NumberFormatException ex) {
                passwordHistory = 0;
            }

            u.updatePassword(encryptedPassword, passwordHistory);
            userRepository.save(u);
            log.debug("Changed password for User: {}", u);
        });
    }

    public String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }

    public boolean matchPassword(String rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            loadUserDetails(u);
            return u;
        });
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities(Long id) {
        User user = userRepository.findOne(id);
        loadUserDetails(user);
        return user;
    }

    @Transactional(readOnly = true)
    public User getUserWithPasswordHistory(Long id) {
        User user = userRepository.findOne(id);
        user.getHistoricPasswords().size();
        return user;
    }

    @Transactional(readOnly = true)
    public Page<User> getAllUsersWithParams(Map<String, String> searchParams, boolean cidIsPresent, Long cid, Pageable pageable) {
        String emailVal = "";
        String statusVal = "";
        String lastNameParam = "";
        String companyParam = "";
        if (searchParams != null && !searchParams.isEmpty()) {
            for (Map.Entry<String, String> entry : searchParams.entrySet()) {
                if ("email".equalsIgnoreCase(entry.getKey())) {
                    emailVal = entry.getValue();
                } else if ("status".equalsIgnoreCase(entry.getKey())) {
                    statusVal = entry.getValue();
                } else if ("lastName".equalsIgnoreCase(entry.getKey())) {
                    lastNameParam = entry.getValue();
                } else if ("company".equalsIgnoreCase(entry.getKey())) {
                    companyParam = entry.getValue();
                }
            }
        }
        String searchWithEmail = "%" + emailVal + "%";
        String searchWithLastName = "%" + lastNameParam + "%";
        String searchWithCompany = "%" + companyParam + "%";

        boolean activated = false;
        boolean enabled = false;
        if (!statusVal.isEmpty()) {
            if ("Active".equalsIgnoreCase(statusVal)) {
                activated = true;
                enabled = true;
            } else if ("InActive".equalsIgnoreCase(statusVal)) {
                activated = true;
                enabled = false;
            } else if ("Locked".equalsIgnoreCase(statusVal)) {
                activated = true;
            } else if ("NOT_VALIDATED".equalsIgnoreCase(statusVal)) {
                activated = false;
            }
        }

        if ("Locked".equalsIgnoreCase(statusVal)) {
            if (cidIsPresent) {
                return userRepository.findAllUsersWithStatusLocked(searchWithEmail, searchWithLastName, searchWithCompany, activated, cid, pageable);
            } else {
                return userRepository.findAllProdSupportUsersWithStatusLocked(searchWithEmail, searchWithLastName, searchWithCompany, activated, pageable);
            }
        } else if ("NOT_VALIDATED".equalsIgnoreCase(statusVal)) {
            if (cidIsPresent) {
                return userRepository.findAllWithStatusUnverified(searchWithEmail, searchWithLastName, searchWithCompany, activated, cid, pageable);
            } else {
                return userRepository.findAllProdSupportWithStatusUnverified(searchWithEmail, searchWithLastName, searchWithCompany, activated, pageable);
            }
        } else if ("Active".equalsIgnoreCase(statusVal) || "InActive".equalsIgnoreCase(statusVal)) {
            if (cidIsPresent) {
                return userRepository.findAllUsersWithStatusActiveOrInActive(searchWithEmail, searchWithLastName, searchWithCompany, activated, enabled, cid, pageable);
            } else {
                return userRepository.findAllProdSupportUsersWithStatusActiveOrInActive(searchWithEmail, searchWithLastName, searchWithCompany, activated, enabled, pageable);
            }
        } else {
            if (cidIsPresent) {
                return userRepository.findAllUsersWithParams(searchWithEmail, searchWithLastName, searchWithCompany, cid, pageable);
            } else {
                return userRepository.findAllProdSupportUsersWithParams(searchWithEmail, searchWithLastName, searchWithCompany, pageable);
            }
        }
    }

    /*
        To address Admin user call
     */
    @Transactional(readOnly = true)
    public Page<User> getAllUsers(Pageable pageable) {
        return userRepository.findAllUsers(pageable);
    }

    /*
        To address non-admin user call
     */
    @Transactional(readOnly = true)
    public Page<User> getAllUsersWithCompany(Pageable pageable, Long cid) {
        return userRepository.findAllUsersWithCompany(pageable, cid);
    }

    @Transactional(readOnly = true)
    public User getUserWithAuthorities() {
        User user = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
        loadUserDetails(user);
        return user;
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithPasswordHistoryByLogin(String login) {
        return userRepository.findOneByLogin(login).map(u -> {
            u.getHistoricPasswords().size(); // eagerly load the association
            return u;
        });
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithPasswordHistoryByResetKey(String key) {
        return userRepository.findOneByResetKey(key).map(u -> {
            u.getHistoricPasswords().size(); // eagerly load the association
            return u;
        });
    }

    private void loadUserDetails(User user) {
        user.getAuthorities().size(); // eagerly load the association
        user.getMemberships().size();
    }

    /**
     * Persistent Token are used for providing automatic authentication, they should be automatically deleted after
     * 30 days.
     * <p>
     * This is scheduled to get fired everyday, at midnight.
     * </p>
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void removeOldPersistentTokens() {
        LocalDate now = LocalDate.now();
        persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).stream().forEach(token -> {
            log.debug("Deleting token {}", token.getSeries());
            User user = token.getUser();
            user.getPersistentTokens().remove(token);
            persistentTokenRepository.delete(token);
        });
    }

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        ZonedDateTime now = ZonedDateTime.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            deleteUser(user);
        }
    }

    private void deleteUser(User u) {
        Set<Membership> memberships = new HashSet<>(u.getMemberships());
        for (Membership membership : memberships) {
            u.getMemberships().remove(membership);
            membership.getCompany().getMemberships().remove(membership);
            membershipRepository.delete(membership);
        }
        userRepository.delete(u);
    }
}
