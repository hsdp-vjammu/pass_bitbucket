package com.philips.respironics.pass.portal.web.rest.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the creating sites in bulk.
 */
public class SiteBulkCreateDTO implements Serializable {

    @NotNull
    private Long studyId;

    private String[] references;

    public Long getStudyId() {
        return studyId;
    }
    public void setStudyId(Long studyId) {
        this.studyId = studyId;
    }

    public String[] getReferences() {
        return references;
    }
    public void setReferences(String[] references) {
        this.references = references;
    }

}
