package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The QuestionResponseType enumeration.
 */
public enum QuestionResponseType {
    NONE(null),
    EITHER_OR("EITHER_OR"),
    RANGE("RANGE"),
    FREE_TEXT("FREE_TEXT");

    private final String text;

    QuestionResponseType(final String text) {
        this.text = text;
    }

    public static QuestionResponseType fromString(String text) {
        if (text != null) {
            for (QuestionResponseType b : QuestionResponseType.values()) {
                if (text.equalsIgnoreCase(b.text)) {
                    return b;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
}
