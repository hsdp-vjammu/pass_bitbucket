package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Monitor;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the MonitorAssigment.
 */
public class MonitorAssignmentDTO implements Serializable {

    @NotNull
    private Long monitorId;

    private String monitorSerialNumber;

    @NotNull
    private Long configurationId;

    private String configurationName;

    @NotNull
    private Long subjectId;

    private String subjectExternalReference;

    private String subjectFirstName;

    private String subjectLastName;

    private Long studyId;

    private String studyExternalReference;

    private StudyStatus studyStatus;

    private String studyName;

    private Long siteId;

    private String siteReference;

    public MonitorAssignmentDTO() {}

    public MonitorAssignmentDTO(Monitor entity) {
        this.monitorId = entity.getId();
        this.monitorSerialNumber = entity.getSerialNumber();
        if (entity.getConfiguration() != null) {
            this.configurationId = entity.getConfiguration().getId();
            this.configurationName = entity.getConfiguration().getName();
        }
        if (entity.getAssignedSubject() != null) {
            this.subjectId = entity.getAssignedSubject().getId();
            this.subjectExternalReference = entity.getAssignedSubject().getExternalReference();
            this.subjectFirstName = entity.getAssignedSubject().getFirstName();
            this.subjectLastName = entity.getAssignedSubject().getLastName();
            if (entity.getAssignedSubject().getStudy() != null) {
                this.studyId = entity.getAssignedSubject().getStudy().getId();
                this.studyExternalReference = entity.getAssignedSubject().getStudy().getExternalReference();
                this.studyStatus = entity.getAssignedSubject().getStudy().getStatus();
                this.studyName = entity.getAssignedSubject().getStudy().getName();
            }
            if (entity.getAssignedSubject().getSite() != null) {
                this.siteId = entity.getAssignedSubject().getSite().getId();
                this.siteReference = entity.getAssignedSubject().getSite().getExternalReference();
            }
        }
    }

    public Long getMonitorId() {
        return monitorId;
    }
    public void setMonitorId(Long monitorId) {
        this.monitorId = monitorId;
    }

    public String getMonitorSerialNumber() {
        return monitorSerialNumber;
    }
    public void setMonitorSerialNumber(String monitorSerialNumber) {
        this.monitorSerialNumber = monitorSerialNumber;
    }

    public Long getConfigurationId() {
        return configurationId;
    }
    public void setConfigurationId(Long configurationId) {
        this.configurationId = configurationId;
    }

    public String getConfigurationName() {
        return configurationName;
    }
    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }

    public Long getSubjectId() {
        return subjectId;
    }
    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectExternalReference() {
        return subjectExternalReference;
    }
    public void setSubjectExternalReference(String subjectExternalReference) {
        this.subjectExternalReference = subjectExternalReference;
    }

    public String getSubjectFirstName() {
        return subjectFirstName;
    }
    public void setSubjectFirstName(String subjectFirstName) {
        this.subjectFirstName = subjectFirstName;
    }

    public String getSubjectLastName() {
        return subjectLastName;
    }
    public void setSubjectLastName(String subjectLastName) {
        this.subjectLastName = subjectLastName;
    }

    public Long getStudyId() {
        return studyId;
    }
    public void setStudyId(Long studyId) {
        this.studyId = studyId;
    }

    public String getStudyExternalReference() {
        return studyExternalReference;
    }
    public void setStudyExternalReference(String studyExternalReference) {
        this.studyExternalReference = studyExternalReference;
    }

    public StudyStatus getStudyStatus() {
        return studyStatus;
    }
    public void setStudyStatus(StudyStatus studyStatus) {
        this.studyStatus = studyStatus;
    }

    public String getStudyName() {
        return studyName;
    }
    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public Long getSiteId() {
        return siteId;
    }
    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public String getSiteReference() {
        return siteReference;
    }
    public void setSiteReference(String siteReference) {
        this.siteReference = siteReference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MonitorAssignmentDTO monitorDTO = (MonitorAssignmentDTO) o;

        if ( ! Objects.equals(monitorId, monitorDTO.monitorId)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(monitorId);
    }

    @Override
    public String toString() {
        return "MonitorDTO{" +
            "monitorId=" + monitorId +
            ", monitorSerialNumber='" + monitorSerialNumber + "'" +
            ", subjectId='" + subjectId + "'" +
            '}';
    }

    public static List<MonitorAssignmentDTO> convert(List<Monitor> entities)
    {
        List<MonitorAssignmentDTO> dtos = new ArrayList<>();
        for (Monitor entity : entities) {
            dtos.add(new MonitorAssignmentDTO(entity));
        }
        return dtos;
    }
}
