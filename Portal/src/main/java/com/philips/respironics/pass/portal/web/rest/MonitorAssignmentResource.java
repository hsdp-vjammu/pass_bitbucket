package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.search.MonitorAssignmentSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.MonitorConfigurationRepository;
import com.philips.respironics.pass.portal.repository.MonitorHistoryRepository;
import com.philips.respironics.pass.portal.repository.MonitorRepository;
import com.philips.respironics.pass.portal.repository.SubjectRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.InvalidMonitorStatusException;
import com.philips.respironics.pass.portal.service.MonitorAssignmentService;
import com.philips.respironics.pass.portal.service.MonitorService;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.MonitorAssignmentDTO;
import com.philips.respironics.pass.portal.web.rest.dto.MonitorDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing MonitorAssignment.
 */
@RestController
@RequestMapping("/api")
public class MonitorAssignmentResource {

    private final Logger log = LoggerFactory.getLogger(MonitorAssignmentResource.class);

    @Inject
    private MonitorRepository monitorRepository;

    @Inject
    private MonitorService monitorService;

    @Inject
    private MonitorHistoryRepository monitorHistoryRepository;

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private MonitorConfigurationRepository monitorConfigurationRepository;

    @Inject
    private MonitorAssignmentService monitorAssignmentService;

    @Inject
    private UserService userService;

    /**
     * POST  /monitorAssignments : Create new a monitor assignment.
     *
     * @param monitorAssignmentDTO the monitor assignment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new monitorDTO, or with status 400 (Bad Request) if the monitor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitorAssignments",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> createMonitorAssignment(@Valid @RequestBody MonitorAssignmentDTO monitorAssignmentDTO) throws URISyntaxException {
        log.debug("REST request to save monitor assignment:  {}", monitorAssignmentDTO);

        Monitor monitor = monitorRepository.findOne(monitorAssignmentDTO.getMonitorId());
        Subject subject = subjectRepository.findOne(monitorAssignmentDTO.getSubjectId());
        MonitorConfiguration configuration = monitorConfigurationRepository.findOne(monitorAssignmentDTO.getConfigurationId());
        if (monitor != null && subject != null && configuration != null) {
            User user = userService.getUserWithAuthorities();
            for (Membership membership : user.getMemberships()) {
                if (membership.getCompany().getId() == monitor.getCompany().getId()
                    && membership.getCompany().getId() == configuration.getCompany().getId()
                    && membership.getCompany().getId() == subject.getStudy().getCompany().getId()) {

                    monitorAssignmentService.updateAssignment(monitor, subject, configuration, monitorAssignmentDTO.getSiteReference());

                    return ResponseEntity.created(new URI("/api/monitorAssignments/"))
                        .headers(HeaderUtil.createEntityCreationAlert("monitorAssignment", monitor.getSerialNumber()))
                        .body(null);
                }
            }
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("monitorAssignment", "invalidState", "MonitorAssignment is invalid state")).body(null);
    }

    /**
     * PUT  /monitorAssignments : Updates an existing monitor assignment.
     *
     * @param monitorAssignmentDTO the monitorAssignmentDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated monitorDTO,
     * or with status 400 (Bad Request) if the monitorDTO is not valid,
     * or with status 500 (Internal Server Error) if the monitorDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitorAssignments",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MonitorDTO> updateMonitorAssignment(@Valid @RequestBody MonitorAssignmentDTO monitorAssignmentDTO) throws URISyntaxException {
        log.debug("REST request to update Monitor Assignment : {}", monitorAssignmentDTO);

        Monitor monitor = monitorRepository.findOne(monitorAssignmentDTO.getMonitorId());
        Subject subject = subjectRepository.findOne(monitorAssignmentDTO.getSubjectId());
        MonitorConfiguration configuration = monitorConfigurationRepository.findOne(monitorAssignmentDTO.getConfigurationId());
        if (monitor != null && subject != null && configuration != null) {
            User user = userService.getUserWithAuthorities();
            for (Membership membership : user.getMemberships()) {
                if (membership.getCompany().getId() == monitor.getCompany().getId()
                    && membership.getCompany().getId() == configuration.getCompany().getId()
                    && membership.getCompany().getId() == subject.getStudy().getCompany().getId()) {

                    monitorAssignmentService.updateAssignment(monitor, subject, configuration, monitorAssignmentDTO.getSiteReference());

                    return ResponseEntity.ok()
                        .headers(HeaderUtil.createEntityUpdateAlert("monitorAssignment", monitor.getSerialNumber()))
                        .body(null);
                }
            }
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("monitorAssignment", "invalidState", "MonitorAssignment is invalid state")).body(null);
    }

    /**
     * GET  /monitorAssignments : get all the monitor assignments.
     *
     * @param cid the company id to display information for
     * @return the ResponseEntity with status 200 (OK) and the list of monitors in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/monitorAssignments",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<MonitorAssignmentDTO>> getAllMonitorAssignmentsForCompany(@RequestParam long cid, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a list of MonitorAssignments of the active Company");

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(cid);
        if (membership != null) {
            Map<String,String> searchParams;
            try {
                searchParams = new ObjectMapper().readValue(search, HashMap.class);
            } catch (IOException e) {
                // Unable to parse search parameters
                searchParams = new HashMap<>();
            }
            Page<Monitor> page = null;
            if (searchParams.isEmpty()) {
                page = monitorRepository.findAllByCompanyAndStatus(membership.getCompany(), MonitorStatus.ASSIGNED, pageable);
            } else {
                page = monitorRepository.findAll(MonitorAssignmentSearchSpecificationBuilder.build(membership.getCompany(), searchParams), pageable);
            }
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/monitorAssignments");
            return new ResponseEntity<>(MonitorAssignmentDTO.convert(page.getContent()), headers, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /monitorAssignments/:id : get the "id" monitor.
     *
     * @param id the id of the monitorAssignmentDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitorAssignmentDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/monitorAssignments/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MonitorAssignmentDTO> getMonitorAssignment(@PathVariable Long id) {
        log.debug("REST request to get Monitor Assignment : {}", id);
        Monitor monitor = monitorRepository.findOne(id);
        MonitorAssignmentDTO monitorAssignmentDTO = null;
        if (monitor.getAssignedSubject() != null) {
            monitorAssignmentDTO = new MonitorAssignmentDTO(monitor);
        }
        return Optional.ofNullable(monitorAssignmentDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * DELETE  /monitorAssignments/:id : delete the "id" monitor assignment.
     *
     * @param id the id of the monitor assignment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/monitorAssignments/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<Void> deleteMonitorAssignment(@PathVariable Long id) {
        log.debug("REST request to delete MonitorAssignment : {}", id);

        Monitor monitor = monitorRepository.findOne(id);
        if (monitor != null) {
            try {
                monitor = monitorAssignmentService.deleteAssignment(monitor);
                return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("monitorAssignment", monitor.getSerialNumber())).build();
            } catch (InvalidMonitorStatusException ex) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createEntityUpdateAlert("monitorAssignment", "invalid status")).body(null);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
