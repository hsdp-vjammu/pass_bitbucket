package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.repository.*;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.security.SecurityUtils;
import com.philips.respironics.pass.portal.service.MailService;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.KeyAndPasswordDTO;
import com.philips.respironics.pass.portal.web.rest.dto.ManagedUserDTO;
import com.philips.respironics.pass.portal.web.rest.dto.RegistrationDTO;
import com.philips.respironics.pass.portal.web.rest.dto.UserDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private MembershipRepository membershipRepository;

    @Inject
    private PopNumberRepository popNumberRepository;

    @Inject
    private UserService userService;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private MailService mailService;

    @Inject
    private CompanyRepository companyRepository;

    @Inject
    private SettingRepository settingRepository;

    /**
     * POST  /register : register the user.
     *
     * @param registrationDTO the managed user DTO for registration
     * @param request the HTTP request
     * @return the ResponseEntity with status 201 (Created) if the user is registred or 400 (Bad Request) if the login or e-mail is already in use
     */
    @RequestMapping(value = "/register",
                    method = RequestMethod.POST,
                    produces={MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Timed
    @Transactional
    public ResponseEntity<?> registerAccount(@Valid @RequestBody RegistrationDTO registrationDTO, HttpServletRequest request) {
        HttpHeaders textPlainHeaders = new HttpHeaders();
        textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);

        return userRepository.findOneByEmail(registrationDTO.getEmail())
            .map(user -> new ResponseEntity<>("e-mail address already in use", textPlainHeaders, HttpStatus.BAD_REQUEST))
            .orElseGet(() -> userRepository.findOneByLogin(registrationDTO.getEmail().toLowerCase())
                .map(user -> new ResponseEntity<>("login already in use", textPlainHeaders, HttpStatus.BAD_REQUEST))
                .orElseGet(() -> {

                    Optional<PopNumber> popNumber = popNumberRepository.findOneByNumber(registrationDTO.getPopNumber());
                    if (popNumber.isPresent() && popNumber.get().getUser() == null) {
                        User user = userService.createUserInformation(registrationDTO.getFirstName(), registrationDTO.getLastName(),
                            registrationDTO.getEmail().toLowerCase(), registrationDTO.getPassword(), registrationDTO.getLangKey());

                        Company company = new Company();
                        company.setName(registrationDTO.getCompanyName());
                        company.setStreetAddress(registrationDTO.getCompanyStreetAddress());
                        company.setCity(registrationDTO.getCompanyCity());
                        company.setZipCode(registrationDTO.getCompanyZipCode());
                        company.setState(registrationDTO.getCompanyState());
                        company.setCountry(registrationDTO.getCompanyCountry());
                        company.setContactPhoneNumber(registrationDTO.getContactPhoneNumber());

                        Membership membership = new Membership();
                        membership.setUser(user);
                        membership.setCompany(company);

                        MembershipAccess membershipAccess = new MembershipAccess();
                        membershipAccess.setMembership(membership);
                        membershipAccess.setAccessLevel(AuthoritiesConstants.COMPANY_ADMIN);
                        membership.getAccessLevels().add(membershipAccess);

                        companyRepository.save(company);
                        userRepository.save(user);
                        membershipRepository.save(membership);

                        popNumber.get().setUser(registrationDTO.getEmail());
                        popNumberRepository.save(popNumber.get());

                        String baseUrl = request.getScheme() + // "http"
                            "://" +                                // "://"
                            request.getServerName() +              // "myhost"
                            ":" +                                  // ":"
                            request.getServerPort() +              // "80"
                            request.getContextPath();              // "/myContextPath" or "" if deployed in root context

                        mailService.sendActivationEmail(user, baseUrl, null);
                        return new ResponseEntity<>(HttpStatus.CREATED);
                    } else {
                        return new ResponseEntity<>("invalid pop number", textPlainHeaders, HttpStatus.BAD_REQUEST);
                    }

                })
        );
    }

    /**
     * GET  /public/validate/password/: validates if a password is valid.
     *
     * @param pw the password to validate
     * @return the ResponseEntity with status 200 (OK) or with status 400 (Bad Request)
     */
    @RequestMapping(value = "/public/validate/password",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> validatePassword(@RequestParam String pw) {
        log.debug("REST request to validate password strength : {}", pw);

        int minPasswordLength = Integer.parseInt(settingRepository.findOneByCategoryAndName(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.MIN_PASSWORD_LENGTH).getValue());

        if (SecurityUtils.validatePasswordStrength(minPasswordLength, pw)) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * GET  /public/validate/passwordhistory/: validates if a password is valid.
     *
     * @param pw the password to validate
     * @return the ResponseEntity with status 200 (OK) or with status 400 (Bad Request)
     */
    @RequestMapping(value = "/public/validate/passwordhistory",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> validatePasswordHistory(@RequestParam String pw, @RequestParam Optional<String> login, @RequestParam Optional<String> key) {
        log.debug("REST request to validate password history : {}", pw);

        Optional<User> user = Optional.empty();
        if (login.isPresent()) {
            user = userService.getUserWithPasswordHistoryByLogin(login.get());
        } else if (key.isPresent()) {
            user = userService.getUserWithPasswordHistoryByResetKey(key.get());
        }

        if (user.isPresent()) {
            boolean match = userService.matchPassword(pw, user.get().getPassword());

            if (!match) {
                Set<HistoricPassword> historicPasswords = user.get().getHistoricPasswords();
                for (HistoricPassword historicPassword : historicPasswords) {
                    if (userService.matchPassword(pw, historicPassword.getPassword())) {
                        match = true;
                        break;
                    }
                }
            }

            if (match) {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            } else {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * GET  /public/validate/email/: validates if an email already exists.
     *
     * @param email the email to validate
     * @return the ResponseEntity with status 200 (OK) or with status 409 (Conflict)
     */
    @RequestMapping(value = "/public/validate/email",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> validateEmail(@RequestParam String email) {
        log.debug("REST request to validate email : {}", email);

        Long userCount = userRepository.countByEmail(email);
        if (userCount != null && userCount.longValue() == 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }

    /**
     * GET  /public/validate/pop/{email}: validates if an email already exists.
     *
     * @param number the pop number to validate
     * @return the ResponseEntity with status 200 (OK) or with status 404 (Not Found)
     */
    @RequestMapping(value = "/public/validate/pop",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> validatePop(@RequestParam String number) {
        log.debug("REST request to validate pop number : {}", number);

        Long popCount = popNumberRepository.countByNumberAndUserNull(number);
        if (popCount != null && popCount.longValue() == 0) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    /**
     * POST   /activate : activate the registered user.
     *
     * @param keyAndPassword the generated key and the new password
     * @return the ResponseEntity with status 200 (OK) if the user has been activates,
     * or status 500 (Internal Server Error) if the user couldn't be activated
     */
    @RequestMapping(value = "/activate",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> activateAccount(@RequestBody KeyAndPasswordDTO keyAndPassword) {
        return userService.activateRegistration(keyAndPassword.getKey(), keyAndPassword.getNewPassword())
            .map(user -> new ResponseEntity<String>(HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * GET  /authenticate : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request
     * @return the login if the user is authenticated
     */
    @RequestMapping(value = "/authenticate",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }

    /**
     * GET  /account : get the current user.
     *
     * @return the ResponseEntity with status 200 (OK) and the current user in body, or status 500 (Internal Server Error) if the user couldn't be returned
     */
    @RequestMapping(value = "/account",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<UserDTO> getAccount() {
        return Optional.ofNullable(userService.getUserWithAuthorities())
            .map(user -> {
                UserDTO userDto = new UserDTO(user);
                return new ResponseEntity<>(userDto, HttpStatus.OK);
            })
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * POST  /account : update the current user information.
     *
     * @param userDTO the current user information
     * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request) or 500 (Internal Server Error) if the user couldn't be updated
     */
    @RequestMapping(value = "/account",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> saveAccount(@Valid @RequestBody UserDTO userDTO) {
        Optional<User> existingUser = userRepository.findOneByEmail(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userDTO.getEmail()))) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("user-management", "emailexists", "Email already in use")).body(null);
        }
        return userRepository
            .findOneByLogin(SecurityUtils.getCurrentUserLogin())
            .map(u -> {
                userService.updateUserInformation(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
                    userDTO.getLangKey());
                return new ResponseEntity<String>(HttpStatus.OK);
            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * POST  /account/change_password : changes the current user's password
     *
     * @param password the new password
     * @return the ResponseEntity with status 200 (OK), or status 400 (Bad Request) if the new password is not strong enough
     */
    @RequestMapping(value = "/account/change_password",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<?> changePassword(@RequestBody String password) {
        if (!checkPasswordLength(password)) {
            return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
        }
        userService.changePassword(password);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * GET  /account/sessions : get the current open sessions.
     *
     * @return the ResponseEntity with status 200 (OK) and the current open sessions in body,
     *  or status 500 (Internal Server Error) if the current open sessions couldn't be retrieved
     */
    @RequestMapping(value = "/account/sessions",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<PersistentToken>> getCurrentSessions() {
        return userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin())
            .map(user -> new ResponseEntity<>(
                persistentTokenRepository.findByUser(user),
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * DELETE  /account/sessions?series={series} : invalidate an existing session.
     *
     * - You can only delete your own sessions, not any other user's session
     * - If you delete one of your existing sessions, and that you are currently logged in on that session, you will
     *   still be able to use that session, until you quit your browser: it does not work in real time (there is
     *   no API for that), it only removes the "remember me" cookie
     * - This is also true if you invalidate your current session: you will still be able to use it until you close
     *   your browser or that the session times out. But automatic login (the "remember me" cookie) will not work
     *   anymore.
     *   There is an API to invalidate the current session, but there is no API to check which session uses which
     *   cookie.
     *
     * @param series the series of an existing session
     * @throws UnsupportedEncodingException if the series couldnt be URL decoded
     */
    @RequestMapping(value = "/account/sessions/{series}",
        method = RequestMethod.DELETE)
    @Timed
    public void invalidateSession(@PathVariable String series) throws UnsupportedEncodingException {
        String decodedSeries = URLDecoder.decode(series, "UTF-8");
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(u -> {
            persistentTokenRepository.findByUser(u).stream()
                .filter(persistentToken -> StringUtils.equals(persistentToken.getSeries(), decodedSeries))
                .findAny().ifPresent(t -> persistentTokenRepository.delete(decodedSeries));
        });
    }

    /**
     * POST   /account/reset_password/init : Send an e-mail to reset the password of the user
     *
     * @param mail the mail of the user
     * @param request the HTTP request
     * @return the ResponseEntity with status 200 (OK) if the e-mail was sent, or status 400 (Bad Request) if the e-mail address is not registred
     */
    @RequestMapping(value = "/account/reset_password/init",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<?> requestPasswordReset(@RequestBody String mail, HttpServletRequest request) {
        return userService.requestPasswordReset(mail)
            .map(user -> {
                String baseUrl = request.getScheme() +
                    "://" +
                    request.getServerName() +
                    ":" +
                    request.getServerPort() +
                    request.getContextPath();
                mailService.sendPasswordResetMail(user, baseUrl);
                return new ResponseEntity<>("e-mail was sent", HttpStatus.OK);
            }).orElse(new ResponseEntity<>("e-mail address not registered", HttpStatus.BAD_REQUEST));
    }

    /**
     * POST   /account/reset_password/finish : Finish to reset the password of the user
     *
     * @param keyAndPassword the generated key and the new password
     * @return the ResponseEntity with status 200 (OK) if the password has been reset,
     * or status 400 (Bad Request) or 500 (Internal Server Error) if the password could not be reset
     */
    @RequestMapping(value = "/account/reset_password/finish",
        method = RequestMethod.POST,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<String> finishPasswordReset(@RequestBody KeyAndPasswordDTO keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
        }
        return userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey())
              .map(user -> new ResponseEntity<String>(HttpStatus.OK))
              .orElse(new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    private boolean checkPasswordLength(String password) {
        return (!StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserDTO.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserDTO.PASSWORD_MAX_LENGTH);
    }
}
