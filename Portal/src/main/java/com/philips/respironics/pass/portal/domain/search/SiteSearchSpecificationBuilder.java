package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SiteSearchSpecificationBuilder {

    public static Specification<Site> build(Study study, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
        }
        return build(study, criteria);
    }

    public static Specification<Site> build(Study study, List<SearchParameter> searchCriteria) {

        List<Specification<Site>> specs = new ArrayList<>();
        specs.add(createSpecification(study));

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else if (param.getKey().startsWith("coordinator_")) {
                String attributeName = param.getKey().replace("coordinator_", "");
                if ("name".equals(attributeName)) {
                    specs.add(buildCoordinatorNameSpec(param.getValue()));
                } else {
                    specs.add(SiteSearchSpecificationBuilder.createCoordinatorSpecification(attributeName, param.getValue()));
                }
            } else {
                specs.add(SiteSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<Site> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<Site> createSpecification(Study study) {
        return new Specification<Site>() {
            @Override
            public Predicate toPredicate(Root<Site> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("study"), study);
            }
        };
    }

    private static Specification<Site> buildGlobalSearch(Object value) {
        List<Specification<Site>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("externalReference", value));
        specs.add(buildCoordinatorNameSpec(value));
        specs.add(createCoordinatorSpecification("email", value));

        Specification<Site> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<Site> buildCoordinatorNameSpec(Object value) {
        Specification<Site> nameSpec = SiteSearchSpecificationBuilder.createCoordinatorSpecification("firstName", value);
        nameSpec = Specifications.where(nameSpec).or(SiteSearchSpecificationBuilder.createCoordinatorSpecification("lastName", value));

        return nameSpec;
    }

    private static Specification<Site> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<Site> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Site> createCoordinatorSpecification(String attributeName, Object value) {
        return new Specification<Site>() {
            @Override
            public Predicate toPredicate(Root<Site> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Site, MembershipAccess> membershipAccess = root.join("membershipAccessLevels", JoinType.LEFT);
                Join<MembershipAccess, Membership> membership = membershipAccess.join("membership", JoinType.LEFT);
                Join<Membership, User> user = membership.join("user", JoinType.LEFT);

                if (user.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(user.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(user.get(attributeName), value);
                }
            }
        };
    }

}
