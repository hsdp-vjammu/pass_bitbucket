package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The WeightUnit enumeration.
 */
public enum WeightUnit {
    NONE(null),
    KG("kg"),
    LB("lb");

    private final String text;

    WeightUnit(final String text) {
        this.text = text;
    }

    public static WeightUnit fromString(String text) {
        if (text != null) {
            for (WeightUnit b : WeightUnit.values()) {
                if (text.equalsIgnoreCase(b.text)) {
                    return b;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
}
