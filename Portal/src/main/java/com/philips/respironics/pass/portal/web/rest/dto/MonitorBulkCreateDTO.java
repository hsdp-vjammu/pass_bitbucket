package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the creating monitors in bulk.
 */
public class MonitorBulkCreateDTO implements Serializable {

    @NotNull
    private Long companyId;

    @NotNull
    private MonitorType type;

    private String[] serialNumbers;

    public Long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public MonitorType getType() {
        return type;
    }
    public void setType(MonitorType type) {
        this.type = type;
    }

    public String[] getSerialNumbers() {
        return serialNumbers;
    }
    public void setSerialNumbers(String[] serialNumbers) {
        this.serialNumbers = serialNumbers;
    }

}
