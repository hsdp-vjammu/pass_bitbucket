package com.philips.respironics.pass.portal.domain.enumeration;

public enum RecurrencePattern {
    DAILY,WEEKLY,MONTHLY
}
