package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MonitorSearchSpecificationBuilder {

    public static Specification<Monitor> build(Company company, Map<String, String> params) {
        return build(company, null, params);
    }

    public static Specification<Monitor> build(Company company, Site site, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                switch (k) {
                    case "status":
                        try {
                            criteria.add(new SearchParameter(k, MonitorStatus.valueOf(v)));
                        } catch (IllegalArgumentException ex) {
                            // Invalid status
                        }
                        break;
                    case "type":
                        try {
                            criteria.add(new SearchParameter(k, MonitorType.valueOf(v)));
                        } catch (IllegalArgumentException ex) {
                            // Invalid status
                        }
                        break;
                    case "study_status":
                        try {
                            criteria.add(new SearchParameter(k, StudyStatus.valueOf(v)));
                        } catch (IllegalArgumentException ex) {
                            // Invalid status
                        }
                        break;
                    default:
                        criteria.add(new SearchParameter(k, v));
                        break;
                }
            });
        }
        return build(company, site, criteria);
    }

    public static Specification<Monitor> build(Company company, Site site, List<SearchParameter> searchCriteria) {

        List<Specification<Monitor>> specs = new ArrayList<>();
        specs.add(createSpecification(company));
        if (site != null) {
            specs.add(createSpecification(site));
        }

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else if (param.getKey().startsWith("assignedSubject_")) {
                String attributeName = param.getKey().replace("assignedSubject_", "");
                if ("name".equals(attributeName)) {
                    specs.add(buildSubjectNameSpec(param.getValue()));
                } else {
                    specs.add(MonitorSearchSpecificationBuilder.createSubjectSpecification(attributeName, param.getValue()));
                }
            } else if (param.getKey().startsWith("study_")) {
                String attributeName = param.getKey().replace("study_", "");
                specs.add(MonitorSearchSpecificationBuilder.createStudySpecification(attributeName, param.getValue()));
             } else {
                specs.add(MonitorSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<Monitor> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<Monitor> createSpecification(Company company) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("company"), company);
            }
        };
    }

    private static Specification<Monitor> createSpecification(Site site) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, Subject> subjectJoin = root.join("assignedSubject", JoinType.LEFT);
                Join<Subject, Site> siteJoin = subjectJoin.join("site", JoinType.LEFT);

                Predicate assignedPredicate = criteriaBuilder.and(criteriaBuilder.equal(root.get("status"), MonitorStatus.ASSIGNED),
                    criteriaBuilder.equal(siteJoin.get("id"), site.getId()));
                Predicate unassignedPredicate = criteriaBuilder.equal(root.get("status"), MonitorStatus.UNASSIGNED);

                return criteriaBuilder.or(assignedPredicate, unassignedPredicate);
            }
        };
    }

    private static Specification<Monitor> buildGlobalSearch(Object value) {
        List<Specification<Monitor>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("serialNumber", value));
        specs.add(buildSubjectNameSpec(value));
        specs.add(createStudySpecification("name", value));

        Specification<Monitor> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<Monitor> buildSubjectNameSpec(Object value) {
        Specification<Monitor> nameSpec = MonitorSearchSpecificationBuilder.createSubjectSpecification("firstName", value);
        nameSpec = Specifications.where(nameSpec).or(MonitorSearchSpecificationBuilder.createSubjectSpecification("lastName", value));

        return nameSpec;
    }

    private static Specification<Monitor> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<Monitor> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Monitor> createSubjectSpecification(String attributeName, Object value) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, Subject> subject = root.join("assignedSubject", JoinType.LEFT);

                if (subject.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(subject.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(subject.get(attributeName), value);
                }
            }
        };
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Monitor> createStudySpecification(String attributeName, Object value) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, Subject> subject = root.join("assignedSubject", JoinType.LEFT);
                Join<Subject, Study> study = subject.join("study", JoinType.LEFT);

                if (study.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(study.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(study.get(attributeName), value);
                }
            }
        };
    }
}
