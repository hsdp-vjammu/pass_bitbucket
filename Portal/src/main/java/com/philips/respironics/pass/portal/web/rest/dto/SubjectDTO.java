package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Monitor;
import com.philips.respironics.pass.portal.domain.Subject;
import com.philips.respironics.pass.portal.domain.enumeration.Gender;
import com.philips.respironics.pass.portal.domain.enumeration.HeightUnit;
import com.philips.respironics.pass.portal.domain.enumeration.WeightUnit;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

/**
 * A DTO for the Subject entity.
 */
public class SubjectDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String uniqueIdentifier;

    @NotNull
    private String externalReference;

    private String firstName;

    private String lastName;

    private LocalDate dateOfBirth;

    private Double weight;

    private WeightUnit weightUnit;

    private Double height;

    private HeightUnit heightUnit;

    private Gender gender;

    private String streetAddress;

    private String zipcode;

    private String city;

    private String state;

    private String country;

    private String phoneNumber;

    private String email;

    private String timeZone;

    private Long studyId;

    private String studyName;

    private Long siteId;

    private String siteReference;

    private String dominantHand;

    private String wearingPosition;

    private Set<MonitorDTO> assignedMonitors = new HashSet<>();

    public SubjectDTO() {}

    public SubjectDTO(Subject entity) {
        this.id = entity.getId();
        this.uniqueIdentifier = entity.getUniqueIdentifier();
        this.externalReference = entity.getExternalReference();
        this.firstName = entity.getFirstName();
        this.lastName = entity.getLastName();
        this.dateOfBirth = entity.getDateOfBirth();
        this.weight = entity.getWeight();
        this.weightUnit = entity.getWeightUnit();
        this.height = entity.getHeight();
        this.heightUnit = entity.getHeightUnit();
        this.gender = entity.getGender();
        this.streetAddress = entity.getStreetAddress();
        this.zipcode = entity.getZipCode();
        this.city = entity.getCity();
        this.state = entity.getState();
        this.country = entity.getCountry();
        this.phoneNumber = entity.getPhoneNumber();
        this.email = entity.getEmail();
        this.timeZone = entity.getTimeZone();
        this.dominantHand = entity.getDominantHand();
        this.wearingPosition = entity.getWearingPosition();
        if (entity.getStudy() != null) {
            this.studyId = entity.getStudy().getId();
            this.studyName = entity.getStudy().getName();
        }
        if (entity.getSite() != null) {
            this.siteId = entity.getSite().getId();
            this.siteReference = entity.getSite().getExternalReference();
        }

        for (Monitor monitorEntity : entity.getAssignedMonitors()) {
            this.assignedMonitors.add(new MonitorDTO(monitorEntity));
        }
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }
    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public String getExternalReference() {
        return externalReference;
    }
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }
    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Double getHeight() {
        return height;
    }
    public void setHeight(Double height) {
        this.height = height;
    }

    public HeightUnit getHeightUnit() {
        return heightUnit;
    }
    public void setHeightUnit(HeightUnit unit) {
        this.heightUnit = unit;
    }

    public Double getWeight() {
        return weight;
    }
    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public WeightUnit getWeightUnit() {
        return weightUnit;
    }
    public void setWeightUnit(WeightUnit unit) {
        this.weightUnit = unit;
    }

    public Gender getGender() {
        return gender;
    }
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getStreetAddress() {
        return streetAddress;
    }
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipcode;
    }
    public void setZipCode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getTimeZone() {
        return timeZone;
    }
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Long getStudyId() { return studyId; }
    public void setStudyId(Long studyId) { this.studyId = studyId; }

    public String getStudyName() { return studyName; }
    public void setStudyName(String studyName) { this.studyName = studyName; }

    public Long getSiteId() { return siteId; }
    public void setSiteId(Long siteId) { this.siteId = siteId; }

    public String getSiteReference() { return siteReference; }
    public void setSiteReference(String siteReference) { this.siteReference = siteReference; }

    public String getDominantHand() {
        return dominantHand;
    }

    public void setDominantHand(String dominantHand) {
        this.dominantHand = dominantHand;
    }

    public String getWearingPosition() {
        return wearingPosition;
    }

    public void setWearingPosition(String wearingPosition) {
        this.wearingPosition = wearingPosition;
    }


    public Set<MonitorDTO> getAssignedMonitors() { return assignedMonitors; }
    public void setAssignedMonitors(Set<MonitorDTO> assignedMonitors) { this.assignedMonitors = assignedMonitors; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubjectDTO subjectDTO = (SubjectDTO) o;

        if ( ! Objects.equals(id, subjectDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SubjectDTO{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", externalReference='" + externalReference + "'" +
            ", firstName='" + firstName + "'" +
            ", lastName='" + lastName + "'" +
            ", dateOfBirth='" + dateOfBirth + "'" +
            ", weight='" + weight + "'" +
            ", height='" + height + "'" +
            ", gender='" + gender + "'" +
            ", streetAddress='" + streetAddress + "'" +
            ", zipcode='" + zipcode + "'" +
            ", city='" + city + "'" +
            ", state='" + state + "'" +
            ", country='" + country + "'" +
            ", phoneNumber='" + phoneNumber + "'" +
            ", dominantHand='" + dominantHand + "'" +
            ", wearingPosition='" + wearingPosition + "'" +
            '}';
    }

    public static List<SubjectDTO> convert(List<Subject> entities)
    {
        List<SubjectDTO> dtos = new ArrayList<>();
        for (Subject entity : entities) {
            dtos.add(new SubjectDTO(entity));
        }
        return dtos;
    }

}
