package com.philips.respironics.pass.portal.domain;

import com.philips.respironics.pass.portal.domain.enumeration.QuestionResponseType;
import com.philips.respironics.pass.portal.domain.enumeration.RecurrencePattern;
import com.philips.respironics.pass.portal.domain.util.StringArrayConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A MonitorConfigurationQuestion.
 */
@Entity
@Table(name = "monitor_configuration_question")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MonitorConfigurationQuestion extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "text", nullable = false)
    private String text;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "response_type", nullable = false)
    private QuestionResponseType responseType;

    @Column(name = "response_value_1")
    private String responseValue1;

    @Column(name = "response_value_2")
    private String responseValue2;

    @Column(name = "response_range_min")
    private Double responseRangeMin;

    @Column(name = "response_range_max")
    private Double responseRangeMax;

    @Column(name = "recurrence_startdate", nullable = false)
    private LocalDate recurrenceStartDate;

    @Column(name = "recurrence_enddate", nullable = false)
    private LocalDate recurrenceEndDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "recurrence_pattern", nullable = false)
    private RecurrencePattern recurrencePattern;

    @Column(name = "recurrence_weekdays")
    @Convert(converter = StringArrayConverter.class)
    private String[] recurrencePatternWeekdays;

    @Column(name = "recurrence_time1")
    private ZonedDateTime recurrenceTime1;

    @Column(name = "recurrence_time2")
    private ZonedDateTime recurrenceTime2;

    @Column(name = "recurrence_time3")
    private ZonedDateTime recurrenceTime3;

    @Column(name = "rec_time1")
    private String recTime1;

    @Column(name = "rec_time2")
    private String recTime2;

    @Column(name = "rec_time3")
    private String recTime3;

    @ManyToOne
    @NotNull
    private MonitorConfiguration monitorConfiguration;

    public MonitorConfigurationQuestion() {  }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public QuestionResponseType getResponseType() {
        return responseType;
    }
    public void setResponseType(QuestionResponseType responseType) {
        this.responseType = responseType;
    }

    public String getResponseValue1() {
        return responseValue1;
    }
    public void setResponseValue1(String value) {
        this.responseValue1 = value;
    }

    public String getResponseValue2() {
        return responseValue2;
    }
    public void setResponseValue2(String value) {
        this.responseValue2 = value;
    }

    public Double getResponseRangeMin() {
        return responseRangeMin;
    }
    public void setResponseRangeMin(Double value) {
        this.responseRangeMin = value;
    }

    public Double getResponseRangeMax() {
        return responseRangeMax;
    }
    public void setResponseRangeMax(Double value) {
        this.responseRangeMax = value;
    }

    public LocalDate getRecurrenceStartDate() {
        return recurrenceStartDate;
    }
    public void setRecurrenceStartDate(LocalDate date) { this.recurrenceStartDate = date; }

    public LocalDate getRecurrenceEndDate() {
        return recurrenceEndDate;
    }
    public void setRecurrenceEndDate(LocalDate date) { this.recurrenceEndDate = date; }

    public String[] getRecurrencePatternWeekdays() {
        return recurrencePatternWeekdays;
    }
    public void setRecurrencePatternWeekdays(String[] values) { this.recurrencePatternWeekdays = values; }

    public RecurrencePattern getRecurrencePattern() {
        return recurrencePattern;
    }
    public void setRecurrencePattern(RecurrencePattern pattern) { this.recurrencePattern = pattern; }

    public ZonedDateTime getRecurrenceTime1() {
        return recurrenceTime1;
    }
    public void setRecurrenceTime1(ZonedDateTime time) { this.recurrenceTime1 = time; }

    public ZonedDateTime getRecurrenceTime2() { return recurrenceTime2; }
    public void setRecurrenceTime2(ZonedDateTime time) { this.recurrenceTime2 = time; }

    public ZonedDateTime getRecurrenceTime3() { return recurrenceTime3; }
    public void setRecurrenceTime3(ZonedDateTime time) { this.recurrenceTime3 = time; }


    public String getRecTime1() {
        return recTime1;
    }

    public void setRecTime1(String recTime1) {
        this.recTime1 = recTime1;
    }

    public String getRecTime2() {
        return recTime2;
    }

    public void setRecTime2(String recTime2) {
        this.recTime2 = recTime2;
    }

    public String getRecTime3() {
        return recTime3;
    }

    public void setRecTime3(String recTime3) {
        this.recTime3 = recTime3;
    }

    public MonitorConfiguration getMonitorConfiguration() {
        return monitorConfiguration;
    }
    public void setMonitorConfiguration(MonitorConfiguration monitorConfiguration) {
        this.monitorConfiguration = monitorConfiguration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MonitorConfigurationQuestion entity = (MonitorConfigurationQuestion) o;
        if(entity.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MonitorConfiguration{" +
            "id=" + id +
            ", text='" + text + "'" +
            ", responseType='" + responseType + "'" +
            '}';
    }
}
