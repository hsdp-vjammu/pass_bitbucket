package com.philips.respironics.pass.portal.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PopNumber.
 */
@Entity
@Table(name = "pop_number")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PopNumber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(min = 32, max = 32)
    @Pattern(regexp = "^[A-Z0-9]*$")
    @Column(name = "number", length = 32, nullable = false)
    private String number;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "user")
    private String user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PopNumber popNumber = (PopNumber) o;
        if(popNumber.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, popNumber.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PopNumber{" +
            "id=" + id +
            ", number='" + number + "'" +
            ", createDate='" + createDate + "'" +
            ", user='" + user + "'" +
            '}';
    }
}
