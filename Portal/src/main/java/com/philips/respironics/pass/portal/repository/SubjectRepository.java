package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.Site;
import com.philips.respironics.pass.portal.domain.Study;
import com.philips.respironics.pass.portal.domain.Subject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Spring Data JPA repository for the Subject entity.
 */
@SuppressWarnings("unused")
public interface SubjectRepository extends JpaRepository<Subject,Long>, JpaSpecificationExecutor<Subject> {

    Long countByStudy(Study study);

    Long countBySite(Site site);

    Page<Subject> findAllByStudyCompany(Company company, Pageable pageable);

    List<Subject> findAllByStudyCompanyAndAssignedMonitorsNull(Company company);

    List<Subject> findAllByStudyCompanyAndSiteAndAssignedMonitorsNull(Company company, Site site);

    Page<Subject> findAllBySite(Site site, Pageable pageable);

    List<Subject> findAllByStudyCompany(Company company);

    //extract Study information based on study Ids for Site Coordinator display
    Page<Subject> findByStudyIdIn(ArrayList<Long> studyIds, Pageable pageable);

    @Query("SELECT DISTINCT s.externalReference FROM Subject AS s WHERE s.externalReference IS NOT NULL AND s.study = ?1 AND NOT s.id = ?2")
    List<String> findExternalReferencesByStudyAndIdNot(Study study, Long id);

}
