package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.MembershipAccess;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the MembershipAccess entity.
 */
public class MembershipAccessDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String accessLevel;

    private Long studyId;

    private String studyName;

    private Long siteId;

    private String siteReference;

    public MembershipAccessDTO() {
    }

    public MembershipAccessDTO(MembershipAccess membershipAccess) {
        this.id = membershipAccess.getId();
        this.accessLevel = membershipAccess.getAccessLevel();
        if (membershipAccess.getSite() != null) {
            this.studyId = membershipAccess.getSite().getStudy().getId();
            this.studyName = membershipAccess.getSite().getStudy().getName();
            this.siteId = membershipAccess.getSite().getId();
            this.siteReference = membershipAccess.getSite().getExternalReference();
        }
    }

    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public String getAccessLevel() { return accessLevel; }
    public void setAccessLevel(String accessLevel) { this.accessLevel = accessLevel; }

    public Long getStudyId() { return studyId; }
    public void setStudyId(Long studyId) { this.studyId = studyId; }

    public String getStudyName() { return studyName; }

    public Long getSiteId() { return siteId; }
    public void setSiteId(Long siteId) { this.siteId = siteId; }

    public String getSiteReference() { return siteReference; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MembershipAccessDTO dto = (MembershipAccessDTO) o;

        if ( ! Objects.equals(id, dto.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MembershipAccessDTO{" +
            "id=" + id +
            ",accessLevel=" + accessLevel +
            '}';
    }
}
