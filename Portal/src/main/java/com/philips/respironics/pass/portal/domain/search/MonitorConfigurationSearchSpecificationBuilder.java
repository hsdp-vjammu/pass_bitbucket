package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.MonitorConfiguration;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MonitorConfigurationSearchSpecificationBuilder {

    public static Specification<MonitorConfiguration> build(Company company, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
        }
        return build(company, criteria);
    }

    public static Specification<MonitorConfiguration> build(Company company, List<SearchParameter> searchCriteria) {

        List<Specification<MonitorConfiguration>> specs = new ArrayList<>();
        specs.add(createSpecification(company));

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else {
                specs.add(MonitorConfigurationSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<MonitorConfiguration> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<MonitorConfiguration> createSpecification(Company company) {
        return new Specification<MonitorConfiguration>() {
            @Override
            public Predicate toPredicate(Root<MonitorConfiguration> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("company"), company);
            }
        };
    }

    private static Specification<MonitorConfiguration> buildGlobalSearch(Object value) {
        List<Specification<MonitorConfiguration>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("name", value));

        Specification<MonitorConfiguration> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<MonitorConfiguration> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<MonitorConfiguration> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

}
