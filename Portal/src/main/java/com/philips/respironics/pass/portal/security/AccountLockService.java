package com.philips.respironics.pass.portal.security;

import com.philips.respironics.pass.portal.domain.Setting;
import com.philips.respironics.pass.portal.domain.User;
import com.philips.respironics.pass.portal.repository.SettingRepository;
import com.philips.respironics.pass.portal.repository.UserRepository;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@Component
public class AccountLockService {

    private final Logger log = LoggerFactory.getLogger(AccountLockService.class);

    private static final ConcurrentMap<String, List<Instant>> failedAttemptsCache = new ConcurrentHashMap<>();
    private static final ConcurrentMap<String, Instant> lockedAccountsCache = new ConcurrentHashMap<>();

    @Inject
    UserRepository userRepository;

    @Inject
    SettingRepository settingRepository;

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public boolean checkLock(final String login) {
        if (StringUtils.isNotBlank(login)) {
            if (lockedAccountsCache.containsKey(login)) {
                return true;
            }

            Setting maxFailedAttemptsSetting = settingRepository.findOneByCategoryAndName(Setting.SESSION_MANAGEMENT.CATEGORY, Setting.SESSION_MANAGEMENT.FAILED_LOGIN_ATTEMPTS);
            Setting failedAttemptsPeriodSetting = settingRepository.findOneByCategoryAndName(Setting.SESSION_MANAGEMENT.CATEGORY, Setting.SESSION_MANAGEMENT.FAILED_LOGIN_ATTEMPTS_PERIOD);
            if (maxFailedAttemptsSetting != null) {
                Instant timestamp = Instant.now();
                int maxFailedAttempts = Integer.parseInt(maxFailedAttemptsSetting.getValue());

                List<Instant> failedAttempts;
                if (failedAttemptsCache.containsKey(login)) {
                    failedAttempts = failedAttemptsCache.get(login);
                    failedAttemptsCache.remove(login);
                } else {
                    failedAttempts = new ArrayList<>();
                }

                if (failedAttemptsPeriodSetting != null) {
                    int failedAttemptsPeriod = Integer.parseInt(failedAttemptsPeriodSetting.getValue());
                    List<Instant> activeFailedAttempts = new ArrayList<>();

                    for (Instant failedAttempt : failedAttempts) {
                        long minutes = Duration.between(failedAttempt, timestamp).toMinutes();
                        if (minutes <= failedAttemptsPeriod) {
                            activeFailedAttempts.add(failedAttempt);
                        }
                    }

                    failedAttempts = activeFailedAttempts;
                }

                failedAttempts.add(timestamp);

                if (failedAttempts.size() >= maxFailedAttempts) {
                    lockedAccountsCache.putIfAbsent(login, Instant.now());
                    Optional<com.philips.respironics.pass.portal.domain.User> user = userRepository.findOneByLogin(login);
                    if (user.isPresent()) {
                        user.get().setLocked(ZonedDateTime.now());
                        userRepository.save(user.get());
                        log.info("User '{}' locked due to too many failed attempts", login);
                    }
                    return true;
                } else {
                    failedAttemptsCache.put(login, failedAttempts);
                    return false;
                }
            }
        }

        return false;
    }

    public void clearAttempts(String login) {
        failedAttemptsCache.remove(login);
    }

    public void clearLock(String login) {
        lockedAccountsCache.remove(login);
        clearAttempts(login);
    }
}
