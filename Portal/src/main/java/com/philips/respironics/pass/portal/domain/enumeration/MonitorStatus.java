package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * Created by usd45540 on 12/19/2016.
 */
public enum MonitorStatus{
    ASSIGNED,UNASSIGNED,DISABLED,REMOVED
}
