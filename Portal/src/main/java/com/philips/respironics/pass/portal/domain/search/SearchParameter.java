package com.philips.respironics.pass.portal.domain.search;

/**
 * Created by ihomer on 3/24/17.
 */
public class SearchParameter {
    private String key;
    private Object value;

    public SearchParameter(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
