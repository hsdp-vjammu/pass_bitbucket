package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Company;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the Company registration entity.
 */
public class CompanyRegistrationDTO extends CompanyDTO implements Serializable {

    @NotNull
    @Size(min = RegistrationDTO.POPNUMBER_LENGTH, max = RegistrationDTO.POPNUMBER_LENGTH)
    private String popNumber;

    public CompanyRegistrationDTO() {
        super();
    }

    public CompanyRegistrationDTO(Company company) {
        super(company);
    }

    public CompanyRegistrationDTO(Company company, boolean includeMemberships) {
        super(company, includeMemberships);
    }

    public String getPopNumber() {
        return popNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CompanyRegistrationDTO companyDTO = (CompanyRegistrationDTO) o;

        if ( ! Objects.equals(popNumber, companyDTO.popNumber)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(popNumber);
    }

    @Override
    public String toString() {
        return "CompanyRegistrationDTO{" +
            ", POPnumber='" + getPopNumber() + "'" +
            ", name='" + getName() + "'" +
            '}';
    }
}
