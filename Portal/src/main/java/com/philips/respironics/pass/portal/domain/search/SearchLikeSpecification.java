package com.philips.respironics.pass.portal.domain.search;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class SearchLikeSpecification<T> implements Specification<T> {

    private String key;
    private Object value;

    public SearchLikeSpecification(final String key, final Object value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        try {
            return criteriaBuilder.like(
                root.<String>get(key), "%" + value + "%");
        } catch (IllegalArgumentException ex) {
            // Invalid property name
            return null;
        }
    }
}
