package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.enumeration.SecurityQuestion;
import com.philips.respironics.pass.portal.validator.PasswordStrength;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * A DTO for registering a new user.
 */
public class RegistrationDTO {

    public static final int POPNUMBER_LENGTH = 32;

    @Size(max = 25)
    private String firstName;

    @Size(max = 25)
    private String lastName;

    @Size(max = 25)
    private String companyName;

    @Size(max = 25)
    private String companyStreetAddress;

    @Size(max = 25)
    private String companyCity;

    @Size(max = 25)
    private String companyZipCode;

    @Size(max = 25)
    private String companyState;

    @Size(max = 3)
    private String companyCountry;

    @Size(max = 50)
    private String contactPhoneNumber;

    @NotNull
    @Email
    @Size(min = 5, max = 60)
    private String email;

    @NotNull
    @PasswordStrength
    private String password;

    @NotNull
    @Size(min = POPNUMBER_LENGTH, max = POPNUMBER_LENGTH)
    private String popNumber;

    @Size(min = 2, max = 5)
    private String langKey;

    public RegistrationDTO() {
    }

    public RegistrationDTO(String firstName, String lastName, String companyName, String companyStreetAddress,
                           String companyCity, String companyZipCode, String companyState, String companyCountry,
                           String contactPhoneNumber, String email, String password, String popNumber, String langKey) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.companyName = companyName;
        this.companyStreetAddress = companyStreetAddress;
        this.companyCity = companyCity;
        this.companyZipCode = companyZipCode;
        this.companyState = companyState;
        this.companyCountry = companyCountry;
        this.contactPhoneNumber = contactPhoneNumber;
        this.email = email;
        this.password = password;
        this.popNumber = popNumber;
        this.langKey = langKey;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPopNumber() {
        return popNumber;
    }

    public String getLangKey() {
        return langKey;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyStreetAddress() {
        return companyStreetAddress;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public String getCompanyZipCode() {
        return companyZipCode;
    }

    public String getCompanyState() {
        return companyState;
    }

    public String getCompanyCountry() {
        return companyCountry;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "RegistrationDTO{" +
            "email=" + getEmail() +
            ", popNumber=" + popNumber +
            "} " + super.toString();
    }
}
