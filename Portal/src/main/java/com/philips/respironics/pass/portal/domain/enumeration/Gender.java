package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE("M"),
    FEMALE("F");

    private final String text;

    Gender(final String text) {
        this.text = text;
    }

    public static Gender fromString(String text) {
        if (text != null) {
            for (Gender b : Gender.values()) {
                if (text.equalsIgnoreCase(b.text)) {
                    return b;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }

}
