package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.MonitorHistory;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatusChangeAction;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A DTO for the MonitorHistory entity.
 */
public class MonitorHistoryDTO extends AbstractAuditingDTO implements Serializable {

    @NotNull
    private MonitorStatusChangeAction changeAction;

    private String companyName;

    private String serialNumber;

    private String assignedSubjectName;

    private String assignedSubjectReference;

    private String assignedSubjectStudyName;

    public MonitorHistoryDTO() {}

    public MonitorHistoryDTO(MonitorHistory entity) {
        this.changeAction = entity.getChangeAction();
        this.companyName = entity.getCompanyName();
        this.assignedSubjectName = entity.getAssignedSubjectName();
        this.assignedSubjectReference = entity.getAssignedSubjectReference();
        this.assignedSubjectStudyName = entity.getAssignedSubjectStudyName();
        this.serialNumber = entity.getSerialNumber();

        this.setCreatedDate(entity.getCreatedDate());
        this.setCreatedBy(entity.getCreatedBy());
        this.setLastModifiedDate(entity.getLastModifiedDate());
        this.setLastModifiedBy(entity.getLastModifiedBy());
    }

    public static List<MonitorHistoryDTO> convert(List<MonitorHistory> entities)
    {
        List<MonitorHistoryDTO> dtos = new ArrayList<>();
        for (MonitorHistory entity : entities) {
            dtos.add(new MonitorHistoryDTO(entity));
        }
        return dtos;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAssignedSubjectName() {
        return assignedSubjectName;
    }

    public void setAssignedSubjectName(String assignedSubjectName) {
        this.assignedSubjectName = assignedSubjectName;
    }

    public String getAssignedSubjectReference() {
        return assignedSubjectReference;
    }

    public void setAssignedSubjectReference(String assignedSubjectReference) {
        this.assignedSubjectReference = assignedSubjectReference;
    }

    public String getAssignedSubjectStudyName() {
        return assignedSubjectStudyName;
    }

    public void setAssignedSubjectStudyName(String assignedSubjectStudyName) {
        this.assignedSubjectStudyName = assignedSubjectStudyName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public MonitorStatusChangeAction getChangeAction() {
        return changeAction;
    }

    public void setChangeAction(MonitorStatusChangeAction changeAction) {
        this.changeAction = changeAction;
    }

}
