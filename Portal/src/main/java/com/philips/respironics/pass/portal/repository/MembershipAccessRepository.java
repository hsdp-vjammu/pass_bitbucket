package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.MembershipAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


/**
 * Spring Data JPA repository for the Study entity.
 */
@SuppressWarnings("unused")
public interface MembershipAccessRepository extends JpaRepository<MembershipAccess,Long>, JpaSpecificationExecutor<MembershipAccess> {
    String SELECT_QUERY = "select id ";
    String COUNT_QUERY = "select count(*) ";
    String CRITERIA_QUERY = " FROM company_user_access AS c where c.site_id = ?1 and c.company_user_id = ?2";
    String SEL_QUERY = SELECT_QUERY + CRITERIA_QUERY;
    String CNT_QUERY = COUNT_QUERY + CRITERIA_QUERY;

    List<MembershipAccess> findAllBySiteId(long siteId);

    @Query(value = SEL_QUERY, countQuery = CNT_QUERY, nativeQuery = true)
    List<MembershipAccess> findBySiteAndCompanyUserId(Long siteId, Long companyUserId);
}
