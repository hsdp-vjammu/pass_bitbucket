package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.service.util.DateTimeUtil;
import com.philips.respironics.pass.portal.web.rest.dto.TDRDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TDRDataService {
    private final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private DateTimeUtil dateTimeUtil;

    @Autowired
    private RestTemplate restTemplate;


    @Value("${remoteurls.dataExtractor}")
    String baseURL;//= "http://passdataextractor.cloud.pcftest.com/tdr/dataitem/?";
    String parsedURL;
    String dataURL = "/timeseries";

    public List getTDRData(String serialNo, String subjectUUID, String requestTimeStampStart, String requestTimeStampEnd, Long page, String timeZone) {
        parsedURL = baseURL + dataURL;
        long convertedStartDateTime = 0;
        long convertedEndDateTime = 0;
        requestTimeStampStart = requestTimeStampStart + " 00:00";
        requestTimeStampEnd = requestTimeStampEnd + " 23:59";

        convertedStartDateTime = dateTimeUtil.shiftTimeZone(requestTimeStampStart, timeZone, "UTC");
        convertedEndDateTime = dateTimeUtil.shiftTimeZone(requestTimeStampEnd, timeZone, "UTC");
        /*convertedStartDateTime = convertedStartDateTime;
        convertedEndDateTime = convertedEndDateTime;*/

        log.debug("TDRURL - " + parsedURL);
        //RestTemplate restTemplate = new RestTemplate();

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(parsedURL)
            // Add query parameter
            .queryParam("serialNo", serialNo)
            .queryParam("subjectUUID", subjectUUID)
            .queryParam("requestTimeStampStart", convertedStartDateTime)
            .queryParam("requestTimeStampEnd", convertedEndDateTime)
            .queryParam("page", page);
        log.debug("URI builder - " + builder.build().toUri());
        ResponseEntity<List<TDRDataDTO>> respData = restTemplate.exchange(builder.build().toUri(),
            HttpMethod.GET, null, new ParameterizedTypeReference<List<TDRDataDTO>>() {
            });
        List<TDRDataDTO> dataList = respData.getBody();
        HttpHeaders headers = respData.getHeaders();
        List responseList = new ArrayList();
        responseList.add(dataList);
        responseList.add(headers);
        parsedURL = "";
        return responseList;
    }

}
