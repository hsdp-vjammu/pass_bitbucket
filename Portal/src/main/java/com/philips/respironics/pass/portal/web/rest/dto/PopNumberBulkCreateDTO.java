package com.philips.respironics.pass.portal.web.rest.dto;

import java.io.Serializable;

/**
 * A DTO for the creating POP numbers in bulk.
 */
public class PopNumberBulkCreateDTO implements Serializable {

    private String[] numbers;

    public String[] getNumbers() { return numbers; }
    public void setNumbers(String[] numbers) {
        this.numbers = numbers;
    }

}
