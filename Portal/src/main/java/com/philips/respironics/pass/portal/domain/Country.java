package com.philips.respironics.pass.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 * A country.
 */
@Entity
@Table(name = "country")
//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @NotNull
    @Column(length = 5)
    @JsonIgnore
    private String displayLanguage;

    @NotNull
    @Size(min = 2, max = 2)
    @Column(name="alpha_2", length = 2)
    private String alpha2;

    @NotNull
    @Size(min = 3, max = 3)
    @Column(name="alpha_3", length = 3)
    private String alpha3;

    @NotNull
    private String currencyCode;

    @NotNull
    private String currencyName;

    @NotNull
    private String name;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayLanguage() {
        return displayLanguage;
    }
    public void setDisplayLanguage(String displayLanguage) {
        this.displayLanguage = displayLanguage;
    }

    public String getAlpha2() {
        return alpha2;
    }
    public void setAlpha2(String alpha2) {
        this.alpha2 = alpha2;
    }

    public String getAlpha3() {
        return alpha3;
    }
    public void setAlpha3(String alpha3) {
        this.alpha3 = alpha3;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }
    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Country authority = (Country) o;
        if(authority.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, authority.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Country{" +
            "id=" + id +
            ", diaplay_language='" + displayLanguage + "'" +
            ", alpha_2='" + alpha2 + "'" +
            ", alpha_3='" + alpha3 + "'" +
            "}";
    }
}
