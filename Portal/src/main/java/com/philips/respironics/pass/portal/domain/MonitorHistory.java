package com.philips.respironics.pass.portal.domain;

import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatusChangeAction;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Monitor change record.
 */
@Entity
@Table(name = "monitor_history")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MonitorHistory extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "serial_number")
    private String serialNumber;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, updatable = false)
    private MonitorType type;

    @ManyToOne
    @NotNull
    private Monitor monitor;

    @ManyToOne
    @NotNull
    private Company company;

    @NotNull
    @Column(name = "company_name", nullable = false, updatable = false)
    private String companyName;

    @ManyToOne
    private Subject assignedSubject;

    @Column(name = "assigned_subject_name", nullable = false, updatable = false)
    private String assignedSubjectName;

    @Column(name = "assigned_subject_reference", nullable = false, updatable = false)
    private String assignedSubjectReference;

    @Column(name = "assigned_subject_study_name", nullable = false, updatable = false)
    private String assignedSubjectStudyName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name="change_action", nullable = false)
    private MonitorStatusChangeAction changeAction;

    public MonitorHistory() {

    }

    public MonitorHistory(MonitorStatusChangeAction changeAction, Monitor monitor, Company company, Subject assignedSubject) {
        if (monitor == null) throw new IllegalArgumentException("missing required argument 'monitor'");
        if (company == null) throw new IllegalArgumentException("missing required argument 'company'");
        if ((MonitorStatusChangeAction.ASSIGN_SUBJECT.equals(changeAction) || MonitorStatusChangeAction.ASSIGN_SUBJECT.equals(changeAction))
            && assignedSubject == null) {
            throw new IllegalArgumentException("missing required argument 'assignedSubject'");
        }

        this.changeAction = changeAction;

        this.monitor = monitor;
        this.serialNumber = monitor.getSerialNumber();
        this.type = monitor.getType();

        this.company = company;
        this.companyName = company.getName();

        if (assignedSubject != null) {
            this.assignedSubjectReference = assignedSubject.getExternalReference();
            this.assignedSubject = assignedSubject;

            StringBuilder subjectName = new StringBuilder();
            if (!StringUtils.isBlank(assignedSubject.getFirstName())) {
                subjectName.append(assignedSubject.getFirstName());
            }
            subjectName.append(" ");
            if (!StringUtils.isBlank(assignedSubject.getLastName())) {
                subjectName.append(assignedSubject.getLastName());
            }

            this.assignedSubjectName = subjectName.toString().trim();
            if (assignedSubject.getStudy() != null) {
                this.assignedSubjectStudyName = assignedSubject.getStudy().getName();
            }
        }

    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public MonitorType getType() {
        return type;
    }
    public void setType(MonitorType type) {
        this.type = type;
    }

    public Monitor getMonitor() {
        return monitor;
    }
    public void setMonitor(Monitor monitor) { this.monitor = monitor; }

    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) { this.company = company; }

    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Subject getAssignedSubject() {
        return assignedSubject;
    }
    public void setAssignedSubject(Subject subject) {
        this.assignedSubject = subject;
    }

    public String getAssignedSubjectReference() {
        return assignedSubjectReference;
    }
    public void setAssignedSubjectReference(String assignedSubjectReference) {
        this.assignedSubjectReference = assignedSubjectReference;
    }

    public String getAssignedSubjectName() {
        return assignedSubjectName;
    }
    public void setAssignedSubjectName(String assignedSubjectName) {
        this.assignedSubjectName = assignedSubjectName;
    }

    public String getAssignedSubjectStudyName() {
        return assignedSubjectStudyName;
    }
    public void setAssignedSubjectStudyName(String assignedSubjectStudyName) {
        this.assignedSubjectStudyName = assignedSubjectStudyName;
    }

    public MonitorStatusChangeAction getChangeAction() {
        return changeAction;
    }
    public void setChangeAction(MonitorStatusChangeAction changeAction) {
        this.changeAction = changeAction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MonitorHistory monitor = (MonitorHistory) o;
        if(monitor.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, monitor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MonitorHistory{" +
            "id=" + id +
            ", serialNumber='" + serialNumber + "'" +
            ", type='" + type + "'" +
            ", changeAction='" + changeAction + "'" +
            '}';
    }
}
