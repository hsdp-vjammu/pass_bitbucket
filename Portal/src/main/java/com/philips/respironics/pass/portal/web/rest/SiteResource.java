package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.search.SiteSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.domain.search.SubjectSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.*;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.SiteService;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.SiteBulkCreateDTO;
import com.philips.respironics.pass.portal.web.rest.dto.SiteDTO;
import com.philips.respironics.pass.portal.web.rest.dto.SubjectDTO;
import com.philips.respironics.pass.portal.web.rest.dto.UserDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.apache.http.entity.ContentType;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing Site.
 */
@RestController
@RequestMapping("/api")
public class SiteResource {

    private final Logger log = LoggerFactory.getLogger(SiteResource.class);

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private SiteService siteService;

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private SubjectRepository subjectRepository;
    @Inject
    private MonitorRepository monitorRepository;

    @Inject
    private UserService userService;

    /**
     * POST  /sites : Create a new site.
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new studyDTO, or with status 400 (Bad Request) if the study has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/sites",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    @Transactional
    public ResponseEntity<String> createSite(@RequestParam("data") String json, @RequestParam(value = "file", required = false) MultipartFile file) throws URISyntaxException {

        SiteBulkCreateDTO sitesDTO;
        try {
            sitesDTO = new ObjectMapper().readValue(json, SiteBulkCreateDTO.class);
        } catch (IOException e) {
            // Unable to parse data
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("site", "invalidCreationRequest", "Invalid request for creating sites")).body(null);
        }

        User user = userService.getUserWithAuthorities();
        Study study = studyRepository.findOne(sitesDTO.getStudyId());
        if (study != null && user.hasMembership(study.getCompany())) {
            Map<String, Object> fileValidationErrors = new HashMap<>();
            Set<String> validSiteReferences = new HashSet<>();
            if (file != null && !file.isEmpty()) {
                if (ContentType.TEXT_PLAIN.getMimeType().equals(file.getContentType())) {
                    try {
                        InputStream inputStream = null;
                        Scanner sc = null;
                        try {
                            inputStream = file.getInputStream();
                            sc = new Scanner(inputStream, "UTF-8");
                            Map<String, List<String>> invalidSiteReferences = new HashMap<>();
                            while (sc.hasNextLine()) {
                                String line = sc.nextLine();
                                List<String> errorCodes = siteService.validateReference(study, line);
                                if (!errorCodes.isEmpty()) {
                                    invalidSiteReferences.put(line, errorCodes);
                                } else {
                                    validSiteReferences.add(line);
                                }
                            }
                            // note that Scanner suppresses exceptions
                            if (sc.ioException() != null) {
                                throw sc.ioException();
                            }
                            if (!invalidSiteReferences.isEmpty()) {
                                fileValidationErrors.put("error", "invalidSiteReferences");
                                fileValidationErrors.put("siteReferences", invalidSiteReferences);
                            }
                        } finally {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                            if (sc != null) {
                                sc.close();
                            }
                        }
                    } catch (IOException ex) {
                        fileValidationErrors.put("error", "errorReadingFile");
                    }
                } else {
                    fileValidationErrors.put("error", "invalidFileType");
                }
            }

            if (sitesDTO.getReferences() != null) {
                for (String reference: sitesDTO.getReferences()) {
                    if (!validSiteReferences.contains(reference)) {
                        List<String> errorCodes = siteService.validateReference(study, reference);
                        if (errorCodes.isEmpty()) {
                            validSiteReferences.add(reference);
                        }
                    }
                }
            }

            if (fileValidationErrors.isEmpty()) {
                siteService.createSites(study, validSiteReferences);
                return ResponseEntity.created(new URI("/api/sites/"))
                    .headers(HeaderUtil.createEntityCreationAlert("site", Integer.toString(validSiteReferences.size())))
                    .body(null);
            } else {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("site", "uploadError", "Error during upload"))
                    .body(new JSONObject(fileValidationErrors).toString());
            }
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("company", "invalid", "User has no access to the given company")).body(null);
    }

    /**
     * GET  /sites : get all the studies for a company.
     *
     * @param studyId the study id to display information for
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of studies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/sites",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<List<SiteDTO>> getAllSitesForCompany(@RequestParam Long studyId, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Sites of a given Study");

        User user = userService.getUserWithAuthorities();
        Study study = studyRepository.findOne(studyId);
        if (study != null && user.hasMembership(study.getCompany())) {

            Map<String,String> searchParams;
            try {
                searchParams = new ObjectMapper().readValue(search, HashMap.class);
            } catch (IOException e) {
                // Unable to parse search parameters
                searchParams = new HashMap<>();
            }

            Page<Site> page;
            if (searchParams.isEmpty()) {
                page = siteRepository.findAllByStudy(study, pageable);
            } else {
                page = siteRepository.findAll(SiteSearchSpecificationBuilder.build(study, searchParams), pageable);
            }
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sites");
            List<SiteDTO> dtos = new ArrayList<>();
            for (Site site : page.getContent()) {
                SiteDTO dto = new SiteDTO(site);
                dto.setNumberOfSubjects(subjectRepository.countBySite(site));
                dtos.add(dto);

                site.getMembershipAccessLevels().stream().forEach(accessLevel -> {
                    dto.getCoordinators().add(new UserDTO(accessLevel.getMembership().getUser()));
                });
            }
            return new ResponseEntity<>(dtos, headers, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /sites/study/:study_id : get all the sites for a study.
     *
     * @param study_id the study id to display information for
     * @return the ResponseEntity with status 200 (OK) and the list of studies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/sites/study/{study_id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<List<SiteDTO>> listAllSitesForStudy(@PathVariable Long study_id)
        throws URISyntaxException {
        log.debug("REST request to get all Sites of a Study");

        List<SiteDTO> siteDTOs = new ArrayList<>();

        User user = userService.getUserWithAuthorities();
        Study study = studyRepository.findOne(study_id);
        Membership membership = user.getMembership(study.getCompany());
        if (study != null && membership != null) {
            List<Site> sites = siteRepository.findAllByStudy(study);
            siteDTOs = SiteDTO.convert(sites);
        }

        return new ResponseEntity<>(siteDTOs, HttpStatus.OK);
    }

    /**
     * GET  /sites/:id : get the "id" site.
     *
     * @param id the id of the studyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the studyDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/sites/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<SiteDTO> getSite(@PathVariable Long id) {
        log.debug("REST request to get Site : {}", id);
        Site site = siteRepository.findOne(id);
        SiteDTO siteDTO = new SiteDTO(site);
        Company company = siteDTO.getCompany();
        long assignedMonitorCount=0;
        long unassignedMonitorCount=0;
        List<Monitor> monitors = monitorRepository.findAllByCompany(company);
        for (Monitor monitor:monitors) {
            if(monitor.getStatus()== MonitorStatus.ASSIGNED){
                if(monitor.getAssignedSubject()!=null){
                    if((monitor.getAssignedSubject().getSite().getId())== id){
                    assignedMonitorCount++;}
                }
            }
            else  if (monitor.getStatus()==MonitorStatus.UNASSIGNED)
            {
                unassignedMonitorCount++;
            }
        }
        //List<Monitor> assignedMonitors1 =monitors.stream().filter(p->p.getAssignedSubject().getSite().getId()==id).collect(Collectors.toList());
        siteDTO.setNumberOfSubjects(subjectRepository.countBySite(site));
        siteDTO.setNumberOfUnassignedMonitors(unassignedMonitorCount);
        siteDTO.setNumberOfSubjectsAssignedToMonitors((long)assignedMonitorCount);
        return Optional.ofNullable(siteDTO)
            .map(result -> {
                return new ResponseEntity<>(result, HttpStatus.OK);
            })
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /sites : delete one ot multiple sites.
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/sites",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    @Transactional
    public ResponseEntity<Void> deleteSite(@RequestParam Long studyId, @RequestParam Long[] ids, HttpServletRequest request) {
        log.debug("REST request to delete Sites for study : {}, {}", studyId, ids);
        User user = userService.getUserWithAuthorities();
        Study study = studyRepository.findOne(studyId);
        int deletedSitesCount = 0;
        if (study != null && user.hasMembership(study.getCompany())) {
            for (Long siteId : ids) {
                Site site = siteRepository.getOne(siteId);
                if (site != null && site.getStudy().getId() == studyId && site.getSubjects().isEmpty()) {
                    siteService.deleteSite(site, user, request);
                    deletedSitesCount += 1;
                }
            }
            return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("site", Integer.toString(deletedSitesCount))).build();
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    //@RequestParam Long studyId
    /**
     * GET  /sites : get all the studies for a company.
     *
     * @param id the site id to display information for
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of studies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/sites/{id}/subjects",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<List<SubjectDTO>> getAllSubjectsForSite(@PathVariable Long id, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Subjects of a given Site");

        User user = userService.getUserWithAuthorities();
        Site site = siteRepository.findOne(id);
        if (site != null && user.hasMembership(site.getStudy().getCompany())) {

            Map<String,String> searchParams;
            try {
                searchParams = new ObjectMapper().readValue(search, HashMap.class);
            } catch (IOException e) {
                // Unable to parse search parameters
                searchParams = new HashMap<>();
            }

            Page<Subject> page;
            if (searchParams.isEmpty()) {
                page = subjectRepository.findAllBySite(site, pageable);
            } else {
                page = subjectRepository.findAll(SubjectSearchSpecificationBuilder.build(site, searchParams), pageable);
            }
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/sites");

            return new ResponseEntity<>(SubjectDTO.convert(page.getContent()), headers, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /sites/validate/:studyId/:reference : validate the reference of a site.
     *
     * @param studyId the id of the study
     * @param reference the reference to validate
     * @return the ResponseEntity with status 200 (OK), or with status 409 (Conflict)
     */
    @RequestMapping(value = "/sites/validate/{studyId}/{reference}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<String> validateSiteReference(@PathVariable Long studyId, @PathVariable String reference) {

        List<String> errorCodes = siteService.validateReference(studyId, reference);
        if (!errorCodes.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }


}
