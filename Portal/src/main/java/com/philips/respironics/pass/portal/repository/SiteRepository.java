package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Site;
import com.philips.respironics.pass.portal.domain.Study;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Site entity.
 */
@SuppressWarnings("unused")
public interface SiteRepository extends JpaRepository<Site,Long>, JpaSpecificationExecutor<Site> {

    Optional<Site> findOneByStudyAndExternalReference(Study study, String externalReference);

    Long countByStudy(Study study);

    Long countByStudyAndExternalReferenceIgnoreCase(Study study, String externalReference);

    List<Site> findAllByStudy(Study study);

    Page<Site> findAllByStudy(Study study, Pageable pageable);

}
