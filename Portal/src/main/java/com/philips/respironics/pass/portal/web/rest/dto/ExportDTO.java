package com.philips.respironics.pass.portal.web.rest.dto;

import java.io.Serializable;

public class ExportDTO implements Serializable {

    private String serialNo;
    private String subjectUUID;
    private String requestTimeStampStart;
    private String requestTimeStampEnd;
    private String timeZone;
    private String fileName;
    private String checkForPwd;
    private String password;
    private String repeatPwd;
    private String healthBandFirmwareVer;
    private String mobileAppSWVer;
    private String PASWebAppSWVer;
    private String studyName;
    private String configurationName;

    public String getPASWebAppSWVer() {
        return PASWebAppSWVer;
    }

    public void setPASWebAppSWVer(String PASWebAppSWVer) {
        this.PASWebAppSWVer = PASWebAppSWVer;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getSubjectUUID() {
        return subjectUUID;
    }

    public void setSubjectUUID(String subjectUUID) {
        this.subjectUUID = subjectUUID;
    }

    public String getRequestTimeStampStart() {
        return requestTimeStampStart;
    }

    public void setRequestTimeStampStart(String requestTimeStampStart) {
        this.requestTimeStampStart = requestTimeStampStart;
    }

    public String getRequestTimeStampEnd() {
        return requestTimeStampEnd;
    }

    public void setRequestTimeStampEnd(String requestTimeStampEnd) {
        this.requestTimeStampEnd = requestTimeStampEnd;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getCheckForPwd() {
        return checkForPwd;
    }

    public void setCheckForPwd(String checkForPwd) {
        this.checkForPwd = checkForPwd;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPwd() {
        return repeatPwd;
    }

    public void setRepeatPwd(String repeatPwd) {
        this.repeatPwd = repeatPwd;
    }

    public String getHealthBandFirmwareVer() {
        return healthBandFirmwareVer;
    }

    public void setHealthBandFirmwareVer(String healthBandFirmwareVer) {
        this.healthBandFirmwareVer = healthBandFirmwareVer;
    }

    public String getMobileAppSWVer() {
        return mobileAppSWVer;
    }

    public void setMobileAppSWVer(String mobileAppSWVer) {
        this.mobileAppSWVer = mobileAppSWVer;
    }


    public String getStudyName() {
        return studyName;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public String getConfigurationName() {
        return configurationName;
    }

    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
}
