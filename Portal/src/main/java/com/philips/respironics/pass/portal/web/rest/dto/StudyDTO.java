package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Study;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Study entity.
 */
public class StudyDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String uniqueIdentifier;

    private String externalReference;

    @Size(max = 25)
    private String name;

    @Size(max = 75)
    private String description;

    @Size(max = 255)
    private String comments;

    private StudyStatus status;

    private ZonedDateTime dateClosed;

    private Long companyId;

    private String companyName;

    private Long numberOfSites = 0L;

    private Long numberOfSubjects = 0L;

    public StudyDTO() {}

    public StudyDTO(Study entity) {
        this.id = entity.getId();
        this.uniqueIdentifier = entity.getUniqueIdentifier();
        this.externalReference = entity.getExternalReference();
        this.name = entity.getName();
        this.description = entity.getDescription();
        this.comments = entity.getComments();
        this.status = entity.getStatus();
        this.dateClosed = entity.getDateClosed();
        if (entity.getCompany() != null) {
            this.companyId = entity.getCompany().getId();
            this.companyName = entity.getCompany().getName();
        }

        this.setCreatedDate(entity.getCreatedDate());
        this.setCreatedBy(entity.getCreatedBy());
        this.setLastModifiedDate(entity.getLastModifiedDate());
        this.setLastModifiedBy(entity.getLastModifiedBy());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }
    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public String getExternalReference() {
        return externalReference;
    }
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }

    public StudyStatus getStatus() {
        return status;
    }
    public void setStatus(StudyStatus status) {
        this.status = status;
    }

    public ZonedDateTime getDateClosed() {
        return dateClosed;
    }
    public void setDateClosed(ZonedDateTime dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getNumberOfSites() { return numberOfSites; }
    public void setNumberOfSites(Long number) { this.numberOfSites = number; }

    public Long getNumberOfSubjects() { return numberOfSubjects; }
    public void setNumberOfSubjects(Long number) { this.numberOfSubjects = number; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StudyDTO studyDTO = (StudyDTO) o;

        if ( ! Objects.equals(id, studyDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "StudyDTO{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", externalReference='" + externalReference + "'" +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", status='" + status + "'" +
            ", dateClosed='" + dateClosed + "'" +
            '}';
    }

    public static List<StudyDTO> convert(List<Study> entities)
    {
        List<StudyDTO> dtos = new ArrayList<>();
        for (Study entity : entities) {
            dtos.add(new StudyDTO(entity));
        }
        return dtos;
    }

}
