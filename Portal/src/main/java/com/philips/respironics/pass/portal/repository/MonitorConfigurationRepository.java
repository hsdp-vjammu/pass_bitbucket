package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.MonitorConfiguration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the MonitorConfiguration entity.
 */
@SuppressWarnings("unused")
public interface MonitorConfigurationRepository extends JpaRepository<MonitorConfiguration,Long>, JpaSpecificationExecutor<MonitorConfiguration> {

    Optional<MonitorConfiguration> findOneById(Long id);

    Page<MonitorConfiguration> findAllByCompany(Company company, Pageable var1);

    List<MonitorConfiguration> findAllByCompany(Company company);

}
