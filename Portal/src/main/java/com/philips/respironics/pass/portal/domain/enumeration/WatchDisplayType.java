package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The WatchDisplayType enumeration.
 */
public enum WatchDisplayType {
    NONE(null),
    DIGITAL("DIGITAL"),
    ANALOG("ANALOG");

    private final String text;

    WatchDisplayType(final String text) {
        this.text = text;
    }

    public static WatchDisplayType fromString(String text) {
        if (text != null) {
            for (WatchDisplayType b : WatchDisplayType.values()) {
                if (text.equalsIgnoreCase(b.text)) {
                    return b;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }
}
