package com.philips.respironics.pass.portal.validator;

import com.philips.respironics.pass.portal.domain.Setting;
import com.philips.respironics.pass.portal.repository.SettingRepository;
import com.philips.respironics.pass.portal.security.SecurityUtils;

import javax.inject.Inject;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordStrengthValidator implements ConstraintValidator<PasswordStrength, String> {

    @Inject
    private SettingRepository settingRepository;

    @Override
    public void initialize(PasswordStrength passwordStrength) {
        // No init required
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        int minPasswordLength = Integer.parseInt(settingRepository.findOneByCategoryAndName(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.MIN_PASSWORD_LENGTH).getValue());

        return SecurityUtils.validatePasswordStrength(minPasswordLength, s);
    }
}
