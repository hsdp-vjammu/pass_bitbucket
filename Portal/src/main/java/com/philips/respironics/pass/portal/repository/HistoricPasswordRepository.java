package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.HistoricPassword;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the HistoricPassword entity.
 */
public interface HistoricPasswordRepository extends JpaRepository<HistoricPassword, Long> {

}
