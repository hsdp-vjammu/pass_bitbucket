package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.MonitorConfigurationQuestion;
import com.philips.respironics.pass.portal.domain.enumeration.QuestionResponseType;
import com.philips.respironics.pass.portal.domain.enumeration.RecurrencePattern;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the MonitorConfigurationQuestion entity.
 */
public class MonitorConfigurationQuestionDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String text;

    @NotNull
    private QuestionResponseType responseType;

    private String responseValue1;

    private String responseValue2;

    private Double responseRangeMin;

    private Double responseRangeMax;

    private LocalDate recurrenceStartDate;

    private LocalDate recurrenceEndDate;

    private RecurrencePattern recurrencePattern;

    private String[] recurrencePatternWeekdays;

    private ZonedDateTime recurrenceTime1;

    private ZonedDateTime recurrenceTime2;

    private ZonedDateTime recurrenceTime3;

    private String recTime1;

    private String recTime2;

    private String recTime3;


    private int index;

    public MonitorConfigurationQuestionDTO() {
        this.recurrencePatternWeekdays = new String[] {"1-MON", "6-SAT"};
    }

    public MonitorConfigurationQuestionDTO(MonitorConfigurationQuestion entity) {
        this.id = entity.getId();
        this.text = entity.getText();
        this.responseType = entity.getResponseType();
        this.responseValue1 = entity.getResponseValue1();
        this.responseValue2 = entity.getResponseValue2();
        this.responseRangeMin = entity.getResponseRangeMin();
        this.responseRangeMax = entity.getResponseRangeMax();
        this.recurrenceStartDate = entity.getRecurrenceStartDate();
        this.recurrenceEndDate = entity.getRecurrenceEndDate();
        this.recurrencePattern = entity.getRecurrencePattern();
        this.recurrencePatternWeekdays = entity.getRecurrencePatternWeekdays();
        this.recurrenceTime1 = entity.getRecurrenceTime1();
        this.recurrenceTime2 = entity.getRecurrenceTime2();
        this.recurrenceTime3 = entity.getRecurrenceTime3();
        this.recTime1 = entity.getRecTime1();
        this.recTime2 = entity.getRecTime2();
        this.recTime3 = entity.getRecTime3();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }

    public QuestionResponseType getResponseType() {
        return responseType;
    }
    public void setResponseType(QuestionResponseType responseType) {
        this.responseType = responseType;
    }

    public String getResponseValue1() {
        return responseValue1;
    }
    public void setResponseValue1(String value) {
        this.responseValue1 = value;
    }

    public String getResponseValue2() {
        return responseValue2;
    }
    public void setResponseValue2(String value) {
        this.responseValue2 = value;
    }

    public Double getResponseRangeMin() {
        return responseRangeMin;
    }
    public void setResponseRangeMin(Double value) {
        this.responseRangeMin = value;
    }

    public Double getResponseRangeMax() {
        return responseRangeMax;
    }
    public void setResponseRangeMax(Double value) {
        this.responseRangeMax = value;
    }

    public LocalDate getRecurrenceStartDate() {
        return recurrenceStartDate;
    }
    public void setRecurrenceStartDate(LocalDate date) { this.recurrenceStartDate = date; }

    public LocalDate getRecurrenceEndDate() {
        return recurrenceEndDate;
    }
    public void setRecurrenceEndDate(LocalDate date) { this.recurrenceEndDate = date; }

    public String[] getRecurrencePatternWeekdays() {
        return recurrencePatternWeekdays;
    }
    public void setRecurrencePatternWeekdays(String[] values) { this.recurrencePatternWeekdays = values; }

    public RecurrencePattern getRecurrencePattern() {
        return recurrencePattern;
    }
    public void setRecurrencePattern(RecurrencePattern pattern) { this.recurrencePattern = pattern; }

    public ZonedDateTime getRecurrenceTime1() {
        return recurrenceTime1;
    }
    public void setRecurrenceTime1(ZonedDateTime time) { this.recurrenceTime1 = time; }

    public ZonedDateTime getRecurrenceTime2() {
        return recurrenceTime2;
    }
    public void setRecurrenceTime2(ZonedDateTime time) { this.recurrenceTime2 = time; }

    public ZonedDateTime getRecurrenceTime3() { return recurrenceTime3; }
    public void setRecurrenceTime3(ZonedDateTime time) { this.recurrenceTime3 = time; }

    public String getRecTime1() {
        return recTime1;
    }

    public void setRecTime1(String recTime1) {
        this.recTime1 = recTime1;
    }

    public String getRecTime2() {
        return recTime2;
    }

    public void setRecTime2(String recTime2) {
        this.recTime2 = recTime2;
    }

    public String getRecTime3() {
        return recTime3;
    }

    public void setRecTime3(String recTime3) {
        this.recTime3 = recTime3;
    }

    public int getIndex() { return index; }
    public void setIndex(int index) { this.index = index; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MonitorConfigurationQuestionDTO dto = (MonitorConfigurationQuestionDTO) o;

        if ( !Objects.equals(id, dto.id) && !Objects.equals(text, dto.text)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text);
    }

    @Override
    public String toString() {
        return "MonitorConfigurationQuestionDTO{" +
            "id=" + id +
            ", text='" + text + "'" +
            ", responseType='" + responseType + "'" +
            '}';
    }

    public static List<MonitorConfigurationQuestionDTO> convert(List<MonitorConfigurationQuestion> entities)
    {
        List<MonitorConfigurationQuestionDTO> dtos = new ArrayList<>();
        for (MonitorConfigurationQuestion entity : entities) {
            dtos.add(new MonitorConfigurationQuestionDTO(entity));
        }
        return dtos;
    }

}
