package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.philips.respironics.pass.portal.service.FeedbackDataService;
import com.philips.respironics.pass.portal.web.rest.dto.FeedbackDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller to extract TDR Data.
 * Export TDR Data using Apache POI
 */
@RestController
@RequestMapping("api")
public class FeedbackDataResource {
    private final Logger log = LoggerFactory.getLogger(TDRDataResource.class);
    @Inject
    private FeedbackDataService feedbackDataService;

    @RequestMapping(value = "/feedback",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<FeedbackDataDTO>> getFeedbackData(
        @RequestParam(value = "serialNo", required = true) String serialNo,
        @RequestParam(value = "subjectId", required = true) String subjectId,
        @RequestParam(value = "feedbackTimeStampStart", required = true) String requestTimeStampStart,
        @RequestParam(value = "feedbackTimeStampEnd", required = true) String requestTimeStampEnd,
        @RequestParam(value = "timeZone", required = true) String timeZone,
        @RequestParam(value = "page", required = true) Long page
    ) {
        log.debug("REST request to get tdr data");
        List data = feedbackDataService.getFeedbackData(serialNo, subjectId, requestTimeStampStart, requestTimeStampEnd, page, timeZone);
        List<FeedbackDataDTO> tdrData = (List<FeedbackDataDTO>) data.get(0);
        //Convert localTimeStamp from unix to date time format
        List<FeedbackDataDTO> convertedTDRData = tdrData.stream()
            .map(FeedbackDataDTO -> dataFormatter(FeedbackDataDTO, timeZone))
            .collect(Collectors.toList());

        HttpHeaders headers = new HttpHeaders();
        HttpHeaders headersFromTDR = (HttpHeaders) data.get(1);
        headers.add("Totalelements", headersFromTDR.get("Totalelements").get(0));
        headers.add("Totalpages", headersFromTDR.get("Totalpages").get(0));

        return new ResponseEntity<>(convertedTDRData, headers, HttpStatus.OK);
    }

    private FeedbackDataDTO dataFormatter(FeedbackDataDTO feedbackDataEntry, String timeZone) {

        /*String convertedDate = feedbackDataEntry.getTimeStamp(feedbackDataEntry.getTimesStamp(), timeZone);
        feedbackDataEntry.setTimesStamp(convertedDate);*/

        String feedbackConvertedDate = feedbackDataEntry.getTimeStamp(feedbackDataEntry.getFeedbackTimestamp(), timeZone);
        feedbackDataEntry.setFeedbackTimestamp(feedbackConvertedDate);

        String notificationFeedbackConvertedDate = feedbackDataEntry.getTimeStamp(feedbackDataEntry.getNotificationFireTimeStamp(), timeZone);
        feedbackDataEntry.setNotificationFireTimeStamp(notificationFeedbackConvertedDate);

        return feedbackDataEntry;
    }
}
