package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    //Reusable constants
    String SELECTED_COLUMNS = "select jhiUser.id, jhiUser.first_name,jhiUser.last_name,jhiUser.email," +
        " jhiUser.login, jhiUser.activated, jhiUser.locked, companyUser.enabled, " +
        " jua.authority_name, cua.access_level, jhiUser.created_by, jhiUser.created_date, jhiUser.last_modified_by, jhiUser.last_modified_date, jhiUser.activation_key, jhiUser.lang_key, jhiUser.password_hash, " +
        " jhiUser.password_changed, jhiUser.reset_key, jhiUser.reset_date ";
    String COMMON_FROM_WHERE_CONDITION = " from company company, company_user companyUser, jhi_user jhiUser, jhi_user_authority jua, company_user_access cua " +
        " where " +
        "company.id = companyUser.company_id  and " +
        "jhiUser.id = companyUser.user_id and " +
        "companyUser.user_id = jua.user_id and " +
        "companyUser.id = cua.company_user_id  ";
    String GROUP_ORDER_BY_CRITERIA = "group by jhiUser.id order by jhiUser.id \n#pageable\n";
    //String GROUP_ORDER_BY_CRITERIA = " order by jhiUser.id \n#pageable\n";
    String COUNT_QUERY = "select count(*) ";

    //Build Query using above constants
    //Query#1 - findAllUsers
    String FIND_ALL_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_COUNT_QUERY = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + GROUP_ORDER_BY_CRITERIA;


    //Production Users query
    //Query#2 - findAllUsersWithParams
    String PARAMS_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.last_name LIKE :lastName and " + "company.Name LIKE :company ";
    String FIND_ALL_WITH_PARAMS_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + PARAMS_CRITERIA  + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_PARAMS_COUNT_CRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + PARAMS_CRITERIA + GROUP_ORDER_BY_CRITERIA;

    //Query#3 - findAllUsersWithStatusActiveOrInActive
    String ACTIVE_INACTIVE_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.activated = :activated and companyUser.enabled = :enabled and jhiUser.locked IS NULL and " +
        " jhiUser.last_name LIKE :lastName and " + " company.Name LIKE :company ";
    String FIND_ALL_WITH_STATUS_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + ACTIVE_INACTIVE_CRITERIA  + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_STATUS_COUNT_CRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + ACTIVE_INACTIVE_CRITERIA + GROUP_ORDER_BY_CRITERIA;


    //Query#4 - findAllUsersWithStatusLocked
    String LOCKED_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.activated = :activated and jhiUser.locked IS NOT NULL and " + " jhiUser.last_name LIKE :lastName and " +
        "company.Name LIKE :company ";
    String FIND_ALL_WITH_STATUS_LOCKED_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + LOCKED_CRITERIA  + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_STATUS_LOCKED_COUNT_CRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + LOCKED_CRITERIA + GROUP_ORDER_BY_CRITERIA;


    //Query#5 - findAllWithStatusUnverified
    String UNVERIFIED_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.activated = :activated and " + " jhiUser.last_name LIKE :lastName and " + " company.Name LIKE :company ";
    String FIND_ALL_WITH_STATUS_UNVERIFIED_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + UNVERIFIED_CRITERIA + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_STATUS_UNVERIFIED_COUNT_CRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + UNVERIFIED_CRITERIA + GROUP_ORDER_BY_CRITERIA;


    //non production support users
    //Query#1.2 - findAllUsersWithCompany
    String COMPANY_CRITERIA = " and company.id = :cid ";
    String FIND_ALL_WITH_COMPANY_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_COMPANY_COUNT_CRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;

    //Query#2 - findAllUsersWithParams
    //String PARAMS_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.last_name LIKE :lastName and " + "company.Name LIKE :company ";
    String FIND_ALL_WITH_PS_PARAMS_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + PARAMS_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_PARAMS_COUNT_PSCRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + PARAMS_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;

    //Query#3 - findAllUsersWithStatusActiveOrInActive
    //String ACTIVE_INACTIVE_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.activated = :activated and companyUser.enabled = :enabled and jhiUser.locked IS NULL and " +
    //    " jhiUser.last_name LIKE :lastName and " + " company.Name LIKE :company ";
    String FIND_ALL_WITH_PS_STATUS_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + ACTIVE_INACTIVE_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_STATUS_COUNT_PSCRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + ACTIVE_INACTIVE_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;


    //Query#4 - findAllUsersWithStatusLocked
    //String LOCKED_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.activated = :activated and jhiUser.locked IS NOT NULL and " + " jhiUser.last_name LIKE :lastName and " +
    //    "company.Name LIKE :company ";
    String FIND_ALL_WITH_PS_STATUS_LOCKED_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + LOCKED_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_STATUS_LOCKED_COUNT_PSCRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + LOCKED_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;


    //Query#5 - findAllWithStatusUnverified
    //String UNVERIFIED_CRITERIA = " and jhiUser.email LIKE :email and " + " jhiUser.activated = :activated and " + " jhiUser.last_name LIKE :lastName and " + " company.Name LIKE :company ";
    String FIND_ALL_WITH_PS_STATUS_UNVERIFIED_CRITERIA = SELECTED_COLUMNS + COMMON_FROM_WHERE_CONDITION + UNVERIFIED_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;
    String FIND_ALL_WITH_STATUS_UNVERIFIED_COUNT_PSCRITERIA = COUNT_QUERY + COMMON_FROM_WHERE_CONDITION + UNVERIFIED_CRITERIA + COMPANY_CRITERIA + GROUP_ORDER_BY_CRITERIA;

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(ZonedDateTime dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);

    Optional<User> findOneById(Long userId);

    Long countByEmail(String email);

    Page<User> findAllByMembershipsCompany(Company company, Pageable pageable);

    @Override
    void delete(User t);

    //Admin queries
    @Query(value = FIND_ALL_CRITERIA, countQuery = FIND_ALL_COUNT_QUERY, nativeQuery = true)
    Page<User> findAllUsers(Pageable pageable);

    @Query(value = FIND_ALL_WITH_PARAMS_CRITERIA, countQuery = FIND_ALL_WITH_PARAMS_COUNT_CRITERIA, nativeQuery = true)
    Page<User> findAllProdSupportUsersWithParams(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, Pageable pageable);

    @Query(value = FIND_ALL_WITH_STATUS_CRITERIA, countQuery = FIND_ALL_WITH_STATUS_COUNT_CRITERIA, nativeQuery = true)
    Page<User> findAllProdSupportUsersWithStatusActiveOrInActive(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("activated") boolean activated, @Param("enabled") boolean enabled,  Pageable pageable);

    @Query(value = FIND_ALL_WITH_STATUS_LOCKED_CRITERIA, countQuery = FIND_ALL_WITH_STATUS_LOCKED_COUNT_CRITERIA, nativeQuery = true)
    Page<User> findAllProdSupportUsersWithStatusLocked(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("activated") boolean activated,  Pageable pageable);

    @Query(value = FIND_ALL_WITH_STATUS_UNVERIFIED_CRITERIA, countQuery = FIND_ALL_WITH_STATUS_UNVERIFIED_COUNT_CRITERIA, nativeQuery = true)
    Page<User> findAllProdSupportWithStatusUnverified(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("activated") boolean activated, Pageable pageable);

    //with company Information - non Production users
    @Query(value = FIND_ALL_WITH_COMPANY_CRITERIA, countQuery = FIND_ALL_WITH_COMPANY_COUNT_CRITERIA, nativeQuery = true)
    Page<User> findAllUsersWithCompany(Pageable pageable, @Param("cid") Long cid);

    @Query(value = FIND_ALL_WITH_PS_PARAMS_CRITERIA, countQuery = FIND_ALL_WITH_PARAMS_COUNT_PSCRITERIA, nativeQuery = true)
    Page<User> findAllUsersWithParams(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("cid") Long cid, Pageable pageable);

    @Query(value = FIND_ALL_WITH_PS_STATUS_CRITERIA, countQuery = FIND_ALL_WITH_STATUS_COUNT_PSCRITERIA, nativeQuery = true)
    Page<User> findAllUsersWithStatusActiveOrInActive(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("activated") boolean activated, @Param("enabled") boolean enabled, @Param("cid") Long cid, Pageable pageable);

    @Query(value = FIND_ALL_WITH_PS_STATUS_LOCKED_CRITERIA, countQuery = FIND_ALL_WITH_STATUS_LOCKED_COUNT_PSCRITERIA, nativeQuery = true)
    Page<User> findAllUsersWithStatusLocked(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("activated") boolean activated, @Param("cid") Long cid, Pageable pageable);

    @Query(value = FIND_ALL_WITH_PS_STATUS_UNVERIFIED_CRITERIA, countQuery = FIND_ALL_WITH_STATUS_UNVERIFIED_COUNT_PSCRITERIA, nativeQuery = true)
    Page<User> findAllWithStatusUnverified(@Param("email") String email, @Param("lastName") String lastName, @Param("company")
        String company, @Param("activated") boolean activated, @Param("cid") Long cid, Pageable pageable);
}
