package com.philips.respironics.pass.portal.service.util;

import java.util.List;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
/*
    Export Utility to export file with meta data, header and data
 */
public abstract class ExcelExportUtil< E extends Object > {
    protected SXSSFWorkbook wb;
    protected Sheet sh;
    protected static final String EMPTY_VALUE = " ";
    /**
     * This method demonstrates how to Auto resize Excel column
     */
    private void autoResizeColumns(int listSize) {

        for (int colIndex = 0; colIndex < listSize; colIndex++) {
            sh.autoSizeColumn(colIndex);
        }
    }
    /**
     *
     * This method will return Style and Font of Header Cell
     *
     * @return
     */
    protected CellStyle getHeaderStyle() {
        // Create a Font for styling header cells
        Font headerFont = wb.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = wb.createCellStyle();
        headerCellStyle.setFont(headerFont);

        return  headerCellStyle;
    }
    /**
     * @param metaDataRows
     */
    private void fillMetaData(String[] metaDataRows, int metaDataNumberOfRows) {  //size 8 for meta data
        wb = new SXSSFWorkbook(100); // keep 100 rows in memory, exceeding rows will be flushed to disk
        sh = wb.createSheet("Sheet 1");
        ((SXSSFSheet) sh).trackAllColumnsForAutoSizing();  //expensive call, can be removed for optimization
        CellStyle headerStyle = getHeaderStyle();
        int rowNum = 0;  //starts with first row
        for (int i = 0; i < metaDataNumberOfRows; i++) {
            Row row = sh.createRow(rowNum++);
            row.createCell(0).setCellValue(metaDataRows[i]);
            row.setRowStyle(headerStyle);
        }
    }
    /**
     * @param headerColumns
     */
    private void fillHeader(String[] headerColumns, int headerColumnStartAt) {
        CellStyle headerStyle = getHeaderStyle();
            Row row = sh.createRow(headerColumnStartAt);
            for (int cellnum = 0; cellnum < headerColumns.length; cellnum++) {
                Cell cell = row.createCell(cellnum);
                cell.setCellValue(headerColumns[cellnum]);
                cell.setCellStyle(headerStyle);
            }
    }
    /**
     * @param headerColumns
     * @param dataList
     * @return
     */
    public final SXSSFWorkbook exportExcel(String[] metaDataRows, int metaDataNumberOfRows, String[] headerColumns, int headerColumnStartAt, List<E> dataList, int dataListStartingRowNum) {
        fillMetaData(metaDataRows, metaDataNumberOfRows);
        fillHeader(headerColumns, headerColumnStartAt);
        autoResizeColumns(headerColumns.length);
        fillData(dataList, dataListStartingRowNum);
        return wb;
    }
    /**
     * @param dataList
     */
     public abstract void fillData(List<E> dataList, int dataListStartingRowNum);
}
