package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MonitorAssignmentSearchSpecificationBuilder {

    public static Specification<Monitor> build(Company company, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
        }
        return build(company, criteria);
    }

    public static Specification<Monitor> build(Company company, List<SearchParameter> searchCriteria) {

        List<Specification<Monitor>> specs = new ArrayList<>();
        specs.add(createSpecification(company));
        specs.add(createSpecification(new SearchParameter("status", MonitorStatus.ASSIGNED)));

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else if (param.getKey().startsWith("assignedSubject_")) {
                String attributeName = param.getKey().replace("assignedSubject_", "");
                specs.add(MonitorAssignmentSearchSpecificationBuilder.createSubjectSpecification(attributeName, param.getValue()));
            } else if (param.getKey().startsWith("study_")) {
                String attributeName = param.getKey().replace("study_", "");
                specs.add(MonitorAssignmentSearchSpecificationBuilder.createStudySpecification(attributeName, param.getValue()));
            } else if (param.getKey().startsWith("site_")) {
                String attributeName = param.getKey().replace("site_", "");
                specs.add(MonitorAssignmentSearchSpecificationBuilder.createSiteSpecification(attributeName, param.getValue()));
            } else if (param.getKey().startsWith("configuration_")) {
                String attributeName = param.getKey().replace("configuration_", "");
                specs.add(MonitorAssignmentSearchSpecificationBuilder.createConfigurationSpecification(attributeName, param.getValue()));
             } else {
                specs.add(MonitorAssignmentSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<Monitor> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<Monitor> createSpecification(Company company) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("company"), company);
            }
        };
    }

    private static Specification<Monitor> buildGlobalSearch(Object value) {
        List<Specification<Monitor>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("serialNumber", value));
        specs.add(createConfigurationSpecification("name", value));
        specs.add(createStudySpecification("name", value));
        specs.add(createSiteSpecification("externalReference", value));
        specs.add(createSubjectSpecification("firstName", value));
        specs.add(createSubjectSpecification("lastName", value));
        specs.add(createSubjectSpecification("externalReference", value));

        Specification<Monitor> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<Monitor> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<Monitor> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Monitor> createSubjectSpecification(String attributeName, Object value) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, Subject> subject = root.join("assignedSubject", JoinType.LEFT);

                if (subject.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(subject.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(subject.get(attributeName), value);
                }
            }
        };
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Monitor> createStudySpecification(String attributeName, Object value) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, Subject> subject = root.join("assignedSubject", JoinType.LEFT);
                Join<Subject, Study> study = subject.join("study", JoinType.LEFT);

                if (study.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(study.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(study.get(attributeName), value);
                }
            }
        };
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Monitor> createSiteSpecification(String attributeName, Object value) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, Subject> subject = root.join("assignedSubject", JoinType.LEFT);
                Join<Subject, Site> site = subject.join("site", JoinType.LEFT);

                if (site.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(site.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(site.get(attributeName), value);
                }
            }
        };
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Monitor> createConfigurationSpecification(String attributeName, Object value) {
        return new Specification<Monitor>() {
            @Override
            public Predicate toPredicate(Root<Monitor> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Monitor, MonitorConfiguration> configuration = root.join("monitorConfiguration", JoinType.LEFT);

                if (configuration.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(configuration.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(configuration.get(attributeName), value);
                }
            }
        };
    }
}
