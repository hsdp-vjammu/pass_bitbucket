package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The HeightUnit enumeration.
 */
public enum HeightUnit {
    NONE(null),
    CM("cm"),
    IN("in");

    private final String text;

    HeightUnit(final String text) {
        this.text = text;
    }

    public static HeightUnit fromString(String text) {
        if (text != null) {
            for (HeightUnit b : HeightUnit.values()) {
                if (text.equalsIgnoreCase(b.text)) {
                    return b;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }

}
