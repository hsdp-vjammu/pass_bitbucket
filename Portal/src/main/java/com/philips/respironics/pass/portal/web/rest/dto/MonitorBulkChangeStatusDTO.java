package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A DTO for the creating monitors in bulk.
 */
public class MonitorBulkChangeStatusDTO implements Serializable {

    @NotNull
    private Long companyId;

    @NotNull
    private MonitorStatus status;

    private Long[] ids;

    public Long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public MonitorStatus getStatus() {
        return status;
    }
    public void setStatus(MonitorStatus status) {
        this.status = status;
    }

    public Long[] getIds() {
        return ids;
    }
    public void setIds(Long[] ids) {
        this.ids = ids;
    }

}
