package com.philips.respironics.pass.portal.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

//import lombok.Data;

//@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedbackDataDTO implements Serializable {

    //private String timesStamp;  //  1530158460000

    private String id;
    private String subjectUUID;
    private String serialNo;
    private String question;
    private String notificationFireTimeStamp;
    private String feedback;
    private String feedbackTimestamp;

    @JsonCreator
    public FeedbackDataDTO() {
    }

    public String getTimeStamp(String timeStamp, String timeZone) {
        if (!"0".equals(timeStamp) && timeStamp != null) {
            Long newDate = Long.parseLong(timeStamp) * 1000;
            ZonedDateTime dateTime = Instant.ofEpochMilli(newDate).atZone(ZoneId.of(timeZone));
            String formatted = dateTime.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy hh:mm a"));
            return formatted;
        } else {
            return null;
        }
    }

    public String getFeedbackTimestamp() {
        return feedbackTimestamp;
    }

    public void setFeedbackTimestamp(String feedbackTimestamp) {
        this.feedbackTimestamp = feedbackTimestamp;
    }

    public String getNotificationFireTimeStamp() {
        return notificationFireTimeStamp;
    }

    public void setNotificationFireTimeStamp(String notificationFireTimeStamp) {
        this.notificationFireTimeStamp = notificationFireTimeStamp;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectUUID() {
        return subjectUUID;
    }

    public void setSubjectUUID(String subjectUUID) {
        this.subjectUUID = subjectUUID;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

}
