package com.philips.respironics.pass.portal.domain.search;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Created by ihomer on 3/28/17.
 */
public class SearchNullSpecification<T> implements Specification<T> {

    private SearchParameter criteria;

    public SearchNullSpecification(SearchParameter criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        try {
            boolean value;
            if (root.get(criteria.getKey()).getJavaType() == Boolean.class) {
                value = (Boolean) criteria.getValue();
            } else {
                value = criteria.getValue() != null;
            }
            if (value) {
                return criteriaBuilder.isNotNull(root.<String>get(criteria.getKey()));
            } else {
                return criteriaBuilder.isNull(root.<String>get(criteria.getKey()));
            }
        } catch (IllegalArgumentException ex) {
            // Invalid property name
            return null;
        }
    }
}
