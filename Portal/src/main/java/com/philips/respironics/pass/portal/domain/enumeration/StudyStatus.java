package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The StudyStatus enumeration.
 */
public enum StudyStatus {
    ACTIVE,INACTIVE
}
