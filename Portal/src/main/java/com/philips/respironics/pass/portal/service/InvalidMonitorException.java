package com.philips.respironics.pass.portal.service;

/**
 * This exception is throw in case of an invalid Monitor.
 */
public class InvalidMonitorException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidMonitorException() { super(); }

    public InvalidMonitorException(String message) { super(message); }

    public InvalidMonitorException(String message, Throwable t) {
        super(message, t);
    }
}
