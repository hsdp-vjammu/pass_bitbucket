package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.Monitor;

import com.philips.respironics.pass.portal.domain.Study;
import com.philips.respironics.pass.portal.domain.Subject;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Spring Data JPA repository for the Monitor entity.
 */
@SuppressWarnings("unused")
public interface MonitorRepository extends JpaRepository<Monitor,Long>, JpaSpecificationExecutor<Monitor> {

    Monitor findOneBySerialNumber(String serialNumber);

    List<Monitor> findAllByCompanyAndStatus(Company company, MonitorStatus status);

    Page<Monitor> findAllByCompanyAndStatus(Company company, MonitorStatus status, Pageable var1);

    Page<Monitor> findAllByCompany(Company company, Pageable var1);
    List<Monitor> findAllByCompany(Company company);

    //extract Study information based on study Ids for Site Coordinator display
    Page<Monitor> findByAssignedSubjectIdInAndStatus(ArrayList<Long> studyIds, MonitorStatus status, Pageable pageable);

    List<Monitor> findAllByTypeAndSerialNumber(MonitorType type, String serialNumber);

    List<Monitor> findAllByAssignedSubjectStudyAndStatus(Study study, MonitorStatus status);

}
