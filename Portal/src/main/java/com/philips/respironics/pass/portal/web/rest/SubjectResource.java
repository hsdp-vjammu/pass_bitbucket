package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.search.SearchParameter;
import com.philips.respironics.pass.portal.domain.search.SubjectSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.SiteRepository;
import com.philips.respironics.pass.portal.repository.StudyRepository;
import com.philips.respironics.pass.portal.repository.SubjectRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.SubjectDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.codehaus.groovy.runtime.DefaultGroovyMethods.collect;

/**
 * REST controller for managing Subject.
 */
@RestController
@RequestMapping("/api")
public class SubjectResource {

    private final Logger log = LoggerFactory.getLogger(SubjectResource.class);

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private UserService userService;

    /**
     * POST  /subjects : Create a new subject.
     *
     * @param subjectDTO the subjectDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new subjectDTO, or with status 400 (Bad Request) if the subject has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/subjects",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    @Transactional
    public ResponseEntity<SubjectDTO> createSubject(@Valid @RequestBody SubjectDTO subjectDTO) throws URISyntaxException {
        log.debug("REST request to save Subject : {}", subjectDTO);
        if (subjectDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("subject", "idexists", "A new subject cannot already have an ID")).body(null);
        }

        Study study = studyRepository.findOne(subjectDTO.getStudyId());
        if (study == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("studyId", "invalid", "An invalid study was selected")).body(null);
        } else if (externalReferenceExists(subjectDTO.getExternalReference(), study, subjectDTO.getId())) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("externalReference", "notunique", "The subject ID is not unique for the selected study")).body(null);
        }

        Subject subject = new Subject();
        subject.setExternalReference(subjectDTO.getExternalReference());
        subject.setFirstName(subjectDTO.getFirstName());
        subject.setLastName(subjectDTO.getLastName());
        subject.setDateOfBirth(subjectDTO.getDateOfBirth());
        subject.setWeight(subjectDTO.getWeight());
        subject.setWeightUnit(subjectDTO.getWeightUnit());
        subject.setHeight(subjectDTO.getHeight());
        subject.setHeightUnit(subjectDTO.getHeightUnit());
        subject.setGender(subjectDTO.getGender());
        subject.setStreetAddress(subjectDTO.getStreetAddress());
        subject.setZipCode(subjectDTO.getZipCode());
        subject.setCity(subjectDTO.getCity());
        subject.setState(subjectDTO.getState());
        subject.setCountry(subjectDTO.getCountry());
        subject.setPhoneNumber(subjectDTO.getPhoneNumber());
        subject.setEmail(subjectDTO.getEmail());
        subject.setTimeZone(subjectDTO.getTimeZone());
        subject.setDominantHand(subjectDTO.getDominantHand());
        subject.setWearingPosition(subjectDTO.getWearingPosition());
        //    subject.setSite(siteRepository.findOne(subjectDTO.getSiteId()));
        subject.setStudy(studyRepository.findOne(subjectDTO.getStudyId()));

        if (StringUtils.isNotBlank(subjectDTO.getSiteReference())) {
            Optional<Site> site = siteRepository.findOneByStudyAndExternalReference(study, subjectDTO.getSiteReference());
            if (site.isPresent()) {
                subject.setSite(site.get());
                site.get().getSubjects().add(subject);
            }
        }
        subject = subjectRepository.save(subject);

        SubjectDTO result = new SubjectDTO(subject);
        return ResponseEntity.created(new URI("/api/subjects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("subject", result.getExternalReference()))
            .body(result);
    }

    /**
     * PUT  /subjects : Updates an existing subject.
     *
     * @param subjectDTO the subjectDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated subjectDTO,
     * or with status 400 (Bad Request) if the subjectDTO is not valid,
     * or with status 500 (Internal Server Error) if the subjectDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/subjects",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    @Transactional
    public ResponseEntity<SubjectDTO> updateSubject(@Valid @RequestBody SubjectDTO subjectDTO) throws URISyntaxException {
        log.debug("REST request to update Subject : {}", subjectDTO);

        if (subjectDTO.getExternalReference() == null) subjectDTO.setExternalReference("");

        if (subjectDTO.getId() == null) {
            return createSubject(subjectDTO);
        }

        Subject subject = subjectRepository.findOne(subjectDTO.getId());
        if (subject == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createEntityUpdateAlert("Subject", "id")).body(null);
        }

        Study study = studyRepository.findOne(subjectDTO.getStudyId());
        if (study == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createEntityUpdateAlert("Subject", "studyId")).body(null);
        } else if (externalReferenceExists(subjectDTO.getExternalReference(), study, subjectDTO.getId())) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createEntityUpdateAlert("Subject", "externalReference")).body(null);
        }

        subject.setStudy(study);
        subject.setExternalReference(subjectDTO.getExternalReference());
        subject.setFirstName(subjectDTO.getFirstName());
        subject.setLastName(subjectDTO.getLastName());
        subject.setGender(subjectDTO.getGender());
        subject.setDateOfBirth(subjectDTO.getDateOfBirth());

        subject.setPhoneNumber(subjectDTO.getPhoneNumber());
        subject.setEmail(subjectDTO.getEmail());
        subject.setStreetAddress(subjectDTO.getStreetAddress());
        subject.setCity(subjectDTO.getCity());
        subject.setZipCode(subjectDTO.getZipCode());
        subject.setState(subjectDTO.getState());
        subject.setCountry(subjectDTO.getCountry());
        subject.setTimeZone(subjectDTO.getTimeZone());

        if (StringUtils.isNotBlank(subjectDTO.getSiteReference())) {
            Optional<Site> site = siteRepository.findOneByStudyAndExternalReference(study, subjectDTO.getSiteReference());
            if (site.isPresent()) {
                subject.setSite(site.get());
                site.get().getSubjects().add(subject);
            }
        } else if (subject.getSite() != null) {
            subject.getSite().getSubjects().remove(subject);
            subject.setSite(null);
        }

        subject.setHeight(subjectDTO.getHeight());
        subject.setHeightUnit(subjectDTO.getHeightUnit());
        subject.setWeight(subjectDTO.getWeight());
        subject.setWeightUnit(subjectDTO.getWeightUnit());
        subject.setDominantHand(subjectDTO.getDominantHand());
        subject.setWearingPosition(subjectDTO.getWearingPosition());


        subject = subjectRepository.save(subject);

        SubjectDTO result = new SubjectDTO(subject);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("subject", subjectDTO.getExternalReference()))
            .body(result);
    }

    /**
     * GET  /subjects : get all the subjects for a company.
     *
     * @param cid the company id to display information for
     * @return the ResponseEntity with status 200 (OK) and the list of subjects in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/subjects",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<List<SubjectDTO>> getAllSubjectsForCompany(@RequestParam long cid, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Subjects");

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(cid);
        if (membership != null) {
            Map<String, String> searchParams;
            Map<String, String> scSearchParams;
            try {
                searchParams = new ObjectMapper().readValue(search, HashMap.class);
            } catch (IOException e) {
                // Unable to parse search parameters
                searchParams = new HashMap<>();
            }
            Page<Subject> page = null;
            MembershipAccess siteCoordinator = membership.getAccessLevel(AuthoritiesConstants.SITE_COORDINATOR);
            //extract StudyIds and change collection for Site Coordinator
            String siteIds = "";
            boolean searchParamsVal = false;
            if (siteCoordinator != null && siteCoordinator.getSite() != null) {
                if (!searchParams.isEmpty() && searchParams != null) {
                    for (Map.Entry<String, String> entry : searchParams.entrySet()) {
                        if ("siteIds".equalsIgnoreCase(entry.getKey())) {
                            siteIds = entry.getValue();
                        } else if("searchSCRecords".equalsIgnoreCase(entry.getKey())) { //SC search with siteIds and searchParams
                            searchParamsVal = "true".equalsIgnoreCase(entry.getValue())? true : false;
                        }
                    }
                    if(searchParamsVal) {
                        List<String> sites = Arrays.asList(siteIds.split(","));
                        ArrayList<Long> extractStudyIds = new ArrayList<Long> ();
                        sites.forEach(siteValue -> extractStudyIds.add(Long.valueOf(siteValue.trim())));

                        page = subjectRepository.findByStudyIdIn(extractStudyIds, pageable);
                    } else { //we have params with siteIds and searchParams
                        //searchParams.remove("searchSCRecords");
                        //searchParams.remove("siteIds");
                        page = subjectRepository.findAll(SubjectSearchSpecificationBuilder.build(membership.getCompany(), searchParams), pageable);
                    }
                }
            } else {  // non-SC's
                if (searchParams.isEmpty()) {
                    page = subjectRepository.findAllByStudyCompany(membership.getCompany(), pageable);
                } else {
                    page = subjectRepository.findAll(SubjectSearchSpecificationBuilder.build(membership.getCompany(), searchParams), pageable);
                }
            }
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/subjects");
            return new ResponseEntity<>(SubjectDTO.convert(page.getContent()), headers, HttpStatus.OK);
        }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/subjects/unassigned",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<SubjectDTO>> getAllUnassignedSubjectsForCompany(@RequestParam long cid, Long subjectId)
        throws URISyntaxException {
        log.debug("REST request to get a page of Monitors of the active Company");

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(cid);
        if (membership != null) {
            List<Subject> subjects;
            subjects = subjectRepository.findAllByStudyCompanyAndAssignedMonitorsNull(membership.getCompany());

            /*MembershipAccess siteCoordinator = membership.getAccessLevel(AuthoritiesConstants.SITE_COORDINATOR);
            if (siteCoordinator != null && siteCoordinator.getSite() != null) {
                subjects = subjectRepository.7789(membership.getCompany(), siteCoordinator.getSite());
            } else {
                subjects = subjectRepository.findAllByStudyCompanyAndAssignedMonitorsNull(membership.getCompany());
            }*/

            if (subjectId != null) {
                Subject subject = subjectRepository.findOne(subjectId);
                if (subject != null) {
                    subjects.add(subject);
                }
            }
            return new ResponseEntity<>(SubjectDTO.convert(subjects), HttpStatus.OK);
        }

        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }

    /**
     * GET  /subjects/:id : get the "id" subject.
     *
     * @param id the id of the subjectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subjectDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/subjects/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<SubjectDTO> getSubject(@PathVariable Long id) {
        log.debug("REST request to get Subject : {}", id);
        Subject subject = subjectRepository.findOne(id);
        SubjectDTO subjectDTO = new SubjectDTO(subject);
        return Optional.ofNullable(subjectDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /subjects/:studyId/:subjectId : get the subject for a given study id and subject id.
     *
     * @param studyId    the id of the a study
     * @param subjectRef the external reference of the subjectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the subjectDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/subjects/{studyId}/{subjectRef}/{subjectId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<SubjectDTO> getSubject(@PathVariable Long studyId, @PathVariable String subjectRef, @PathVariable Long subjectId) {
        log.debug("REST request to get Subject : {}", subjectRef);

        Study study = studyRepository.findOne(studyId);
        if (study != null) {
            if (externalReferenceExists(subjectRef, study, subjectId)) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * DELETE  /subjects/:id : delete the "id" subject.
     *
     * @param id the id of the subjectDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/subjects/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<Void> deleteSubject(@PathVariable Long id) {
        log.debug("REST request to delete Subject : {}", id);
        subjectRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("subject", id.toString())).build();
    }

    private boolean externalReferenceExists(String reference, Study study, Long subjectId) {
        boolean exists = false;
        if (subjectId == null) {
            subjectId = 0L;
        }
        List<String> externalReferences = subjectRepository.findExternalReferencesByStudyAndIdNot(study, subjectId);
        for (String string : externalReferences) {
            if (reference != null && string != null && string.equalsIgnoreCase(reference)) {
                exists = true;
                break;
            }
        }
        return exists;
    }
}
