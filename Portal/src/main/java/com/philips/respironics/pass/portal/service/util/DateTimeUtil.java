package com.philips.respironics.pass.portal.service.util;


import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class DateTimeUtil {
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm";

    public DateTimeUtil() {
    }

    public long shiftTimeZone(String dateString, String sourceTimeZone, String destTimeZone) {
        try {
            LocalDateTime localDateTime = LocalDateTime.parse(dateString, DateTimeFormatter.ofPattern(DATE_FORMAT));

            ZoneId sourceZoneId = ZoneId.of(sourceTimeZone);
            ZoneId destZoneId = ZoneId.of(destTimeZone);

            ZonedDateTime dateWithSourceTimeZone = localDateTime.atZone(sourceZoneId);

            ZonedDateTime dateWithDestTimeZone = dateWithSourceTimeZone.withZoneSameInstant(destZoneId);

            return dateWithDestTimeZone.toEpochSecond();

        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        return 0;
    }

}
