package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Authority;
import com.philips.respironics.pass.portal.domain.User;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    @Size(min = 5, max = 100)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 100)
    private String lastName;

    @Email
    @NotNull
    @Size(min = 5, max = 100)
    private String email;

    private boolean activated = false;

    private boolean locked;

    private Long passwordAge;

    @Size(min = 2, max = 5)
    private String langKey;

    private Set<String> authorities;

    private Set<MembershipDTO> memberships = new HashSet<>();

    public UserDTO() {
    }

    public UserDTO(User user) {
        this(user, true);
    }

    public UserDTO(User user, boolean includeMemberships) {
        this(user.getLogin(), user.getFirstName(), user.getLastName(),
            user.getEmail(), user.getActivated(), user.getPasswordChanged(), user.getLocked(),
            user.getLangKey(),
            user.getAuthorities().stream().map(Authority::getName)
                .collect(Collectors.toSet()));

        if (includeMemberships) {
            user.getMemberships().forEach(membership-> {
                MembershipDTO membershipDTO = new MembershipDTO(membership, this);
                memberships.add(membershipDTO);
            });
        }
    }

    public UserDTO(String login, String firstName, String lastName,
                    String email, boolean activated, ZonedDateTime passwordChanged, ZonedDateTime locked,
                   String langKey, Set<String> authorities) {

        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;

        if (passwordChanged != null) {
            this.passwordAge = Duration.between(passwordChanged, ZonedDateTime.now()).toDays();
        } else {
            this.passwordAge = Long.valueOf(0L);
        }

        this.locked = locked != null;
        this.langKey = langKey;
        this.authorities = authorities;
    }

    public void setLogin(String login) { this.login = login; }
    public String getLogin() { return login; }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public Long getPasswordAge() { return passwordAge; }
    public void setPasswordAge(Long passwordAge) { this.passwordAge = passwordAge; }

    public boolean isActivated() {
        return activated;
    }

    public boolean isLocked() {
        return locked;
    }

    public String getLangKey() {
        return langKey;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }
    public void setAuthorities(Set<String> authorities) {
        this.authorities = authorities;
    }

    public Set<MembershipDTO> getMemberships() { return memberships; }
    public void setMemberships(Set<MembershipDTO> memberships) { this.memberships = memberships; }

    @Override
    public String toString() {
        return "UserDTO{" +
            "email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", authorities=" + authorities +
            "}";
    }
}
