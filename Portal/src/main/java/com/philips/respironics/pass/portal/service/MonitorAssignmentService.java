package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatusChangeAction;
import com.philips.respironics.pass.portal.repository.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service class for managing studies.
 */
@Service
@Transactional
public class MonitorAssignmentService {

    @Inject
    private MonitorRepository monitorRepository;

    @Inject
    private MonitorHistoryRepository monitorHistoryRepository;

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private MonitorConfigurationRepository  monitorConfigurationRepository;

    @Inject
    private MonitorService monitorService;

    @Transactional
    public void updateAssignment(Monitor monitor, Subject subject, MonitorConfiguration configuration, String siteReference) {

        Subject assignedSubject = monitor.getAssignedSubject();

        // Check if Monitor was already assigned to a subject
        if (assignedSubject != null && assignedSubject.getId() != subject.getId()) {
            // Monitor was already assigned to a different subject, unassign subject first

            monitor.setAssignedSubject(null);

            assignedSubject.getAssignedMonitors().remove(monitor);
            subjectRepository.save(assignedSubject);

            monitorHistoryRepository.save(new MonitorHistory(MonitorStatusChangeAction.UNASSIGN_SUBJECT, monitor, monitor.getCompany(), assignedSubject));
        }

        // Assign monitor to the subject
        if (monitor.getAssignedSubject() == null) {
            monitor.setAssignedSubject(subject);
            monitor.setMonitorCreatedDate(ZonedDateTime.now());

            subject.getAssignedMonitors().add(monitor);

            monitorHistoryRepository.save(new MonitorHistory(MonitorStatusChangeAction.ASSIGN_SUBJECT, monitor, monitor.getCompany(), subject));
        }

        if (StringUtils.isNotBlank(siteReference)) {
            Optional<Site> site = siteRepository.findOneByStudyAndExternalReference(subject.getStudy(), siteReference);
            if (site.isPresent()) {
                subject.setSite(site.get());
                site.get().getSubjects().add(subject);
            }
        } else if (subject.getSite() != null) {
            subject.getSite().getSubjects().remove(subject);
            subject.setSite(null);
        }
        subjectRepository.save(subject);

        MonitorConfiguration assignedConfiguration = monitor.getConfiguration();

        // Check if Monitor was already assigned to a configuration
        if (assignedConfiguration != null && assignedConfiguration.getId() != configuration.getId()) {
            // Monitor was already assigned to a different configuration, unassign configuration first

            monitor.setConfiguration(null);

            assignedConfiguration.getMonitors().remove(monitor);
            monitorConfigurationRepository.save(assignedConfiguration);
        }

        // Assign monitor to the subject
        if (monitor.getConfiguration() == null) {
            monitor.setConfiguration(configuration);

            configuration.getMonitors().add(monitor);
            monitorConfigurationRepository.save(configuration);
        }
        monitor.setAssignment_status(1);  //new assignment
        monitorRepository.saveAndFlush(monitor);
    }

    @Transactional
    public Monitor deleteAssignment(Monitor monitor) throws InvalidMonitorStatusException {
        return monitorService.changeStatus(monitor, MonitorStatus.UNASSIGNED);
    }
}

