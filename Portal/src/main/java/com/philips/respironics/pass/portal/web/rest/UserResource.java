package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.config.Constants;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.repository.*;
import com.philips.respironics.pass.portal.security.AccountLockService;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.MailService;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.ManagedUserDTO;
import com.philips.respironics.pass.portal.web.rest.dto.MembershipAccessDTO;
import com.philips.respironics.pass.portal.web.rest.dto.UDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing users.
 *
 * <p>This class accesses the User entity, and needs to fetch its collection of authorities.</p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * </p>
 * <p>
 * We use a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>Another option would be to have a specific JPA entity graph to handle this case.</p>
 */
@RestController
@RequestMapping("/api")
public class UserResource {

    private final Logger log = LoggerFactory.getLogger(UserResource.class);

    @Inject
    private UserRepository userRepository;

    @Inject
    private MembershipRepository membershipRepository;

    @Inject
    private MembershipAccessRepository membershipAccessRepository;

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private MailService mailService;

    @Inject
    private UserService userService;


    @Inject
    private CompanyRepository companyRepository;

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private AccountLockService accountLockService;

    /**
     * POST  /users  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     * </p>
     *
     * @param managedUserDTO the user to create
     * @param request        the HTTP request
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request) if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntaxt is incorrect
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<?> createUser(@RequestBody ManagedUserDTO managedUserDTO, HttpServletRequest request) throws URISyntaxException {
        log.debug("REST request to save User : {}", managedUserDTO);

        managedUserDTO.setLogin(managedUserDTO.getEmail());

        //Lowercase the user login before comparing with database
        if (userRepository.findOneByLogin(managedUserDTO.getLogin().toLowerCase()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use"))
                .body(null);
        } else if (userRepository.findOneByEmail(managedUserDTO.getEmail()).isPresent()) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("userManagement", "emailexists", "Email already in use"))
                .body(null);
        } else {
            User currentUser = userService.getUserWithAuthorities();
            Set<String> authorities = new HashSet<>();
            if (managedUserDTO.getMemberships() != null && !managedUserDTO.getMemberships().isEmpty()) {
                authorities.add(AuthoritiesConstants.USER);
            } else if (currentUser.hasAuthority(AuthoritiesConstants.ADMIN)) {
                authorities.add(AuthoritiesConstants.ADMIN);
            }
            managedUserDTO.setAuthorities(authorities);

            User newUser = userService.createUser(managedUserDTO);

            String baseUrl = request.getScheme() + // "http"
                "://" +                                // "://"
                request.getServerName() +              // "myhost"
                ":" +                                  // ":"
                request.getServerPort() +              // "80"
                request.getContextPath();              // "/myContextPath" or "" if deployed in root context
            mailService.sendActivationEmail(newUser, baseUrl, "X");
            return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
                .headers(HeaderUtil.createAlert("userManagement.created", newUser.getLogin()))
                .body(newUser);
        }
    }

    /**
     * PUT  /users : Updates an existing User.
     *
     * @param managedUserDTO the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user,
     * or with status 400 (Bad Request) if the login or email is already in use,
     * or with status 500 (Internal Server Error) if the user couldnt be updated
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<ManagedUserDTO> updateUser(@RequestBody ManagedUserDTO managedUserDTO, HttpServletRequest request) {
        log.debug("REST request to update User : {}", managedUserDTO);
        Set<String> sitesList = managedUserDTO.getSites();

        return userRepository
            .findOneById(managedUserDTO.getId())
            .map(user -> {
                user.setFirstName(managedUserDTO.getFirstName());
                user.setLastName(managedUserDTO.getLastName());

                User currentUser = userService.getUserWithAuthorities();
                user.setLastModifiedBy(currentUser.getLogin());
                user.setLastModifiedDate(ZonedDateTime.now());
                managedUserDTO.getMemberships().stream().forEach(membershipDTO -> {
                        if (currentUser.hasAuthority(AuthoritiesConstants.ADMIN)
                            || currentUser.hasMembership(membershipDTO.getCompany().getId(), AuthoritiesConstants.COMPANY_ADMIN)) {
                            boolean membershipChanged = false;
                            Membership membership = user.getMembership(membershipDTO.getCompany().getId());
                            if (membership == null) {
                                Company company = companyRepository.findOne(membershipDTO.getCompany().getId());
                                if (company != null) {
                                    membership = new Membership();
                                    membership.setCompany(company);
                                    membership.setUser(user);

                                    user.getMemberships().add(membership);
                                    company.getMemberships().add(membership);

                                    membership.setEnabled(membershipDTO.getEnabled());

                                    membershipChanged = true;
                                }
                            } else if (membershipDTO.getEnabled() != membership.getEnabled()) {
                                membership.setEnabled(membershipDTO.getEnabled());

                                membershipChanged = true;
                            }


                            // First delete any removed access levels
                            List<String> newAccessLevels = membershipDTO.getAccessLevels().stream().map(level -> level.getAccessLevel()).collect(Collectors.toList());
                            Iterator<MembershipAccess> iCurrentAccess = membership.getAccessLevels().iterator();
                            while (iCurrentAccess.hasNext()) {
                                MembershipAccess currentAccessLevel = iCurrentAccess.next();
                                if (!newAccessLevels.contains(currentAccessLevel.getAccessLevel())) {
                                    iCurrentAccess.remove();
                                    membershipChanged = true;
                                }
                            }

                            // Add any new access levels -- CHANGE THIS
                            List<String> currentAccessLevels = membership.getAccessLevels().stream().map(level -> level.getAccessLevel()).collect(Collectors.toList());
                            for (MembershipAccessDTO accessLevelDTO : membershipDTO.getAccessLevels()) {
                                if (!currentAccessLevels.contains(accessLevelDTO.getAccessLevel())) {
                                    MembershipAccess accessLevel = new MembershipAccess();
                                    accessLevel.setMembership(membership);
                                    accessLevel.setAccessLevel(accessLevelDTO.getAccessLevel());
                                    membership.getAccessLevels().add(accessLevel);
                                    membershipChanged = true;
                                }
                            }

                            if (!sitesList.isEmpty()) {  //if sites list empty no changes to persist
                                for (String newSite : sitesList) {
                                    for (MembershipAccessDTO accessLevelDTO : membershipDTO.getAccessLevels()) { //** No need to loop around here
                                        if (currentAccessLevels.contains(accessLevelDTO.getAccessLevel())) {
                                            MembershipAccess accessLevel = new MembershipAccess();
                                            accessLevel.setMembership(membership);
                                            accessLevel.setAccessLevel(accessLevelDTO.getAccessLevel());
                                            switch (accessLevelDTO.getAccessLevel()) {
                                                case AuthoritiesConstants.SITE_COORDINATOR:
                                                    Study study = studyRepository.findOne(accessLevelDTO.getStudyId());  //check study present
                                                    Optional<Site> site = siteRepository.findOneByStudyAndExternalReference(study, newSite);  //check site present
                                                    if (site.isPresent()) {
                                                        //validation here to check site already present in COMPANY_USER_ACCESS table
                                                        Long companyID = study.getCompany().getId();//get company ID for given study eg.5
                                                        Long userID = user.getId();//verify this matching with company user access record eg.67
                                                        Long memberID = membershipRepository.findMembershipsByCompanyAndUser(userID, companyID); //Must return only one value
                                                        //check in company User table with given siteId and companyUserId combination to find records
                                                        Long siteID = site.get().getId();
                                                        Long companyUserID = memberID; //memberID coming from CompanyUser table, based on given user and company ID
                                                        List<MembershipAccess> membershipAccessList = membershipAccessRepository.findBySiteAndCompanyUserId(siteID, companyUserID);
                                                        if (membershipAccessList.isEmpty()) { //no matching sites found in company_user_access table, add new
                                                            accessLevel.setSite(site.get());
                                                            site.get().getMembershipAccessLevels().add(accessLevel);
                                                        }
                                                    } else {
                                                        //no site found - do nothing
                                                    }
                                                default:
                                                    //add site only when new one added, not null sites
                                                    if (accessLevel.getSite() != null) {
                                                        membership.getAccessLevels().add(accessLevel);
                                                    }
                                            }
                                            membershipChanged = true;
                                        }
                                    }

                                }
                            } //if sites list empty no changes persisted
                            membershipRepository.save(membership);  //persisting new sites here

                            if (membershipChanged) {
                                String baseUrl = request.getScheme() + // "http"
                                    "://" +                                // "://"
                                    request.getServerName() +              // "myhost"
                                    ":" +                                  // ":"
                                    request.getServerPort() +              // "80"
                                    request.getContextPath();              // "/myContextPath" or "" if deployed in root context
                                mailService.sendMembershipChangedEmail(user, membership, baseUrl);
                            }
                        } else {
                            new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                        }
                    }
                );

                /* TODO: Only for testing purposes. Remove when no longer required by V&V */
                if (managedUserDTO.getPasswordAge() != null && user.getPasswordChanged() != null) {
                    long passwordAge = Duration.between(user.getPasswordChanged(), ZonedDateTime.now()).toDays();
                    if (passwordAge != managedUserDTO.getPasswordAge().longValue()) {
                        user.setPasswordChanged(ZonedDateTime.now().minusDays(managedUserDTO.getPasswordAge().intValue()));
                    }
                }
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createAlert("userManagement.updated", managedUserDTO.getLogin()))
                    .body(new ManagedUserDTO(userRepository
                        .findOne(managedUserDTO.getId())));
            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));

    }

    /**
     * GET  /users : get all users.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     * @throws URISyntaxException if the pagination headers couldnt be generated
     */
    @RequestMapping(value = "/users",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<UDTO>> getAllUsers(@RequestParam Optional<Long> cid, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {

        Map<String, String> searchParams;
        try {
            searchParams = new ObjectMapper().readValue(search, HashMap.class);
        } catch (IOException e) {
            // Unable to parse search parameters
            searchParams = new HashMap<>();
        }

        Page<User> page;
        Company company = null;
        if (cid.isPresent()) {  //company Id present only for non-admin users
            company = companyRepository.findOne(cid.get());
        }
        if (searchParams.isEmpty()) {
            if (company != null) {
                page = userService.getAllUsersWithCompany(pageable, cid.get()); // non Prod. support
            } else {
                page = userService.getAllUsers(pageable);  //use this call if Prod. Support
            }
        } else {
            if (company != null) {
                page = userService.getAllUsersWithParams(searchParams, cid.isPresent(), cid.get(), pageable); // non Prod. support
            } else {
                page = userService.getAllUsersWithParams(searchParams, cid.isPresent(), null, pageable); // Prod. support
            }
        }
        List<UDTO> managedUserDTOs = page.getContent().stream()
            .map(UDTO::new)
            .collect(Collectors.toList());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
        return new ResponseEntity<>(managedUserDTOs, headers, HttpStatus.OK);
    }

    /**
     * GET  /users/:login : get the "login" user.
     *
     * @param login the login of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "login" user, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/users/{login:" + Constants.LOGIN_REGEX + "}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ManagedUserDTO> getUser(@PathVariable String login) {
        log.debug("REST request to get User : {}", login);
        return userService.getUserWithAuthoritiesByLogin(login)
            .map(ManagedUserDTO::new)
            .map(managedUserDTO -> new ResponseEntity<>(managedUserDTO, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  USER :login : delete the "login" User.
     *
     * @param login the login of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/users/{login:" + Constants.LOGIN_REGEX + "}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    public ResponseEntity<Void> deleteUser(@PathVariable String login, @RequestParam Long cid, HttpServletRequest request) {
        log.debug("REST request to delete User: {}", login);

        Optional<User> user = userService.getUserWithAuthoritiesByLogin(login);
        if (user.isPresent()) {

            Set<Membership> memberships = new HashSet<>(user.get().getMemberships());
            Set<Membership> deletedMembership = new HashSet<>();
            for (Membership membership : memberships) {
                if (membership.getCompany().getId().equals(cid)) {
                    membership.getCompany().getMemberships().remove(membership);
                    user.get().getMemberships().remove(membership);
                    membershipRepository.delete(membership);

                    deletedMembership.add(membership);
                }
            }
            userRepository.save(user.get());

            for (Membership membership : deletedMembership) {
                String baseUrl = request.getScheme() + // "http"
                    "://" +                                // "://"
                    request.getServerName() +              // "myhost"
                    ":" +                                  // ":"
                    request.getServerPort() +              // "80"
                    request.getContextPath();              // "/myContextPath" or "" if deployed in root context
                mailService.sendMembershipChangedEmail(user.get(), membership, baseUrl);
            }

            return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
