package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.repository.CompanyRepository;
import com.philips.respironics.pass.portal.repository.MembershipRepository;
import com.philips.respironics.pass.portal.repository.PopNumberRepository;
import com.philips.respironics.pass.portal.repository.UserRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.CompanyService;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.CompanyDTO;
import com.philips.respironics.pass.portal.web.rest.dto.CompanyRegistrationDTO;
import com.philips.respironics.pass.portal.web.rest.dto.StudyDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Company.
 */
@RestController
@RequestMapping("/api")
public class CompanyResource {

    private final Logger log = LoggerFactory.getLogger(CompanyResource.class);

    @Inject
    private CompanyRepository companyRepository;

    @Inject
    private CompanyService companyService;

    @Inject
    private UserService userService;

    @Inject
    private UserRepository userRepository;

    @Inject
    private MembershipRepository membershipRepository;

    @Inject
    private PopNumberRepository popNumberRepository;

    /**
     * POST  /companies : Create a new company.
     *
     * @param companyDTO the companyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new companyDTO, or with status 400 (Bad Request) if the company has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<CompanyDTO> createCompany(@Valid @RequestBody CompanyDTO companyDTO) throws URISyntaxException {
        log.debug("REST request to save Company : {}", companyDTO);
        if (companyDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("company", "idexists", "A new company cannot already have an ID")).body(null);
        }

        Company company = new Company();
        company.setName(companyDTO.getName());
        company.setStreetAddress(companyDTO.getStreetAddress());
        company.setZipCode(companyDTO.getZipCode());
        company.setCity(companyDTO.getCity());
        company.setState(companyDTO.getState());
        company.setCountry(companyDTO.getCountry());
        company.setContactPhoneNumber(companyDTO.getContactPhoneNumber());

        company = companyRepository.save(company);
        CompanyDTO result = new CompanyDTO(company, false);
        return ResponseEntity.created(new URI("/api/companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("company", result.getId().toString()))
            .body(result);
    }

    /**
     * POST  /companies/register : Register a new company for the current user.
     *
     * @param companyRegistrationDTO the companyRegistrationDTO to register
     * @return the ResponseEntity with status 201 (Created) and with body the new companyDTO, or with status 400 (Bad Request) if the company has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/companies/register",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<?> registerCompany(@Valid @RequestBody CompanyRegistrationDTO companyRegistrationDTO) throws URISyntaxException {
        log.debug("REST request to register Company : {}", companyRegistrationDTO);

        HttpHeaders textPlainHeaders = new HttpHeaders();
        textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);

        User user = userService.getUserWithAuthorities();
        Optional<PopNumber> popNumber = popNumberRepository.findOneByNumber(companyRegistrationDTO.getPopNumber());
        if (popNumber.isPresent() && popNumber.get().getUser() == null) {
            Company company = new Company();
            company.setName(companyRegistrationDTO.getName());
            company.setStreetAddress(companyRegistrationDTO.getStreetAddress());
            company.setZipCode(companyRegistrationDTO.getZipCode());
            company.setCity(companyRegistrationDTO.getCity());
            company.setState(companyRegistrationDTO.getState());
            company.setCountry(companyRegistrationDTO.getCountry());
            company.setContactPhoneNumber(companyRegistrationDTO.getContactPhoneNumber());

            Membership membership = new Membership();
            membership.setUser(user);
            membership.setCompany(company);

            MembershipAccess membershipAccess = new MembershipAccess();
            membershipAccess.setMembership(membership);
            membershipAccess.setAccessLevel(AuthoritiesConstants.COMPANY_ADMIN);
            membership.getAccessLevels().add(membershipAccess);

            company.getMemberships().add(membership);
            user.getMemberships().add(membership);

            companyRepository.save(company);
            membershipRepository.save(membership);
            userRepository.save(user);

            popNumber.get().setUser(user.getLogin());
            popNumberRepository.save(popNumber.get());

            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("invalid pop number", textPlainHeaders, HttpStatus.BAD_REQUEST);
        }

    }

    /**
     * PUT  /companies : Updates an existing company.
     *
     * @param companyDTO the companyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated companyDTO,
     * or with status 400 (Bad Request) if the companyDTO is not valid,
     * or with status 500 (Internal Server Error) if the companyDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<CompanyDTO> updateCompany(@Valid @RequestBody CompanyDTO companyDTO) throws URISyntaxException {
        log.debug("REST request to update Company : {}", companyDTO);
        if (companyDTO.getId() == null) {
            return createCompany(companyDTO);
        }

        return companyRepository
            .findOneById(companyDTO.getId())
            .map(company -> {
                company.setName(companyDTO.getName());
                company.setStreetAddress(companyDTO.getStreetAddress());
                company.setCity(companyDTO.getCity());
                company.setZipCode(companyDTO.getZipCode());
                company.setState(companyDTO.getState());
                company.setCountry(companyDTO.getCountry());
                company.setContactPhoneNumber(companyDTO.getContactPhoneNumber());
                companyRepository.save(company);
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("company", companyDTO.getId().toString()))
                    .body(new CompanyDTO(companyService
                        .getCompanyWithMemberships(companyDTO.getId()).get()));
            })
            .orElseGet(() -> new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR));
    }

    /**
     * GET  /companies : get all the companies.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of companies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/companies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<CompanyDTO>> getAllCompanies(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Companies");
        Page<Company> page = companyRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/companies");
        return new ResponseEntity<>(CompanyDTO.convert(page.getContent()), headers, HttpStatus.OK);
    }

    /**
     * GET  /companies/:id : get the "id" company.
     *
     * @param id the id of the companyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the companyDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/companies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<CompanyDTO> getCompany(@PathVariable Long id) {
        log.debug("REST request to get Company : {}", id);

        return companyService.getCompanyWithMemberships(id)
            .map(CompanyDTO::new)
            .map(companyDTO -> new ResponseEntity<>(companyDTO, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /companies/:id : delete the "id" company.
     *
     * @param id the id of the companyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/companies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured({AuthoritiesConstants.ADMIN})
    @Transactional
    public ResponseEntity<Void> deleteCompany(@PathVariable Long id) {
        log.debug("REST request to delete Company : {}", id);
        Company company=companyRepository.findOne(id);
       List<Membership>  memberships = membershipRepository.findMembershipsByCompany(company);
        for (Membership membership : memberships) {
            PopNumber popNumber=popNumberRepository.findPopNumberByUser(membership.getUser().getEmail());
            if(popNumber!=null){
            popNumberRepository.delete(popNumber.getId());}
            User user = membership.getUser();
            if(user!=null) {
                userRepository.delete(user);
            }
        }
        companyRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("company", id.toString())).build();
    }

}
