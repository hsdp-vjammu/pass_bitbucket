package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Monitor;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Monitor entity.
 */
public class MonitorDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String serialNumber;

    @NotNull
    private MonitorType type;

    @NotNull
    private MonitorStatus status;

    private ZonedDateTime monitorCreatedDate;

    private Long companyId;

    private String companyName;

    private Long assignedSubjectId;

    private String assignedSubjectFirstName;

    private String assignedSubjectLastName;

    private Long studyId;

    private String studyName;

    private StudyStatus studyStatus;

    private boolean assignment_Status;

    public MonitorDTO() {}

    public MonitorDTO(Monitor entity) {
        this.id = entity.getId();
        this.serialNumber = entity.getSerialNumber();
        this.type = entity.getType();
        this.status = entity.getStatus();

        this.monitorCreatedDate = entity.getMonitorCreatedDate();
        if (entity.getCompany() != null) {
            this.companyId = entity.getCompany().getId();
            this.companyName = entity.getCompany().getName();
        }
        if (entity.getAssignedSubject() != null) {
            this.assignedSubjectId = entity.getAssignedSubject().getId();
            this.assignedSubjectFirstName = entity.getAssignedSubject().getFirstName();
            this.assignedSubjectLastName = entity.getAssignedSubject().getLastName();
            if (entity.getAssignedSubject().getStudy() != null) {
                this.studyId = entity.getAssignedSubject().getStudy().getId();
                this.studyName = entity.getAssignedSubject().getStudy().getName();
                this.studyStatus = entity.getAssignedSubject().getStudy().getStatus();
            }
        }
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public MonitorType getType() {
        return type;
    }
    public void setType(MonitorType type) {
        this.type = type;
    }

    public Long getCompanyId() {
        return companyId;
    }
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getAssignedSubjectId() {
        return assignedSubjectId;
    }
    public void setAssignedSubjectId(Long subjectId) {
        this.assignedSubjectId = subjectId;
    }

    public String getAssignedSubjectFirstName() {
        return assignedSubjectFirstName;
    }
    public void setAssignedSubjectFirstName(String assignedSubjectFirstName) {
        this.assignedSubjectFirstName = assignedSubjectFirstName;
    }

    public boolean isAssignment_Status() {
        return assignment_Status;
    }

    public void setAssignment_Status(boolean assignment_Status) {
        this.assignment_Status = assignment_Status;
    }

    public String getAssignedSubjectLastName() {
        return assignedSubjectLastName;
    }
    public void setAssignedSubjectLastName(String assignedSubjectLastName) {
        this.assignedSubjectLastName = assignedSubjectLastName;
    }

    public Long getStudyId() {
        return studyId;
    }
    public void setStudyId(Long studyId) {
        this.studyId = studyId;
    }

    public String getStudyName() {
        return studyName;
    }
    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public MonitorStatus getStatus() {
        return status;
    }
    public void setStatus(MonitorStatus status) {
        this.status = status;
    }

    public ZonedDateTime getMonitorCreatedDate() {
        return monitorCreatedDate;
    }

    public void setMonitorCreatedDate(ZonedDateTime monitorCreatedDate) {
        this.monitorCreatedDate = monitorCreatedDate;
    }

    public StudyStatus getStudyStatus() {
        return studyStatus;
    }

    public void setStudyStatus(StudyStatus studyStatus) {
        this.studyStatus = studyStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MonitorDTO monitorDTO = (MonitorDTO) o;

        if ( ! Objects.equals(id, monitorDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MonitorDTO{" +
            "id=" + id +
            ", serialNumber='" + serialNumber + "'" +
            ", type='" + type + "'" +
            '}';
    }

    public static List<MonitorDTO> convert(List<Monitor> entities)
    {
        List<MonitorDTO> dtos = new ArrayList<>();
        for (Monitor entity : entities) {
            dtos.add(new MonitorDTO(entity));
        }
        return dtos;
    }

}
