package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.Site;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

/**
 * A DTO for the Site entity.
 */
public class SiteDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    @NotNull
    private String externalReference;

    private Long studyId;

    private String studyName;
    private StudyStatus studyStatus;
    private Company company;

    private Long numberOfSubjects;
    private Long numberOfUnassignedMonitors;
    private Long numberOfSubjectsAssignedToMonitors;
    private boolean selected;
   // private Long numberOfSubjectsAssignedToConfiguration;

    private Set<UserDTO> coordinators = new HashSet<>();

    public SiteDTO() {}

    public SiteDTO(Site entity) {
        this.id = entity.getId();
        this.externalReference = entity.getExternalReference();
        if (entity.getStudy() != null) {
            this.studyId = entity.getStudy().getId();
            this.studyName = entity.getStudy().getName();
            this.studyStatus = entity.getStudy().getStatus();
            this.company = entity.getStudy().getCompany();
        }

        this.setCreatedDate(entity.getCreatedDate());
        this.setCreatedBy(entity.getCreatedBy());
        this.setLastModifiedDate(entity.getLastModifiedDate());
        this.setLastModifiedBy(entity.getLastModifiedBy());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getExternalReference() {
        return externalReference;
    }
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public Long getStudyId() {
        return studyId;
    }
    public void setStudyId(Long studyId) {
        this.studyId = studyId;
    }

    public String getStudyName() {
        return studyName;
    }
    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }
    public StudyStatus getStudyStatus() {
        return studyStatus;
    }
    public void setStudyStatus(StudyStatus studyStatus) {
        this.studyStatus= studyStatus;
    }

    public Long getNumberOfSubjects() { return numberOfSubjects; }
    public void setNumberOfSubjects(Long number) { this.numberOfSubjects = number; }

    public Set<UserDTO> getCoordinators() { return coordinators; }
    public void setCoordinators(Set<UserDTO> coordinators) { this.coordinators = coordinators; }
    public Long getNumberOfUnassignedMonitors() {
        return numberOfUnassignedMonitors;
    }

    public void setNumberOfUnassignedMonitors(Long number) {
        this.numberOfUnassignedMonitors = number;
    }
    public Long getNumberOfSubjectsAssignedToMonitors() {
        return numberOfSubjectsAssignedToMonitors;
    }

    public void setNumberOfSubjectsAssignedToMonitors(Long number) {
        this.numberOfSubjectsAssignedToMonitors = number;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SiteDTO dto = (SiteDTO) o;

        if ( ! Objects.equals(id, dto.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SiteDTO{" +
            "id=" + id +
            ", externalReference='" + externalReference + "'" +
            '}';
    }

    public static List<SiteDTO> convert(List<Site> entities)
    {
        List<SiteDTO> dtos = new ArrayList<>();
        for (Site entity : entities) {
            dtos.add(new SiteDTO(entity));
        }
        return dtos;
    }

}
