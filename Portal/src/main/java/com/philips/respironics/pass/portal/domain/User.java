package com.philips.respironics.pass.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.philips.respironics.pass.portal.config.Constants;
import com.philips.respironics.pass.portal.domain.enumeration.SecurityQuestion;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * A user.
 */
@Entity
@Table(name = "jhi_user")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 100)
    @Column(length = 100, unique = true, nullable = false)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password_hash",length = 60)
    private String password;

    @Column(name = "password_changed")
    @JsonIgnore
    private ZonedDateTime passwordChanged;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @Email
    @Size(max = 100)
    @Column(length = 100, unique = true)
    private String email;

    @NotNull
    @Column(nullable = false)
    private boolean activated = false;

    @Column(name = "locked")
    @JsonIgnore
    private ZonedDateTime locked;

    @Size(min = 2, max = 5)
    @Column(name = "lang_key", length = 5)
    private String langKey;

    @Size(max = 20)
    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Column(name = "reset_key", length = 20)
    private String resetKey;

    @Column(name = "reset_date", nullable = true)
    private ZonedDateTime resetDate = null;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
        name = "jhi_user_authority",
        joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "authority_name", referencedColumnName = "name")})
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Authority> authorities = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<PersistentToken> persistentTokens = new HashSet<>();

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<HistoricPassword> historicPasswords = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Membership> memberships = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    //Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = login.toLowerCase(Locale.ENGLISH);
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
        setPasswordChanged(ZonedDateTime.now());
    }

    public ZonedDateTime getPasswordChanged() {
        return passwordChanged;
    }
    public void setPasswordChanged(ZonedDateTime passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() { return email; }

    public void setEmail(String email) {
        if (email != null && !email.equals(this.email)) {
            this.email = email;
            setLogin(email);
        }
    }

    public boolean getActivated() {
        return activated;
    }
    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public String getActivationKey() {
        return activationKey;
    }
    public void setActivationKey(String activationKey) {
        this.activationKey = activationKey;
    }

    public ZonedDateTime getLocked() {
        return locked;
    }
    public void setLocked(ZonedDateTime locked) {
        this.locked = locked;
    }

    public String getResetKey() {
        return resetKey;
    }
    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    public ZonedDateTime getResetDate() {
       return resetDate;
    }
    public void setResetDate(ZonedDateTime resetDate) {
       this.resetDate = resetDate;
    }

    public String getLangKey() {
        return langKey;
    }
    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }

    public Set<Authority> getAuthorities() { return authorities; }
    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    public Set<PersistentToken> getPersistentTokens() {
        return persistentTokens;
    }
    public void setPersistentTokens(Set<PersistentToken> persistentTokens) {
        this.persistentTokens = persistentTokens;
    }

    public Set<HistoricPassword> getHistoricPasswords() {
        return historicPasswords;
    }
    public void setHistoricPasswords(Set<HistoricPassword> historicPasswords) {
        this.historicPasswords = historicPasswords;
    }

    public Set<Membership> getMemberships() { return memberships; }
    public void setMemberships(Set<Membership> memberships) { this.memberships = memberships; }

    public void updatePassword(String password, int maxHistoric) {

        if (StringUtils.isNotBlank(this.password)) {
            if (this.historicPasswords.size() >= maxHistoric) {
                List<HistoricPassword> passwords = new ArrayList<>(this.historicPasswords);
                Collections.sort(passwords, (obj1, obj2) ->
                    obj2.getCreatedDate().compareTo(obj1.getCreatedDate() )
                );
                for (int index = 4; index < passwords.size(); index++) {
                    this.historicPasswords.remove(passwords.get(index));
                }
            }

            HistoricPassword historicPassword = new HistoricPassword(this.password);
            historicPassword.setUser(this);
            this.historicPasswords.add(historicPassword);
        }

        this.setPassword(password);
    }

    public boolean hasAuthority(String authority) {
        boolean hasAuth = false;
        if (StringUtils.isNotBlank(authority)) {
            for (Authority auth : this.authorities) {
                if (authority.equals(auth.getName())) {
                    hasAuth = true;
                    break;
                }
            }
        }
        return hasAuth;
    }

    public boolean hasMembership(Company company) {
        if (company != null) {
            return hasMembership(company.getId(), null);
        } else {
            return false;
        }
    }

    public boolean hasMembership(Long companyId) {
        return hasMembership(companyId, null);
    }

    public boolean hasMembership(Company company, String accessLevel) {
        if (company != null) {
            return hasMembership(company.getId(), accessLevel);
        } else {
            return false;
        }
    }

    public boolean hasMembership(Long companyId, String accessLevel) {
        boolean hasMembership = false;
        Membership membership = getMembership(companyId);
        if (membership != null) {
            if (StringUtils.isNotBlank(accessLevel)) {
                for (MembershipAccess access : membership.getAccessLevels()) {
                    if (accessLevel.equals(access.getAccessLevel())) {
                        hasMembership = true;
                    }
                }
            } else {
                hasMembership = true;
            }
        }
        return hasMembership;
    }

    public Membership getMembership(Company company) {
        if (company != null) {
            return getMembership(company.getId());
        }
        return null;
    }

    public Membership getMembership(Long companyId) {
        if (companyId != null) {
            for (Membership membership : this.memberships) {
                if (companyId.equals(membership.getCompany().getId())) {
                    return membership;
                }
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        User user = (User) o;

        if (!login.equals(user.login)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "User{" +
            "login='" + login + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", email='" + email + '\'' +
            ", activated='" + activated + '\'' +
            ", langKey='" + langKey + '\'' +
            ", activationKey='" + activationKey + '\'' +
            "}";
    }
}
