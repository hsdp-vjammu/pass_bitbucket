package com.philips.respironics.pass.portal.security;

import com.philips.respironics.pass.portal.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.ZonedDateTime;
import java.util.Optional;

@Component
class SecurityEventListener implements
    ApplicationListener<AbstractAuthenticationEvent>, LogoutHandler {

    private static final Logger log = LoggerFactory.getLogger(SecurityEventListener.class);

    @Inject
    UserRepository userRepository;

    @Inject
    AccountLockService accountLockService;

    @Override
    @Transactional
    public void onApplicationEvent(AbstractAuthenticationEvent event) {
        if (AuthenticationFailureBadCredentialsEvent.class.equals(event.getClass())) {
            String login = null;
            if (event.getAuthentication().getPrincipal() instanceof String) {
                login = (String)event.getAuthentication().getPrincipal();
            } else if (event.getAuthentication().getPrincipal() instanceof User) {
                login = ((User)event.getAuthentication().getPrincipal()).getUsername();
            }

            if (accountLockService.checkLock(login)) {
                throw new LockedException("User locked");
            }

            log.info("Failed login attempt");
        }
        if (AuthenticationSuccessEvent.class.equals(event.getClass())) {
            String login = ((User)event.getAuthentication().getPrincipal()).getUsername();

            accountLockService.clearAttempts(login);
        }
        event.getAuthentication().getPrincipal();
    }

    @Override
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
        //def username = authentication.getPrincipal().hasProperty('username')?.getProperty(principal) ?: principal
        //log.info "[user:$username] event=Logout remoteAddress=${details.remoteAddress}"
    }

}

