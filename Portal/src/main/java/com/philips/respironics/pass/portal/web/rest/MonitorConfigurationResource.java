package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.Membership;
import com.philips.respironics.pass.portal.domain.MonitorConfiguration;
import com.philips.respironics.pass.portal.domain.MonitorConfigurationQuestion;
import com.philips.respironics.pass.portal.domain.User;
import com.philips.respironics.pass.portal.domain.search.MonitorConfigurationSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.MonitorConfigurationRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.MonitorConfigurationDTO;
import com.philips.respironics.pass.portal.web.rest.dto.MonitorConfigurationQuestionDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * REST controller for managing MonitorConfiguration.
 */
@RestController
@RequestMapping("/api")
public class MonitorConfigurationResource {

    private final Logger log = LoggerFactory.getLogger(MonitorConfigurationResource.class);

    @Inject
    private MonitorConfigurationRepository monitorConfigurationRepository;

    @Inject
    private UserService userService;

    /**
     * POST  /monitorConfigurations : Create a new monitor configuration.
     *
     * @param monitorConfigurationDTO the monitor configuration to create
     * @return the ResponseEntity with status 201 (Created) and with body the new monitorConfigurationDTO, or with status 400 (Bad Request) if the monitor configuration has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitorConfigurations",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<MonitorConfigurationDTO> createMonitorConfiguration(@Valid @RequestBody MonitorConfigurationDTO monitorConfigurationDTO) throws URISyntaxException {
        log.debug("REST request to save MonitorConfiguration : {}", monitorConfigurationDTO);
        if (monitorConfigurationDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("monitorConfiguration", "idexists", "A new monitor configuration cannot already have an ID")).body(null);
        }

        User user = userService.getUserWithAuthorities();
        for (Membership membership : user.getMemberships()) {
            if (membership.getCompany().getId() == monitorConfigurationDTO.getCompanyId()) {

                MonitorConfiguration configuration = new MonitorConfiguration();
                configuration.setCompany(membership.getCompany());

                copyMonitorData(monitorConfigurationDTO, configuration, user);

                configuration = monitorConfigurationRepository.save(configuration);
                MonitorConfigurationDTO result = new MonitorConfigurationDTO(configuration);
                return ResponseEntity.created(new URI("/api/monitorConfigurations/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert("monitorConfiguration", result.getName()))
                    .body(result);
            }
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("company", "invalid", "Company is not the active company for this user")).body(null);
    }

    /**
     * PUT  /monitorConfigurations : Updates an existing monitor configuration.
     *
     * @param monitorConfigurationDTO the monitorConfigurationDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated monitorConfigurationDTO,
     * or with status 400 (Bad Request) if the monitorConfigurationDTO is not valid,
     * or with status 500 (Internal Server Error) if the monitorConfigurationDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitorConfigurations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<MonitorConfigurationDTO> updateMonitorConfiguration(@Valid @RequestBody MonitorConfigurationDTO monitorConfigurationDTO) throws URISyntaxException {
        log.debug("REST request to update MonitorConfiguration : {}", monitorConfigurationDTO);
        if (monitorConfigurationDTO.getId() == null) {
            return createMonitorConfiguration(monitorConfigurationDTO);
        }

        User user = userService.getUserWithAuthorities();
        MonitorConfiguration configuration = monitorConfigurationRepository.findOne(monitorConfigurationDTO.getId());
        if (configuration != null) {
            for (MonitorConfigurationQuestion question : new ArrayList<>(configuration.getQuestions())) {
                question.setMonitorConfiguration(null);
                configuration.getQuestions().remove(question);
            }

            copyMonitorData(monitorConfigurationDTO, configuration, user);

            configuration = monitorConfigurationRepository.save(configuration);
            MonitorConfigurationDTO result = new MonitorConfigurationDTO(configuration);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("monitorConfiguration", monitorConfigurationDTO.getName()))
                .body(result);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    private void copyMonitorData(MonitorConfigurationDTO dto, MonitorConfiguration entity, User user) {
        entity.setName(dto.getName());
        entity.setMonitorType(dto.getMonitorType());
        entity.setDisplayType(dto.getDisplayType());
        entity.setDisplayBlinded(dto.getDisplayBlinded());
        entity.setMobileAppBlinded(dto.getMobileAppBlinded());

        entity.setPalSedentaryMin(dto.getPalSedentaryMin());
        entity.setPalSedentaryMax(dto.getPalSedentaryMax());
        entity.setPalLightMin(dto.getPalLightMin());
        entity.setPalLightMax(dto.getPalLightMax());
        entity.setPalMediumMin(dto.getPalMediumMin());
        entity.setPalMediumMax(dto.getPalMediumMax());
        entity.setPalVigorousMin(dto.getPalVigorousMin());
        entity.setSedentaryAlert(dto.getSedentaryAlert());
        entity.setSedentaryAlertEnabled(dto.isSedentaryAlertEnabled());
        entity.setTargetSteps(dto.getTargetSteps());
        entity.setTargetAee(dto.getTargetAee());

        for (MonitorConfigurationQuestionDTO questionDTO : dto.getQuestions()) {
            MonitorConfigurationQuestion question = new MonitorConfigurationQuestion();
            question.setMonitorConfiguration(entity);
            question.setText(questionDTO.getText());
            question.setResponseType(questionDTO.getResponseType());

            question.setResponseValue1(questionDTO.getResponseValue1());
            question.setResponseValue2(questionDTO.getResponseValue2());

            question.setResponseRangeMin(questionDTO.getResponseRangeMin());
            question.setResponseRangeMax(questionDTO.getResponseRangeMax());

            question.setRecurrenceStartDate(questionDTO.getRecurrenceStartDate());
            question.setRecurrenceEndDate(questionDTO.getRecurrenceEndDate());
            question.setRecurrencePattern(questionDTO.getRecurrencePattern());
            question.setRecurrencePatternWeekdays(questionDTO.getRecurrencePatternWeekdays());
            question.setRecurrenceTime1(questionDTO.getRecurrenceTime1());
            question.setRecurrenceTime2(questionDTO.getRecurrenceTime2());
            question.setRecurrenceTime3(questionDTO.getRecurrenceTime3());

            question.setRecTime1(questionDTO.getRecTime1());
            question.setRecTime2(questionDTO.getRecTime2());
            question.setRecTime3(questionDTO.getRecTime3());

            entity.getQuestions().add(question);
        }

        entity.setLastModifiedDate(ZonedDateTime.now());
        entity.setLastModifiedBy(user.getLogin());
    }

    /**
     * GET  /monitorConfigurations : get all the monitor configurations.
     *
     * @param cid the company id to display information for
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of monitors in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/monitorConfigurations",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<List<MonitorConfigurationDTO>> getAllMonitorConfigurationsForCompany(@RequestParam int cid, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of MonitorConfigurations of the active Company");

        User user = userService.getUserWithAuthorities();
        for (Membership membership : user.getMemberships()) {
            if (membership.getCompany().getId() == cid) {
                Map<String,String> searchParams;
                try {
                    searchParams = new ObjectMapper().readValue(search, HashMap.class);
                } catch (IOException e) {
                    // Unable to parse search parameters
                    searchParams = new HashMap<>();
                }

                Page<MonitorConfiguration> page;
                if (searchParams.isEmpty()) {
                    page = monitorConfigurationRepository.findAllByCompany(membership.getCompany(), pageable);
                } else {
                    page = monitorConfigurationRepository.findAll(MonitorConfigurationSearchSpecificationBuilder.build(membership.getCompany(), searchParams), pageable);
                }
                HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/monitorConfigurations");
                return new ResponseEntity<>(MonitorConfigurationDTO.convert(page.getContent()), headers, HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /monitorConfigurations : list all the monitor configurations (compact style).
     *
     * @param cid the company id to display information for
     * @return the ResponseEntity with status 200 (OK) and the list of monitors in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/monitorConfigurations/list",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<List<MonitorConfigurationDTO>> listMonitorConfigurationsForCompany(@RequestParam int cid)
        throws URISyntaxException {
        log.debug("REST request to get a list of MonitorConfigurations of the active Company");

        User user = userService.getUserWithAuthorities();
        for (Membership membership : user.getMemberships()) {
            if (membership.getCompany().getId() == cid) {
                List<MonitorConfiguration> configurations = monitorConfigurationRepository.findAllByCompany(membership.getCompany());
                List<MonitorConfigurationDTO> dtos = new ArrayList<>();
                for (MonitorConfiguration configuration : configurations) {
                    MonitorConfigurationDTO dto = new MonitorConfigurationDTO();
                    dto.setId(configuration.getId());
                    dto.setName(configuration.getName());

                    dtos.add(dto);
                }
                return new ResponseEntity<>(dtos, HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /monitorConfigurations/:id : get the "id" monitor configuration.
     *
     * @param id the id of the monitorConfigurationDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitorConfigurationDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/monitorConfigurations/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<MonitorConfigurationDTO> getMonitorConfiguration(@PathVariable Long id) {
        log.debug("REST request to get MonitorConfiguration : {}", id);
        MonitorConfiguration configuration = monitorConfigurationRepository.findOne(id);
        MonitorConfigurationDTO configurationDTO = new MonitorConfigurationDTO(configuration);

        return Optional.ofNullable(configurationDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    /**
     * DELETE  /monitorConfigurations/:id : delete the "id" monitor configuration.
     *
     * @param id the id of the monitorConfigurationDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/monitorConfigurations/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<Void> deleteMonitorConfiguration(@PathVariable Long id) {
        log.debug("REST request to delete MonitorConfiguration : {}", id);

        MonitorConfiguration configuration = monitorConfigurationRepository.findOne(id);
        if (configuration != null) {
            if (configuration.getMonitors() != null && configuration.getMonitors().size() > 0) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createEntityDeletionAlert("monitorConfiguration", "configurationAssigned")).body(null);
            } else {
                monitorConfigurationRepository.delete(id);
                return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("monitorConfiguration", id.toString())).build();
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
