package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.domain.search.MonitorSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.MonitorHistoryRepository;
import com.philips.respironics.pass.portal.repository.MonitorRepository;
import com.philips.respironics.pass.portal.repository.SubjectRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.*;
import com.philips.respironics.pass.portal.web.rest.dto.*;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.apache.http.entity.ContentType;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing Monitor.
 */
@RestController
@RequestMapping("/api")
public class MonitorResource {

    private final Logger log = LoggerFactory.getLogger(MonitorResource.class);

    @Inject
    private MonitorRepository monitorRepository;

    @Inject
    private MonitorHistoryRepository monitorHistoryRepository;

    @Inject
    private MonitorHistoryService monitorHistoryService;

    @Inject
    private MonitorService monitorService;

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private UserService userService;

    /**
     * POST  /monitors : Create new monitors.
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new monitorDTO, or with status 400 (Bad Request) if the monitor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitors",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> createMonitor(@RequestParam("data") String json, @RequestParam(value = "file", required = false) MultipartFile file) throws URISyntaxException {
        log.debug("REST request to save monitors: {}", json);

        MonitorBulkCreateDTO monitorsDTO;
        try {
            monitorsDTO = new ObjectMapper().readValue(json, MonitorBulkCreateDTO.class);
        } catch (IOException e) {
            // Unable to parse data
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("monitor", "invalidCreationRequest", "Invalid request for creating monitors")).body(null);
        }

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(monitorsDTO.getCompanyId());
        if (membership != null) {
            Map<String, Object> fileValidationErrors = new HashMap<>();
            Set<String> validSerialNumbers = new HashSet<>();
            if (file != null && !file.isEmpty()) {
                if (ContentType.TEXT_PLAIN.getMimeType().equals(file.getContentType())) {
                    try {
                        InputStream inputStream = null;
                        Scanner sc = null;
                        try {
                            inputStream = file.getInputStream();
                            sc = new Scanner(inputStream, "UTF-8");
                            Map<String, List<String>> invalidSerialNumbers = new HashMap<>();
                            while (sc.hasNextLine()) {
                                String line = sc.nextLine();
                                List<String> errorCodes = monitorService.validateSerialNumber(membership.getCompany().getId(), monitorsDTO.getType(), line);
                                if (!errorCodes.isEmpty()) {
                                    invalidSerialNumbers.put(line, errorCodes);
                                } else {
                                    validSerialNumbers.add(line);
                                }
                            }
                            // note that Scanner suppresses exceptions
                            if (sc.ioException() != null) {
                                throw sc.ioException();
                            }
                            if (!invalidSerialNumbers.isEmpty()) {
                                fileValidationErrors.put("error", "invalidSerialNumbers");
                                fileValidationErrors.put("serialNumbers", invalidSerialNumbers);
                                // fileValidationErrors.put("error", "messageForUser");
                            }
                        } finally {
                            if (inputStream != null) {
                                inputStream.close();
                            }
                            if (sc != null) {
                                sc.close();
                            }
                        }
                    } catch (IOException ex) {
                        fileValidationErrors.put("error", "errorReadingFile");
                    }
                } else {
                    fileValidationErrors.put("error", "invalidFileType");
                }
            }

            if (monitorsDTO.getSerialNumbers() != null && monitorsDTO.getSerialNumbers().length > 0) {
                for (String serialNumber : monitorsDTO.getSerialNumbers()) {
                    if (!validSerialNumbers.contains(serialNumber)) {
                        List<String> errorCodes = monitorService.validateSerialNumber(membership.getCompany().getId(), monitorsDTO.getType(), serialNumber);
                        if (errorCodes.isEmpty()) {
                            validSerialNumbers.add(serialNumber);
                        }
                    }
                }
            }

            if (fileValidationErrors.isEmpty()) {
                monitorService.createMonitors(membership.getCompany(), monitorsDTO.getType(), validSerialNumbers);
                return ResponseEntity.created(new URI("/api/monitors/"))
                    .headers(HeaderUtil.createEntityCreationAlert("monitor", Integer.toString(validSerialNumbers.size())))
                    .body(null);
            } else {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("monitor", "uploadError", "Error during upload"))
                    .body(new JSONObject(fileValidationErrors).toString());
            }
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("company", "invalid", "Company is not the active company for this user")).body(null);
    }

    /**
     * PUT  /monitors : Updates an existing monitor.
     *
     * @param monitorDTO the monitorDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated monitorDTO,
     * or with status 400 (Bad Request) if the monitorDTO is not valid,
     * or with status 500 (Internal Server Error) if the monitorDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitors",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MonitorDTO> updateMonitor(@Valid @RequestBody MonitorDTO monitorDTO) throws URISyntaxException {
        log.debug("REST request to update Monitor : {}", monitorDTO);

        Monitor monitor = monitorRepository.findOne(monitorDTO.getId());
        if (monitor != null) {
            try {
                if(monitorDTO.getStatus().equals(MonitorStatus.ASSIGNED)) { //to handle subject change, must change monitor assignment_status to true
                    monitor = monitorService.changeAssignmentStatus(monitor, monitorDTO.getStatus(), 1);
                } else {
                    monitor = monitorService.changeStatus(monitor, monitorDTO.getStatus());
                }

                MonitorDTO result = new MonitorDTO(monitor);
                return ResponseEntity.ok()
                    .headers(HeaderUtil.createEntityUpdateAlert("monitor", monitorDTO.getId().toString()))
                    .body(result);
            } catch (InvalidMonitorStatusException ex) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createEntityUpdateAlert("monitor", "invalid status")).body(null);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * PUT  /monitors : Changes the status for a list of monitors.
     *
     * @param monitorsDTO the monitors to change status for
     * @return the ResponseEntity with status 200 (OK) and with body the new monitorDTO, or with status 400 (Bad Request) if a status change is invalid
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/monitors/changeStatus",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<String> updateMonitors(@Valid @RequestBody MonitorBulkChangeStatusDTO monitorsDTO) throws URISyntaxException {
        log.debug("REST request to changes status to {} for monitors: {}", monitorsDTO.getStatus(), monitorsDTO.getIds());

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(monitorsDTO.getCompanyId());
        if (membership != null) {
            try {
                monitorService.changeStatus(membership.getCompany(), monitorsDTO.getStatus(), monitorsDTO.getIds());
            } catch (InvalidMonitorException e) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } catch (InvalidMonitorStatusException e) {
                return ResponseEntity.badRequest().headers(HeaderUtil.createEntityUpdateAlert("monitor", "invalid status")).body(null);
            }
            return ResponseEntity.status(HttpStatus.OK)
                .body(null);
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("company", "invalid", "Company is not the active company for this user")).body(null);
    }

    /**
     * GET  /monitors : get all the monitors.
     *
     * @param cid      the company id to display information for
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of monitors in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/monitors",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<MonitorDTO>> getAllMonitorsForCompany(@RequestParam long cid, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Monitors of the active Company");

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(cid);
        if (membership != null) {
            Map<String, String> searchParams;
            try {
                searchParams = new ObjectMapper().readValue(search, HashMap.class);
            } catch (IOException e) {
                // Unable to parse search parameters
                searchParams = new HashMap<>();
            }
            Page<Monitor> page;

            MembershipAccess siteCoordinator = membership.getAccessLevel(AuthoritiesConstants.SITE_COORDINATOR);
            if (siteCoordinator == null && searchParams.isEmpty()) {
                page = monitorRepository.findAllByCompany(membership.getCompany(), pageable);
            } else if (siteCoordinator == null) {
                page = monitorRepository.findAll(MonitorSearchSpecificationBuilder.build(membership.getCompany(), searchParams), pageable);
            } else {
                page = monitorRepository.findAll(MonitorSearchSpecificationBuilder.build(membership.getCompany(), siteCoordinator.getSite(), searchParams), pageable);
            }

            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/monitors");
            return new ResponseEntity<>(MonitorDTO.convert(page.getContent()), headers, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/monitors/unallocated",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    public ResponseEntity<List<MonitorDTO>> getAllUnallocatedMonitorsForCompany(@RequestParam long cid, Long subjectId)
        throws URISyntaxException {
        log.debug("REST request to get a page of Monitors of the active Company");

        User user = userService.getUserWithAuthorities();
        Membership membership = user.getMembership(cid);
        if (membership != null) {
            List<Monitor> monitors = monitorRepository.findAllByCompanyAndStatus(membership.getCompany(), MonitorStatus.UNASSIGNED);
            if (subjectId != null) {
                Subject subject = subjectRepository.findOne(subjectId);
                if (subject != null && subject.getAssignedMonitors() != null) {
                    monitors.addAll(subject.getAssignedMonitors());
                }
            }
            return new ResponseEntity<>(MonitorDTO.convert(monitors), HttpStatus.OK);
        }

        return new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
    }

    /**
     * GET  /monitors/:id : get the "id" monitor.
     *
     * @param id the id of the monitorDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitorDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/monitors/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<MonitorDTO> getMonitor(@PathVariable Long id) {
        log.debug("REST request to get Monitor : {}", id);
        Monitor monitor = monitorRepository.findOne(id);
        MonitorDTO monitorDTO = new MonitorDTO(monitor);
        return Optional.ofNullable(monitorDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /monitors/validate/:companyId/:type/:sn : validate the given serial number and type.
     *
     * @param companyId the given company
     * @param type      the given monitor type
     * @param sn        the serial number to validate
     * @return status 200 (OK), when the validation passes or an error when the validation fails
     */
    @RequestMapping(value = "/monitors/validate/{companyId}/{type}/{sn}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<String>> validateSerialNumber(@PathVariable Long companyId, @PathVariable MonitorType type, @PathVariable String sn) {
        log.debug("REST request to validate serial number : {} (type: {}, company: {})", sn, type, companyId);

        List<String> errorCodes = monitorService.validateSerialNumber(companyId, type, sn);

        if (errorCodes.size() == 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                .headers(HeaderUtil.createAlert("passApp.monitor.validate.serialNumber." + errorCodes.get(0), "validation failed"))
                .body(errorCodes);
        }
    }

    /**
     * GET  /monitors/history/:id : get the history of the given monitor.
     *
     * @param id the id of monitor for the monitorHistoryDTOs to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitorHistoryDTOs, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/monitors/history/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<MonitorHistoryDTO>> getMonitorHistory(@PathVariable Long id) {
        log.debug("REST request to get Monitor History : {}", id);
        Monitor monitor = monitorRepository.findOne(id);
        if (monitor == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        List<MonitorHistory> monitorHistory = monitorHistoryRepository.findAllByMonitor(monitor);
        return new ResponseEntity<>(
            MonitorHistoryDTO.convert(monitorHistory),
            HttpStatus.OK);
    }

    /**
     * GET  /monitors/history/subject/:assignedSubject : get the data based on assignedSubject
     *
     * @param assignedSubject the id of subject for the subject_monitorHistoryDTOs to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitorHistoryDTOs, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/monitors/history/subject/{assignedSubject}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Subject_MonitorHistoryDTO>> getMonitorsSubjectHistory(@PathVariable Long assignedSubject) {
        log.debug("REST request to get Monitor History based on assigned Subject: {}", assignedSubject);
        Subject subject = new Subject();
        subject.setId(assignedSubject);
        List<MonitorHistory> monitorHistory = monitorHistoryService.getAllMonitorsBySubjectId(subject);
        Collections.sort(monitorHistory, (obj1, obj2) ->
            obj2.getLastModifiedDate().compareTo(obj1.getLastModifiedDate())
        );
        return new ResponseEntity<>(
            Subject_MonitorHistoryDTO.convert(monitorHistory),
            HttpStatus.OK);
    }

    /**
     * GET  /monitors/history/subject/:assignedSubject : get the data based on assignedSubject
     *
     * @param assignedSubject the id of subject for the subject_monitorHistoryDTOs to retrieve
     * @param serialNumber the device serial Number for the subject_monitorHistoryDTOs to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the monitorHistoryDTOs, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/monitors/history/subject/{assignedSubject}/sn/{serialNumber}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<Subject_MonitorHistoryDTO>> getMonitorsSubjectHistory(@PathVariable Long assignedSubject, @PathVariable String serialNumber) {
        log.debug("REST request to get count for Monitor History based on assigned Subject and serial Number: {}", assignedSubject, serialNumber);
        Subject subject = new Subject();
        subject.setId(assignedSubject);
        List<MonitorHistory> monitorHistory = monitorHistoryService.getCountForMonitorsBySubjectIdAndSerialNumber(subject, serialNumber);
        return new ResponseEntity<>(
            Subject_MonitorHistoryDTO.convert(monitorHistory),
            HttpStatus.OK);
    }
}
