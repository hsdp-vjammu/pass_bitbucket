package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.PopNumber;
import com.philips.respironics.pass.portal.domain.search.PopNumberSearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.PopNumberRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.PopNumberService;
import com.philips.respironics.pass.portal.web.rest.dto.PopNumberBulkCreateDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.http.entity.ContentType;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing PopNumber.
 */
@RestController
@RequestMapping("/api")
public class PopNumberResource {

    private final Logger log = LoggerFactory.getLogger(PopNumberResource.class);

    @Inject
    private PopNumberRepository popNumberRepository;

    @Inject
    private PopNumberService popNumberService;

    /**
     * POST  /pop-numbers : Create new pop numbers.
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new PopNumberBulkCreateDTO, or with status 400 (Bad Request) if the POP number has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/pop-numbers",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<String> createPopNumber(@RequestParam("data") String json, @RequestParam(value = "file", required = false) MultipartFile file) throws URISyntaxException {
        log.debug("REST request to save pop numbers: {}", json);

        PopNumberBulkCreateDTO popNumbersDTO;
        try {
            popNumbersDTO = new ObjectMapper().readValue(json, PopNumberBulkCreateDTO.class);
        } catch (IOException e) {
            // Unable to parse data
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("popNumber", "invalidCreationRequest", "Invalid request for creating POP numbers")).body(null);
        }

        Map<String, Object> fileValidationErrors = new HashMap<>();
        Set<String> validNumbers = new HashSet<>();

        if (file != null && !file.isEmpty()) {
            if (ContentType.TEXT_PLAIN.getMimeType().equals(file.getContentType())) {
                try {
                    InputStream inputStream = null;
                    Scanner sc = null;
                    try {
                        inputStream = file.getInputStream();
                        sc = new Scanner(inputStream, "UTF-8");
                        Map<String, List<String>> invalidNumbers = new HashMap<>();
                        while (sc.hasNextLine()) {
                            String line = sc.nextLine();
                            List<String> errorCodes = popNumberService.validateNumber(line);
                            if (!errorCodes.isEmpty()) {
                                invalidNumbers.put(line, errorCodes);
                            } else {
                                validNumbers.add(popNumberService.sanitize(line));
                            }
                        }
                        // note that Scanner suppresses exceptions
                        if (sc.ioException() != null) {
                            throw sc.ioException();
                        }
                        if (!invalidNumbers.isEmpty()) {
                            fileValidationErrors.put("error", "invalidNumbers");
                            fileValidationErrors.put("numbers", invalidNumbers);
                        }
                    } finally {
                        if (inputStream != null) {
                            inputStream.close();
                        }
                        if (sc != null) {
                            sc.close();
                        }
                    }
                } catch (IOException ex) {
                    fileValidationErrors.put("error", "errorReadingFile");
                }
            } else {
                fileValidationErrors.put("error", "invalidFileType");
            }
        }

        if (popNumbersDTO.getNumbers() != null && popNumbersDTO.getNumbers().length > 0) {
            for (String number: popNumbersDTO.getNumbers()) {
                number = popNumberService.sanitize(number);
                if (!validNumbers.contains(number)) {
                    List<String> errorCodes = popNumberService.validateNumber(number);
                    if (errorCodes.isEmpty()) {
                        validNumbers.add(number);
                    }
                }
            }
        }

        if (fileValidationErrors.isEmpty()) {
            for (String number : validNumbers) {
                PopNumber entity = new PopNumber();
                entity.setNumber(number);

                popNumberRepository.save(entity);
            }

            return ResponseEntity.created(new URI("/api/pop-numbers/"))
                .headers(HeaderUtil.createEntityCreationAlert("popNumber", Integer.toString(validNumbers.size())))
                .body(null);
        } else {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert("popNumber", "uploadError", "Error during upload"))
                .body(new JSONObject(fileValidationErrors).toString());
        }

    }

    /**
     * GET  /pop-numbers : get all the popNumbers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of popNumbers in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/pop-numbers",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<List<PopNumber>> getAllPopNumbers(@RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of PopNumbers");

        Map<String,String> searchParams;
        try {
            searchParams = new ObjectMapper().readValue(search, HashMap.class);
        } catch (IOException e) {
            // Unable to parse search parameters
            searchParams = new HashMap<>();
        }

        Page<PopNumber> page;
        if (searchParams.isEmpty()) {
            page = popNumberRepository.findAll(pageable);
        } else {
            page = popNumberRepository.findAll(PopNumberSearchSpecificationBuilder.build(searchParams), pageable);
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/pop-numbers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /pop-numbers/:id : get the "id" popNumber.
     *
     * @param id the id of the popNumber to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the popNumber, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/pop-numbers/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<PopNumber> getPopNumber(@PathVariable Long id) {
        log.debug("REST request to get PopNumber : {}", id);
        PopNumber popNumber = popNumberRepository.findOne(id);
        return Optional.ofNullable(popNumber)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /pop-numbers/:id : delete the "id" popNumber.
     *
     * @param id the id of the popNumber to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/pop-numbers/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    public ResponseEntity<Void> deletePopNumber(@PathVariable Long id) {
        log.debug("REST request to delete PopNumber : {}", id);
        popNumberRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("popNumber", id.toString())).build();
    }

    /**
     * GET  /pop-numbers/validate/:number : validate the given pop number.
     *
     * @param number the POP number to validate
     * @return status 200 (OK), when the validation passes or an error when the validation fails
     */
    @RequestMapping(value = "/pop-numbers/validate/{number}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<String>> validateSerialNumber(@PathVariable String number) {
        log.debug("REST request to validate POP number : {}", number);

        List<String> errorCodes = popNumberService.validateNumber(number);

        if (errorCodes.size() == 0) {
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                .headers(HeaderUtil.createAlert("passApp.popNumber.validate.number." + errorCodes.get(0), "validation failed"))
                .body(errorCodes);
        }
    }

}
