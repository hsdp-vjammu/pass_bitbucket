package com.philips.respironics.pass.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.philips.respironics.pass.portal.config.Constants;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * A Study.
 */
@Entity
@Table(name = "study")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Study extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.UUID_REGEX)
    @Size(min = 36, max = 36)
    @Column(name = "unique_identifier", nullable = false, updatable = false)
    private String uniqueIdentifier;

    @Column(name = "external_reference")
    private String externalReference;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "comments")
    private String comments;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private StudyStatus status;

    @Column(name = "date_closed")
    private ZonedDateTime dateClosed;

    @ManyToOne
    @NotNull
    private Company company;

    @OneToMany(mappedBy = "study")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Site> sites = new HashSet<>();

    public Study() {
        this.uniqueIdentifier = UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public String getExternalReference() {
        return externalReference;
    }
    public void setExternalReference(String externalReference) {
        this.externalReference = externalReference;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getComments() {
        return comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }

    public StudyStatus getStatus() {
        return status;
    }
    public void setStatus(StudyStatus status) {
       if (this.status == null || !this.status.equals(status)) {
            switch (status) {
                case ACTIVE:
                    this.setDateClosed(null);
                    break;
                case INACTIVE:
                    this.setDateClosed(ZonedDateTime.now());
                    break;
            }
        }
        this.status = status;
    }

    public ZonedDateTime getDateClosed() {
        return dateClosed;
    }
    public void setDateClosed(ZonedDateTime dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<Site> getSites() { return sites; }
    public void setSites(Set<Site> sites) { this.sites = sites; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Study study = (Study) o;
        if(study.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, study.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Study{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", externalReference='" + externalReference + "'" +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            ", status='" + status + "'" +
            ", dateClosed='" + dateClosed + "'" +
            '}';
    }
}
