package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SubjectSearchSpecificationBuilder {

    public static Specification<Subject> build(Company company, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
        }
        return build(company, criteria);
    }

    public static Specification<Subject> build(Company company, List<SearchParameter> searchCriteria) {

        List<Specification<Subject>> specs = new ArrayList<>();
        specs.add(createSpecification(company));

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue(), true));
            } else if (param.getKey().startsWith("study_")) {
                String attributeName = param.getKey().replace("study_", "");
                specs.add(SubjectSearchSpecificationBuilder.createStudySpecification(attributeName, param.getValue()));
            } else if (param.getKey().startsWith("site_")) {
                String attributeName = param.getKey().replace("site_", "");
                specs.add(SubjectSearchSpecificationBuilder.createSiteSpecification(attributeName, param.getValue()));
            } else if (param.getKey().startsWith("monitor_")) {
                String attributeName = param.getKey().replace("monitor_", "");
                specs.add(SubjectSearchSpecificationBuilder.createMonitorSpecification(attributeName, param.getValue()));
             } else {
                specs.add(SubjectSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<Subject> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    public static Specification<Subject> build(Site site, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
        }
        return build(site, criteria);
    }

    public static Specification<Subject> build(Site site, List<SearchParameter> searchCriteria) {

        List<Specification<Subject>> specs = new ArrayList<>();
        specs.add(createSpecification(site));

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue(), false));
            }
        }

        Specification<Subject> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<Subject> createSpecification(Company company) {
        return new Specification<Subject>() {
            @Override
            public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Subject, Study> study = root.join("study", JoinType.LEFT);
                return criteriaBuilder.equal(study.get("company"), company);
            }
        };
    }

    private static Specification<Subject> createSpecification(Site site) {
        return new Specification<Subject>() {
            @Override
            public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("site"), site);
            }
        };
    }

    private static Specification<Subject> buildGlobalSearch(Object value, boolean includeReferences) {
        List<Specification<Subject>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("firstName", value));
        specs.add(createLikeSpecification("lastName", value));
        specs.add(createLikeSpecification("externalReference", value));
        if (includeReferences) {
            specs.add(createSiteSpecification("externalReference", value));
            specs.add(createStudySpecification("name", value));
            specs.add(createMonitorSpecification("serialNumber", value));
        }

        Specification<Subject> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<Subject> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<Subject> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Subject> createStudySpecification(String attributeName, Object value) {
        return new Specification<Subject>() {
            @Override
            public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Subject, Study> study = root.join("study", JoinType.LEFT);

                if (study.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(study.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(study.get(attributeName), value);
                }
            }
        };
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Subject> createSiteSpecification(String attributeName, Object value) {
        return new Specification<Subject>() {
            @Override
            public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Subject, Site> site = root.join("site", JoinType.LEFT);

                if (site.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(site.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(site.get(attributeName), value);
                }
            }
        };
    }

    @SuppressWarnings("Duplicates")
    private static Specification<Subject> createMonitorSpecification(String attributeName, Object value) {
        return new Specification<Subject>() {
            @Override
            public Predicate toPredicate(Root<Subject> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

                Join<Subject, Monitor> monitor = root.join("assignedMonitors", JoinType.LEFT);

                if (monitor.get(attributeName).getJavaType() == String.class) {
                    return criteriaBuilder.like(monitor.get(attributeName), "%" + value + "%");
                } else {
                    return criteriaBuilder.equal(monitor.get(attributeName), value);
                }
            }
        };
    }

}
