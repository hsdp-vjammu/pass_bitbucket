package com.philips.respironics.pass.portal.domain.util;

import org.apache.commons.lang.StringUtils;

import javax.persistence.AttributeConverter;

public class StringArrayConverter implements AttributeConverter<String[], String> {

    @Override
    public String convertToDatabaseColumn(String[] values) {
        return String.join(",", values);
    }

    @Override
    public String[] convertToEntityAttribute(String dbData) {
        if (StringUtils.isBlank(dbData)) {
            return new String[]{};
        } else {
            return dbData.split(",");
        }
    }

}
