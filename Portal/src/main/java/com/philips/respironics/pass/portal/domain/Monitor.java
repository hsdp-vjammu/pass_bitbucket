package com.philips.respironics.pass.portal.domain;

import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Monitor.
 */
@Entity
@Table(name = "monitor")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Monitor extends AbstractAuditingEntity implements Serializable {

    private final transient Logger log = LoggerFactory.getLogger(Monitor.class);

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "serial_number", nullable = false, updatable = false)
    private String serialNumber;

    @Column(name = "created_date", insertable = false, updatable = false)
    private ZonedDateTime monitorCreatedDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name="status", nullable = false)
    private MonitorStatus status;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private MonitorType type;

    @ManyToOne
    @NotNull
    private Company company;

    @ManyToOne
    private Subject assignedSubject;

    @Column(name="assignment_status", nullable = true)
    private  int assignment_status;

    @ManyToOne
    private MonitorConfiguration monitorConfiguration;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public MonitorType getType() {
        return type;
    }
    public void setType(MonitorType type) {
        this.type = type;
    }

    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }

    public int getAssignment_status() {
        return assignment_status;
    }

    public void setAssignment_status(int assignment_status) {
        this.assignment_status = assignment_status;
    }


    public Subject getAssignedSubject() {
        return assignedSubject;
    }
    public void setAssignedSubject(Subject subject) {
        if (MonitorStatus.ASSIGNED.equals(this.getStatus()) || MonitorStatus.UNASSIGNED.equals(this.getStatus())) {
            this.assignedSubject = subject;
            if (this.assignedSubject != null) {
                this.setStatus(MonitorStatus.ASSIGNED);
            } else {
                this.setStatus(MonitorStatus.UNASSIGNED);
            }
        } else {
            log.error("Invalid monitor state for (un)assigning a subject for monitor {0} in state {1}", this.serialNumber, this.status);
        }
    }

    public MonitorConfiguration getConfiguration() {
        return monitorConfiguration;
    }
    public void setConfiguration(MonitorConfiguration monitorConfiguration) {
        this.monitorConfiguration = monitorConfiguration;
    }

    public MonitorStatus getStatus() {
        return status;
    }
    public void setStatus(MonitorStatus status) {
        this.status = status;
    }

    public ZonedDateTime getMonitorCreatedDate() {
        return monitorCreatedDate;
    }
    public void setMonitorCreatedDate(ZonedDateTime monitorCreatedDate) {
        this.monitorCreatedDate = monitorCreatedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Monitor monitor = (Monitor) o;
        if(monitor.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, monitor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Monitor{" +
            "id=" + id +
            ", serialNumber='" + serialNumber + "'" +
            ", type='" + type + "'" +
            '}';
    }
}
