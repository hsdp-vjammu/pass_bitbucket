package com.philips.respironics.pass.portal.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;

public class ConfigurationDTO {

    private int passwordMinLength;

    private int maxPasswordAge;

    private int passwordExpirationThreshold;

    private int sessionTimeout;

    @JsonCreator
    public ConfigurationDTO() {
    }

    public int getPasswordMinLength() {
        return passwordMinLength;
    }

    public int getMaxPasswordAge() { return maxPasswordAge; }

    public int getPasswordExpirationThreshold() {
        return passwordExpirationThreshold;
    }

    public int getSessionTimeout() { return sessionTimeout; }

    public void setPasswordPolicyDTO(PasswordPolicyDTO dto) {
        this.passwordMinLength = dto.getPasswordMinLength();
        this.maxPasswordAge = dto.getMaxPasswordAge();
        this.passwordExpirationThreshold = dto.getPasswordExpirationThreshold();

        this.sessionTimeout = dto.getSessionTimeout();
    }
}
