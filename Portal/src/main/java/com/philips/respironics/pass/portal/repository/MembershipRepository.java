package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Membership;
import com.philips.respironics.pass.portal.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import com.philips.respironics.pass.portal.domain.Company;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Membership entity.
 */
@SuppressWarnings("unused")
public interface MembershipRepository extends JpaRepository<Membership,Long> {
    String SELECT_QUERY = "select id ";
    String COUNT_QUERY = "select count(*) ";
    String CRITERIA_QUERY = " FROM company_user AS c where c.user_id = ?1 and c.company_id = ?2";
    String SEL_QUERY = SELECT_QUERY + CRITERIA_QUERY;
    String CNT_QUERY = COUNT_QUERY + CRITERIA_QUERY;

    List<Membership> findMembershipsByCompany(Company company);

    @Query(value = SEL_QUERY, countQuery = CNT_QUERY, nativeQuery = true)
    Long findMembershipsByCompanyAndUser(Long userId, Long companyId);
}
