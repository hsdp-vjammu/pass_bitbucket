package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.joda.time.DateTime;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class HttpMonitorController {

    @RequestMapping(value = "/check",
        method = RequestMethod.GET,
        produces = MediaType.TEXT_PLAIN_VALUE)
    @Timed
    public ResponseEntity<String> check() {
        return new ResponseEntity<String>("Still alive at " + DateTime.now().toString(), HttpStatus.OK);
    }

}
