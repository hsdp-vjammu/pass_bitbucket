package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.User;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UDTO {

    public String roleName = "";
    private Long id;
    private String firstName;
    private String lastName;
    private CompanyDTO company;
    private List accountStatus = new ArrayList<String>();
    private ZonedDateTime createdDate;
    private String email;
    private boolean enabled;
    private Set<MembershipDTO> memberships = new HashSet<>();
    private List<MembershipAccessDTO> accessLevels = new ArrayList<>();
    private List companies = new ArrayList<String>();
    private List authorities = new ArrayList<String>();

    public UDTO() {
    }

    public UDTO(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        user.getMemberships().forEach(membership -> {
            MembershipDTO membershipDTO = new MembershipDTO(membership, this);
            memberships.add(membershipDTO);
        });

        memberships.forEach(membership -> {
            this.enabled = membership.getEnabled();
            this.company = membership.getCompany();
            this.companies.add(company.getName());
            this.accessLevels = membership.getAccessLevels();
            this.accessLevels.forEach(roleLevel -> {
                if ("ACL_COMPADMIN".equalsIgnoreCase(roleLevel.getAccessLevel())) {
                    roleName = "Company Administrator";
                } else if ("ACL_CLINICIAN".equalsIgnoreCase(roleLevel.getAccessLevel())) {
                    roleName = "Clinician";
                } else if ("ACL_SITECOORD".equalsIgnoreCase(roleLevel.getAccessLevel())) {
                    roleName = "Site Coordinator";
                }
                this.authorities.add(roleName);
            });
            this.accountStatus.add(getUserAccountStatus(user.getActivated(), this.enabled, user.getLocked()));

        });
        this.createdDate = user.getCreatedDate();
    }

    public ZonedDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(ZonedDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(List accountStatus) {
        this.accountStatus = accountStatus;
    }

    public List getCompanies() {
        return companies;
    }

    public void setCompanies(List companies) {
        this.companies = companies;
    }

    public List getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List authorities) {
        this.authorities = authorities;
    }

    private String getUserAccountStatus(boolean activated, boolean enabled, ZonedDateTime locked) {
        String status;
        if (activated) {
            if (locked == null) {
                if (enabled) {
                    status = "Active";
                } else {
                    status = "InActive";
                }
            } else {
                status = "Locked";
            }
        } else {  //user not activated
            status = "Unverified";
        }
        return status;
    }

    @Override
    public String toString() {
        return "UDTO{" +
            "id=" + id +
            "email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", accountStatus='" + accountStatus + '\'' +
            ", createdDate='" + createdDate + '\'' +
            "}";
    }
}
