package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatusChangeAction;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.repository.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service class for managing studies.
 */
@Service
@Transactional
public class SiteService {

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private MembershipRepository membershipRepository;

    @Inject
    private MailService mailService;

    public void createSites(Study study, Set<String> references) {
        createSites(study, references.toArray(new String[references.size()]));
    }

    @Transactional(propagation= Propagation.REQUIRED)
    public void createSites(Study study, String[] references) {
        for (String reference : references) {
            Site site = new Site();
            site.setStudy(study);
            site.setExternalReference(reference);

            site = siteRepository.save(site);

            study.getSites().add(site);
        }
    }

    @Transactional(propagation= Propagation.REQUIRED)
    public void deleteSite(Site site, User user, HttpServletRequest request) {
        Study study = site.getStudy();
        Set<MembershipAccess> accessLevels = new HashSet<>(site.getMembershipAccessLevels());
        for (MembershipAccess accessLevel : accessLevels) {
            Membership membership = accessLevel.getMembership();
            membership.getAccessLevels().remove(accessLevel);
            if (membership.getAccessLevels().isEmpty()) {
                membershipRepository.delete(membership);

                membership.getCompany().getMemberships().remove(membership);
                membership.getUser().getMemberships().remove(membership);
            } else {
                membershipRepository.save(membership);
            }

            String baseUrl = request.getScheme() + // "http"
                "://" +                                // "://"
                request.getServerName() +              // "myhost"
                ":" +                                  // ":"
                request.getServerPort() +              // "80"
                request.getContextPath();              // "/myContextPath" or "" if deployed in root context
            mailService.sendMembershipChangedEmail(user, membership, baseUrl);
        }

        siteRepository.delete(site);

        study.getSites().remove(site);
    }

    public List<String> validateReference(Long studyId, String reference) {
        Study study = studyRepository.findOne(studyId);
        return validateReference(study, reference);
    }

    public List<String> validateReference(Study study, String reference) {
        List<String> errorCodes = new ArrayList<>();

        if (StringUtils.isNotBlank(reference)) {
            if (reference.length() > 25) {
                errorCodes.add("EXCEEDED_MAX_LENGTH");
            } else {
                Long count = siteRepository.countByStudyAndExternalReferenceIgnoreCase(study, reference);
                if (count > 0) {
                    errorCodes.add("DUPLICATE");
                }
            }
        } else {
            errorCodes.add("EMPTY");
        }
        return errorCodes;
    }

}

