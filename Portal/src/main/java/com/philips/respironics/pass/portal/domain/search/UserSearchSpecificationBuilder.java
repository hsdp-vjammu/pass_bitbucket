package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserSearchSpecificationBuilder {

    public static Specification<User> build(Company company, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                criteria.add(new SearchParameter(k, v));
            });
            params.forEach((k,v)->{
                switch (k) {
                    case "status":
                        switch (v) {
                            case "NOT_VALIDATED":
                                criteria.add(new SearchParameter("activated", false));
                                break;
                            case "LOCKED":
                                criteria.add(new SearchParameter("locked", true));
                                break;
                            case "ACTIVE":
                                criteria.add(new SearchParameter("memberships.enabled", true));
                                break;
                            case "INACTIVE":
                                criteria.add(new SearchParameter("memberships.enabled", false));
                                break;
                            default:
                                break;
                        }
                        break;
                    default:
                        criteria.add(new SearchParameter(k, v));
                        break;
                }
            });
        }
        return build(company, criteria);
    }

    public static Specification<User> build(Company company, List<SearchParameter> searchCriteria) {

        List<Specification<User>> specs = new ArrayList<>();
        if (company != null) {
            specs.add(createSpecification(company));
        }

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else if ("locked".equals(param.getKey())) {
                specs.add(UserSearchSpecificationBuilder.createNullSpecification(param));
            } else {
                specs.add(UserSearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<User> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<User> createSpecification(Company company) {
        return new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                Join<Subject, Membership> membership = root.join("memberships", JoinType.LEFT);

                return criteriaBuilder.equal(membership.get("company"), company);
            }
        };
    }

    private static Specification<User> buildGlobalSearch(Object value) {
        List<Specification<User>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("email", value));

        Specification<User> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<User> createNullSpecification(SearchParameter criteria) {
        return new SearchNullSpecification<>(criteria);
    }

    private static Specification<User> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<User> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }
}
