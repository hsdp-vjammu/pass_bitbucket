package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.repository.StudyRepository;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;

/**
 * Service class for managing studies.
 */
@Service
@Transactional
public class StudyService {

    @Inject
    private StudyRepository studyRepository;

    @Transactional(readOnly = true)
    public String generateUniqueStudyId() {
        int length = 8;

        String studyId = null;
        do {
            studyId = RandomStringUtils.randomAlphanumeric(length);
        } while (studyRepository.countByExternalReference(studyId) > 0);

        return studyId;
    }

}

