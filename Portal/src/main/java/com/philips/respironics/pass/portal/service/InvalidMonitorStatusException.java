package com.philips.respironics.pass.portal.service;

/**
 * This exception is throw in case of an invalid Monitor status.
 */
public class InvalidMonitorStatusException extends Exception {

    private static final long serialVersionUID = 1L;

    public InvalidMonitorStatusException() { super(); }

    public InvalidMonitorStatusException(String message) { super(message); }

    public InvalidMonitorStatusException(String message, Throwable t) {
        super(message, t);
    }
}
