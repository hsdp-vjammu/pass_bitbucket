package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatusChangeAction;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.repository.MonitorConfigurationRepository;
import com.philips.respironics.pass.portal.repository.MonitorHistoryRepository;
import com.philips.respironics.pass.portal.repository.MonitorRepository;
import com.philips.respironics.pass.portal.repository.SubjectRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Service class for managing studies.
 */
@Service
@Transactional
public class MonitorService {

    private static String SERIALNUMBER_PATTERN = "^[a-zA-Z0-9\\-]*$";

    @Inject
    private MonitorRepository monitorRepository;

    @Inject
    private MonitorConfigurationRepository monitorConfigurationRepository;

    @Inject
    private MonitorHistoryRepository monitorHistoryRepository;

    @Inject
    private SubjectRepository subjectRepository;

    public void createMonitors(Company company, MonitorType type, Set<String> serialNumbers) {
        createMonitors(company, type, serialNumbers.toArray(new String[serialNumbers.size()]));
    }

    @Transactional
    public void createMonitors(Company company, MonitorType type, String[] serialNumbers) {
        for (String serialNumber : serialNumbers) {
            Monitor monitor = new Monitor();
            monitor.setCompany(company);
            monitor.setType(type);
            monitor.setSerialNumber(serialNumber.trim());
            monitor.setStatus(MonitorStatus.UNASSIGNED);
            monitor = monitorRepository.save(monitor);
            MonitorHistory monitorHistory = new MonitorHistory(MonitorStatusChangeAction.REGISTER, monitor, company, null);
            monitorHistoryRepository.save(monitorHistory);
        }
    }

    @Transactional(propagation= Propagation.REQUIRED)
    public void changeStatus(Company company, MonitorStatus status, Long[] monitorIds) throws InvalidMonitorException, InvalidMonitorStatusException {
        for (Long id : monitorIds) {
            Monitor monitor = monitorRepository.getOne(id);
            if (monitor.getCompany().getId().equals(company.getId())) {
                changeStatus(monitor, status);
            } else {
                throw new InvalidMonitorException();
            }
        }
    }

    @Transactional(propagation= Propagation.REQUIRED)
    public Monitor changeAssignmentStatus(Monitor monitor, MonitorStatus status, int assignmentStatus) throws InvalidMonitorStatusException {
        if(MonitorStatus.ASSIGNED.equals(status)) {
            monitor.setAssignment_status(1);  //subject got changed update status
        } else {
            throw new InvalidMonitorStatusException();
        }
        return monitorRepository.save(monitor);
    }

    @Transactional(propagation= Propagation.REQUIRED)
    public Monitor changeStatus(Monitor monitor, MonitorStatus status) throws InvalidMonitorStatusException {
        switch (status) {
            case UNASSIGNED:
                if (MonitorStatus.ASSIGNED.equals(monitor.getStatus())) {
                    Subject subject = monitor.getAssignedSubject();
                    if (subject != null) {
                        monitor.setAssignedSubject(null);
                        subject.getAssignedMonitors().remove(monitor);
                        subjectRepository.save(subject);

                        MonitorConfiguration configuration = monitor.getConfiguration();
                        if (configuration != null) {
                            monitor.setConfiguration(null);
                            configuration.getMonitors().remove(monitor);
                            monitorConfigurationRepository.save(configuration);
                        }

                        MonitorHistory monitorHistory = new MonitorHistory(MonitorStatusChangeAction.UNASSIGN_SUBJECT, monitor, monitor.getCompany(), subject);
                        monitorHistoryRepository.save(monitorHistory);

                        monitor.setStatus(status);
                        monitor.setAssignment_status(0);
                    }
                } else if (MonitorStatus.DISABLED.equals(monitor.getStatus())) {
                    monitor.setStatus(status);
                    monitor.setAssignment_status(0);

                    MonitorHistory monitorHistory = new MonitorHistory(MonitorStatusChangeAction.ENABLE, monitor, monitor.getCompany(), null);
                    monitorHistoryRepository.save(monitorHistory);
                } else if (MonitorStatus.REMOVED.equals(monitor.getStatus())) {
                    monitor.setStatus(status);
                    monitor.setAssignment_status(0);

                    MonitorHistory monitorHistory = new MonitorHistory(MonitorStatusChangeAction.RE_REGISTER, monitor, monitor.getCompany(), null);
                    monitorHistoryRepository.save(monitorHistory);

                } else {
                    throw new InvalidMonitorStatusException();
                }
                break;
            case DISABLED:
                if (MonitorStatus.UNASSIGNED.equals(monitor.getStatus())) {
                    monitor.setStatus(status);
                    monitor.setAssignment_status(0);

                    MonitorHistory monitorHistory = new MonitorHistory(MonitorStatusChangeAction.DISABLE, monitor, monitor.getCompany(), null);
                    monitorHistoryRepository.save(monitorHistory);
                } else {
                    throw new InvalidMonitorStatusException();
                }
                break;
            case REMOVED:
                if (MonitorStatus.UNASSIGNED.equals(monitor.getStatus()) || MonitorStatus.DISABLED.equals(monitor.getStatus())) {
                    monitor.setStatus(status);
                    monitor.setAssignment_status(0);

                    MonitorHistory monitorHistory = new MonitorHistory(MonitorStatusChangeAction.REMOVE, monitor, monitor.getCompany(), null);
                    monitorHistoryRepository.save(monitorHistory);
                } else {
                    throw new InvalidMonitorStatusException();
                }
                break;
            default:
                throw new InvalidMonitorStatusException();
        }
        return monitorRepository.save(monitor);
    }

    public List<String> validateSerialNumber(Long companyId, MonitorType monitorType, String serialNumber) {
        List<String> errorCodes = new ArrayList<>();
        boolean currentErrorCode=false;

        if (StringUtils.isNotBlank(serialNumber)) {
            if (serialNumber.length() > 32) {
                errorCodes.add("TOO_LONG");
                currentErrorCode=true;
                //return errorCodes;
            }
            if (!serialNumber.matches(SERIALNUMBER_PATTERN)) {
                errorCodes.add("INVALID_CHARACTERS");
                //return errorCodes;
                currentErrorCode=true;
            }
            if (!currentErrorCode){
            List<Monitor> monitors = monitorRepository.findAllByTypeAndSerialNumber(monitorType, serialNumber);
            monitors.forEach(monitor -> {
                if(monitor.getCompany().getId().equals(companyId)) {
                    errorCodes.add("ALREADY_REGISTERED");
                    return;
                } else if (!MonitorStatus.REMOVED.equals(monitor.getStatus())) {
                    errorCodes.add("ALREADY_REGISTERED");
                    return;
                }
            });}
        } else {
            errorCodes.add("EMPTY");
        }

        return errorCodes;
    }

}

