package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Company;

import org.springframework.data.jpa.repository.*;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Company entity.
 */
@SuppressWarnings("unused")
public interface CompanyRepository extends JpaRepository<Company,Long> {

    Optional<Company> findOneById(Long companyId);

}
