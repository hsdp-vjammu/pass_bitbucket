/**
 * Data Access Objects used by WebSocket services.
 */
package com.philips.respironics.pass.portal.web.websocket.dto;
