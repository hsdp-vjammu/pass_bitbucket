package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.repository.PopNumberRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Service class for managing pop numbers.
 */
@Service
@Transactional
public class PopNumberService {

    private static String POPNUMBER_PATTERN = "^[a-fA-F0-9\\-]*$";

    @Inject
    private PopNumberRepository popNumberRepository;

    public String sanitize(String number) {
        if (StringUtils.isNotBlank(number)) {
            return number.replace("-", "").toUpperCase();
        } else {
            return number;
        }
    }
    public List<String> validateNumber(String number) {
        List<String> errorCodes = new ArrayList<>();

        if (StringUtils.isNotBlank(number)) {
            number = sanitize(number);
            if (number.length() > 32) {
                errorCodes.add("TOO_LONG");
            }
            if (!number.matches(POPNUMBER_PATTERN)) {
                errorCodes.add("INVALID_CHARACTERS");
            }
            if (popNumberRepository.findOneByNumber(number).isPresent()) {
                errorCodes.add("ALREADY_REGISTERED");
            }
        } else {
            errorCodes.add("EMPTY");
        }

        return errorCodes;
    }

}

