package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.MembershipAccess;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;


/**
 * A DTO for the Company entity.
 */
public class CompanyDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private String uniqueIdentifier;

    @NotNull
    private String name;

    private String streetAddress;

    private String zipcode;

    private String city;

    private String state;

    private String country;

    private String contactPhoneNumber;

    private Set<MembershipDTO> memberships = new HashSet<>();

    public CompanyDTO() {
    }

    public CompanyDTO(Company company) {
        this(company, true);
    }

    public CompanyDTO(Company company, boolean includeMemberships) {
        this.id = company.getId();
        this.name = company.getName();
        this.uniqueIdentifier = company.getUniqueIdentifier();
        this.streetAddress = company.getStreetAddress();
        this.zipcode = company.getZipCode();
        this.city = company.getCity();
        this.state = company.getState();
        this.country = company.getCountry();
        this.contactPhoneNumber = company.getContactPhoneNumber();

        if (includeMemberships) {
            company.getMemberships().forEach(membership-> {
                MembershipDTO membershipDTO = new MembershipDTO(membership, this);
                memberships.add(membershipDTO);
            });
        }
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }
    public void setUniqueIdentifier(String uniqueIdentifier) {
        this.uniqueIdentifier = uniqueIdentifier;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getStreetAddress() {
        return streetAddress;
    }
    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getZipCode() {
        return zipcode;
    }
    public void setZipCode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }
    public void setContactPhoneNumber(String phoneNumber) {
        this.contactPhoneNumber = phoneNumber;
    }

    public Set<MembershipDTO> getMemberships() { return memberships; }
    public void setMemberships(Set<MembershipDTO> memberships) { this.memberships = memberships; }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CompanyDTO companyDTO = (CompanyDTO) o;

        if ( ! Objects.equals(id, companyDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CompanyDTO{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", name='" + name + "'" +
            '}';
    }

    public static List<CompanyDTO> convert(List<Company> entities)
    {
        List<CompanyDTO> dtos = new ArrayList<>();
        for (Company entity : entities) {
            dtos.add(new CompanyDTO(entity));
        }
        return dtos;
    }
}
