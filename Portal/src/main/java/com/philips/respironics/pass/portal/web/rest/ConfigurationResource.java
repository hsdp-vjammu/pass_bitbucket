package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.philips.respironics.pass.portal.domain.Setting;
import com.philips.respironics.pass.portal.repository.SettingRepository;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.web.rest.dto.ConfigurationDTO;
import com.philips.respironics.pass.portal.web.rest.dto.PasswordPolicyDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URISyntaxException;

/**
 * Controller for view and managing Log Level at runtime.
 */
@RestController
@RequestMapping("/api")
public class ConfigurationResource {

    @Inject
    private SettingRepository settingRepository;

    @RequestMapping(value = "/public/config",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ConfigurationDTO> getConfiguration() {
        ConfigurationDTO config = new ConfigurationDTO();

        PasswordPolicyDTO policy = new PasswordPolicyDTO();
        policy.loadPasswordPolicies(settingRepository.findAllByCategory(Setting.PASSWORD_POLICY.CATEGORY));
        policy.loadSessionManagement(settingRepository.findAllByCategory(Setting.SESSION_MANAGEMENT.CATEGORY));

        config.setPasswordPolicyDTO(policy);

        return new ResponseEntity<>(config, HttpStatus.OK);
    }

    /**
     * GET  /config/password-policy : get the password policy configuration.
     *
     * @return the ResponseEntity with status 200 (OK) and with body the PasswordPolicyDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/config/password-policy",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<PasswordPolicyDTO> getPasswordPolicy() {

        PasswordPolicyDTO policy = new PasswordPolicyDTO();

        policy.loadPasswordPolicies(settingRepository.findAllByCategory(Setting.PASSWORD_POLICY.CATEGORY));
        policy.loadSessionManagement(settingRepository.findAllByCategory(Setting.SESSION_MANAGEMENT.CATEGORY));

        return new ResponseEntity<>(policy, HttpStatus.OK);
    }

    /**
     * PUT  /config/password-policy : Updates the password policy configuration.
     *
     * @param passwordPolicyDTO the password policy configuration to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated passwordPolicyDTO,
     * or with status 400 (Bad Request) if the passwordPolicyDTO is not valid,
     * or with status 500 (Internal Server Error) if the passwordPolicyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/config/password-policy",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.ADMIN)
    @Transactional
    public ResponseEntity<PasswordPolicyDTO> updatePasswordPolicy(@Valid @RequestBody PasswordPolicyDTO passwordPolicyDTO) throws URISyntaxException {

        /* Password policy settings */
        saveSetting(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.MIN_PASSWORD_LENGTH, passwordPolicyDTO.getPasswordMinLength());
        saveSetting(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.MAX_PASSWORD_AGE, passwordPolicyDTO.getMaxPasswordAge());
        saveSetting(Setting.PASSWORD_POLICY.CATEGORY, Setting.PASSWORD_POLICY.PASSWORD_EXPIRATION_THRESHHOLD, passwordPolicyDTO.getPasswordExpirationThreshold());

        /* Session management settings */
        saveSetting(Setting.SESSION_MANAGEMENT.CATEGORY, Setting.SESSION_MANAGEMENT.SESSION_TIMEOUT, passwordPolicyDTO.getSessionTimeout());
        saveSetting(Setting.SESSION_MANAGEMENT.CATEGORY, Setting.SESSION_MANAGEMENT.FAILED_LOGIN_ATTEMPTS, passwordPolicyDTO.getFailedLoginAttempts());
        saveSetting(Setting.SESSION_MANAGEMENT.CATEGORY, Setting.SESSION_MANAGEMENT.FAILED_LOGIN_ATTEMPTS_PERIOD, passwordPolicyDTO.getFailedLoginAttemptsPeriod());

        return new ResponseEntity<>(passwordPolicyDTO, HttpStatus.OK);
    }

    private void saveSetting(String category, String name, int value) {
        saveSetting(category, name, Integer.toString(value));
    }

    private void saveSetting(String category, String name, String value) {
        Setting passwordPolicySetting = settingRepository.findOneByCategoryAndName(category, name);
        if (passwordPolicySetting != null) {
            passwordPolicySetting.setValue(value);
        }
        settingRepository.save(passwordPolicySetting);
    }

}
