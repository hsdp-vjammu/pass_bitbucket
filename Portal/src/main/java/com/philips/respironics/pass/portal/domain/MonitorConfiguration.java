package com.philips.respironics.pass.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.philips.respironics.pass.portal.config.Constants;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorType;
import com.philips.respironics.pass.portal.domain.enumeration.WatchDisplayType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

/**
 * A MonitorConfiguration.
 */
@Entity
@Table(name = "monitor_configuration")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class MonitorConfiguration extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Pattern(regexp = Constants.UUID_REGEX)
    @Size(min = 36, max = 36)
    @Column(name = "unique_identifier", nullable = false, updatable = false)
    private String uniqueIdentifier;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "monitor_type", nullable = false)
    private MonitorType monitorType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "display_type", nullable = false)
    private WatchDisplayType displayType;

    @NotNull
    @Column(name = "display_blinded", nullable = false)
    private boolean displayBlinded = false;

    @NotNull
    @Column(name = "mobile_app_blinded", nullable = false)
    private boolean mobileAppBlinded = false;

    @NotNull
    @Column(name = "pal_sedentary_min", nullable = false)
    private Integer palSedentaryMin;

    @NotNull
    @Column(name = "pal_sedentary_max", nullable = false)
    private Integer palSedentaryMax;

    @NotNull
    @Column(name = "pal_light_min", nullable = false)
    private Integer palLightMin;

    @NotNull
    @Column(name = "pal_light_max", nullable = false)
    private Integer palLightMax;

    @NotNull
    @Column(name = "pal_medium_min", nullable = false)
    private Integer palMediumMin;

    @NotNull
    @Column(name = "pal_medium_max", nullable = false)
    private Integer palMediumMax;

    @NotNull
    @Column(name = "pal_vigorous_min", nullable = false)
    private Integer palVigorousMin;

    @NotNull
    @Column(name = "sedentary_alert", nullable = false)
    private Integer sedentaryAlert;

    @NotNull
    @Column(name = "sedentary_alert_enabled", nullable = false)
    private boolean sedentaryAlertEnabled = false;

    @NotNull
    @Column(name = "target_steps", nullable = false)
    private Integer targetSteps;

    @NotNull
    @Column(name = "target_aee", nullable = false)
    private Integer targetAee;

    @ManyToOne
    @NotNull
    private Company company;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "monitorConfiguration")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MonitorConfigurationQuestion> questions = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade={CascadeType.PERSIST, CascadeType.REFRESH}, mappedBy = "monitorConfiguration")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Monitor> monitors = new HashSet<>();

    public MonitorConfiguration() {
        this.uniqueIdentifier = UUID.randomUUID().toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueIdentifier() {
        return uniqueIdentifier;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public MonitorType getMonitorType() {
        return monitorType;
    }
    public void setMonitorType(MonitorType monitorType) {
        this.monitorType = monitorType;
    }

    public WatchDisplayType getDisplayType() {
        return displayType;
    }
    public void setDisplayType(WatchDisplayType displayType) {
        this.displayType = displayType;
    }

    public boolean getDisplayBlinded() {
        return displayBlinded;
    }
    public void setDisplayBlinded(boolean blinded) {
        this.displayBlinded = blinded;
    }

    public boolean getMobileAppBlinded() {
        return mobileAppBlinded;
    }
    public void setMobileAppBlinded(boolean blinded) {
        this.mobileAppBlinded = blinded;
    }

    public Integer getPalSedentaryMax() {
        return palSedentaryMax;
    }
    public void setPalSedentaryMax(Integer value) {
        this.palSedentaryMax = value;
    }

    public Integer getPalSedentaryMin() {
        return palSedentaryMin;
    }
    public void setPalSedentaryMin(Integer value) {
        this.palSedentaryMin = value;
    }

    public Integer getPalLightMin() {
        return palLightMin;
    }
    public void setPalLightMin(Integer value) {
        this.palLightMin = value;
    }

    public Integer getPalLightMax() {
        return palLightMax;
    }
    public void setPalLightMax(Integer value) {
        this.palLightMax = value;
    }

    public Integer getPalMediumMin() {
        return palMediumMin;
    }
    public void setPalMediumMin(Integer value) {
        this.palMediumMin = value;
    }

    public Integer getPalMediumMax() {
        return palMediumMax;
    }
    public void setPalMediumMax(Integer value) {
        this.palMediumMax = value;
    }

    public Integer getPalVigorousMin() {
        return palVigorousMin;
    }
    public void setPalVigorousMin(Integer value) {
        this.palVigorousMin = value;
    }

    public Integer getSedentaryAlert() {
        return sedentaryAlert;
    }
    public void setSedentaryAlert(Integer value) {
        this.sedentaryAlert = value;
    }

    public boolean isSedentaryAlertEnabled() { return sedentaryAlertEnabled;  }
    public void setSedentaryAlertEnabled(boolean enabled) { this.sedentaryAlertEnabled = enabled;  }

    public Integer getTargetSteps() {
        return targetSteps;
    }
    public void setTargetSteps(Integer value) {
        this.targetSteps = value;
    }

    public Integer getTargetAee() {
        return targetAee;
    }
    public void setTargetAee(Integer value) {
        this.targetAee = value;
    }

    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<MonitorConfigurationQuestion> getQuestions() {
        return questions;
    }
    public void setQuestions(Set<MonitorConfigurationQuestion> questions) {
        this.questions = questions;
    }

    public Set<Monitor> getMonitors() {
        return monitors;
    }
    public void setMonitors(Set<Monitor> monitors) {
        this.monitors = monitors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MonitorConfiguration configuration = (MonitorConfiguration) o;
        if(configuration.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, configuration.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MonitorConfiguration{" +
            "id=" + id +
            ", uniqueIdentifier='" + uniqueIdentifier + "'" +
            ", name='" + name + "'" +
            ", monitorType='" + monitorType + "'" +
            '}';
    }
}
