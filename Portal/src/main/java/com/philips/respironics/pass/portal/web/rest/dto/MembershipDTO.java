package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.Membership;
import com.philips.respironics.pass.portal.domain.MembershipAccess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the Study entity.
 */
public class MembershipDTO extends AbstractAuditingDTO implements Serializable {

    private Long id;

    private boolean enabled;

    private UserDTO user;

    private CompanyDTO company;

    private List<MembershipAccessDTO> accessLevels = new ArrayList<>();

    public MembershipDTO() {
    }

    public MembershipDTO(Membership membership, Object referenceClass) {
        this.id = membership.getId();
        this.enabled = membership.getEnabled();
        if (!UserDTO.class.isAssignableFrom(referenceClass.getClass())) {
            this.user = new UserDTO(membership.getUser(), false);
        }
        if (!CompanyDTO.class.isAssignableFrom(referenceClass.getClass())) {
            this.company = new CompanyDTO(membership.getCompany(), false);
        }
        for (MembershipAccess accessLevel : membership.getAccessLevels()) {
            this.getAccessLevels().add(new MembershipAccessDTO(accessLevel));
        }
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public UserDTO getUser() {
        return user;
    }
    public void setUser(UserDTO user) {
        this.user = user;
    }

    public CompanyDTO getCompany() {
        return company;
    }
    public void setCompany(CompanyDTO company) {
        this.company = company;
    }

    public List<MembershipAccessDTO> getAccessLevels() {
        return accessLevels;
    }
    public void setAccessLevels(List<MembershipAccessDTO> accessLevels) {
        this.accessLevels = accessLevels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MembershipDTO dto = (MembershipDTO) o;

        if ( ! Objects.equals(id, dto.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "MembershipDTO{" +
            "id=" + id +
            '}';
    }
}
