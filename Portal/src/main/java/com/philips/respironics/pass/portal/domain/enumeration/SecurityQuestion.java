package com.philips.respironics.pass.portal.domain.enumeration;

/**
 * The SecurityQuestion enumeration.
 */
public enum SecurityQuestion {
    NONE("NONE"),
    MAIDEN_NAME("MAIDEN_NAME"),
    SPORT("SPORT"),
    PET("PET"),
    HIGH_SCHOOL("HIGH_SCHOOL"),
    VACATION_SPOT("VACATION_SPOT");

    private final String text;

    SecurityQuestion(final String text) {
        this.text = text;
    }

    public static SecurityQuestion fromString(String text) {
        if (text != null) {
            for (SecurityQuestion b : SecurityQuestion.values()) {
                if (text.equalsIgnoreCase(b.text)) {
                    return b;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return text;
    }

}
