package com.philips.respironics.pass.portal.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String COMPANY_ADMIN = "ACL_COMPADMIN";

    public static final String CLINICIAN = "ACL_CLINICIAN";

    public static final String SITE_COORDINATOR = "ACL_SITECOORD";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    private AuthoritiesConstants() {
    }
}
