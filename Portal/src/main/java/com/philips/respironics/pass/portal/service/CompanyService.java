package com.philips.respironics.pass.portal.service;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.Membership;
import com.philips.respironics.pass.portal.repository.CompanyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Optional;

/**
 * Service class for managing companies.
 */
@Service
@Transactional
public class CompanyService {

    @Inject
    private CompanyRepository companyRepository;

    @Transactional(readOnly = true)
    public Optional<Company> getCompanyWithMemberships(Long id) {
        return companyRepository.findOneById(id).map(o -> {
            // eagerly load the associations
            for (Membership membership : o.getMemberships()) {
                membership.getUser().getAuthorities().size();
            }

            return o;
        });
    }

}

