package com.philips.respironics.pass.portal.web.rest.dto;

import com.philips.respironics.pass.portal.domain.MonitorHistory;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatusChangeAction;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * A DTO for the MonitorHistory for Subject screen monitor serial number drop down values.
 */
public class Subject_MonitorHistoryDTO extends AbstractAuditingDTO implements Serializable {

    private String serialNumber;

    private ZonedDateTime lastModifiedDate;

    public Subject_MonitorHistoryDTO() {}

    public Subject_MonitorHistoryDTO(MonitorHistory entity) {
        this.lastModifiedDate = entity.getLastModifiedDate();
        this.serialNumber = entity.getSerialNumber();
    }

    public static List<Subject_MonitorHistoryDTO> convert(List<MonitorHistory> entities)
    {
        List<Subject_MonitorHistoryDTO> dtos = new ArrayList<>();
        for (MonitorHistory entity : entities) {
            dtos.add(new Subject_MonitorHistoryDTO(entity));
        }
        return dtos;
    }


    @Override
    public ZonedDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    @Override
    public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
