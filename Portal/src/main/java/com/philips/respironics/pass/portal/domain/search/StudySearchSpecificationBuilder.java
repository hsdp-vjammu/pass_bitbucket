package com.philips.respironics.pass.portal.domain.search;

import com.philips.respironics.pass.portal.domain.Company;
import com.philips.respironics.pass.portal.domain.Study;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StudySearchSpecificationBuilder {

    public static Specification<Study> build(Company company, Map<String, String> params) {
        List<SearchParameter> criteria = new ArrayList<>();
        if (params != null && !params.isEmpty()) {
            params.forEach((k,v)->{
                switch (k) {
                    case "status":
                        try {
                            criteria.add(new SearchParameter(k, StudyStatus.valueOf(v)));
                        } catch (IllegalArgumentException ex) {
                            // Invalid status
                        }
                        break;
                    default:
                        criteria.add(new SearchParameter(k, v));
                        break;
                }
            });
        }
        return build(company, criteria);
    }

    public static Specification<Study> build(Company company, List<SearchParameter> searchCriteria) {

        List<Specification<Study>> specs = new ArrayList<>();
        specs.add(createSpecification(company));

        for (SearchParameter param : searchCriteria) {
            if ("$".equals(param.getKey())) {
                specs.add(buildGlobalSearch(param.getValue()));
            } else {
                specs.add(StudySearchSpecificationBuilder.createSpecification(param));
            }
        }

        Specification<Study> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }

    private static Specification<Study> createSpecification(Company company) {
        return new Specification<Study>() {
            @Override
            public Predicate toPredicate(Root<Study> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                return criteriaBuilder.equal(root.get("company"), company);
            }
        };
    }

    private static Specification<Study> buildGlobalSearch(Object value) {
        List<Specification<Study>> specs = new ArrayList<>();
        specs.add(createLikeSpecification("name", value));
        specs.add(createLikeSpecification("description", value));
//        specs.add(createLikeSpecification("uniqueIdentifier", value));

        Specification<Study> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).or(specs.get(i));
        }
        return result;
    }

    private static Specification<Study> createSpecification(SearchParameter criteria) {
        return new SearchSpecification<>(criteria);
    }

    private static Specification<Study> createLikeSpecification(String key, Object value) {
        return new SearchLikeSpecification<>(key, value);
    }

}
