package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.philips.respironics.pass.portal.service.TDRDataService;
import com.philips.respironics.pass.portal.web.rest.dto.ExportDTO;
import com.philips.respironics.pass.portal.web.rest.dto.TDRDataDTO;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.crypt.EncryptionMode;
import org.apache.poi.poifs.crypt.Encryptor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * REST controller to extract TDR Data.
 * Export TDR Data using Apache POI
 */
@RestController
@RequestMapping("api")
public class TDRDataResource {
    private static String[] excelHeaderColumns = {"Time Stamp", "Activity Counts", "Heart Rate BPM", "Respiration Rate", "Total Energy Expenditure", "Active Energy Expenditure", "Steps", "Not Worn", "Run", "Walk",
        "Cycle", "Active Minutes", "Resting Heart Rate", "Heart Rate Recovery", "Cardio Fitness Index", "VO2Max"};

    private final Logger log = LoggerFactory.getLogger(TDRDataResource.class);
    @Inject
    private TDRDataService tdrDataService;

    @RequestMapping(value = "/dataitem",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TDRDataDTO>> getTDRData(
        @RequestParam(value = "serialNo", required = true) String serialNo,
        @RequestParam(value = "subjectUUID", required = true) String subjectUUID,
        @RequestParam(value = "requestTimeStampStart", required = true) String requestTimeStampStart,
        @RequestParam(value = "requestTimeStampEnd", required = true) String requestTimeStampEnd,
        @RequestParam(value = "timeZone", required = true) String timeZone,
        @RequestParam(value = "page", required = true) Long page
    ) {
        log.debug("REST request to get tdr data");
        List data = tdrDataService.getTDRData(serialNo, subjectUUID, requestTimeStampStart, requestTimeStampEnd, page, timeZone);
        List<TDRDataDTO> tdrData = (List<TDRDataDTO>) data.get(0);
        //Convert localTimeStamp from unix to date time format
        List<TDRDataDTO> convertedTDRData = tdrData.stream()
            .map(TDRDataDTO -> dataFormatter(TDRDataDTO, timeZone))
            .collect(Collectors.toList());

        HttpHeaders headers = new HttpHeaders();
        HttpHeaders headersFromTDR = (HttpHeaders) data.get(1);
        headers.add("Totalelements", headersFromTDR.get("Totalelements").get(0));
        headers.add("Totalpages", headersFromTDR.get("Totalpages").get(0));

        return new ResponseEntity<>(convertedTDRData, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/processCSVData",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<TDRDataDTO>> getCSVExportTDRData(@RequestBody ExportDTO exportDTO) {
        log.debug("REST request to process data for CSV Export");

        //get processed total datalist records to export
        List tdrData = getTDRDataForExport(exportDTO);
        List<TDRDataDTO> data = (List<TDRDataDTO>) tdrData.get(0);
        HttpHeaders headersFromTDR = (HttpHeaders) tdrData.get(1);
        return new ResponseEntity<>(data, headersFromTDR, HttpStatus.OK);
    }

    @RequestMapping(value = "file-download",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody
    HttpEntity<byte[]> download
        (
            @RequestBody ExportDTO exportDTO,
            HttpServletRequest request,
            HttpServletResponse response) {
        log.debug("REST request to generate Excel for TDR Data: {}", exportDTO.getFileName());

        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        byte[] document = null;
        //get processed data list records to export
        List tdrData = getTDRDataForExport(exportDTO);

        List<TDRDataDTO> data = (List<TDRDataDTO>) tdrData.get(0);
        HttpHeaders headersFromTDR = (HttpHeaders) tdrData.get(1);

        //generate Header Meta Data information
        StringBuilder sb = new StringBuilder("");
        sb.append("Subject UUID:                   " + exportDTO.getSubjectUUID() + "\n");
        sb.append("Analysis range:               " + exportDTO.getRequestTimeStampStart() + " - " + exportDTO.getRequestTimeStampEnd() + "\n");
        sb.append("Health band firmware version: " + exportDTO.getHealthBandFirmwareVer() + "\n");
        sb.append("Mobile application version:   " + exportDTO.getMobileAppSWVer() + "\n");
        sb.append("PAS Web Application version:  " + exportDTO.getPASWebAppSWVer() + "\n");
        sb.append("Study Name:                   " + exportDTO.getStudyName() + "\n");
        sb.append("Configuration Name:           " + exportDTO.getConfigurationName() + "\n");
        sb.append("Device SN:                    " + exportDTO.getSerialNo() + "\n" + "\n");

        String text = sb.toString();
        String[] lines = text.split("\\r?\\n");

        //pass data to create excel file
        try {
            document = generateExcelFile(response, document, data, lines, exportDTO);
            response.reset();
            response.setHeader("Expires", "0"); //eliminates browser caching
            response.setHeader("Content-disposition", "attachment;filename=" + exportDTO.getFileName());
            response.setHeader("TotalElements", headersFromTDR.get("Totalelements").get(0));
            response.setHeader("TotalPages", headersFromTDR.get("Totalpages").get(0));
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            log.debug("Unable to generate Excel file. " + e.getStackTrace().toString());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new HttpEntity<byte[]>(document);
    }

    /*
        Method to generate consolidate tdr data
     */
    private List getTDRDataForExport(@RequestBody ExportDTO exportDTO) {
        StopWatch stopWatch = new StopWatch("Performance Test TDR Data iteration/collection Result");
        stopWatch.start("getTDRDataForExport Method started...");
        //get TDR Data for process
        Long page = new Long(0);  //set to first page
        List<TDRDataDTO> tdrDataCollection = null;
        List data = tdrDataService.getTDRData(exportDTO.getSerialNo(), exportDTO.getSubjectUUID(), exportDTO.getRequestTimeStampStart(), exportDTO.getRequestTimeStampEnd(), page, exportDTO.getTimeZone());
        List<TDRDataDTO> tdrData = (List<TDRDataDTO>) data.get(0);  //data

        //iterate thru epoch and process formatting - first record
        tdrDataCollection = tdrData.stream()
            .map(TDRDataDTO -> dataFormatter(TDRDataDTO, exportDTO.getTimeZone()))
            .collect(Collectors.toList());

        //after getting first call processed, iterate thru total number of pages
        HttpHeaders headersFromTDR = (HttpHeaders) data.get(1);  // headers
        int totalPages = new Integer(headersFromTDR.get("Totalpages").get(0)).intValue();
        for (Long pages = new Long(1); pages < totalPages; pages++) {
            List getTDRDataRecord = tdrDataService.getTDRData(exportDTO.getSerialNo(), exportDTO.getSubjectUUID(), exportDTO.getRequestTimeStampStart(), exportDTO.getRequestTimeStampEnd(), pages, exportDTO.getTimeZone());
            List<TDRDataDTO> tdrDataDTOS = (List<TDRDataDTO>) getTDRDataRecord.get(0);
            //iterate thru epoch and process formatting
            List<TDRDataDTO> tdrDataEpoch = tdrDataDTOS.stream()
                .map(TDRDataDTO -> dataFormatter(TDRDataDTO, exportDTO.getTimeZone()))
                .collect(Collectors.toList());
            tdrDataCollection.addAll(tdrDataEpoch);
        }

        List responseList = new ArrayList();
        responseList.add(tdrDataCollection);
        responseList.add(headersFromTDR);
        stopWatch.stop();
        log.info(stopWatch.prettyPrint());
        return responseList;
    }

    private byte[] generateExcelFile(HttpServletResponse response, byte[] document, List<TDRDataDTO> dataList, String[] metaDataRows, ExportDTO exportDTO) throws IOException {

        // Create a Workbook
        FastByteArrayOutputStream output = null;
        SXSSFWorkbook workbook = (new ExportTDRDataResponseExcel()).exportExcel(metaDataRows, 8, excelHeaderColumns, 9, dataList, 10);

        try {
            //verify for password protection
            if ("true".equalsIgnoreCase(exportDTO.getCheckForPwd())) {
                // Prepare
                POIFSFileSystem fs = new POIFSFileSystem();
                EncryptionInfo info = new EncryptionInfo(EncryptionMode.agile);
                //CipherAlgorithm.aes192, -1, HashAlgorithm.sha384, -1, null);
                Encryptor enc = info.getEncryptor();
                enc.confirmPassword(exportDTO.getPassword());
                //Encryption
                try (OutputStream encryptedDS = enc.getDataStream(fs)) {
                    workbook.write(encryptedDS);
                } catch (Exception e) {
                    log.debug("Unable to generate Excel file, file Encryption failed. " + e.getStackTrace().toString());
                }
                FastByteArrayOutputStream output2 = new FastByteArrayOutputStream();
                fs.writeFilesystem(output2);
                output2.flush();
                output2.close();

                document = output2.toByteArrayUnsafe(); //to boost performance
            } else {
                // Write the output to a file
                output = new FastByteArrayOutputStream();
                workbook.write(output);
                output.flush();
                output.close();
                document = output.toByteArrayUnsafe(); //to boost performance
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (workbook != null) {
                    // Closing the workbook
                    workbook.dispose();
                    workbook.close();
                }
            } catch (IOException e) {
            }
        }
        return document;
    }

    private TDRDataDTO dataFormatter(TDRDataDTO tdrDataEntry, String timeZone) {

        String convertedDate = tdrDataEntry.getTimeStamp(tdrDataEntry.getTimeStamp(), timeZone);
        tdrDataEntry.setTimeStamp(convertedDate);

        //Format Active Energy Expenditure to 4 decimals
        String activeEE = tdrDataEntry.getActiveEnergyExpenditure();
        activeEE = getFormattedEnergyExpenditureValue(activeEE);
        tdrDataEntry.setActiveEnergyExpenditure(activeEE);

        //Format Total Energy Expenditure
        String totalEE = tdrDataEntry.getTotalEnergyExpenditure();
        totalEE = getFormattedEnergyExpenditureValue(totalEE);
        tdrDataEntry.setTotalEnergyExpenditure(totalEE);
        return tdrDataEntry;
    }

    private String getFormattedEnergyExpenditureValue(String energyExpenditure) {
        if (!"0.0".equals(energyExpenditure)) {
            BigDecimal activeEEbigDecimal = new BigDecimal(energyExpenditure);
            DecimalFormat df = new DecimalFormat("#.####");
            df.setMinimumFractionDigits(4);
            energyExpenditure = df.format(activeEEbigDecimal);
        }
        return energyExpenditure;
    }
}
