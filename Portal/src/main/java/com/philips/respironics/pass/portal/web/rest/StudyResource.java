package com.philips.respironics.pass.portal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.philips.respironics.pass.portal.domain.*;
import com.philips.respironics.pass.portal.domain.enumeration.MonitorStatus;
import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;
import com.philips.respironics.pass.portal.domain.search.StudySearchSpecificationBuilder;
import com.philips.respironics.pass.portal.repository.*;
import com.philips.respironics.pass.portal.security.AuthoritiesConstants;
import com.philips.respironics.pass.portal.service.InvalidMonitorStatusException;
import com.philips.respironics.pass.portal.service.MonitorService;
import com.philips.respironics.pass.portal.service.UserService;
import com.philips.respironics.pass.portal.web.rest.dto.StudyDTO;
import com.philips.respironics.pass.portal.web.rest.util.HeaderUtil;
import com.philips.respironics.pass.portal.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing Study.
 */
@RestController
@RequestMapping("/api")
public class StudyResource {

    private final Logger log = LoggerFactory.getLogger(StudyResource.class);

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private CompanyRepository companyRepository;

    @Inject
    private SiteRepository siteRepository;

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private MonitorRepository monitorRepository;

    @Inject
    private MonitorService monitorService;

    @Inject
    private UserService userService;

    /**
     * POST  /studies : Create a new study.
     *
     * @param studyDTO the studyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new studyDTO, or with status 400 (Bad Request) if the study has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/studies",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<StudyDTO> createStudy(@Valid @RequestBody StudyDTO studyDTO) throws URISyntaxException {
        log.debug("REST request to save Study : {}", studyDTO);
        if (studyDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("study", "idexists", "A new study cannot already have an ID")).body(null);
        }

        User user = userService.getUserWithAuthorities();
        for (Membership membership : user.getMemberships()) {
            if (membership.getCompany().getId() == studyDTO.getCompanyId()) {

                Study study = new Study();
                study.setCompany(membership.getCompany());
                study.setName(studyDTO.getName());
                study.setDescription(studyDTO.getDescription());
                study.setStatus(studyDTO.getStatus());
                study.setComments(studyDTO.getComments());

                study = studyRepository.save(study);
                StudyDTO result = new StudyDTO(study);
                return ResponseEntity.created(new URI("/api/studies/" + result.getId()))
                    .headers(HeaderUtil.createEntityCreationAlert("study", result.getExternalReference()))
                    .body(result);
            }
        }

        return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("company", "invalid", "Company is not the active company for this user")).body(null);
    }

    /**
     * PUT  /studies : Updates an existing study.
     *
     * @param studyDTO the studyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated studyDTO,
     * or with status 400 (Bad Request) if the studyDTO is not valid,
     * or with status 500 (Internal Server Error) if the studyDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/studies",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<StudyDTO> updateStudy(@Valid @RequestBody StudyDTO studyDTO) throws URISyntaxException {
        log.debug("REST request to update Study : {}", studyDTO);
        if (studyDTO.getId() == null) {
            return createStudy(studyDTO);
        }

        Study study = studyRepository.findOne(studyDTO.getId());
        if (study != null) {
            study.setName(studyDTO.getName());
            study.setDescription(studyDTO.getDescription());
            study.setComments(studyDTO.getComments());
            study = studyRepository.save(study);
            StudyDTO result = new StudyDTO(study);
            result.setNumberOfSites(siteRepository.countByStudy(study));
            result.setNumberOfSubjects(subjectRepository.countByStudy(study));
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("study", studyDTO.getExternalReference()))
                .body(result);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    /**
     * PUT  /studies/changeStatus : Updates an existing study.
     *
     * @param studyDTO the studyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated studyDTO,
     * or with status 400 (Bad Request) if the studyDTO is not valid,
     * or with status 500 (Internal Server Error) if the studyDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/studies/changeStatus",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<StudyDTO> updateStudyStatus(@Valid @RequestBody StudyDTO studyDTO) throws URISyntaxException, InvalidMonitorStatusException {
        log.debug("REST request to update Study status : {}", studyDTO);

        Study study = studyRepository.findOne(studyDTO.getId());
        if (study != null) {
            study.setStatus(studyDTO.getStatus());

            if (StudyStatus.INACTIVE.equals(studyDTO.getStatus())) {
                List<Monitor> assignedMonitors = monitorRepository.findAllByAssignedSubjectStudyAndStatus(study, MonitorStatus.ASSIGNED);
                for (Monitor monitor : assignedMonitors) {
                    try {
                        monitorService.changeStatus(monitor, MonitorStatus.UNASSIGNED);
                    } catch (InvalidMonitorStatusException e) {
                        log.error("Unable to unassign monitor: {}", monitor.getSerialNumber());
                        throw e;
                    }
                }
            }

            study = studyRepository.save(study);
            StudyDTO result = new StudyDTO(study);
            result.setNumberOfSites(siteRepository.countByStudy(study));
            result.setNumberOfSubjects(subjectRepository.countByStudy(study));
            return ResponseEntity.ok()
                .body(result);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    /**
     * GET  /studies : get all the studies for a company.
     *
     * @param cid the company id to display information for
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of studies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/studies",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured({AuthoritiesConstants.USER,AuthoritiesConstants.ADMIN})

    public ResponseEntity<List<StudyDTO>> getAllStudiesForCompany(@RequestParam int cid, @RequestParam String search, Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Studies of the active Company");
        User user = userService.getUserWithAuthorities();
        for (Membership membership : user.getMemberships()) {
            if (membership.getCompany().getId() == cid) {
                List<StudyDTO> studyDTOList = new ArrayList<>();
                StudyDTO studyDTO;
                Map<String,String> searchParams;
                try {
                    searchParams = new ObjectMapper().readValue(search, HashMap.class);
                } catch (IOException e) {
                    // Unable to parse search parameters
                    searchParams = new HashMap<>();
                }

                Page<Study> page;
                if (searchParams.isEmpty()) {
                    page = studyRepository.findAllByCompany(membership.getCompany(), pageable);
                } else {
                    page = studyRepository.findAll(StudySearchSpecificationBuilder.build(membership.getCompany(), searchParams), pageable);
                }
                for (Study study : page.getContent()) {
                    studyDTO = new StudyDTO(study);
                    studyDTO.setNumberOfSites(siteRepository.countByStudy(study));
                    studyDTO.setNumberOfSubjects(subjectRepository.countByStudy(study));
                    studyDTOList.add(studyDTO);
                }
                HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/studies");
                //return new ResponseEntity<>(studyMapper.studiesToStudyDTOs(page.getContent()), headers, HttpStatus.OK);
                return new ResponseEntity<>(studyDTOList, headers, HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /studies/list : get all the studies for a company.
     *
     * @param cid the company id to display information for
     * @return the ResponseEntity with status 200 (OK) and the list of studies in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/studies/list",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured({AuthoritiesConstants.USER,AuthoritiesConstants.ADMIN})
    public ResponseEntity<List<StudyDTO>> listAllStudiesForCompany(@RequestParam int cid)
        throws URISyntaxException {
        log.debug("REST request to get all Studies of the active Company");

        User user = userService.getUserWithAuthorities();
        if(user.hasAuthority(AuthoritiesConstants.ADMIN)) {
            List<Study> studies = studyRepository.findAllByCompanyId((long)cid);
            return new ResponseEntity<>(StudyDTO.convert(studies), HttpStatus.OK);
        }
        else {
            for (Membership membership : user.getMemberships()) {
                if (membership.getCompany().getId() == cid) {

                    List<Study> studies = studyRepository.findAllByCompany(membership.getCompany());
                    return new ResponseEntity<>(StudyDTO.convert(studies), HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * GET  /studies/:id : get the "id" study.
     *
     * @param id the id of the studyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the studyDTO, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/studies/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<StudyDTO> getStudy(@PathVariable Long id) {
        log.debug("REST request to get Study : {}", id);
        Study study = studyRepository.findOne(id);
        StudyDTO studyDTO = new StudyDTO(study);
        return Optional.ofNullable(studyDTO)
            .map(result -> {
                result.setNumberOfSites(siteRepository.countByStudy(study));
                result.setNumberOfSubjects(subjectRepository.countByStudy(study));
                return new ResponseEntity<>(result, HttpStatus.OK);
            })
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * GET  /subjects/:companyId/:studyName/:studyId : validate uniqueness of a study name within a company.
     *
     * @param companyId the id of the a company
     * @param studyName the external reference of the subjectDTO to retrieve
     * @return the ResponseEntity with status 200 (OK), or with status 404 (Not Found)
     */
    @RequestMapping(value = "/studies/{companyId}/{studyName}/{studyId}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Transactional(readOnly = true)
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<String> validateStudyName(@PathVariable Long companyId, @PathVariable String studyName, @PathVariable Long studyId) {
        log.debug("REST request to get Study : {}", studyName);

        Company company = companyRepository.findOne(companyId);
        if (company != null) {
            if (studyId == null) {
                studyId = 0L;
            }
            Long nameCount = studyRepository.countByCompanyAndNameIgnoreCaseAndIdNot(company, studyName, studyId);
            if (nameCount == 0) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.CONFLICT);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * DELETE  /studies/:id : delete the "id" study.
     *
     * @param id the id of the studyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/studies/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured(AuthoritiesConstants.USER)
    public ResponseEntity<Void> deleteStudy(@PathVariable Long id) {
        log.debug("REST request to delete Study : {}", id);
        studyRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("study", id.toString())).build();
    }

}
