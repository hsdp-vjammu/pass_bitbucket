package com.philips.respironics.pass.portal.service;

    import com.philips.respironics.pass.portal.domain.MonitorHistory;
    import com.philips.respironics.pass.portal.domain.Subject;
    import com.philips.respironics.pass.portal.repository.MonitorHistoryRepository;
    import org.springframework.stereotype.Service;
    import org.springframework.transaction.annotation.Transactional;

    import javax.inject.Inject;
    import java.util.List;

@Service
@Transactional
public class MonitorHistoryService {
    @Inject
    private MonitorHistoryRepository monitorHistoryRepository;

    @Transactional(readOnly = true)
    public List<MonitorHistory> getAllMonitorsBySubjectId(Subject assignedSubject) {
        return monitorHistoryRepository.findDistinctByAssignedSubject(assignedSubject);
    }

    @Transactional(readOnly = true)
    public List<MonitorHistory> getCountForMonitorsBySubjectIdAndSerialNumber(Subject assignedSubject, String serialNumber) {
        return monitorHistoryRepository.findDistinctByAssignedSubjectAndSerialNumber(assignedSubject, serialNumber);
    }
}
