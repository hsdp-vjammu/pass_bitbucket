package com.philips.respironics.pass.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Membership (to a Company).
 */
@Entity
@Table(name = "company_user")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Membership extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private boolean enabled = true;

    @ManyToOne
    @NotNull
    private User user;

    @ManyToOne
    @NotNull
    private Company company;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "membership")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<MembershipAccess> accessLevels = new HashSet<>();

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }

    public Company getCompany() {
        return company;
    }
    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<MembershipAccess> getAccessLevels() { return accessLevels; }
    public void setAccessLevels(Set<MembershipAccess> accessLevels) { this.accessLevels = accessLevels; }

    public boolean hasAccessLevel(String level) {
        MembershipAccess accessLevel = getAccessLevel(level);
        return accessLevel != null;
    }

    public MembershipAccess getAccessLevel(String level) {
        if (StringUtils.isNotBlank(level)) {
            for (MembershipAccess accessLevel : this.accessLevels) {
                if (accessLevel.getAccessLevel().equals(level)) {
                    return accessLevel;
                }
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Membership entity = (Membership) o;
        if(entity.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, entity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Membership{" +
            "id=" + id +
            '}';
    }
}
