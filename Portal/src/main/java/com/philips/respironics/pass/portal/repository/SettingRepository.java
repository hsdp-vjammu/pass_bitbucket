package com.philips.respironics.pass.portal.repository;

import com.philips.respironics.pass.portal.domain.Setting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Spring Data JPA repository for the Setting entity.
 */
@SuppressWarnings("unused")
public interface SettingRepository extends JpaRepository<Setting,Long> {

    Setting findOneByCategoryAndName(String category, String name);

    List<Setting> findAllByCategory(String category);

}
