package com.philips.respironics.pass.portal.domain.enumeration;

public enum MonitorStatusChangeAction {
    REGISTER,ASSIGN_SUBJECT,UNASSIGN_SUBJECT,DISABLE,ENABLE,REMOVE,RE_REGISTER
}
