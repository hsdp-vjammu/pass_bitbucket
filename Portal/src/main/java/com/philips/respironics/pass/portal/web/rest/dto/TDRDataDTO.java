package com.philips.respironics.pass.portal.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

//import lombok.Data;

//@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TDRDataDTO implements Serializable {

    private String timeStamp;  //  1530158460000

    private String activityCounts;
    private String heartRate;
    private String respirationRate;
    private String steps;
    private String offWrist;
    private String run;
    private String walk;
    private String cycle;
    private String activeMinutes;
    private String restingHeartRate;
    private String recoveryHeartRate;
    private String cardioFitnessIndex;
    private String vo2Max;

    private String activeEnergyExpenditure;
    private String totalEnergyExpenditure;


    @JsonCreator
    public TDRDataDTO() {
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getTimeStamp(String timeStamp, String timeZone) {
        if (timeStamp != null) {
            Long newDate = Long.parseLong(timeStamp) * 1000;
            //ZonedDateTime dateTime = Instant.ofEpochMilli(newDate).atZone(ZoneId.of("America/Indiana/Indianapolis"));
            ZonedDateTime dateTime = Instant.ofEpochMilli(newDate).atZone(ZoneId.of(timeZone));
            String formatted = dateTime.format(DateTimeFormatter.ofPattern("dd/MMM/yyyy hh:mm a"));
            return formatted;
        } else {
            return null;
        }
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getActivityCounts() {
        return activityCounts;
    }

    public void setActivityCounts(String activityCounts) {
        this.activityCounts = activityCounts;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getRespirationRate() {
        return respirationRate;
    }

    public void setRespirationRate(String respirationRate) {
        this.respirationRate = respirationRate;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getOffWrist() {
        return offWrist;
    }

    public void setOffWrist(String offWrist) {
        this.offWrist = offWrist;
    }

    public String getRun() {
        return run;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getWalk() {
        return walk;
    }

    public void setWalk(String walk) {
        this.walk = walk;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getActiveMinutes() {
        return activeMinutes;
    }

    public void setActiveMinutes(String activeMinutes) {
        this.activeMinutes = activeMinutes;
    }

    public String getRestingHeartRate() {
        return restingHeartRate;
    }

    public void setRestingHeartRate(String restingHeartRate) {
        this.restingHeartRate = restingHeartRate;
    }

    public String getRecoveryHeartRate() {
        return recoveryHeartRate;
    }

    public void setRecoveryHeartRate(String recoveryHeartRate) {
        this.recoveryHeartRate = recoveryHeartRate;
    }

    public String getCardioFitnessIndex() {
        return cardioFitnessIndex;
    }

    public void setCardioFitnessIndex(String cardioFitnessIndex) {
        this.cardioFitnessIndex = cardioFitnessIndex;
    }

    public String getVo2Max() {
        return vo2Max;
    }

    public void setVo2Max(String vo2Max) {
        this.vo2Max = vo2Max;
    }

    public String getActiveEnergyExpenditure() {
        return activeEnergyExpenditure;
    }

    public void setActiveEnergyExpenditure(String activeEnergyExpenditure) {
        this.activeEnergyExpenditure = activeEnergyExpenditure;
    }

    public String getTotalEnergyExpenditure() {
        return totalEnergyExpenditure;
    }

    public void setTotalEnergyExpenditure(String totalEnergyExpenditure) {
        this.totalEnergyExpenditure = totalEnergyExpenditure;
    }


}
