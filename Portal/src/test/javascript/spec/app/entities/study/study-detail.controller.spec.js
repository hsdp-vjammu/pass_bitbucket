'use strict';

describe('Controller Tests', function() {

    describe('Study Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockStudy, MockCompany;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockStudy = jasmine.createSpy('MockStudy');
            MockCompany = jasmine.createSpy('MockCompany');


            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Study': MockStudy,
                'Company': MockCompany
            };
            createController = function() {
                $injector.get('$controller')("StudyDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'passApp:studyUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
