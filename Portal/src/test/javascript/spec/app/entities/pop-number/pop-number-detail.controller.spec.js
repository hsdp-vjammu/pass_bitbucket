'use strict';

describe('Controller Tests', function() {

    describe('PopNumber Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPopNumber;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPopNumber = jasmine.createSpy('MockPopNumber');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'PopNumber': MockPopNumber
            };
            createController = function() {
                $injector.get('$controller')("PopNumberDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'passApp:popNumberUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
