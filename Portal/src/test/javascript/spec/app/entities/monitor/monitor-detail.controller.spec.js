'use strict';

describe('Controller Tests', function() {

    describe('Monitor Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockMonitor, MockCompany, MockSubject;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockMonitor = jasmine.createSpy('MockMonitor');
            MockCompany = jasmine.createSpy('MockCompany');
            MockSubject = jasmine.createSpy('MockSubject');


            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'Monitor': MockMonitor,
                'Company': MockCompany,
                'Subject': MockSubject
            };
            createController = function() {
                $injector.get('$controller')("MonitorDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'passApp:monitorUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
