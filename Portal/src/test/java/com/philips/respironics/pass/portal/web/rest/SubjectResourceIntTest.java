package com.philips.respironics.pass.portal.web.rest;

import com.philips.respironics.pass.portal.PassApp;
import com.philips.respironics.pass.portal.domain.Subject;
import com.philips.respironics.pass.portal.domain.enumeration.Gender;
import com.philips.respironics.pass.portal.repository.SubjectRepository;
import com.philips.respironics.pass.portal.web.rest.dto.SubjectDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SubjectResource REST controller.
 *
 * @see SubjectResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PassApp.class)
@WebAppConfiguration
@IntegrationTest
public class SubjectResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_EXTERNAL_REFERENCE = "AAAAA";
    private static final String UPDATED_EXTERNAL_REFERENCE = "BBBBB";
    private static final String DEFAULT_FIRST_NAME = "AAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBB";
    private static final String DEFAULT_LAST_NAME = "AAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBB";

    private static final LocalDate DEFAULT_DATE_OF_BIRTH = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_OF_BIRTH = LocalDate.now();
    //private static final String DEFAULT_DATE_OF_BIRTH_STR = DEFAULT_DATE_OF_BIRTH.toString();

    private static final Double DEFAULT_WEIGHT = 1D;
    private static final Double UPDATED_WEIGHT = 2D;

    private static final Double DEFAULT_HEIGHT = 1D;
    private static final Double UPDATED_HEIGHT = 2D;

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;
    private static final String DEFAULT_STREET_ADDRESS = "AAAAA";
    private static final String UPDATED_STREET_ADDRESS = "BBBBB";
    private static final String DEFAULT_CITY = "AAAAA";
    private static final String UPDATED_CITY = "BBBBB";
    private static final String DEFAULT_STATE = "AAAAA";
    private static final String UPDATED_STATE = "BBBBB";
    private static final String DEFAULT_COUNTRY = "AAAAA";
    private static final String UPDATED_COUNTRY = "BBBBB";
    private static final String DEFAULT_PHONE_NUMBER = "AAAAA";
    private static final String UPDATED_PHONE_NUMBER = "BBBBB";

    @Inject
    private SubjectRepository subjectRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSubjectMockMvc;

    private Subject subject;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SubjectResource subjectResource = new SubjectResource();
        ReflectionTestUtils.setField(subjectResource, "subjectRepository", subjectRepository);
        this.restSubjectMockMvc = MockMvcBuilders.standaloneSetup(subjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        subject = new Subject();
        subject.setExternalReference(DEFAULT_EXTERNAL_REFERENCE);
        subject.setFirstName(DEFAULT_FIRST_NAME);
        subject.setLastName(DEFAULT_LAST_NAME);
        subject.setDateOfBirth(DEFAULT_DATE_OF_BIRTH);
        subject.setWeight(DEFAULT_WEIGHT);
        subject.setHeight(DEFAULT_HEIGHT);
        subject.setGender(DEFAULT_GENDER);
        subject.setStreetAddress(DEFAULT_STREET_ADDRESS);
        subject.setCity(DEFAULT_CITY);
        subject.setState(DEFAULT_STATE);
        subject.setCountry(DEFAULT_COUNTRY);
        subject.setPhoneNumber(DEFAULT_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void createSubject() throws Exception {
        int databaseSizeBeforeCreate = subjectRepository.findAll().size();

        // Create the Subject
        SubjectDTO subjectDTO = new SubjectDTO(subject);

        restSubjectMockMvc.perform(post("/api/subjects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subjectDTO)))
                .andExpect(status().isCreated());

        // Validate the Subject in the database
        List<Subject> subjects = subjectRepository.findAll();
        assertThat(subjects).hasSize(databaseSizeBeforeCreate + 1);
        Subject testSubject = subjects.get(subjects.size() - 1);
        assertThat(testSubject.getExternalReference()).isEqualTo(DEFAULT_EXTERNAL_REFERENCE);
        assertThat(testSubject.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testSubject.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testSubject.getDateOfBirth()).isEqualTo(DEFAULT_DATE_OF_BIRTH);
        assertThat(testSubject.getWeight()).isEqualTo(DEFAULT_WEIGHT);
        assertThat(testSubject.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testSubject.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testSubject.getStreetAddress()).isEqualTo(DEFAULT_STREET_ADDRESS);
        assertThat(testSubject.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testSubject.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testSubject.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testSubject.getPhoneNumber()).isEqualTo(DEFAULT_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void checkUniqueIdentifierIsRequired() throws Exception {
        int databaseSizeBeforeTest = subjectRepository.findAll().size();

        // Create the Subject, which fails.
        SubjectDTO subjectDTO = new SubjectDTO(subject);

        restSubjectMockMvc.perform(post("/api/subjects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subjectDTO)))
                .andExpect(status().isBadRequest());

        List<Subject> subjects = subjectRepository.findAll();
        assertThat(subjects).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSubjects() throws Exception {
        // Initialize the database
        subjectRepository.saveAndFlush(subject);

        // Get all the subjects
        restSubjectMockMvc.perform(get("/api/subjects?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(subject.getId().intValue())))
                .andExpect(jsonPath("$.[*].externalReference").value(hasItem(DEFAULT_EXTERNAL_REFERENCE.toString())))
                .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
                .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
                //.andExpect(jsonPath("$.[*].dateOfBirth").value(hasItem(DEFAULT_DATE_OF_BIRTH_STR)))
                .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT.doubleValue())))
                .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
                .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
                .andExpect(jsonPath("$.[*].streetAddress").value(hasItem(DEFAULT_STREET_ADDRESS.toString())))
                .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY.toString())))
                .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
                .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
                .andExpect(jsonPath("$.[*].phoneNumber").value(hasItem(DEFAULT_PHONE_NUMBER.toString())));
    }

    @Test
    @Transactional
    public void getSubject() throws Exception {
        // Initialize the database
        subjectRepository.saveAndFlush(subject);

        // Get the subject
        restSubjectMockMvc.perform(get("/api/subjects/{id}", subject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(subject.getId().intValue()))
            .andExpect(jsonPath("$.externalReference").value(DEFAULT_EXTERNAL_REFERENCE.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            //.andExpect(jsonPath("$.dateOfBirth").value(DEFAULT_DATE_OF_BIRTH_STR))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT.doubleValue()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT.doubleValue()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.streetAddress").value(DEFAULT_STREET_ADDRESS.toString()))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.phoneNumber").value(DEFAULT_PHONE_NUMBER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSubject() throws Exception {
        // Get the subject
        restSubjectMockMvc.perform(get("/api/subjects/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSubject() throws Exception {
        // Initialize the database
        subjectRepository.saveAndFlush(subject);
        int databaseSizeBeforeUpdate = subjectRepository.findAll().size();

        // Update the subject
        Subject updatedSubject = new Subject();
        updatedSubject.setId(subject.getId());
        updatedSubject.setExternalReference(UPDATED_EXTERNAL_REFERENCE);
        updatedSubject.setFirstName(UPDATED_FIRST_NAME);
        updatedSubject.setLastName(UPDATED_LAST_NAME);
        updatedSubject.setDateOfBirth(UPDATED_DATE_OF_BIRTH);
        updatedSubject.setWeight(UPDATED_WEIGHT);
        updatedSubject.setHeight(UPDATED_HEIGHT);
        updatedSubject.setGender(UPDATED_GENDER);
        updatedSubject.setStreetAddress(UPDATED_STREET_ADDRESS);
        updatedSubject.setCity(UPDATED_CITY);
        updatedSubject.setState(UPDATED_STATE);
        updatedSubject.setCountry(UPDATED_COUNTRY);
        updatedSubject.setPhoneNumber(UPDATED_PHONE_NUMBER);
        SubjectDTO subjectDTO = new SubjectDTO(updatedSubject);

        restSubjectMockMvc.perform(put("/api/subjects")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(subjectDTO)))
                .andExpect(status().isOk());

        // Validate the Subject in the database
        List<Subject> subjects = subjectRepository.findAll();
        assertThat(subjects).hasSize(databaseSizeBeforeUpdate);
        Subject testSubject = subjects.get(subjects.size() - 1);
        assertThat(testSubject.getExternalReference()).isEqualTo(UPDATED_EXTERNAL_REFERENCE);
        assertThat(testSubject.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testSubject.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testSubject.getDateOfBirth()).isEqualTo(UPDATED_DATE_OF_BIRTH);
        assertThat(testSubject.getWeight()).isEqualTo(UPDATED_WEIGHT);
        assertThat(testSubject.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testSubject.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testSubject.getStreetAddress()).isEqualTo(UPDATED_STREET_ADDRESS);
        assertThat(testSubject.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testSubject.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testSubject.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testSubject.getPhoneNumber()).isEqualTo(UPDATED_PHONE_NUMBER);
    }

    @Test
    @Transactional
    public void deleteSubject() throws Exception {
        // Initialize the database
        subjectRepository.saveAndFlush(subject);
        int databaseSizeBeforeDelete = subjectRepository.findAll().size();

        // Get the subject
        restSubjectMockMvc.perform(delete("/api/subjects/{id}", subject.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Subject> subjects = subjectRepository.findAll();
        assertThat(subjects).hasSize(databaseSizeBeforeDelete - 1);
    }
}
