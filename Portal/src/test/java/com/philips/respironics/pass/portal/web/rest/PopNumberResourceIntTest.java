package com.philips.respironics.pass.portal.web.rest;

import com.philips.respironics.pass.portal.PassApp;
import com.philips.respironics.pass.portal.domain.PopNumber;
import com.philips.respironics.pass.portal.repository.PopNumberRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the PopNumberResource REST controller.
 *
 * @see PopNumberResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PassApp.class)
@WebAppConfiguration
@IntegrationTest
public class PopNumberResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_NUMBER = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_NUMBER = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_IMPORT_DATE_STR = dateTimeFormatter.format(DEFAULT_CREATE_DATE);
    private static final String DEFAULT_USER = "AAAAA";
    private static final String UPDATED_USER = "BBBBB";

    @Inject
    private PopNumberRepository popNumberRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restPopNumberMockMvc;

    private PopNumber popNumber;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PopNumberResource popNumberResource = new PopNumberResource();
        ReflectionTestUtils.setField(popNumberResource, "popNumberRepository", popNumberRepository);
        this.restPopNumberMockMvc = MockMvcBuilders.standaloneSetup(popNumberResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        popNumber = new PopNumber();
        popNumber.setNumber(DEFAULT_NUMBER);
        popNumber.setCreateDate(DEFAULT_CREATE_DATE);
        popNumber.setUser(DEFAULT_USER);
    }

    @Test
    @Transactional
    public void createPopNumber() throws Exception {
        int databaseSizeBeforeCreate = popNumberRepository.findAll().size();

        // Create the PopNumber

        restPopNumberMockMvc.perform(post("/api/pop-numbers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(popNumber)))
                .andExpect(status().isCreated());

        // Validate the PopNumber in the database
        List<PopNumber> popNumbers = popNumberRepository.findAll();
        assertThat(popNumbers).hasSize(databaseSizeBeforeCreate + 1);
        PopNumber testPopNumber = popNumbers.get(popNumbers.size() - 1);
        assertThat(testPopNumber.getNumber()).isEqualTo(DEFAULT_NUMBER);
        assertThat(testPopNumber.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testPopNumber.getUser()).isEqualTo(DEFAULT_USER);
    }

    @Test
    @Transactional
    public void checkNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = popNumberRepository.findAll().size();
        // set the field null
        popNumber.setNumber(null);

        // Create the PopNumber, which fails.

        restPopNumberMockMvc.perform(post("/api/pop-numbers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(popNumber)))
                .andExpect(status().isBadRequest());

        List<PopNumber> popNumbers = popNumberRepository.findAll();
        assertThat(popNumbers).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPopNumbers() throws Exception {
        // Initialize the database
        popNumberRepository.saveAndFlush(popNumber);

        // Get all the popNumbers
        restPopNumberMockMvc.perform(get("/api/pop-numbers?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(popNumber.getId().intValue())))
                .andExpect(jsonPath("$.[*].number").value(hasItem(DEFAULT_NUMBER.toString())))
                .andExpect(jsonPath("$.[*].importDate").value(hasItem(DEFAULT_IMPORT_DATE_STR)))
                .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }

    @Test
    @Transactional
    public void getPopNumber() throws Exception {
        // Initialize the database
        popNumberRepository.saveAndFlush(popNumber);

        // Get the popNumber
        restPopNumberMockMvc.perform(get("/api/pop-numbers/{id}", popNumber.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(popNumber.getId().intValue()))
            .andExpect(jsonPath("$.number").value(DEFAULT_NUMBER.toString()))
            .andExpect(jsonPath("$.importDate").value(DEFAULT_IMPORT_DATE_STR))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPopNumber() throws Exception {
        // Get the popNumber
        restPopNumberMockMvc.perform(get("/api/pop-numbers/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePopNumber() throws Exception {
        // Initialize the database
        popNumberRepository.saveAndFlush(popNumber);
        int databaseSizeBeforeUpdate = popNumberRepository.findAll().size();

        // Update the popNumber
        PopNumber updatedPopNumber = new PopNumber();
        updatedPopNumber.setId(popNumber.getId());
        updatedPopNumber.setNumber(UPDATED_NUMBER);
        updatedPopNumber.setCreateDate(UPDATED_CREATE_DATE);
        updatedPopNumber.setUser(UPDATED_USER);

        restPopNumberMockMvc.perform(put("/api/pop-numbers")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedPopNumber)))
                .andExpect(status().isOk());

        // Validate the PopNumber in the database
        List<PopNumber> popNumbers = popNumberRepository.findAll();
        assertThat(popNumbers).hasSize(databaseSizeBeforeUpdate);
        PopNumber testPopNumber = popNumbers.get(popNumbers.size() - 1);
        assertThat(testPopNumber.getNumber()).isEqualTo(UPDATED_NUMBER);
        assertThat(testPopNumber.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testPopNumber.getUser()).isEqualTo(UPDATED_USER);
    }

    @Test
    @Transactional
    public void deletePopNumber() throws Exception {
        // Initialize the database
        popNumberRepository.saveAndFlush(popNumber);
        int databaseSizeBeforeDelete = popNumberRepository.findAll().size();

        // Get the popNumber
        restPopNumberMockMvc.perform(delete("/api/pop-numbers/{id}", popNumber.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<PopNumber> popNumbers = popNumberRepository.findAll();
        assertThat(popNumbers).hasSize(databaseSizeBeforeDelete - 1);
    }
}
