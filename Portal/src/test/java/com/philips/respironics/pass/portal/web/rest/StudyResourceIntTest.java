package com.philips.respironics.pass.portal.web.rest;

import com.philips.respironics.pass.portal.PassApp;
import com.philips.respironics.pass.portal.domain.Study;
import com.philips.respironics.pass.portal.repository.StudyRepository;
import com.philips.respironics.pass.portal.web.rest.dto.StudyDTO;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.philips.respironics.pass.portal.domain.enumeration.StudyStatus;

/**
 * Test class for the StudyResource REST controller.
 *
 * @see StudyResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = PassApp.class)
@WebAppConfiguration
@IntegrationTest
public class StudyResourceIntTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").withZone(ZoneId.of("Z"));

    private static final String DEFAULT_EXTERNAL_REFERENCE = "AAAAA";
    private static final String UPDATED_EXTERNAL_REFERENCE = "BBBBB";
    private static final String DEFAULT_NAME = "AAAAA";
    private static final String UPDATED_NAME = "BBBBB";
    private static final String DEFAULT_DESCRIPTION = "AAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBB";

    private static final StudyStatus DEFAULT_STATUS = StudyStatus.ACTIVE;
    private static final StudyStatus UPDATED_STATUS = StudyStatus.INACTIVE;

    private static final ZonedDateTime DEFAULT_DATE_CLOSED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneId.systemDefault());
    private static final ZonedDateTime UPDATED_DATE_CLOSED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final String DEFAULT_DATE_CLOSED_STR = dateTimeFormatter.format(DEFAULT_DATE_CLOSED);

    @Inject
    private StudyRepository studyRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restStudyMockMvc;

    private Study study;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        StudyResource studyResource = new StudyResource();
        ReflectionTestUtils.setField(studyResource, "studyRepository", studyRepository);
        this.restStudyMockMvc = MockMvcBuilders.standaloneSetup(studyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        study = new Study();
        study.setExternalReference(DEFAULT_EXTERNAL_REFERENCE);
        study.setName(DEFAULT_NAME);
        study.setDescription(DEFAULT_DESCRIPTION);
        study.setStatus(DEFAULT_STATUS);
        study.setDateClosed(DEFAULT_DATE_CLOSED);
    }

    @Test
    @Transactional
    public void createStudy() throws Exception {
        int databaseSizeBeforeCreate = studyRepository.findAll().size();

        // Create the Study
        StudyDTO studyDTO = new StudyDTO(study);

        restStudyMockMvc.perform(post("/api/studies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(studyDTO)))
                .andExpect(status().isCreated());

        // Validate the Study in the database
        List<Study> studies = studyRepository.findAll();
        assertThat(studies).hasSize(databaseSizeBeforeCreate + 1);
        Study testStudy = studies.get(studies.size() - 1);
        assertThat(testStudy.getExternalReference()).isEqualTo(DEFAULT_EXTERNAL_REFERENCE);
        assertThat(testStudy.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testStudy.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testStudy.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testStudy.getDateClosed()).isEqualTo(DEFAULT_DATE_CLOSED);
    }

    @Test
    @Transactional
    public void checkUniqueIdentifierIsRequired() throws Exception {
        int databaseSizeBeforeTest = studyRepository.findAll().size();

        // Create the Study, which fails.
        StudyDTO studyDTO = new StudyDTO(study);

        restStudyMockMvc.perform(post("/api/studies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(studyDTO)))
                .andExpect(status().isBadRequest());

        List<Study> studies = studyRepository.findAll();
        assertThat(studies).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = studyRepository.findAll().size();
        // set the field null
        study.setName(null);

        // Create the Study, which fails.
        StudyDTO studyDTO = new StudyDTO(study);

        restStudyMockMvc.perform(post("/api/studies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(studyDTO)))
                .andExpect(status().isBadRequest());

        List<Study> studies = studyRepository.findAll();
        assertThat(studies).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = studyRepository.findAll().size();
        // set the field null
        study.setStatus(null);

        // Create the Study, which fails.
        StudyDTO studyDTO = new StudyDTO(study);

        restStudyMockMvc.perform(post("/api/studies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(studyDTO)))
                .andExpect(status().isBadRequest());

        List<Study> studies = studyRepository.findAll();
        assertThat(studies).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStudies() throws Exception {
        // Initialize the database
        studyRepository.saveAndFlush(study);

        // Get all the studies
        restStudyMockMvc.perform(get("/api/studies?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(study.getId().intValue())))
                .andExpect(jsonPath("$.[*].externalReference").value(hasItem(DEFAULT_EXTERNAL_REFERENCE.toString())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
                .andExpect(jsonPath("$.[*].dateClosed").value(hasItem(DEFAULT_DATE_CLOSED_STR)));
    }

    @Test
    @Transactional
    public void getStudy() throws Exception {
        // Initialize the database
        studyRepository.saveAndFlush(study);

        // Get the study
        restStudyMockMvc.perform(get("/api/studies/{id}", study.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(study.getId().intValue()))
            .andExpect(jsonPath("$.externalReference").value(DEFAULT_EXTERNAL_REFERENCE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.dateClosed").value(DEFAULT_DATE_CLOSED_STR));
    }

    @Test
    @Transactional
    public void getNonExistingStudy() throws Exception {
        // Get the study
        restStudyMockMvc.perform(get("/api/studies/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStudy() throws Exception {
        // Initialize the database
        studyRepository.saveAndFlush(study);
        int databaseSizeBeforeUpdate = studyRepository.findAll().size();

        // Update the study
        Study updatedStudy = new Study();
        updatedStudy.setId(study.getId());
        updatedStudy.setExternalReference(UPDATED_EXTERNAL_REFERENCE);
        updatedStudy.setName(UPDATED_NAME);
        updatedStudy.setDescription(UPDATED_DESCRIPTION);
        updatedStudy.setStatus(UPDATED_STATUS);
        updatedStudy.setDateClosed(UPDATED_DATE_CLOSED);
        StudyDTO studyDTO = new StudyDTO(updatedStudy);

        restStudyMockMvc.perform(put("/api/studies")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(studyDTO)))
                .andExpect(status().isOk());

        // Validate the Study in the database
        List<Study> studies = studyRepository.findAll();
        assertThat(studies).hasSize(databaseSizeBeforeUpdate);
        Study testStudy = studies.get(studies.size() - 1);
        assertThat(testStudy.getExternalReference()).isEqualTo(UPDATED_EXTERNAL_REFERENCE);
        assertThat(testStudy.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testStudy.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testStudy.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testStudy.getDateClosed()).isEqualTo(UPDATED_DATE_CLOSED);
    }

    @Test
    @Transactional
    public void deleteStudy() throws Exception {
        // Initialize the database
        studyRepository.saveAndFlush(study);
        int databaseSizeBeforeDelete = studyRepository.findAll().size();

        // Get the study
        restStudyMockMvc.perform(delete("/api/studies/{id}", study.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Study> studies = studyRepository.findAll();
        assertThat(studies).hasSize(databaseSizeBeforeDelete - 1);
    }
}
