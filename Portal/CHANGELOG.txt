/*---------------------------------------*/
TO CHANGE THE APPLICATION VERSION CHANGE:
1) build.gradle -> line 82: version = ...
2) src/main/webapp/app/app.constants.js -> line 6: .constant('VERSION', '...')
3) Add a section in this file
/*---------------------------------------*/
/* 2.0.3-beta69 */
US 22308 - Site Coordinator enhancements
   - Subject List search functionality fix
   - Assignment search functionality fix
/***********************************/
/* 2.0.3-beta68 */
US 22308 - Site Coordinator enhancements
   - Creating Subject dialog, display studies dropdown information based on logged in Site Coordinator
   - Subject list display only assigned studies for given Site Coordinator
   - Display only assigned subjects based on user login credentials (SC) for Add Assignment dialog
/***********************************/
/* 2.0.3-beta67 */
US 21498 - Feedback report - remove time stamp changes
US 12877 - Site Coordinator menu bar information display issues fixed
US 22308 - Site Coordinator enhancements - display study information on Assigned Sites and Add or Edit user screen
/***********************************/
/* 2.0.3-beta66 */
US22308 - Adding Clinician to existing user issue addressed
/***********************************/
/* 2.0.3-beta65 */
US22308 - Site Coordinator new design and functionality
DE3914 - Site coordinator updates not sticking defect fix
DE4067 - Configuration feedback schedules screen not showing saved times for FireFox and IE fix
/***********************************/
/* 2.0.3-beta64 */
US21498 -  Feedback data display and endpoint integration
/***********************************/
/* 2.0.3-beta63b */
changes to the base URL for dataextractor
/***********************************/
/* 2.0.3-beta63a */
Changes to remove Let
/***********************************/
/* 2.0.3-beta63 */
US21498 -  Feedback UI development
DE4107  -  Subject attributes not reflecting web app updates
        -  Subject header display changes
/***********************************/
/* 2.0.3-beta62 */
US22303 -  preventing the Subject ID and Study fields to be edited after the subject has been assigned or was assigned to a configuration
US22304 -  last 16 digits of the subject's unique identifier shall be listed on the subject data display page
        -  The 32 digits of the subject's unique identifier shall be listed on the subject management page
/***********************************/
/* 2.0.3-beta61 */
US22303 -  preventing the Subject ID and Study fields to be edited after the subject has been assigned toa configuration
        -  display UUID and integration
/***********************************/
/* 2.0.3-beta60 */
US19842 - Fixed issues with the excel export with large data sets
/***********************************/
/* 2.0.3-beta59 */
Excel export fix.
/* 2.0.3-beta58 */
US20254 - Fixed issue with the datalist where the stardates and enddates are not being accurately converted to UTC
/***********************************/
/* 2.0.3-beta57 */
US20254 - Datalist to pass time along with the date for data extraction
DE 4026 - Fire fox issue with datalist and monitor database change
US20254 - Smart Configs
US20254 - Datalist start date and end date has the correct start time and end time
/***********************************/
/* 2.0.3-beta56 */
US20254 - Datalist to pass time along with the date for data extraction
/***********************************/
/* 2.0.3-beta55 */
DE 3950 - Configuration alert changes to fix time zone based alert issues
/***********************************/
/* 2.0.3-beta54 */
TA_47458_PJ - Total Energy Expenditure and NaN display changes
TA_47458_PJ - Format Total/Active Energy Expenditure to 4 decimals and Remove Battery Level column changes
/***********************************/
/* 2.0.3-beta53 */
US20253- Changes to the end point to call into the infrasturcture and not TDR
US20253 - Implemented pageable
Dispaying Subject TimeZone information on the subject details page
Changed Energy Expenditure label to Active Energy Expenditure
Functionality to remove a column by dragging the column away from the datalist
Changed label from monitor to device clean up
/***********************************/
/* 2.0.3-beta52 */
DE-3926 Devices Page advance search - Study Status - add ALL option
DE-3872 - User Page changes - advanced search pagination fixes
Export to Excel Lable spacing change
/***********************************/
/* 2.0.3-beta51 */
DE3872 - User list not filtered at company level
Export Button, space issue (Label)
/***********************************/
/* 2.0.3-beta50 */
US20540 - Export to excel with password functionality.
/***********************************/
/* 2.0.3-beta49 */
DE3884 is logged for this issue. The export does not work for FireFox, Chrome or IE.
DE3886 Data List column Recovery Heart Rate should be Heart Rate Recovery
/***********************************/
/* 2.0.3-beta47 */
US 19473 - Completed CardiFitness Index and V22Max
 - Added tool tips for the above two columns
Timestamp formatted to eliminate the seconds
/***********************************/
/* 2.0.3-beta46 */
TA41850_TDR Service call move to backend changes
US 19473 - Completed Resting BatteryState
 - Added tool tip
US 19473 - Completed Resting BatteryState
 - Added tool tip
US 19473 - Completed Resting Heart Rate and Recovery Heart Rate
 - Added tool tips for the above two columns
US 19473 - Completed Resting Heart Rate and Recovery Heart Rate
 - Added tool tips for the above two columns
US19429 - agGrid Implementation for pinned columns, locked headers and Monitor serial number drop down changes
/***********************************/
/* 2.0.3-beta45 */
corrected the tool tip for not worn and changed the property for sleep to sleepIntent
Changed label from sleep to sleep intent
DE3608_Subject Device serial number show default and display dropdown with serial numbers associated with subject changes - part 2
Tool tips changes, includes translate functionality for Users and Data List screen.

/***********************************/
/* 2.0.3-beta44 */
US19473-Data List tool tips changes
DE3608_Subject Device serial number show default and display dropdown with serial numbers associated with subject changes - part 2
/***********************************/
/* 2.0.3-beta43 */
DE3609_Subject Device serial number blocker issue fixed with width changes - part 1
/***********************************/
/* 2.0.3-beta42 */
US13390- Product Support shall be able to edit, disable/enable access and delete companies in PAS.
US17159 - Study page changes and refactoring
/***********************************/
/* 2.0.3-beta41 */
US 19157 - The User shall be able to view the detailed epoch by epoch list in PAS for Not Worn, Walk, Run, Cycle, Activity Counts and Sleep intent.
DE 3469 - Product Support User Administration page cleanup work - removed blank option for account status
/***********************************/
/* 2.0.3-beta40 */
US 19157 - The User shall be able to view the detailed epoch by epoch list in PAS for Not Worn, Walk, Run, Cycle and Sleep intent.
Completed Not Worn, Walk, Run, Cycle
US 18784 - Company Admin login page fix
DE 3469 - Product Support User Administration page cleanup work
/***********************************/
/* 2.0.3-beta39 */
US18977 - The User shall be able to view the detailed epoch by epoch list in PAS for AEE and Activity Count
/***********************************/
/* 2.0.3-beta38 */
/***********************************/
DE3422 - Page numbers are wrong on the subject screen
US 13391 -  User/Production Support search functionality
/***********************************/
/* 2.0.3-beta37 */
/***********************************/
US 13361 - The User shall be able to view the detailed epoch by epoch list in PAS
/***********************************/
/* 2.0.3-beta36 */
/***********************************/
US-13267 - Sedentary alert disable functionality changes, updated display value to YES/NO instead of true/false and change from hide/show to disabled - TC4221
US-13267 - Sedentary alert disable functionality changes - TC4221
US13390-Product Support shall be able to edit and delete the companies in PAS(TC3589)
US17801-Tool tips shall be added to explain the Account Status in the User pages for the Product Support and Clinician views
Also text wrapping for configuration questions
/***********************************/
/* 2.0.3-beta35 */
/***********************************/
US11275 - fixing the length for questions
Company column to be visible in user management page
/***********************************/
/* 2.0.3-beta34 */
/***********************************/
Analaysis start date and end date
/***********************************/
/* 2.0.3-beta33 */
/***********************************/
US17159 - Incorporated the new UI design and integrated the analysis start date
/***********************************/
/* 2.0.3-beta32 */
/***********************************/
DE2782 - Configuration Settings screen radio buttons behaving erratically when de/selected.
/***********************************/
/* 2.0.3-beta31 */
/***********************************/
PHb contract implemented to retrieve data
/***********************************/
/* 2.0.3-beta30 */
/***********************************/
Removed AW5 and integrated to PHb
Copyright year changes
/***********************************/
/* 2.0.3-beta29 */
/***********************************/
Implemented paging logic.
/***********************************/
/* 2.0.3-beta28 */
/***********************************/
Changes to include the new TDR contract AW5DataBlobV1
/***********************************/
/* 2.0.3-beta27 */
/***********************************/
DE2627:Able to edit own user via Add User screen as company admin
/***********************************/
/* 2.0.3-beta26 */
/***********************************/
US13361: Retrieve data from TDR and show it on the Web App for a subject.
DE2923-POP page - Cannot go past 1st page
/***********************************/
/* 2.0.3-beta25 */
/***********************************/
Fixed date of birth issue.
/***********************************/
/* 2.0.3-beta24 */
/***********************************/
US15473-The Site Coordinator (SC) Dashboard shall display summary information of items limited to the site(s) the coordinator is assigned to.
Added a new entity to track the status from TDR.
/***********************************/
/* 2.0.3-beta23 */
/***********************************/
DE2692 : Monitor bulk upload general messaging issues

/***********************************/
/* 2.0.3-beta22 */
/***********************************/
DE2692 : Monitor bulk upload general messaging issues
DE2639 : Wrong error message when adding a blank POP number

/***********************************/
/* 2.0.3-beta21 */
/***********************************/
DE2691 : Monitor bulk upload passApp.monitor.error.undefined
DE2693 : Monitors in removed status cannot be readded

/***********************************/
/* 2.0.3-beta20 */
/***********************************/
DE2607 - Sites created/assigned on-the-fly not listed on the Assignments or Subjects pages
DE2633 - User is able to create assignments with inactive study to subjects.

/***********************************/
/* 2.0.3-beta19 */
/***********************************/
DE2607 Sites assigned not showing up on the Assignments page
Re-worked the functionality by removing the add site button.

/***********************************/
/* 2.0.3-beta18 */
/***********************************/
DE2607 Sites assigned not showing up on the Assignments page


/***********************************/
/* 2.0.3-beta17 */
/***********************************/
Fixed issues with wrong color being shown for the different statuses on the user management screen
DE2682 - Changing the study assigned to an existing subject, clears the subject id.
DE2703 - Height and weight fields on Subject attributes tab is accepting 0 as a valid input.


/***********************************/
/* 2.0.3-beta16 */
/***********************************/
US13391 - Product Support shall be able to monitor the User access for the PAS application.


/***********************************/
/* 2.0.3-beta14 */
/***********************************/
US13375 - The Dashboard for the Comp. Admin, and Clinician shall have a section that shows study status on the company.

/***********************************/
/* 2.0.3-beta13 */
/***********************************/
US13391 - Made changes to display the company name along with the roles
US13391 - Disabled the Edit button on the detail page


/***********************************/
/* 2.0.3-beta12 */
/***********************************/
US11762:PAS Subject Create and Edit workflow shall have Identity, Demographics, and Attributes tabs

/***********************************/
/* 2.0.3-beta11 */
/***********************************/
US13341:The Subject creation work flow shall include input for the subject's dominant hand and wearing position

/***********************************/
/* 2.0.3-beta10 */
/***********************************/

/***********************************/
/* 2.0.3-beta9 */
/***********************************/
US13235: Display user states 'Activated' and 'Inactivated' in user management (for Company Admin users only)
DE2561: Fixed use of enter in upload dialogs

/***********************************/
/* 2.0.3-beta8 */
/***********************************/
US6809: Removed pincode and security questions from account registration
US13267: Added PALs and daily targets to monitor configuration

/***********************************/
/* 2.0.3-beta7 */
/***********************************/
US9335: Added file upload for POP numbers
US12874: All subject assignments for a study are now deleted when the study is inactivated
DE2369: Fixed incorrect behaviour of disabled pagination buttons in IE
Changed menu for Product Support users based on remarks from Peter
Fixed Site ID selection when adding a new Subject
Added search capabilities to user management page
Added confirmation dialog when deleting site(s)
Fixed error message on POP number upload

/***********************************/
/* 2.0.3-beta6 */
/***********************************/
US12879: Changed locked account message (removed part about Company Administrator)
US12876: Added check for inactivated memberships during login
US12876: Added setting the inactivated status for memberships
Added roles to login page company selection
US12873 & US12877: User role (and site id for site coordinators) are now visible

/***********************************/
/* 2.0.3-beta5 */
/***********************************/
US12731: Existing users can now create new companies with a valid POP number
Remember search, sort and paging parameters when navigating away and back to the subject management page
DE2535: Fixed site selection for IE
US12711: Company Administrator can no longer create studies (only a Clinician can)

/***********************************/
/* 2.0.3-beta4 */
/***********************************/
US6824 (TC3206): Changed locking mechanism to show locked message for any login that exceeds the allowed login attempts (not only existing accounts)
US9354: Moved Subject from the Setup menu back to the main menu
US10374 (TC2891): Show last 8 characters of study uuid as Study ID
US12403: Send email on revoking membership by deleting user
US12711: Company Administrator can now (in)activate a study
US12727: Site coordinator menu and filtering
US12730: Site ID is now a mandatory field in assignment
DE2094: Added validation for length (max 32 characters) and special characters

/***********************************/
/* 2.0.3-beta3 */
/***********************************/
US12403: Added site management
DE2336: Show error on blank line in monitor serialnumbers upload
DE1862: Menu is now according to US11751

/***********************************/
/* 2.0.3-beta2 */
/***********************************/
US12403: Transformed siteid 'tag' into entities
US12415: Added ability to set site coordinator role in user management
US12415: Send email on role change for company

/***********************************/
/* 2.0.3-beta1 */
/***********************************/
US12414: Added Company Administrator page
US12415: User management is implemented (except for the site coordinator role)
US11751: Disabled 'Setup' and 'Assignment' menu items if user is not Clinician

/***********************************/
/* 2.0.3-alpha */
/***********************************/
Implementation of new UI design

/***********************************/
/* 2.0.2-beta34 */
/***********************************/
All sorting, paging and searching is now server side (for clinician related screens)
DE2370: Fixed clearing the value for subject in assignment dialog
DE2370: Improved loading site ids and open on focus
Merged login view and company selection view into a single page

/***********************************/
/* 2.0.2-beta33 */
/***********************************/
Removed database column encryption to plain text columns
After setting new password, the user is redirected to the dashboard
Added link to registration success page

/***********************************/
/* 2.0.2-beta32 */
/***********************************/
Moved company selection to login procedure
Reset password functionality now includes a password history check
DE2336: Added check that skips blank lines in monitor upload file
Fixed password expiration check (for new login procedure)

/***********************************/
/* 2.0.2-beta31 */
/***********************************/
US11738: Study Management now uses server side search
US11668: Merged 'Upload monitors' dialog with 'Add monitors' dialog
US11762: Removed 'Assignment' step in creation and editing a subject
Fixed password reset messages
Validated account registration and activation
US11751: Changes to the menu for a Company Admin (requirements are not final yet)
US11648: Study status can now only be set or changed by a company admin
CSS fixes (buttons without text or hidden behind footer)

/***********************************/
/* 2.0.2-beta30 */
/***********************************/
Create separate template for login page
US11673: Added unit conversion for weight and height (subject creation and edit)
Changed assignment creation dialog title (removed 'edit')
Updated message in the assignment creation dialog according to product owner specifications
US11668: Added dialog for uploading monitors

/***********************************/
/* 2.0.2-beta29 */
/***********************************/
US11470: Removed ability to edit assignment (unassign only)
DE2237: Back buttons on detail pages are now 'history-aware'
DE2238: Added message to assignment creation dialog (TC3142)
DE2239: Subject detail page has back button
DE2240: Clear advanced search options when panel closes
DE2241: Disabled autofill for form on assignment creation dialog
DE2242: Changed Site ID validation to allow free text input (max 25 chars)
DE2243: Added validation to only allow existing unassigned monitor input (same goes for subject)
DE2246: Fixed 'Assigned' indication on Configuration Management page
DE2247: Changed the order of fields in the configuration details page

/***********************************/
/* 2.0.2-beta28 */
/***********************************/
DE2169: Fixed issues where session timeout would not activate
DE2182: Dialogs are now closed before automatic logout
Menu bar is now actively reloaded depending on the access level for the selected company
Fixed warning message for assigned configurations (TC3150)
DE2228: Fixed IE javascript error preventing creation or editing of configurations

/***********************************/
/* 2.0.2-beta27 */
/***********************************/
Fixed indication of 'Assigned' for configurations after creating or editing an assignment
Changed icons-dash indication to yes-no indications in Configuration Management
Fixed issue allowing to create a Configuration with a blank name
Fixed issue bypassing Questions step (in Configuration) without adding at least 1 question
Fixed date notations in Schedule Feedback (Configuration Management)
Changed button text from 'Add assignment' to 'Create assignment'

/***********************************/
/* 2.0.2-beta26 */
/***********************************/
US11416 && US11470 && US11469: Implemented assignment management
US11292: Added message when trying to delete an assigned configuration

/***********************************/
/* 2.0.2-beta25 */
/***********************************/
US11275 & US11290: Finished validations

/***********************************/
/* 2.0.2-beta24 */
/***********************************/
US11275 & US11290: Added 'Add Questions' and 'Schedule Responses' to the main workflow (detail like validation not yet finished)

/***********************************/
/* 2.0.2-beta23 */
/***********************************/
US9631: Implemented bulk actions for changing monitor state
US11131: Added Configuration Management page
US11273: Added creation of new Configuration (only ' Settings' for now)

/***********************************/
/* 2.0.2-beta22 */
/***********************************/
DE2108: Fixed internal server errors when sorting on the monitors page
DE2150: Fixed internal server errors on monitors and subjects pages

/***********************************/
/* 2.0.2-beta21 */
/***********************************/
DE2094: Added error message on duplicate serial number input
DE1860: Fixed the password expiration checks
DE1914: Implemented automatic logout on session timeout
US11131: Created basic CRUD views for Configuration Management

/***********************************/
/* 2.0.2-beta20 */
/***********************************/
US9939: Changed text on button from 'Create new monitor(s)' to 'Add Monitor'
US10427: Added monitor history to details page
US9631: Created 'Disable' button on Monitor detail page for unassigned monitors

/***********************************/
/* 2.0.2-beta19 */
/***********************************/
DE2026: Corrected sort order of subject table (due to server side data encryption)
US9939: Changed monitor creation dialog according to wireframes (allowing bulk creation)

/***********************************/
/* 2.0.2-beta18 */
/***********************************/
DE2026: Subjects table now has the some sorting paradigm as the other tables in the application
DE2050: Study comment field is now limited to 255 characters

/***********************************/
/* 2.0.2-beta17 */
/***********************************/
* Fixed issue that prevented creation of studies

/***********************************/
/* 2.0.2-beta16 */
/***********************************/
* US6845: Implemented all table columns for study management as specified in TC2867
* US6846: Implemented sorting, filtering and search on the studies table
* US10023: Implemented field for creating and editing a study
* US6848: Auto-generate Study ID
* WIREFRAMES: Added check for study name uniqueness

/***********************************/
/* 2.0.2-beta15 */
/***********************************/
* Added display of locked users in user management
* Added (un)locking users in user management

/***********************************/
/* 2.0.2-beta14 */
/***********************************/
* TC2462: Fixed Subject ID input field (drop-down selection) while editing a subject. Now similar to new subject.
* Display less confusing role (or 'profile') names in user management. ROLE_ADMIN -> 'Product Support', ROLE_USER -> 'Clinician'

/***********************************/
/* 2.0.2-beta13 */
/***********************************/
* Added possibility for V&V to change 'password change date'
* US9837: Implemented validator for password history

/***********************************/
/* 2.0.2-beta12 */
/***********************************/
* Fixed constraint exception in automatically deleting not activated users
* Implemented account locking based on password policy settings
* DE1760: Fixed incorrect query causing problems with checking uniqueness.
* US6824: Added all password policy settings.
* Fixed issue with the company registration failing. ng-model names had to be corrected to match the RegisterAccountDTO field names.
* Fixed issue with security answer 2 not being limited to 50 character length

/***********************************/
/* 2.0.2-beta11 */
/***********************************/
* Removed double click from subjects table
* Implemented keeping password history of the last 5 passwords (on change and reset)
* DE1818: Fixed error messages for phone number fields in subject creation and editing
* US6824: Added menu item and page for configuring password policy.
* US8659/US8660: Implemented password expiration check and warning
* US9588: Added password strength validation to password change and password reset pages


/***********************************/
/* 2.0.2-beta10 */
/***********************************/
* DE1758: Fixed tab issue in Firefox
* DE1760: Fixed consistency of subject ID validation
* US6873: Enlarged local part of email validation tot 50 characters
* US9354: Show correct menu items for clinician and company admin
* US9130: Typeahead now also has drop down on empty input
* US6809: Reshuffled fields according to changed specs
* US9216: Added User Agreement check to registration
* US8659/US8660: Added required data to do expiration checks in the front-end

/***********************************/
/* 2.0.2-beta9 */
/***********************************/
* DE1754: Resolved issues with confirm password validation
* DE1755: Prevent users from selection the same question for both security question 1 and 2
* DE1756: Fixed changing pincode in Firefox (allow backspace, delete and arrow keys).

/***********************************/
/* 2.0.2-beta8 */
/***********************************/
* DE1672: Fixed internal server error
* Display and editing of address field for Company
* Domain entity change to prepare for setting the administrator for a Company
* Fixed deleting users (for test purposes)

/***********************************/
/* 2.0.2-beta7 */
/***********************************/
* TECHNICAL DEBT: Improved getting data based on the active company, so concurrent user (with the same account) will not bother each other.
* US6821: Password strength validation
* US6808: Moved user login to home page instead of modal dialog
* US6808: Changed main menu accoding to wireframes

/***********************************/
/* 2.0.2-beta6 */
/***********************************/
* US6811: Updated messages to contain texts from the US
* Fixed issues with selecting active company (caused by changes regarding registration)

/***********************************/
/* 2.0.2-beta5 */
/***********************************/
* DE1591: Fixed possibility to bypass validation (in IE11)
* DE1592: Fix numbers-only input for IE11
* DE1590: Error message is now also showing when typing a wrong character before leaving the field
* DE1693: Updated field validation to latest specifications in test cases (as pointed out in the defect)

/***********************************/
/* 2.0.2-beta4 */
/***********************************/
* US6873: Updated email validation for subject creation and editing to latest specification
* US6810: Created fixed list of security questions
* US6808: Landing page for login and registration
* US6809: Created wizard for registering a new company admin user (including the company)

/***********************************/
/* 2.0.2-beta3 */
/***********************************/
* US6813: Formatting and validation of 32 hexadecimal character POP number on input and display
* US6813; List POP numbers

/***********************************/
/* 2.0.2-beta2 */
/***********************************/
* US6873: Removed validation for first name and last name (as agreed with product owner and V&V)
* US6875: Implemented editing of subject data
* DE1588: Error message is no displayed on invalid phone number input
* DE1589: Improved email validation
* DE1590: Fixed color change on error and error message translation for Site ID field
* DE1592: Added validation for max number and added more detail to the error message
* DE1593: Improved error message
* DE1595: Invalid error message translation for entity.validation.invalidInput.alphanumeric
* DE1602: Fixed date format

REMARKS:
- The phone number component does not allow for the phone number format (as displayed in the field placeholder)
  to be displayed in the error message. To enable this a custom component will have to be developed.

OPEN ISSUES/KNOWN BUGS:
- The phone number component is displaying unwanted behaviour.

/***********************************/
/* 2.0.2-beta1 */
/***********************************/
* Upgraded Angular from version 1.5.2 to 1.5.8 and JQuery from 2.2.2 to 2.2.4
* US6871: Implemented double click on subjects table row
* US6875: Created subject details page with tabs (display only, no editing yet)
* US6873: Updated field validation in subject creation wizard (according to updated specifications)
* US6873: Date selection for 'date of birth' now starts with year selection
